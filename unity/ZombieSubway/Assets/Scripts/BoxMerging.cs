﻿#if (UNITY_EDITOR)

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Subway
{

    public class BoxMerging : EditorWindow
    {

        [MenuItem("Subway/Box Merging/Move Child To Occlusion Box")]
        static void MoveChildToBox()
        {
            var scene = SceneManager.GetActiveScene();
            var gameObjects = scene.GetRootGameObjects();
            GameObject occlusions = FindByName(gameObjects, "Occlusions");
            GameObject level = FindByName(gameObjects, "Level");

            var level2 = new GameObject();
            level2.name = "Level2";

            List<Transform> alreadyAdded = new List<Transform>();
            for (var i = 0; i < occlusions.transform.childCount; i++)
            {
                var child = occlusions.transform.GetChild(i);
                var box = child.GetComponent<BoxCollider>();
                var bounds = box.bounds;

                var intersecting = GetMeshInsideBounds(level, bounds);
                var group = new GameObject();
                group.transform.parent = level2.transform;
                group.name = box.name;

                //Debug.Log(box.name + ", child count: " + intersecting.Count);
                foreach (var m in intersecting)
                {
                    if (alreadyAdded.Contains(m))
                    {
                        continue;
                    }

                    Object.Instantiate(m, group.transform);
                    alreadyAdded.Add(m);
                }
            }

            level2.transform.localPosition = level.transform.localPosition;

            Debug.Log("Level2 created");
        }

        private static List<Transform> GetMeshInsideBounds(GameObject level, Bounds bounds)
        {
            List<Transform> intersecting = new List<Transform>();
            for (var i = 0; i < level.transform.childCount; i++)
            {
                var child = level.transform.GetChild(i);
                var childBound = new Bounds(child.transform.position, Vector3.zero);
                foreach (Renderer r in child.GetComponentsInChildren<Renderer>())
                {
                    childBound.Encapsulate(r.bounds);
                }
                if (bounds.Intersects(childBound))
                {
                    intersecting.Add(child);
                }
            }
            return intersecting;
        }


        [MenuItem("Subway/Box Merging/Merge Mesh In Selected GameObject")]
        static void MergeMesh()
        {
            var selections = Selection.transforms;
            foreach (var t in selections)
            {
                Debug.Log("Merging in " + t.name);
                CombineChildren(t.gameObject);
            }
        }

        [MenuItem("Subway/Box Merging/Merge Mesh In Occlusions Boxes")]
        static void MergeMeshInOcclusionBoxes()
        {
            var scene = SceneManager.GetActiveScene();
            var gameObjects = scene.GetRootGameObjects();
            GameObject level = FindByName(gameObjects, "Level");
            for (var i = 0; i < level.transform.childCount; i++)
            {
                var child = level.transform.GetChild(i);
                CombineChildren(child.gameObject);
                Debug.Log("Merging element in " + child.name);
            }
        }

        public static void CombineChildren(GameObject parent)
        {
            // Find all mesh filter submeshes and separate them by their cooresponding materials
            List<Material> materials = new List<Material>();
            List<List<CombineInstance>> combineInstanceByMaterial = new List<List<CombineInstance>>();
            MeshFilter[] meshFilters = parent.GetComponentsInChildren<MeshFilter>();

            foreach (MeshFilter meshFilter in meshFilters)
            {
                MeshRenderer meshRenderer = meshFilter.GetComponent<MeshRenderer>();
                if (meshRenderer != null)
                {
                    for (int s = 0; s < meshFilter.sharedMesh.subMeshCount; s++)
                    {
                        int materialArrayIndex = materials.FindIndex(p => p.name == meshRenderer.sharedMaterials[s].name);
                        if (materialArrayIndex == -1)
                        {
                            materials.Add(meshRenderer.sharedMaterials[s]);
                            materialArrayIndex = materials.Count - 1;
                            combineInstanceByMaterial.Add(new List<CombineInstance>());
                        }

                        CombineInstance combineInstance = new CombineInstance();
                        combineInstance.mesh = meshFilter.sharedMesh;
                        combineInstance.transform = meshRenderer.transform.localToWorldMatrix;
                        combineInstance.subMeshIndex = s;
                        combineInstanceByMaterial[materialArrayIndex].Add(combineInstance);
                    }
                }

                //Disable child meshes
                meshFilter.gameObject.SetActive(false);
            }

            // Combine by material index into per-material meshes
            for (int m = 0; m < materials.Count; m++)
            {
                var idx = 0;
                var split = SplitCombineInstance(combineInstanceByMaterial[m]); // Get combine instance by material and split by 65k vertex max
                foreach (var combineInstanceList in split)
                {
                    CombineInstance[] combineInstanceArray = combineInstanceList.ToArray();
                    var material = materials[m];

                    Debug.Log("Merging " + (material.name + "_grouped" + idx));

                    var newMesh = new Mesh();
                    newMesh.CombineMeshes(combineInstanceArray, true, true, true);
                    Unwrapping.GenerateSecondaryUVSet(newMesh);

                    var child = new GameObject();
                    child.name = material.name + "_grouped" + idx;
                    child.transform.parent = parent.transform;
                    var meshFilter = child.AddComponent<MeshFilter>();
                    meshFilter.sharedMesh = newMesh;
                    var renderer = child.GetComponent<Renderer>();
                    if (renderer == null)
                    {
                        renderer = child.AddComponent<MeshRenderer>();
                    }
                    renderer.material = material;
                    idx++;
                }
            }
        }

        private static List<List<CombineInstance>> SplitCombineInstance(List<CombineInstance> input)
        {
            List<List<CombineInstance>> output = new List<List<CombineInstance>>();

            var totalVertex = 5000000;
            List<CombineInstance> current = null;

            foreach (var ci in input)
            {
                if ((totalVertex + ci.mesh.vertexCount) > 65000)
                {
                    totalVertex = 0;
                    current = new List<CombineInstance>();
                    output.Add(current);
                }

                current.Add(ci);
                totalVertex += ci.mesh.vertexCount;
            }

            return output;
        }

        private static GameObject FindByName(GameObject[] gameObjects, string name)
        {
            GameObject found = null;
            for (var i = 0; i < gameObjects.Length; i++)
            {
                if (gameObjects[i].name == name)
                {
                    found = gameObjects[i];
                }
            }
            return found;
        }
    }

}

#endif