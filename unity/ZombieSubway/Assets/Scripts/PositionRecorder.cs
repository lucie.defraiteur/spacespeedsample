﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Subway
{

    public class PathData
    {
        public List<Vector3> positions = new List<Vector3>();
        public List<Quaternion> rotations = new List<Quaternion>();
        public List<Vector3> eulerRotations = new List<Vector3>();
    }

    // Press space to save a point
    // Press P to export to json
    public class PositionRecorder : MonoBehaviour
    {

        PathData path;

        void Start()
        {
            path = new PathData();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var p = transform.localPosition;
                var r = transform.localRotation;
                var angle = transform.localRotation.eulerAngles;
                path.positions.Add(p);
                if (path.rotations.Count > 0)
                {
                    var last = (path.eulerRotations[path.eulerRotations.Count - 1]);
                    var newangle = new Vector3(
                        last.x + Mathf.DeltaAngle(last.x, angle.x),
                        last.y + Mathf.DeltaAngle(last.y, angle.y),
                        last.z + Mathf.DeltaAngle(last.z, angle.z)
                        );
                    path.eulerRotations.Add(newangle);
                    path.rotations.Add(r);

                    //Debug.Log("Angle2 = " + newangle.ToString() + ", initial = " + angle.ToString());
                }
                else
                {
                    path.rotations.Add(r);
                    path.eulerRotations.Add(angle);
                }
                Debug.Log("Point recorded " + p.x + "," + p.y + "," + p.z);
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("Saving path data to Assets/Temp/path.json");
                SaveAsJson("path", path);
            }
        }

        void SaveAsJson(string filename, object obj)
        {
            string folder = null;
            string path = null;
#if UNITY_EDITOR
            folder = "Assets/Temp/";
            path = folder + filename + ".json";
#endif
            new FileInfo(folder).Directory.Create();

            string str = JsonUtility.ToJson(obj);
            using (FileStream fs = new FileStream(path, FileMode.Create))
            {
                using (StreamWriter writer = new StreamWriter(fs))
                {
                    writer.Write(str);
                }
            }

#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
        }

    }

}
