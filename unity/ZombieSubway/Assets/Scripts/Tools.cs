﻿#if (UNITY_EDITOR)


using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


namespace Subway
{
    public class Tools : EditorWindow
    {

        [MenuItem("Subway/Tools/Generate Camera Animation")]
        static void GenerateCameraAnimation()
        {
            float speed = 2;

            string filePath = "Assets/Temp/path.json";
            string dataAsJson = File.ReadAllText(filePath);
            PathData loadedData = JsonUtility.FromJson<PathData>(dataAsJson);

            AnimationClip clip = new AnimationClip();

            AnimationCurve cpx = new AnimationCurve();
            AnimationCurve cpy = new AnimationCurve();
            AnimationCurve cpz = new AnimationCurve();
            AnimationCurve crx = new AnimationCurve();
            AnimationCurve cry = new AnimationCurve();
            AnimationCurve crz = new AnimationCurve();
            //AnimationCurve crw = new AnimationCurve();

            var time = 0;

            for (var i = 0; i < loadedData.positions.Count; i++)
            {
                var position = loadedData.positions[i];
                var rotation = loadedData.eulerRotations[i];

                if (i > 0)
                {
                    var previous = loadedData.positions[i - 1];
                    var distance = Vector3.Distance(previous, position);
                    if (distance < 0.2)
                    {
                        time += 2;
                    }
                    else
                    {
                        time += (int)Mathf.Round(distance * 1 / speed);
                    }
                }

                cpx.AddKey(time, position.x);
                cpy.AddKey(time, position.y);
                cpz.AddKey(time, position.z);

                crx.AddKey(time, rotation.x);
                cry.AddKey(time, rotation.y);
                crz.AddKey(time, rotation.z);
                //crw.AddKey(time, rotation.w);
            }

            clip.SetCurve("CamHolder", typeof(Transform), "m_LocalPosition.x", cpx);
            clip.SetCurve("CamHolder", typeof(Transform), "m_LocalPosition.y", cpy);
            clip.SetCurve("CamHolder", typeof(Transform), "m_LocalPosition.z", cpz);

            clip.SetCurve("CamHolder", typeof(Transform), "m_LocalEulerRotation.x", crx);
            clip.SetCurve("CamHolder", typeof(Transform), "m_LocalEulerRotation.y", cry);
            clip.SetCurve("CamHolder", typeof(Transform), "m_LocalEulerRotation.z", crz);
            //clip.SetCurve("CamHolder", typeof(Transform), "m_LocalRotation.w", crw);

            AssetDatabase.CreateAsset(clip, "Assets/Temp/CamAnimTemp.anim");
        }

        [MenuItem("Subway/Debug/Get world position")]
        public static void PrintGlobalPosition()
        {
            if (Selection.activeGameObject != null)
            {
                Debug.Log(Selection.activeGameObject.name + " is at " + Selection.activeGameObject.transform.position);
            }
        }
    }

}

#endif
