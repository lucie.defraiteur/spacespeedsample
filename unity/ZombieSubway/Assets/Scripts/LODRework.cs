﻿#if (UNITY_EDITOR)

using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityExporter;

namespace Subway
{

    public class LODRework : EditorWindow
    {

        [MenuItem("Subway/LOD/Destroy LOD (keep last)")]
        static void DestroyLOD()
        {
            var selections = Selection.transforms;
            for (var i = 0; i < selections.Length; i++)
            {
                var selection = selections[i];
                var lodgroups = selection.GetComponentsInChildren<LODGroup>();

                for (var j = 0; j < lodgroups.Length; j++)
                {
                    var lodgroup = lodgroups[j];
                    DestroyLODNode(lodgroup.gameObject, lodgroup, 0);
                }
            }
        }

        private static void DestroyLODNode(GameObject gameObject, LODGroup lodGroup, int idx)
        {
            //GameObject childToKeep = null;
            var lods = lodGroup.GetLODs();
            var keep = Mathf.Max(0, lods.Length - 1 - idx);
            for (var i = lods.Length - 1; i >= 0; i--)
            {
                var lod = lods[i];
                if (i == keep)
                {
                    var renderers = CountRenderers(lod);
                    if (renderers == 0)
                    {
                        // No renderer, keep next
                        keep--;
                    }
                    else
                    {
                        // Keep this one
                        continue;
                    }
                }

                // renderers ??
                for (var r = 0; r < lod.renderers.Length; r++)
                {
                    var renderer = lod.renderers[r];
                    if (renderer == null)
                    {
                        continue;
                    }

                    var child = renderer.gameObject;
                    child.SetActive(false);
                    GameObject.DestroyImmediate(child.gameObject);
                }
            }

            DestroyImmediate(lodGroup);
        }

        private static int CountRenderers(LOD lod)
        {
            int count = 0;
            if (lod.renderers == null) return 0;
            foreach (var r in lod.renderers)
            {
                if (r != null) count++;
            }
            return count;
        }

        [MenuItem("Subway/LOD/Move up single leaf")]
        static void MoveUpSingleLeaf()
        {
            var selections = Selection.transforms;
            for (var i = 0; i < selections.Length; i++)
            {
                var selection = selections[i];
                MoveUpSingleLeaf0(selection);
            }
        }
        static void MoveUpSingleLeaf0(Transform transform)
        {
            SceneHelper.traverse(transform.gameObject, node =>
            {
                if (node.transform.childCount <= 0)
                {
                    // It's leaf
                    var leaf = node;
                    var parent = leaf.transform.parent;
                    var sibblingsCount = parent.childCount;
                    if (sibblingsCount > 1) return;

                    if (leaf.transform.localPosition != Vector3.zero) return;
                    if (leaf.transform.localRotation != Quaternion.identity) return;
                    if (leaf.transform.localScale != Vector3.one) return;

                    leaf.transform.parent = parent.transform.parent;
                    leaf.transform.localPosition = parent.transform.localPosition;
                    leaf.transform.localRotation = parent.transform.localRotation;
                    leaf.transform.localScale = parent.transform.localScale;
                    GameObject.DestroyImmediate(parent.gameObject);
                }
            });
        }



        [MenuItem("Subway/LOD/Destroy Orphan")]
        static void DestroyOrphan()
        {
            var selections = Selection.transforms;
            for (var i = 0; i < selections.Length; i++)
            {
                var selection = selections[i];
                DestroyOrphan0(selection);
            }
        }

        private static void DestroyOrphan0(Transform transform)
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                DestroyOrphan0(transform.GetChild(i));
            }

            if (transform.childCount == 0)
            {
                var renderer = transform.GetComponent<MeshRenderer>();
                if (renderer != null) return;

                var light = transform.GetComponent<Light>();
                if (light != null) return;

                DestroyImmediate(transform.gameObject);
                return;
            }
        }
    }

}

#endif