﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintUvScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Debug.Log("Starting " + gameObject.name);

        List<Vector2> uvs0 = new List<Vector2>();
        List<Vector2> uvs1 = new List<Vector2>();

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.GetUVs(0, uvs0);
        mesh.GetUVs(1, uvs1);
        Debug.Log(gameObject.name + " uv => " + uvs0[0] + " / " + uvs0[1] + " / " + uvs0[2]);
        Debug.Log(gameObject.name + " uv2 => " + uvs1[0] + " / " + uvs1[1] + " / " + uvs1[2]);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
