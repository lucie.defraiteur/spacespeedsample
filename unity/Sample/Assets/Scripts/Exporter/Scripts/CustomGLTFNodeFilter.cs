﻿#if (UNITY_EDITOR)

using UnityEngine;
using GLTF.Schema;

namespace UnityExporter
{
    public interface CustomGLTFNodeFilter
    {
        bool exportNode(Transform transform);
    }

    public class DefaultGLTFNodeFilter : CustomGLTFNodeFilter
    {
        public bool exportNode(Transform transform)
        {
            if (ExporterOptions.ExportOnlyActive)
            {
                if (!transform.gameObject.activeSelf) return false;
                if (!transform.gameObject.activeInHierarchy) return false;
            }

            return true;
        }
    }
}

#endif