#if (UNITY_EDITOR)

using GLTF;
using GLTF.Schema;
using UnityEngine;

namespace UnityExporter
{
    public static class SchemaExtensions
    {
        // glTF matrix: column vectors, column-major storage, +Y up, +Z forward, -X right, right-handed
        // unity matrix: column vectors, column-major storage, +Y up, +Z forward, +X right, left-handed
        // multiply by a negative X scale to convert handedness
        public static readonly GLTF.Math.Vector3 CoordinateSpaceConversionScale = new GLTF.Math.Vector3(-1, 1, 1);
        public static bool CoordinateSpaceConversionRequiresHandednessFlip
        {
            get
            {
                return CoordinateSpaceConversionScale.X * CoordinateSpaceConversionScale.Y * CoordinateSpaceConversionScale.Z < 0.0f;
            }
        }

        public static readonly GLTF.Math.Vector4 TangentSpaceConversionScale = new GLTF.Math.Vector4(-1, 1, 1, -1);

        public static void SetUnityTransform(this Node node, Transform transform)
        {
            node.Translation = transform.localPosition.ToGltfVector3Convert();
            node.Rotation = transform.localRotation.ToGltfQuaternionConvert();
            node.Scale = transform.localScale.ToGltfVector3Raw();
        }

        public static GLTF.Math.Vector3 ToGltfVector3Convert(this Vector3 unityVec3)
        {
            Vector3 coordinateSpaceConversionScale = CoordinateSpaceConversionScale.ToUnityVector3Raw();
            GLTF.Math.Vector3 gltfVec3 = Vector3.Scale(unityVec3, coordinateSpaceConversionScale).ToGltfVector3Raw();
            return gltfVec3;
        }

        public static GLTF.Math.Quaternion ToGltfQuaternionConvert(this Quaternion unityQuat)
        {
            // get the original axis and apply conversion scale as well as potential rotation axis flip
            Vector3 origAxis = new Vector3(unityQuat.x, unityQuat.y, unityQuat.z);
            float axisFlipScale = CoordinateSpaceConversionRequiresHandednessFlip ? -1.0f : 1.0f;
            Vector3 newAxis = axisFlipScale * Vector3.Scale(origAxis, CoordinateSpaceConversionScale.ToUnityVector3Raw());

            // then put the quaternion back together and return it
            return new GLTF.Math.Quaternion(newAxis.x, newAxis.y, newAxis.z, unityQuat.w);
        }

        public static GLTF.Math.Matrix4x4 ToGltfMatrix4x4Convert(this Matrix4x4 unityMat)
        {
            Vector3 coordinateSpaceConversionScale = CoordinateSpaceConversionScale.ToUnityVector3Raw();
            Matrix4x4 convert = Matrix4x4.Scale(coordinateSpaceConversionScale);
            GLTF.Math.Matrix4x4 gltfMat = (convert * unityMat * convert).ToGltfMatrix4x4Raw();
            return gltfMat;
        }

        public static GLTF.Math.Vector3 ToGltfVector3Raw(this Vector3 unityVec3)
        {
            GLTF.Math.Vector3 gltfVec3 = new GLTF.Math.Vector3(unityVec3.x, unityVec3.y, unityVec3.z);
            return gltfVec3;
        }
        public static GLTF.Math.Vector4 ToGltfVector4Raw(this Vector4 unityVec4)
        {
            GLTF.Math.Vector4 gltfVec4 = new GLTF.Math.Vector4(unityVec4.x, unityVec4.y, unityVec4.z, unityVec4.w);
            return gltfVec4;
        }
        public static GLTF.Math.Matrix4x4 ToGltfMatrix4x4Raw(this Matrix4x4 unityMat)
        {
            GLTF.Math.Vector4 c0 = unityMat.GetColumn(0).ToGltfVector4Raw();
            GLTF.Math.Vector4 c1 = unityMat.GetColumn(1).ToGltfVector4Raw();
            GLTF.Math.Vector4 c2 = unityMat.GetColumn(2).ToGltfVector4Raw();
            GLTF.Math.Vector4 c3 = unityMat.GetColumn(3).ToGltfVector4Raw();
            GLTF.Math.Matrix4x4 rawGltfMat = new GLTF.Math.Matrix4x4(c0.X, c0.Y, c0.Z, c0.W, c1.X, c1.Y, c1.Z, c1.W, c2.X, c2.Y, c2.Z, c2.W, c3.X, c3.Y, c3.Z, c3.W);
            return rawGltfMat;
        }

        public static Vector3 ToUnityVector3Raw(this GLTF.Math.Vector3 vec3)
        {
            return new Vector3(vec3.X, vec3.Y, vec3.Z);
        }
        public static Vector4 ToUnityVector4Raw(this GLTF.Math.Vector4 vec4)
        {
            return new Vector4(vec4.X, vec4.Y, vec4.Z, vec4.W);
        }
        public static Quaternion ToUnityQuaternionRaw(this GLTF.Math.Quaternion quat)
        {
            return new Quaternion(quat.X, quat.Y, quat.Z, quat.W);
        }
        public static Matrix4x4 ToUnityMatrix4x4Raw(this GLTF.Math.Matrix4x4 gltfMat)
        {
            Vector4 rawUnityCol0 = gltfMat.GetColumn(0).ToUnityVector4Raw();
            Vector4 rawUnityCol1 = gltfMat.GetColumn(1).ToUnityVector4Raw();
            Vector4 rawUnityCol2 = gltfMat.GetColumn(2).ToUnityVector4Raw();
            Vector4 rawUnityCol3 = gltfMat.GetColumn(3).ToUnityVector4Raw();
            Matrix4x4 rawUnityMat = new UnityEngine.Matrix4x4();
            rawUnityMat.SetColumn(0, rawUnityCol0);
            rawUnityMat.SetColumn(1, rawUnityCol1);
            rawUnityMat.SetColumn(2, rawUnityCol2);
            rawUnityMat.SetColumn(3, rawUnityCol3);

            return rawUnityMat;
        }
        public static GLTF.Math.Vector4 GetColumn(this GLTF.Math.Matrix4x4 mat, uint columnNum)
        {
            switch (columnNum)
            {
                case 0:
                    {
                        return new GLTF.Math.Vector4(mat.M11, mat.M21, mat.M31, mat.M41);
                    }
                case 1:
                    {
                        return new GLTF.Math.Vector4(mat.M12, mat.M22, mat.M32, mat.M42);
                    }
                case 2:
                    {
                        return new GLTF.Math.Vector4(mat.M13, mat.M23, mat.M33, mat.M43);
                    }
                case 3:
                    {
                        return new GLTF.Math.Vector4(mat.M14, mat.M24, mat.M34, mat.M44);
                    }
                default:
                    throw new System.Exception("column num is out of bounds");
            }
        }

        public static UnityEngine.Vector3[] ConvertVector3CoordinateSpaceAndCopy(Vector3[] array, GLTF.Math.Vector3 coordinateSpaceCoordinateScale)
        {
            var returnArray = new UnityEngine.Vector3[array.Length];

            for (int i = 0; i < array.Length; i++)
            {
                returnArray[i].x = array[i].x * coordinateSpaceCoordinateScale.X;
                returnArray[i].y = array[i].y * coordinateSpaceCoordinateScale.Y;
                returnArray[i].z = array[i].z * coordinateSpaceCoordinateScale.Z;
            }

            return returnArray;
        }

        public static Vector4[] ConvertVector4CoordinateSpaceAndCopy(Vector4[] array, GLTF.Math.Vector4 coordinateSpaceCoordinateScale)
        {
            var returnArray = new Vector4[array.Length];

            for (var i = 0; i < array.Length; i++)
            {
                returnArray[i].x = array[i].x * coordinateSpaceCoordinateScale.X;
                returnArray[i].y = array[i].y * coordinateSpaceCoordinateScale.Y;
                returnArray[i].z = array[i].z * coordinateSpaceCoordinateScale.Z;
                returnArray[i].w = array[i].w * coordinateSpaceCoordinateScale.W;
            }

            return returnArray;
        }

        public static UnityEngine.Vector2[] FlipTexCoordArrayVAndCopy(UnityEngine.Vector2[] array)
        {
            var returnArray = new UnityEngine.Vector2[array.Length];

            for (var i = 0; i < array.Length; i++)
            {
                returnArray[i].x = array[i].x;
                returnArray[i].y = 1.0f - array[i].y;
            }

            return returnArray;
        }

        public static int[] FlipFacesAndCopy(int[] triangles)
        {
            int[] returnArr = new int[triangles.Length];
            for (int i = 0; i < triangles.Length; i += 3)
            {
                int temp = triangles[i];
                returnArr[i] = triangles[i + 2];
                returnArr[i + 1] = triangles[i + 1];
                returnArr[i + 2] = temp;
            }

            return returnArr;
        }

        public static GLTF.Math.Color ToNumericsColorRaw(this UnityEngine.Color color, bool convertToLinear)
        {
            var c = color;
            if (convertToLinear)
            {
                c = color.linear;
            }
            return new GLTF.Math.Color(c.r, c.g, c.b, c.a);
        }

        public static UnityEngine.Color RGBAToRBG(this UnityEngine.Color color)
        {
            // Suppose background white (255,255,255)
            UnityEngine.Color res = new UnityEngine.Color();
            res.r = ((1 - color.a) * 0) + (color.a * color.r);
            res.g = ((1 - color.a) * 0) + (color.a * color.g);
            res.b = ((1 - color.a) * 0) + (color.a * color.b);
            return res;
        }

    }


    public struct UShort4
    {
        public ushort x;
        public ushort y;
        public ushort z;
        public ushort w;

        public UShort4(ushort _x, ushort _y, ushort _z, ushort _w)
        {
            x = _x;
            y = _y;
            z = _z;
            w = _w;
        }
    }
}

#endif