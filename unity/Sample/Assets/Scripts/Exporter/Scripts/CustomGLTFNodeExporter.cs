﻿#if (UNITY_EDITOR)

using UnityEngine;
using GLTF.Schema;

namespace UnityExporter
{
    public interface CustomGLTFNodeExporter
    {
        void exportNode(Transform transform, Node node);
    }

    public class DefaultCustomGLTFNodeExporter : CustomGLTFNodeExporter
    {
        public void exportNode(Transform transform, Node node)
        {
            // To nothing
        }
    }
}

#endif