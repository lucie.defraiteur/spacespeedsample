﻿#if (UNITY_EDITOR)

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UnityExporter
{


    //
    // Data by property (ie: rotation, translation, etc.)
    //
    class AnimationCurveData
    {
        public GLTF.Schema.SamplerId SamplerId { get; private set; }

        private Dictionary<string, AnimationCurve> curves = new Dictionary<string, AnimationCurve>();
        private GLTFExporter exporter;
        private AnimationPropertys animationProperty;
        private AnimationClip clip;

        public AnimationCurveData(GLTFExporter exporter, AnimationClip clip, AnimationPropertys property, GLTF.Schema.SamplerId samplerId)
        {
            this.exporter = exporter;
            this.animationProperty = property;
            this.clip = clip;
            this.SamplerId = samplerId;
        }

        public void AddCurve(string propertyName, AnimationCurve curve)
        {
            var key = this.GetPropertyKey(propertyName);
            curves.Add(key, curve);
        }

        private string GetPropertyKey(string property)
        {
            if (property.StartsWith("blendShape."))
            {
                return property.Substring("blendShape.".Length);
            }
            if (property.EndsWith(".x"))
            {
                return "x";
            }
            if (property.EndsWith(".y"))
            {
                return "y";
            }
            if (property.EndsWith(".z"))
            {
                return "z";
            }
            if (property.EndsWith(".w"))
            {
                return "w";
            }
            else
            {
                throw new NotImplementedException();
            }
        }


        public void ExportSampler(GLTF.Schema.AnimationSampler sampler)
        {
            AnimationCurve curveX;
            AnimationCurve curveY;
            AnimationCurve curveZ;
            AnimationCurve curveW;

            this.curves.TryGetValue("x", out curveX);
            this.curves.TryGetValue("y", out curveY);
            this.curves.TryGetValue("z", out curveZ);
            this.curves.TryGetValue("w", out curveW);

            if (this.animationProperty == AnimationPropertys.Scale)
            {
                ExportAnimationSampler<Vector3>(
                    sampler,
                    new AnimationCurve[] { curveX, curveY, curveZ },
                    keyIndex => new Vector3(curveX.keys[keyIndex].inTangent, curveY.keys[keyIndex].inTangent, curveZ.keys[keyIndex].inTangent),
                    keyIndex => new Vector3(curveX.keys[keyIndex].value, curveY.keys[keyIndex].value, curveZ.keys[keyIndex].value),
                    keyIndex => new Vector3(curveX.keys[keyIndex].outTangent, curveY.keys[keyIndex].outTangent, curveZ.keys[keyIndex].outTangent),
                    time => new Vector3(curveX.Evaluate(time), curveY.Evaluate(time), curveZ.Evaluate(time)),
                    values => this.exporter.accessorExporter.Export(values));
            }
            else if (this.animationProperty == AnimationPropertys.Translation)
            {
                ExportAnimationSampler<Vector3>(
                    sampler,
                    new AnimationCurve[] { curveX, curveY, curveZ },
                    keyIndex => GetRightHandedVector(new Vector3(curveX.keys[keyIndex].inTangent, curveY.keys[keyIndex].inTangent, curveZ.keys[keyIndex].inTangent)),
                    keyIndex => GetRightHandedVector(new Vector3(curveX.keys[keyIndex].value, curveY.keys[keyIndex].value, curveZ.keys[keyIndex].value)),
                    keyIndex => GetRightHandedVector(new Vector3(curveX.keys[keyIndex].outTangent, curveY.keys[keyIndex].outTangent, curveZ.keys[keyIndex].outTangent)),
                    time => GetRightHandedVector(new Vector3(curveX.Evaluate(time), curveY.Evaluate(time), curveZ.Evaluate(time))),
                    values => this.exporter.accessorExporter.Export(values));
            }
            else if (this.animationProperty == AnimationPropertys.Rotation || this.animationProperty == AnimationPropertys.EulerRotation)
            {
                ExportAnimationSampler<Quaternion>(
                    sampler,
                    new AnimationCurve[] { curveX, curveY, curveZ, curveW },
                    keyIndex => GetRightHandedQuaternion(new Quaternion(curveX.keys[keyIndex].inTangent, curveY.keys[keyIndex].inTangent, curveZ.keys[keyIndex].inTangent, curveW.keys[keyIndex].inTangent)),
                    keyIndex => GetRightHandedQuaternion(CreateNormalizedQuaternion(curveX.keys[keyIndex].value, curveY.keys[keyIndex].value, curveZ.keys[keyIndex].value, curveW.keys[keyIndex].value)),
                    keyIndex => GetRightHandedQuaternion(new Quaternion(curveX.keys[keyIndex].outTangent, curveY.keys[keyIndex].outTangent, curveZ.keys[keyIndex].outTangent, curveW.keys[keyIndex].outTangent)),
                    time => GetRightHandedQuaternion(CreateNormalizedQuaternion(curveX.Evaluate(time), curveY.Evaluate(time), curveZ.Evaluate(time), curveW.Evaluate(time))),
                    values => this.exporter.accessorExporter.Export(values));
            }
            else if (this.animationProperty == AnimationPropertys.BlendShape)
            {
                this.ExportAnimationSamplerWeight(sampler);
            }
            else
            {
                Debug.LogWarning("Export not implemented for animation of type: " + animationProperty);
            }
        }

        private void ExportAnimationSamplerWeight(GLTF.Schema.AnimationSampler sampler)
        {
            var weightCurves = this.curves.Values.ToList();
            ExportAnimationSampler<IEnumerable<float>>(
                sampler,
                weightCurves,
                keyIndex => weightCurves.Select(curve => curve.keys[keyIndex].inTangent / 100),
                keyIndex => weightCurves.Select(curve => curve.keys[keyIndex].value / 100),
                keyIndex => weightCurves.Select(curve => curve.keys[keyIndex].outTangent / 100),
                time => weightCurves.Select(curve => curve.Evaluate(time) / 100),
                values => this.exporter.accessorExporter.Export(values.SelectMany(value => value)));
        }

        private void ExportAnimationSampler<T>(GLTF.Schema.AnimationSampler sampler,
            IEnumerable<AnimationCurve> curves,
            Func<int, T> getInTangent,
            Func<int, T> getValue,
            Func<int, T> getOutTangent,
            Func<float, T> evaluate,
            Func<IEnumerable<T>, GLTF.Schema.AccessorId> exportData)
        {
            var testSpline = true;
            if (typeof(T) == typeof(Quaternion))
            {
                testSpline = false; // TODO: threejs quaternion smooth
            }

            // If we can use cubicspline, we will output for each key: inTangent, value, outTangent
            // If we can not use cubicspline, we will evaluate the ull animation every 1/30s.
            //
            if (testSpline && CanExportCurvesAsSpline(curves))
            {
                var firstCurve = curves.First();
                var input = new float[firstCurve.keys.Length];
                var output = new T[firstCurve.keys.Length * 3];
                for (int keyIndex = 0; keyIndex < firstCurve.keys.Length; keyIndex++)
                {
                    // https://github.com/KhronosGroup/glTF/blob/master/specification/2.0/README.md#appendix-c-spline-interpolation
                    input[keyIndex] = firstCurve.keys[keyIndex].time;
                    output[keyIndex * 3 + 0] = getInTangent(keyIndex);
                    output[keyIndex * 3 + 1] = getValue(keyIndex);
                    output[keyIndex * 3 + 2] = getOutTangent(keyIndex);
                }

                sampler.Input = this.exporter.accessorExporter.Export(input);
                sampler.Interpolation = GLTF.Schema.InterpolationType.CUBICSPLINE;
                sampler.Output = exportData(output);
            }
            else
            {
                if (testSpline)
                {
                    Debug.Log("Can not export " + this.clip.name + "/" + this.animationProperty + " as cubic spline. Using linear interpolation.");
                }

                var input = new List<float>();
                var output = new List<T>();
                var maxTime = curves.Max(curve => curve.keys.Last().time);
                for (float time = 0; time < maxTime; time += 1.0f / 30.0f)
                {
                    input.Add(time);
                    output.Add(evaluate(time));
                }

                sampler.Input = this.exporter.accessorExporter.Export(input);
                sampler.Interpolation = GLTF.Schema.InterpolationType.LINEAR;
                sampler.Output = exportData(output);
            }

        }

        private static bool CanExportCurvesAsSpline(IEnumerable<AnimationCurve> curves)
        {
            AnimationCurve firstCurve = curves.First();
            IEnumerable<AnimationCurve> remainingCurves = curves.Skip(1);
            // All curves must have the same number of keys.
            if (!remainingCurves.All(curve => curve.keys.Length == firstCurve.keys.Length))
            {
                return false;
            }
            for (int keyIndex = 0; keyIndex < firstCurve.keys.Length; keyIndex++)
            {
                // All curves must have the same time values.
                if (!remainingCurves.All(curve => Mathf.Approximately(curve.keys[keyIndex].time, firstCurve.keys[keyIndex].time)))
                {
                    return false;
                }
            }
            return true;
        }

        private static Vector3 GetRightHandedVector(Vector3 value)
        {
            return SchemaExtensions.ToGltfVector3Convert(value).ToUnityVector3Raw();
            // return new Vector3(value.x, value.y, -value.z);
        }
        private static Quaternion GetRightHandedQuaternion(Quaternion value)
        {
            return SchemaExtensions.ToGltfQuaternionConvert(value).ToUnityQuaternionRaw();
            // return new Quaternion(-value.x, -value.y, value.z, value.w);
        }
        private static Quaternion CreateNormalizedQuaternion(float x, float y, float z, float w)
        {
            var factor = 1.0f / Mathf.Sqrt(x * x + y * y + z * z + w * w);
            return new Quaternion(x * factor, y * factor, z * factor, w * factor);
        }
    }

}

#endif
