﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFMaterialExporter : GLTFExporterBase
    {
        public GLTFMaterialExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.MaterialId Export(Material materialObj)
        {
            GLTF.Schema.MaterialId id = this.exporter.GetMaterialId(_root, materialObj);
            if (id != null)
            {
                return id;
            }

            var material = new GLTF.Schema.GLTFMaterial();
            material.Name = materialObj.name;

            if (materialObj.HasProperty("_Cutoff"))
            {
                material.AlphaCutoff = materialObj.GetFloat("_Cutoff");
            }

            var opaque = true;
            switch (materialObj.GetTag("RenderType", false, ""))
            {
                case "TransparentCutout":
                    opaque = false;
                    material.AlphaMode = GLTF.Schema.AlphaMode.MASK;
                    break;
                case "Transparent":
                    opaque = false;
                    material.AlphaMode = GLTF.Schema.AlphaMode.BLEND;
                    break;
                default:
                    material.AlphaMode = GLTF.Schema.AlphaMode.OPAQUE;
                    break;
            }

            material.DoubleSided = materialObj.HasProperty("_Cull") && materialObj.GetInt("_Cull") == (float)UnityEngine.Rendering.CullMode.Off;

            if (materialObj.HasProperty("_EmissionColor"))
            {
                var emmissionOn = materialObj.IsKeywordEnabled("_EMISSION");
                if (emmissionOn)
                {
                    material.EmissiveFactor = materialObj.GetColor("_EmissionColor").ToNumericsColorRaw(true);
                }
            }

            if (materialObj.HasProperty("_EmissionMap"))
            {
                var emissionTex = materialObj.GetTexture("_EmissionMap");
                if (emissionTex != null)
                {
                    material.EmissiveTexture = this.exporter.textureExporter.ExportTextureInfo(emissionTex, TextureMapType.Emission, opaque);
                    this.exporter.textureExporter.ExportTextureTransform(material.EmissiveTexture, materialObj, "_EmissionMap");
                }
            }

            if (materialObj.HasProperty("_BumpMap"))
            {
                var normalTex = materialObj.GetTexture("_BumpMap");
                if (normalTex != null)
                {
                    material.NormalTexture = this.exporter.textureExporter.ExportNormalTextureInfo(normalTex, TextureMapType.Bump, materialObj);
                    this.exporter.textureExporter.ExportTextureTransform(material.NormalTexture, materialObj, "_BumpMap");
                }
            }

            if (materialObj.HasProperty("_OcclusionMap"))
            {
                var occTex = materialObj.GetTexture("_OcclusionMap");
                if (occTex != null)
                {
                    material.OcclusionTexture = this.exporter.textureExporter.ExportOcclusionTextureInfo(occTex, TextureMapType.Occlusion, materialObj);
                    this.exporter.textureExporter.ExportTextureTransform(material.OcclusionTexture, materialObj, "_OcclusionMap");
                }
            }

            if (IsPBRMetallicRoughness(materialObj))
            {
                material.PbrMetallicRoughness = this.exporter.textureExporter.ExportPBRMetallicRoughness(materialObj, opaque);
            }
            else if (IsCommonConstant(materialObj))
            {
                material.CommonConstant = this.exporter.textureExporter.ExportCommonConstant(materialObj);
            }
            else
            {
                // MBA fix for BB (else no texture because of shader vaccumm)
                //material.PbrMetallicRoughness = ExportPBRMetallicRoughness(materialObj);
                Debug.Log("Check shader of material: " + material.Name + ". Texture might not be exported.");
            }

            this.exporter._materials.Add(materialObj);

            id = new GLTF.Schema.MaterialId
            {
                Id = _root.Materials.Count,
                Root = _root
            };
            _root.Materials.Add(material);

            return id;
        }


        private bool IsPBRMetallicRoughness(Material material)
        {
            return material.HasProperty("_Metallic") && material.HasProperty("_MetallicGlossMap");
        }

        private bool IsCommonConstant(Material material)
        {
            return material.HasProperty("_AmbientFactor") &&
            material.HasProperty("_LightMap") &&
            material.HasProperty("_LightFactor");
        }
    }
}

#endif