#if (UNITY_EDITOR)

using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

namespace UnityExporter
{

    class GLTFSpriteExporter : GLTFExporterBase
    {
        public static string EXTENSION_NAME = "GB_sprites";
        private bool extensionAdded = false;
        private SpriteRootExtension extension;

        private List<Sprite> sprites = new List<Sprite>();

        public GLTFSpriteExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public int Export(Sprite sprite)
        {
            var idx = this.sprites.IndexOf(sprite);
            if (idx >= 0)
            {
                return idx; // Already exported
            }

            // Register and create extension root
            if (this.extension == null)
            {
                this.RegisterExtension();
            }

            var spriteId = this.extension.sprites.Count;
            this.AddSprite(sprite);

            return spriteId;
        }

        private void AddSprite(Sprite sprite)
        {
            // {
            //     "texture" : 15, 
            // }

            // TODO manage atlas
            if (sprite.packed)
            {
                Debug.LogWarning("Packed sprite not supported yet. Sprite " + sprite.name + " will not be export correctly.");
            }

            var textureId = this.exporter.textureExporter.ExportTextureInfo(sprite.texture, TextureMapType.Sprite, false);

            JObject o = new JObject();
            o.Add("texture", textureId.Index.Id);

            this.extension.sprites.Add(o);
        }

        private void RegisterExtension()
        {
            if (this.extensionAdded) return;

            if (_root.Extensions == null)
            {
                _root.Extensions = new Dictionary<string, GLTF.Schema.IExtension>();
            }
            extension = new SpriteRootExtension();
            _root.Extensions.Add(EXTENSION_NAME, extension);

            if (_root.ExtensionsUsed == null)
            {
                _root.ExtensionsUsed = new List<string>(new[] { EXTENSION_NAME });
            }
            else
            {
                _root.ExtensionsUsed.Add(EXTENSION_NAME);
            }

            if (ExporterOptions.RequireExtensions)
            {
                if (_root.ExtensionsRequired == null)
                {
                    _root.ExtensionsRequired = new List<string>(new[] { EXTENSION_NAME });
                }
                else
                {
                    _root.ExtensionsRequired.Add(EXTENSION_NAME);
                }
            }

            this.extensionAdded = true;
        }
    }

    // Definition at root levels (list of sprites with details)
    class SpriteRootExtension : GLTF.Schema.IExtension
    {
        public JArray sprites;
        public SpriteRootExtension()
        {
            this.sprites = new JArray();
        }
        public GLTF.Schema.IExtension Clone(GLTF.Schema.GLTFRoot root)
        {
            return new SpriteRootExtension(); // TODO
        }
        public JProperty Serialize()
        {
            JObject ext = new JObject();
            ext.Add(new JProperty("sprites", this.sprites));
            return new JProperty(GLTFSpriteExporter.EXTENSION_NAME, ext);
        }
    }

    // Definition at node level
    // reference sprite to use
    class SpriteNodeExtension : GLTF.Schema.IExtension
    {
        private int Id;
        public SpriteNodeExtension(int id)
        {
            this.Id = id;
        }
        public GLTF.Schema.IExtension Clone(GLTF.Schema.GLTFRoot root)
        {
            return new SpriteNodeExtension(this.Id);
        }
        public JProperty Serialize()
        {
            JObject ext = new JObject();
            ext.Add(new JProperty("sprite", this.Id));
            return new JProperty(GLTFSpriteExporter.EXTENSION_NAME, ext);
        }
    }

}

#endif