#if (UNITY_EDITOR)

using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;

namespace UnityExporter
{

    class GLTFLightExporter : GLTFExporterBase
    {
        public static string EXTENSION_NAME = "KHR_lights_punctual";
        private bool extensionAdded = false;
        private LightRootExtension extension;

        public GLTFLightExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public void ExportLight(Light light, GLTF.Schema.Node node)
        {
            if (light.isActiveAndEnabled == false) return; // ignore inactive light
            if (light.lightmapBakeType == LightmapBakeType.Baked)
            {
                // baked light
                if (ExporterOptions.ExportLightmap) return; // lightmap export is active, ignore baked light
            }

            // Register and create extension root
            // Register light at root level
            if (this.extension == null)
            {
                this.RegisterExtension();
            }
            var lightCount = this.extension.lights.Count;
            this.AddLight(light);

            // Create extension at node level
            // Register light at node level (will have position / rotation attached)
            if (node.Extensions == null)
            {
                node.Extensions = new Dictionary<string, GLTF.Schema.IExtension>();
            }
            node.Extensions[EXTENSION_NAME] = new LightNodeExtension(lightCount);
        }

        private void AddLight(Light light)
        {
            // {
            //     "color" : [1.0,1.0,1.0], 
            //     "name" : "Directional", 
            //     "type" : "directional"
            // }

            JObject o = new JObject();

            var lightType = light.type;
            if (lightType == LightType.Directional)
            {
                o.Add("type", "directional");
            }
            else if (lightType == LightType.Point || lightType == LightType.Area)
            {
                if (lightType == LightType.Area)
                {
                    Debug.LogWarning("Area light not supported. Using PointLight instead");
                }

                o.Add("type", "point");
                o.Add("range", light.range);

            }
            else if (lightType == LightType.Spot)
            {
                o.Add("type", "spot");
                o.Add("range", light.range);

                var spot = new JObject();
                spot.Add("outerConeAngle", light.spotAngle * Mathf.Deg2Rad);
                o.Add("spot", spot);
            }

            // if (light.color != null)
            // {
            JArray color = new JArray();
            color.Add(light.color.r);
            color.Add(light.color.g);
            color.Add(light.color.b);
            o.Add("color", color);
            // }

            o.Add("intensity", light.intensity);
            o.Add("name", light.name);

            this.extension.lights.Add(o);
        }

        private void RegisterExtension()
        {
            if (this.extensionAdded) return;

            if (_root.Extensions == null)
            {
                _root.Extensions = new Dictionary<string, GLTF.Schema.IExtension>();
            }
            extension = new LightRootExtension();
            _root.Extensions.Add(EXTENSION_NAME, extension);

            if (_root.ExtensionsUsed == null)
            {
                _root.ExtensionsUsed = new List<string>(new[] { EXTENSION_NAME });
            }
            else
            {
                _root.ExtensionsUsed.Add(EXTENSION_NAME);
            }

            if (ExporterOptions.RequireExtensions)
            {
                if (_root.ExtensionsRequired == null)
                {
                    _root.ExtensionsRequired = new List<string>(new[] { EXTENSION_NAME });
                }
                else
                {
                    _root.ExtensionsRequired.Add(EXTENSION_NAME);
                }
            }

            this.extensionAdded = true;
        }
    }

    // Definition at root levels (list of lights with details)
    class LightRootExtension : GLTF.Schema.IExtension
    {
        public JArray lights;

        public LightRootExtension()
        {
            this.lights = new JArray();
        }
        public GLTF.Schema.IExtension Clone(GLTF.Schema.GLTFRoot root)
        {
            return new LightRootExtension(); // TODO
        }
        public JProperty Serialize()
        {
            JObject ext = new JObject();
            ext.Add(new JProperty("lights", this.lights));

            return new JProperty(GLTFLightExporter.EXTENSION_NAME, ext);
        }
    }

    // Definition at node level
    class LightNodeExtension : GLTF.Schema.IExtension
    {
        private int Id;

        public LightNodeExtension(int id)
        {
            this.Id = id;
        }
        public GLTF.Schema.IExtension Clone(GLTF.Schema.GLTFRoot root)
        {
            return new LightNodeExtension(this.Id);
        }
        public JProperty Serialize()
        {
            JObject ext = new JObject();
            ext.Add(new JProperty("light", this.Id));

            return new JProperty(GLTFLightExporter.EXTENSION_NAME, ext);
        }
    }

}

#endif