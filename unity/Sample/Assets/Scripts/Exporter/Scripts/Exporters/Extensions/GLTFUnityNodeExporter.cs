﻿#if (UNITY_EDITOR)

using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json.Linq;
using GLTF.Schema;

namespace UnityExporter
{

    class GLTFUnityNodeExporter : GLTFExporterBase
    {

        public GLTFUnityNodeExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public void exportNode(Transform transform, Node node)
        {
            var data = this.Export0(transform, node);
            if (data.Count <= 0)
            {
                return;
            }

            var extra = this.exporter.getExtra(node);
            extra.Add("gbunity", data);
        }

        private JObject Export0(Transform transform, Node node)
        {
            JObject ext = new JObject();

            var gameObject = transform.gameObject;
            if (gameObject.isStatic)
            {
                ext.Add("static", true); // we consider object not static by default
            }
            if (!ExporterOptions.ExportOnlyActive && !gameObject.activeSelf)
            {
                ext.Add("activeSelf", false); // we consider object not active by default
            }

            // If a sprite renderer is defined on the node export texture/sprite info
            var spriteRenderer = transform.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null && spriteRenderer.sprite != null)
            {
                var spriteId = this.exporter.spriteExporter.Export(spriteRenderer.sprite);
                var spriteInfo = new JObject();
                spriteInfo.Add("id", spriteId);
                if (spriteRenderer.color != null)
                {
                    var rgb = spriteRenderer.color.RGBAToRBG();
                    var color = new float[] { rgb.r, rgb.g, rgb.b };
                    spriteInfo.Add("color", new JArray(color));
                }
                ext.Add("sprite", spriteInfo);
            }

            return ext;
        }
    }

}

#endif