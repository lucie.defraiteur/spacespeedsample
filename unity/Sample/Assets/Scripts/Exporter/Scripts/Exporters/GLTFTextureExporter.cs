﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFTextureExporter : GLTFExporterBase
    {
        public GLTFTextureExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.TextureInfo ExportTextureInfo(Texture texture, TextureMapType textureMapType, bool opaque)
        {
            var info = new GLTF.Schema.TextureInfo();
            info.Index = ExportTexture(texture, textureMapType, opaque);
            return info;
        }

        public void ExportTextureTransform(GLTF.Schema.TextureInfo def, Material mat, string texName)
        {
            Vector2 offset = mat.GetTextureOffset(texName);
            Vector2 scale = mat.GetTextureScale(texName);

            if (offset == Vector2.zero && scale == Vector2.one) return;

            if (_root.ExtensionsUsed == null)
            {
                _root.ExtensionsUsed = new List<string>(
                    new[] { GLTF.Schema.ExtTextureTransformExtensionFactory.EXTENSION_NAME }
                );
            }
            else if (!_root.ExtensionsUsed.Contains(GLTF.Schema.ExtTextureTransformExtensionFactory.EXTENSION_NAME))
            {
                _root.ExtensionsUsed.Add(GLTF.Schema.ExtTextureTransformExtensionFactory.EXTENSION_NAME);
            }

            if (ExporterOptions.RequireExtensions)
            {
                if (_root.ExtensionsRequired == null)
                {
                    _root.ExtensionsRequired = new List<string>(
                        new[] { GLTF.Schema.ExtTextureTransformExtensionFactory.EXTENSION_NAME }
                    );
                }
                else if (!_root.ExtensionsRequired.Contains(GLTF.Schema.ExtTextureTransformExtensionFactory.EXTENSION_NAME))
                {
                    _root.ExtensionsRequired.Add(GLTF.Schema.ExtTextureTransformExtensionFactory.EXTENSION_NAME);
                }
            }

            if (def.Extensions == null)
                def.Extensions = new Dictionary<string, GLTF.Schema.IExtension>();

            def.Extensions[GLTF.Schema.ExtTextureTransformExtensionFactory.EXTENSION_NAME] = new GLTF.Schema.ExtTextureTransformExtension(
                new GLTF.Math.Vector2(offset.x, -offset.y),
                new GLTF.Math.Vector2(scale.x, scale.y),
                0 // TODO: support UV channels
            );
        }

        public GLTF.Schema.NormalTextureInfo ExportNormalTextureInfo(
            Texture texture,
            TextureMapType textureMapType,
            Material material)
        {
            var info = new GLTF.Schema.NormalTextureInfo();
            info.Index = ExportTexture(texture, textureMapType, true);
            if (material.HasProperty("_BumpScale"))
            {
                info.Scale = material.GetFloat("_BumpScale");
            }
            return info;
        }

        public GLTF.Schema.OcclusionTextureInfo ExportOcclusionTextureInfo(
            Texture texture,
            TextureMapType textureMapType,
            Material material)
        {
            var info = new GLTF.Schema.OcclusionTextureInfo();

            info.Index = ExportTexture(texture, textureMapType, true);

            if (material.HasProperty("_OcclusionStrength"))
            {
                info.Strength = material.GetFloat("_OcclusionStrength");
            }

            return info;
        }

        public GLTF.Schema.PbrMetallicRoughness ExportPBRMetallicRoughness(Material material, bool opaque)
        {
            var pbr = new GLTF.Schema.PbrMetallicRoughness();

            if (material.HasProperty("_Color"))
            {
                pbr.BaseColorFactor = material.GetColor("_Color").ToNumericsColorRaw(true);
            }

            if (material.HasProperty("_MainTex"))
            {
                var mainTex = material.GetTexture("_MainTex");

                if (mainTex != null)
                {
                    pbr.BaseColorTexture = ExportTextureInfo(mainTex, TextureMapType.Main, opaque);
                    ExportTextureTransform(pbr.BaseColorTexture, material, "_MainTex");
                }
            }

            if (material.HasProperty("_Metallic"))
            {
                var metallicGlossMap = material.GetTexture("_MetallicGlossMap");
                pbr.MetallicFactor = (metallicGlossMap != null) ? 1.0 : material.GetFloat("_Metallic");
            }

            if (material.HasProperty("_Glossiness"))
            {
                var metallicGlossMap = material.GetTexture("_MetallicGlossMap");
                pbr.RoughnessFactor = (metallicGlossMap != null) ? 1.0 : material.GetFloat("_Glossiness");
            }

            if (material.HasProperty("_MetallicGlossMap"))
            {
                var mrTex = material.GetTexture("_MetallicGlossMap");

                if (mrTex != null)
                {
                    pbr.MetallicRoughnessTexture = ExportTextureInfo(mrTex, TextureMapType.MetallicGloss, true);
                    ExportTextureTransform(pbr.MetallicRoughnessTexture, material, "_MetallicGlossMap");
                }
            }
            else if (material.HasProperty("_SpecGlossMap"))
            {
                var mgTex = material.GetTexture("_SpecGlossMap");

                if (mgTex != null)
                {
                    pbr.MetallicRoughnessTexture = ExportTextureInfo(mgTex, TextureMapType.SpecGloss, true);
                    ExportTextureTransform(pbr.MetallicRoughnessTexture, material, "_SpecGlossMap");
                }
            }

            return pbr;
        }

        public GLTF.Schema.MaterialCommonConstant ExportCommonConstant(Material materialObj)
        {
            if (_root.ExtensionsUsed == null)
            {
                _root.ExtensionsUsed = new List<string>(new[] { "KHR_materials_common" });
            }
            else if (!_root.ExtensionsUsed.Contains("KHR_materials_common"))
            {
                _root.ExtensionsUsed.Add("KHR_materials_common");
            }

            if (ExporterOptions.RequireExtensions)
            {
                if (_root.ExtensionsRequired == null)
                {
                    _root.ExtensionsRequired = new List<string>(new[] { "KHR_materials_common" });
                }
                else if (!_root.ExtensionsRequired.Contains("KHR_materials_common"))
                {
                    _root.ExtensionsRequired.Add("KHR_materials_common");
                }
            }

            var constant = new GLTF.Schema.MaterialCommonConstant();

            if (materialObj.HasProperty("_AmbientFactor"))
            {
                constant.AmbientFactor = materialObj.GetColor("_AmbientFactor").ToNumericsColorRaw(true);
            }

            if (materialObj.HasProperty("_LightMap"))
            {
                var lmTex = materialObj.GetTexture("_LightMap");

                if (lmTex != null)
                {
                    constant.LightmapTexture = ExportTextureInfo(lmTex, TextureMapType.Light, true);
                    ExportTextureTransform(constant.LightmapTexture, materialObj, "_LightMap");
                }

            }

            if (materialObj.HasProperty("_LightFactor"))
            {
                constant.LightmapFactor = materialObj.GetColor("_LightFactor").ToNumericsColorRaw(true);
            }

            return constant;
        }

        protected GLTF.Schema.TextureId ExportTexture(Texture textureObj, TextureMapType textureMapType, bool opaque)
        {
            GLTF.Schema.TextureId id = this.exporter.GetTextureId(_root, textureObj);
            if (id != null)
            {
                return id;
            }

            var texture = new GLTF.Schema.GLTFTexture();

            //If texture name not set give it a unique name using count
            if (textureObj.name == "")
            {
                textureObj.name = (_root.Textures.Count + 1).ToString();
            }
            texture.Name = textureObj.name;

            texture.Source = this.exporter.imageExporter.Export(textureObj, textureMapType, opaque);
            texture.Sampler = this.exporter.samplerExporter.Export(textureObj);

            this.exporter._textures.Add(textureObj);

            id = new GLTF.Schema.TextureId
            {
                Id = _root.Textures.Count,
                Root = _root
            };

            _root.Textures.Add(texture);

            return id;
        }
    }
}

#endif
