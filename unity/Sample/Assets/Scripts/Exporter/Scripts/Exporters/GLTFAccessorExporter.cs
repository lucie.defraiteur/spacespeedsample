﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFAccessorExporter : GLTFExporterBase
    {
        public GLTFAccessorExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.AccessorId Export(int[] arr, bool isIndices = false)
        {
            var _bufferWriter = this.exporter._bufferWriter;

            var count = arr.Length;

            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.SCALAR;

            int min = arr[0];
            int max = arr[0];

            for (var i = 1; i < count; i++)
            {
                var cur = arr[i];

                if (cur < min)
                {
                    min = cur;
                }
                if (cur > max)
                {
                    max = cur;
                }
            }

            var byteOffset = _bufferWriter.BaseStream.Position;

            if (max <= byte.MaxValue && min >= byte.MinValue)
            {
                accessor.ComponentType = GLTF.Schema.GLTFComponentType.UnsignedByte;

                foreach (var v in arr)
                {
                    _bufferWriter.Write((byte)v);
                }
            }
            else if (max <= sbyte.MaxValue && min >= sbyte.MinValue && !isIndices)
            {
                accessor.ComponentType = GLTF.Schema.GLTFComponentType.Byte;

                foreach (var v in arr)
                {
                    _bufferWriter.Write((sbyte)v);
                }
            }
            else if (max <= short.MaxValue && min >= short.MinValue && !isIndices)
            {
                accessor.ComponentType = GLTF.Schema.GLTFComponentType.Short;

                foreach (var v in arr)
                {
                    _bufferWriter.Write((short)v);
                }
            }
            else if (max <= ushort.MaxValue && min >= ushort.MinValue)
            {
                accessor.ComponentType = GLTF.Schema.GLTFComponentType.UnsignedShort;

                foreach (var v in arr)
                {
                    _bufferWriter.Write((ushort)v);
                }
            }
            else if (min >= uint.MinValue)
            {
                accessor.ComponentType = GLTF.Schema.GLTFComponentType.UnsignedInt;

                foreach (var v in arr)
                {
                    _bufferWriter.Write((uint)v);
                }
            }
            else
            {
                accessor.ComponentType = GLTF.Schema.GLTFComponentType.Float;

                foreach (var v in arr)
                {
                    _bufferWriter.Write((float)v);
                }
            }

            accessor.Min = new List<double> { min };
            accessor.Max = new List<double> { max };

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;

            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        public GLTF.Schema.AccessorId Export(IEnumerable<float> arr)
        {
            var _bufferWriter = this.exporter._bufferWriter;

            var count = arr.Count();
            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.SCALAR;

            float min = arr.Min();
            float max = arr.Max();

            var byteOffset = _bufferWriter.BaseStream.Position;
            accessor.ComponentType = GLTF.Schema.GLTFComponentType.Float;
            foreach (var v in arr)
            {
                _bufferWriter.Write((float)v);
            }

            accessor.Min = new List<double> { min };
            accessor.Max = new List<double> { max };

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;
            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        public GLTF.Schema.AccessorId Export(IEnumerable<Vector2> arr)
        {
            var _bufferWriter = this.exporter._bufferWriter;
            var count = arr.Count();

            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.ComponentType = GLTF.Schema.GLTFComponentType.Float;
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.VEC2;

            accessor.Min = new List<double> { arr.Select(value => value.x).Min(), arr.Select(value => value.y).Min() };
            accessor.Max = new List<double> { arr.Select(value => value.x).Max(), arr.Select(value => value.y).Max() };

            var byteOffset = _bufferWriter.BaseStream.Position;

            foreach (var vec in arr)
            {
                _bufferWriter.Write(vec.x);
                _bufferWriter.Write(vec.y);
            }

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;

            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        public GLTF.Schema.AccessorId Export(IEnumerable<Vector3> arr)
        {
            var _bufferWriter = this.exporter._bufferWriter;
            var count = arr.Count();

            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.ComponentType = GLTF.Schema.GLTFComponentType.Float;
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.VEC3;

            accessor.Min = new List<double> { arr.Select(value => value.x).Min(), arr.Select(value => value.y).Min(), arr.Select(value => value.z).Min() };
            accessor.Max = new List<double> { arr.Select(value => value.x).Max(), arr.Select(value => value.y).Max(), arr.Select(value => value.z).Max() };

            var byteOffset = _bufferWriter.BaseStream.Position;

            foreach (var vec in arr)
            {
                _bufferWriter.Write(vec.x);
                _bufferWriter.Write(vec.y);
                _bufferWriter.Write(vec.z);
            }

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;

            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        public GLTF.Schema.AccessorId Export(IEnumerable<Quaternion> arr)
        {
            var toV4 = arr.Select(q => new Vector4(q.x, q.y, q.z, q.w));
            return this.Export(toV4);
        }

        public GLTF.Schema.AccessorId Export(IEnumerable<Vector4> arr)
        {
            var _bufferWriter = this.exporter._bufferWriter;
            var count = arr.Count();

            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.ComponentType = GLTF.Schema.GLTFComponentType.Float;
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.VEC4;

            accessor.Min = new List<double> { arr.Select(value => value.x).Min(), arr.Select(value => value.y).Min(), arr.Select(value => value.z).Min(), arr.Select(value => value.w).Min() };
            accessor.Max = new List<double> { arr.Select(value => value.x).Max(), arr.Select(value => value.y).Max(), arr.Select(value => value.z).Max(), arr.Select(value => value.w).Max() };

            var byteOffset = _bufferWriter.BaseStream.Position;

            foreach (var vec in arr)
            {
                _bufferWriter.Write(vec.x);
                _bufferWriter.Write(vec.y);
                _bufferWriter.Write(vec.z);
                _bufferWriter.Write(vec.w);
            }

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;

            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        public GLTF.Schema.AccessorId Export(IEnumerable<Matrix4x4> arr)
        {
            var _bufferWriter = this.exporter._bufferWriter;
            var count = arr.Count();

            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.ComponentType = GLTF.Schema.GLTFComponentType.Float;
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.MAT4;

            var byteOffset = _bufferWriter.BaseStream.Position;

            foreach (var value in arr)
            {
                _bufferWriter.Write(value.m00);
                _bufferWriter.Write(value.m10);
                _bufferWriter.Write(value.m20);
                _bufferWriter.Write(value.m30);
                _bufferWriter.Write(value.m01);
                _bufferWriter.Write(value.m11);
                _bufferWriter.Write(value.m21);
                _bufferWriter.Write(value.m31);
                _bufferWriter.Write(value.m02);
                _bufferWriter.Write(value.m12);
                _bufferWriter.Write(value.m22);
                _bufferWriter.Write(value.m32);
                _bufferWriter.Write(value.m03);
                _bufferWriter.Write(value.m13);
                _bufferWriter.Write(value.m23);
                _bufferWriter.Write(value.m33);
            }

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;
            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        public GLTF.Schema.AccessorId Export(IEnumerable<UShort4> arr)
        {
            var _bufferWriter = this.exporter._bufferWriter;
            var count = arr.Count();

            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.ComponentType = GLTF.Schema.GLTFComponentType.UnsignedShort;
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.VEC4;

            var byteOffset = _bufferWriter.BaseStream.Position;

            foreach (var value in arr)
            {
                _bufferWriter.Write(value.x);
                _bufferWriter.Write(value.y);
                _bufferWriter.Write(value.z);
                _bufferWriter.Write(value.w);
            }

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;
            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        public GLTF.Schema.AccessorId Export(Color[] arr)
        {
            var _bufferWriter = this.exporter._bufferWriter;
            var count = arr.Length;

            if (count == 0)
            {
                throw new Exception("Accessors can not have a count of 0.");
            }

            var accessor = new GLTF.Schema.Accessor();
            accessor.ComponentType = GLTF.Schema.GLTFComponentType.Float;
            accessor.Count = count;
            accessor.Type = GLTF.Schema.GLTFAccessorAttributeType.VEC4;

            float minR = arr[0].r;
            float minG = arr[0].g;
            float minB = arr[0].b;
            float minA = arr[0].a;
            float maxR = arr[0].r;
            float maxG = arr[0].g;
            float maxB = arr[0].b;
            float maxA = arr[0].a;

            for (var i = 1; i < count; i++)
            {
                var cur = arr[i];

                if (cur.r < minR)
                {
                    minR = cur.r;
                }
                if (cur.g < minG)
                {
                    minG = cur.g;
                }
                if (cur.b < minB)
                {
                    minB = cur.b;
                }
                if (cur.a < minA)
                {
                    minA = cur.a;
                }
                if (cur.r > maxR)
                {
                    maxR = cur.r;
                }
                if (cur.g > maxG)
                {
                    maxG = cur.g;
                }
                if (cur.b > maxB)
                {
                    maxB = cur.b;
                }
                if (cur.a > maxA)
                {
                    maxA = cur.a;
                }
            }

            accessor.Min = new List<double> { minR, minG, minB, minA };
            accessor.Max = new List<double> { maxR, maxG, maxB, maxA };

            var byteOffset = _bufferWriter.BaseStream.Position;

            foreach (var color in arr)
            {
                _bufferWriter.Write(color.r);
                _bufferWriter.Write(color.g);
                _bufferWriter.Write(color.b);
                _bufferWriter.Write(color.a);
            }

            var byteLength = _bufferWriter.BaseStream.Position - byteOffset;

            accessor.BufferView = ExportBufferView((int)byteOffset, (int)byteLength);

            var id = new GLTF.Schema.AccessorId
            {
                Id = _root.Accessors.Count,
                Root = _root
            };
            _root.Accessors.Add(accessor);

            return id;
        }

        private GLTF.Schema.BufferViewId ExportBufferView(int byteOffset, int byteLength)
        {
            // var _bufferWriter = this.exporter._bufferWriter;
            var _bufferId = this.exporter._bufferId;
            var bufferView = new GLTF.Schema.BufferView
            {
                Buffer = _bufferId,
                ByteOffset = byteOffset,
                ByteLength = byteLength
            };

            var id = new GLTF.Schema.BufferViewId
            {
                Id = _root.BufferViews.Count,
                Root = _root
            };

            _root.BufferViews.Add(bufferView);

            return id;
        }

    }
}

#endif