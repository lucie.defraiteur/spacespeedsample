﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace UnityExporter
{
    class GLTFImageExporter : GLTFExporterBase
    {
        protected Material _metalGlossChannelSwapMaterial;
        protected Material _normalChannelMaterial;
        protected Material _lightmapChannelMaterial;

        public GLTFImageExporter(GLTFExporter exporter) : base(exporter)
        {
            var metalGlossChannelSwapShader = Resources.Load("MetalGlossChannelSwap", typeof(Shader)) as Shader;
            _metalGlossChannelSwapMaterial = new Material(metalGlossChannelSwapShader);

            var normalChannelShader = Resources.Load("NormalChannel", typeof(Shader)) as Shader;
            _normalChannelMaterial = new Material(normalChannelShader);

            var lightmapChannelShader = Resources.Load("LightmapChannel", typeof(Shader)) as Shader;
            _lightmapChannelMaterial = new Material(lightmapChannelShader);
        }

        public GLTF.Schema.ImageId Export(Texture texture, TextureMapType texturMapType, bool opaque)
        {
            GLTF.Schema.ImageId id = this.exporter.GetImageId(_root, texture);
            if (id != null)
            {
                return id;
            }

            var jpeg = (opaque && ExporterOptions.JpgEncoding);
            var image = new GLTF.Schema.GLTFImage();
            image.Name = texture.name;

            this.exporter._imageInfos.Add(new ImageInfo
            {
                texture = texture as Texture2D,
                textureMapType = texturMapType,
                jpg = jpeg
            });

            var imagePath = this.exporter.RetrieveTexturePath(texture);
            var filenamePath = Path.ChangeExtension(imagePath, jpeg ? ".jpg" : ".png");
            if (!ExporterOptions.ExportFullPath)
            {
                filenamePath = Path.ChangeExtension(texture.name, jpeg ? ".jpg" : ".png");
            }
            image.Uri = Uri.EscapeUriString(filenamePath);

            id = new GLTF.Schema.ImageId
            {
                Id = _root.Images.Count,
                Root = _root
            };

            _root.Images.Add(image);

            return id;
        }


        public void Export(string outputPath, List<ImageInfo> imageInfos)
        {
            for (int t = 0; t < imageInfos.Count; ++t)
            {
                var image = imageInfos[t].texture;
                int height = image.height;
                int width = image.width;
                var jpeg = imageInfos[t].jpg;

                switch (imageInfos[t].textureMapType)
                {
                    case TextureMapType.MetallicGloss:
                        ExportMetallicGlossTexture(image, outputPath, jpeg);
                        break;
                    case TextureMapType.Bump:
                        ExportNormalTexture(image, outputPath, jpeg);
                        break;
                    case TextureMapType.Light:
                        ExportLightTexture(image, outputPath, jpeg);
                        break;
                    default:
                        ExportTexture(image, outputPath, jpeg);
                        break;
                }
            }
        }

        /// <summary>
        /// This converts Unity's metallic-gloss texture representation into GLTF's metallic-roughness specifications. 
        /// Unity's metallic-gloss A channel (glossiness) is inverted and goes into GLTF's metallic-roughness G channel (roughness).
        /// Unity's metallic-gloss R channel (metallic) goes into GLTF's metallic-roughess B channel.
        /// </summary>
        /// <param name="texture">Unity's metallic-gloss texture to be exported</param>
        /// <param name="outputPath">The location to export the texture</param>
        private void ExportMetallicGlossTexture(Texture2D texture, string outputPath, bool jpeg)
        {
            var destRenderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
            Graphics.Blit(texture, destRenderTexture, _metalGlossChannelSwapMaterial);

            this.WriteImage(texture, destRenderTexture, outputPath, jpeg);
        }

        /// <summary>
        /// This export's the normal texture. If a texture is marked as a normal map, the values are stored in the A and G channel.
        /// To output the correct normal texture, the A channel is put into the R channel.
        /// </summary>
        /// <param name="texture">Unity's normal texture to be exported</param>
        /// <param name="outputPath">The location to export the texture</param>
        private void ExportNormalTexture(Texture2D texture, string outputPath, bool jpeg)
        {
            var destRenderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);
            Graphics.Blit(texture, destRenderTexture, _normalChannelMaterial);

            this.WriteImage(texture, destRenderTexture, outputPath, jpeg);
        }

        private void ExportLightTexture(Texture2D texture, string outputPath, bool jpeg)
        {
            var destRenderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB);
            Graphics.Blit(texture, destRenderTexture, _lightmapChannelMaterial);
            //Graphics.Blit(texture, destRenderTexture);

            this.WriteImage(texture, destRenderTexture, outputPath, jpeg);
        }

        private void ExportTexture(Texture2D texture, string outputPath, bool jpeg)
        {
            var destRenderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB);
            Graphics.Blit(texture, destRenderTexture);

            this.WriteImage(texture, destRenderTexture, outputPath, jpeg);
        }

        private void WriteImage(Texture2D texture, RenderTexture destRenderTexture, string outputPath, bool jpeg)
        {
            var texturePath = AssetDatabase.GetAssetPath(texture);
            var textureExt = Path.GetExtension(texturePath);
            // Debug.Log("Exporting " + texturePath + " / " + textureExt + ", hasAlpha ? " + texture.HasAlpha());

            var exportTexture = new Texture2D(texture.width, texture.height);
            exportTexture.ReadPixels(new Rect(0, 0, destRenderTexture.width, destRenderTexture.height), 0, 0);
            exportTexture.Apply();

            var finalFilenamePath = ConstructImageFilenamePath(texture, outputPath, jpeg);
            if (jpeg)
            {
                File.WriteAllBytes(finalFilenamePath, exportTexture.EncodeToJPG(ExporterOptions.JpgQuality));
            }
            else
            {
                File.WriteAllBytes(finalFilenamePath, exportTexture.EncodeToPNG());
            }

            RenderTexture.active = null;
            destRenderTexture.Release();
            if (Application.isEditor)
            {
                GameObject.DestroyImmediate(exportTexture);
            }
            else
            {
                GameObject.Destroy(exportTexture);
            }
        }

        private string ConstructImageFilenamePath(Texture2D texture, string outputPath, bool jpeg)
        {
            var imagePath = this.exporter.RetrieveTexturePath(texture);
            var filenamePath = Path.Combine(outputPath, imagePath);
            if (!ExporterOptions.ExportFullPath)
            {
                filenamePath = outputPath + "/" + texture.name;
            }

            var file = new FileInfo(filenamePath);
            file.Directory.Create();
            return (jpeg ? Path.ChangeExtension(filenamePath, ".jpg") : Path.ChangeExtension(filenamePath, ".png"));
        }
    }
}

#endif