﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFSceneExporter : GLTFExporterBase
    {
        public GLTFSceneExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.SceneId Export(string name, Transform[] rootObjTransforms)
        {
            var scene = new GLTF.Schema.GLTFScene();
            scene.Name = name;

            scene.Nodes = new List<GLTF.Schema.NodeId>(rootObjTransforms.Length);
            foreach (var transform in rootObjTransforms)
            {
                if (!doExport(transform))
                    continue;

                var node = exporter.nodeExporter.Export(transform);
                scene.Nodes.Add(node);
            }
            exporter._root.Scenes.Add(scene);

            exporter.skinExporter.Export(rootObjTransforms);

            return new GLTF.Schema.SceneId
            {
                Id = exporter._root.Scenes.Count - 1,
                Root = exporter._root
            };
        }
    }
}

#endif