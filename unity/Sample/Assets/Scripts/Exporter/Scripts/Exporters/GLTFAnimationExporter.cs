﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;


namespace UnityExporter
{
    class GLTFAnimationExporter : GLTFExporterBase
    {
        private readonly Dictionary<object, int> _animationCache = new Dictionary<object, int>();
        private readonly AnimationSkeletonDataCache _skeletonDataCache;

        public GLTFAnimationExporter(GLTFExporter exporter) : base(exporter)
        {
            _skeletonDataCache = new AnimationSkeletonDataCache(exporter);
        }

        public void Export(Transform nodeTransform, Animator animator, GLTF.Schema.NodeId nodeId)
        {
            if (!animator.enabled)
            {
                return;
            }

            var clips = this.GetAnimationClips(animator);
            if (clips == null || clips.Count <= 0)
            {
                Debug.LogWarning("No animation clip inside " + animator.name);
                return;
            }


            RuntimeAnimatorController runtimeAnimatorController = animator.runtimeAnimatorController;

            if (animator.isHuman)
            {
                // Debug.LogWarning("Ignoring humanoid animation " + animator.name);
                // Export via skin exporter
                return;
            }

            Debug.Log("Exporting animation " + animator.name + " on " + nodeTransform.name);
            foreach (var clip in clips)
            {
                // Debug.Log("Exporting clip " + clip.name);
                this.ExportAnimationClip(nodeTransform.gameObject, clip, nodeId);
            }
        }

        private List<AnimationClip> GetAnimationClips(Animator animator)
        {
            RuntimeAnimatorController runtimeAnimatorController = animator.runtimeAnimatorController;
            UnityEditor.Animations.AnimatorController animationController = runtimeAnimatorController as UnityEditor.Animations.AnimatorController;

            var clips = new List<AnimationClip>();
            if (animationController == null)
            {
                return clips;
            }

            foreach (var layer in animationController.layers)
            {
                foreach (var state in layer.stateMachine.states)
                {
                    var motion = state.state.motion;
                    if (motion is AnimationClip)
                    {
                        clips.Add(motion as AnimationClip);
                    }
                    else
                    {
                        var childs = GetAnimationClips(motion as BlendTree);
                        clips.AddRange(childs);
                    }
                }
            }
            return clips;
        }

        private List<AnimationClip> GetAnimationClips(BlendTree blendTree)
        {
            var clips = new List<AnimationClip>();
            foreach (var child in blendTree.children)
            {
                var motion = child.motion;
                if (motion is AnimationClip)
                {
                    clips.Add(motion as AnimationClip);
                }
                else
                {
                    clips.AddRange(GetAnimationClips(motion as BlendTree));
                }
            }
            return clips;
        }

        private void ExportAnimationClip(GameObject gameObject, AnimationClip clip, GLTF.Schema.NodeId rootNodeId)
        {
            if (IsAlreadyExported(clip, rootNodeId))
                return; // skip if already exported

            var hasChildren = false;

            List<GLTF.Schema.AnimationChannel> channels = new List<GLTF.Schema.AnimationChannel>();
            List<GLTF.Schema.AnimationSampler> samplers = new List<GLTF.Schema.AnimationSampler>();
            List<AnimationCurveData> curveDatas = new List<AnimationCurveData>();

            // Collect curve data and create channels
            foreach (var binding in AnimationUtility.GetCurveBindings(clip))
            {
                // Debug.Log(clip.name + ": exporting " + binding.propertyName + " / " + binding.path);

                var curve = AnimationUtility.GetEditorCurve(clip, binding);
                var property = this.PropertyToTarget(binding.propertyName);
                if (property == AnimationPropertys.NotImplemented)
                {
                    Debug.LogWarning("Not Implemented keyframe property : " + binding.propertyName);
                    continue;
                }
                if (property == AnimationPropertys.EulerRotation)
                {
                    Debug.LogWarning("Interpolation setting of AnimationClip should be Quaternion");
                    continue;
                }

                var subNodeId = this.FindNodeId(rootNodeId, binding.path);
                if (subNodeId != rootNodeId) hasChildren = true;

                var samplerId = this.AddChannelAndGetSampler(subNodeId, channels, samplers, property);
                var curveData = curveDatas.FirstOrDefault(x => x.SamplerId == samplerId);
                if (curveData == null)
                {
                    curveData = new AnimationCurveData(this.exporter, clip, property, samplerId);
                    curveDatas.Add(curveData);
                }

                curveData.AddCurve(binding.propertyName, curve);
            }

            // Write samplers
            foreach (var curve in curveDatas)
            {
                var samplerIndex = curve.SamplerId.Id;
                var sampler = samplers[samplerIndex];

                curve.ExportSampler(sampler);
            }

            //exported channels and samplers for this animation clip, cache it and attach to GLTF root
            var index = _root.Animations.Count;
            _animationCache.Add(clip, index);
            var animation = new GLTF.Schema.GLTFAnimation()
            {
                Name = clip.name,
                Channels = channels,
                Samplers = samplers
            };
            _root.Animations.Add(animation);

            if (hasChildren)
            {
                var extra = this.exporter.getExtra(animation);
                extra.Add("root", rootNodeId.Id);
            }
        }

        // Find a child node of root
        // rootId: 25, path: Group1/Group21/Cube20
        private GLTF.Schema.NodeId FindNodeId(GLTF.Schema.NodeId rootId, string path)
        {
            if (path == null || path.Length <= 0)
            {
                return rootId;
            }

            var root = this.exporter._root.Nodes[rootId.Id];
            var split = path.Split('/');
            var parent = root;
            GLTF.Schema.Node found = null;
            for (var i = 0; i < split.Length; i++)
            {
                var name = split[i];
                parent = this.FindNodeWithName(parent, name);
                if (parent == null)
                {
                    Debug.LogWarning("Cannot find child with name " + name + " inside " + parent.Name);
                    break;
                }
                else
                {
                    if (i == (split.Length - 1))
                    {
                        found = parent;
                        break;
                    }
                }
            }

            if (found != null)
            {
                return this.exporter.nodeIdsByNode[found];
            }
            return null;
        }

        // Search a child node by name
        private GLTF.Schema.Node FindNodeWithName(GLTF.Schema.Node parent, string name)
        {
            foreach (var childId in parent.Children)
            {
                var node = this.exporter._root.Nodes[childId.Id];
                if (node.Name == name)
                {
                    return node;
                }
            }
            return null;
        }

        private GLTF.Schema.SamplerId AddChannelAndGetSampler(GLTF.Schema.NodeId nodeIndex, List<GLTF.Schema.AnimationChannel> channels, List<GLTF.Schema.AnimationSampler> samplers, AnimationPropertys property)
        {
            // m_LocalRotation.x => rotation
            // m_LocalRotation.y => rotation
            // m_LocalScale.x => scale
            // etc.
            var channelPathName = this.GetChannelPath(property);

            // Find channel
            var channel = channels.FirstOrDefault(x => x.Target.Node == nodeIndex && x.Target.Path == channelPathName);
            if (channel != null)
            {
                return channel.Sampler;
            }

            // Not found, create new
            GLTF.Schema.SamplerId samplerId = new GLTF.Schema.SamplerId()
            {
                Id = samplers.Count,
                Root = _root,
            };

            var sampler = new GLTF.Schema.AnimationSampler();
            samplers.Add(sampler);

            channel = new GLTF.Schema.AnimationChannel
            {
                Sampler = samplerId,
                Target = new GLTF.Schema.AnimationChannelTarget
                {
                    Node = nodeIndex,
                    Path = channelPathName,
                },
            };
            channels.Add(channel);

            return samplerId;
        }

        private AnimationPropertys PropertyToTarget(string property)
        {
            if (property.StartsWith("m_LocalPosition."))
            {
                return AnimationPropertys.Translation;
            }
            else if (property.StartsWith("localEulerAnglesRaw."))
            {
                return AnimationPropertys.EulerRotation;
            }
            else if (property.StartsWith("m_LocalRotation."))
            {
                return AnimationPropertys.Rotation;
            }
            else if (property.StartsWith("m_LocalScale."))
            {
                return AnimationPropertys.Scale;
            }
            else if (property.StartsWith("blendShape."))
            {
                return AnimationPropertys.BlendShape;
            }
            else
            {
                return AnimationPropertys.NotImplemented;
            }
        }



        private GLTF.Schema.GLTFAnimationChannelPath GetChannelPath(AnimationPropertys property)
        {
            switch (property)
            {
                case AnimationPropertys.Translation:
                    return GLTF.Schema.GLTFAnimationChannelPath.translation;
                case AnimationPropertys.EulerRotation:
                case AnimationPropertys.Rotation:
                    return GLTF.Schema.GLTFAnimationChannelPath.rotation;
                case AnimationPropertys.Scale:
                    return GLTF.Schema.GLTFAnimationChannelPath.scale;
                case AnimationPropertys.BlendShape:
                    return GLTF.Schema.GLTFAnimationChannelPath.weights;
                default: throw new NotImplementedException();
            }
        }


        private bool IsAlreadyExported(AnimationClip clip, GLTF.Schema.NodeId rootNodeId)
        {
            int index;
            if (_animationCache.TryGetValue(clip, out index))
            {
                // Debug.Log("Clip already exported: " + clip.name);
                var existing = this.exporter._root.Animations[index];
                var extra = this.exporter.getExtra(existing);
                if (extra.GetValue("othersRoot") == null)
                {
                    extra.Add("othersRoot", new Newtonsoft.Json.Linq.JArray());
                }

                Newtonsoft.Json.Linq.JArray othersRoot = (Newtonsoft.Json.Linq.JArray)extra.GetValue("othersRoot");
                othersRoot.Add(rootNodeId.Id);
                return true; // skip if already exported
            }
            return false;
        }

        public void Export(SkinnedMeshRenderer skinnedMesh, Animator animator)
        {
            if (!animator.enabled)
                return;
            if (!animator.isHuman)
                return;

            var clips = this.GetAnimationClips(animator);
            if (clips == null || clips.Count <= 0)
            {
                Debug.LogWarning("No animation clip inside " + animator.name);
                return;
            }

            var rootNodeId = this.exporter.nodeIdByTransform[animator.transform];

            // Debug.Log("Exporting human animation " + animator.name);
            foreach (var clip in clips)
            {
                this.ExportHumanAnimationClip(skinnedMesh, animator, clip, rootNodeId);
            }
        }

        private void ExportHumanAnimationClip(SkinnedMeshRenderer skinnedMesh, Animator animator, AnimationClip clip, GLTF.Schema.NodeId rootNodeId)
        {
            if (IsAlreadyExported(clip, rootNodeId))
                return; // skip if already exported

            var hasChildren = true;

            List<GLTF.Schema.AnimationChannel> channels = new List<GLTF.Schema.AnimationChannel>();
            List<GLTF.Schema.AnimationSampler> samplers = new List<GLTF.Schema.AnimationSampler>();

            var source = animator.gameObject;
            var settings = AnimationUtility.GetAnimationClipSettings(clip);

            // Collect curve data and create channels
            var i = 0;
            Transform[] bones = skinnedMesh.bones;
            AnimationSkeletonData[] datas = new AnimationSkeletonData[bones.Length];
            foreach (var bone in bones)
            {
                datas[i] = new AnimationSkeletonData(this.exporter, clip, skinnedMesh, bone, this._skeletonDataCache);
                i++;
            }

            // var worldPosition1 = bones[0].transform.position;

            if (!AnimationMode.InAnimationMode())
            {
                AnimationMode.StartAnimationMode();
            }

            AnimationMode.BeginSampling();
            var duration = settings.stopTime - settings.startTime;
            var delta = 1 / clip.frameRate;
            var frames = Mathf.Ceil(duration / delta);
            for (var f = 0; f < frames; f++)
            {
                var frameTime = Math.Min(settings.startTime + f * delta, settings.stopTime);
                clip.SampleAnimation(source, frameTime);
                for (i = 0; i < bones.Length; i++)
                {
                    var data = datas[i];
                    data.SaveSamplingData(frameTime - settings.startTime); // time relative to start
                }
            }

            // reset
            clip.SampleAnimation(source, 0);

            AnimationMode.EndSampling();

            if (AnimationMode.InAnimationMode())
            {
                AnimationMode.StopAnimationMode();
            }

            for (i = 0; i < bones.Length; i++)
            {
                var data = datas[i];
                data.ExportSampler(channels, samplers);
            }

            //exported channels and samplers for this animation clip, cache it and attach to GLTF root
            var index = _root.Animations.Count;
            _animationCache.Add(clip, index);
            var animation = new GLTF.Schema.GLTFAnimation()
            {
                Name = clip.name,
                Channels = channels,
                Samplers = samplers
            };
            _root.Animations.Add(animation);

            if (hasChildren)
            {
                var extra = this.exporter.getExtra(animation);
                extra.Add("root", rootNodeId.Id);
            }
        }
    }

    public enum Interpolations
    {
        LINEAR,
        STEP,
        CUBICSPLINE
    }

    public enum AnimationPropertys
    {
        Translation,
        EulerRotation,
        Rotation,
        Scale,
        Weight,
        BlendShape,
        NotImplemented
    }


}

#endif