﻿#if (UNITY_EDITOR) 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UnityExporter
{
    class GLTFPrimitiveExporter : GLTFExporterBase
    {
        public GLTFPrimitiveExporter(GLTFExporter exporter) : base(exporter)
        {
        }

        public GLTF.Schema.MeshPrimitive[] Export(GameObject gameObject)
        {
            Mesh meshObj;
            Material[] materialsObj;

            var filter = gameObject.GetComponent<MeshFilter>();
            var renderer = gameObject.GetComponent<MeshRenderer>();
            var skinnedMeshRenderer = gameObject.GetComponent<SkinnedMeshRenderer>();
            if (skinnedMeshRenderer != null)
            {
                meshObj = skinnedMeshRenderer.sharedMesh;
                materialsObj = skinnedMeshRenderer.sharedMaterials;
            }
            else
            {
                meshObj = filter.sharedMesh;
                materialsObj = renderer.sharedMaterials;
            }

            var prims = new GLTF.Schema.MeshPrimitive[meshObj.subMeshCount];

            // don't export any more accessors if this mesh is already exported
            GLTF.Schema.MeshPrimitive[] primVariations;
            if (this.exporter.meshToPrims.TryGetValue(meshObj, out primVariations)
                && meshObj.subMeshCount == primVariations.Length)
            {
                for (var i = 0; i < primVariations.Length; i++)
                {
                    prims[i] = new GLTF.Schema.MeshPrimitive(primVariations[i], _root)
                    {
                        Material = this.exporter.materialExporter.Export(materialsObj[i])
                    };
                }

                return prims;
            }

            GLTF.Schema.AccessorId aPosition = null, aNormal = null, aTangent = null,
                aTexcoord0 = null, aTexcoord1 = null, aColor0 = null, aWeights0 = null, aJoints0 = null;

            aPosition = ExportAccessor(SchemaExtensions.ConvertVector3CoordinateSpaceAndCopy(meshObj.vertices, SchemaExtensions.CoordinateSpaceConversionScale));

            if (meshObj.normals.Length != 0 && ExporterOptions.ExporMeshNormal)
                aNormal = ExportAccessor(SchemaExtensions.ConvertVector3CoordinateSpaceAndCopy(meshObj.normals, SchemaExtensions.CoordinateSpaceConversionScale));

            if (meshObj.tangents.Length != 0 && ExporterOptions.ExporMeshTangent)
                aTangent = ExportAccessor(SchemaExtensions.ConvertVector4CoordinateSpaceAndCopy(meshObj.tangents, SchemaExtensions.TangentSpaceConversionScale));

            if (meshObj.uv.Length != 0)
                aTexcoord0 = ExportAccessor(SchemaExtensions.FlipTexCoordArrayVAndCopy(meshObj.uv));

            if (meshObj.uv2.Length != 0 && ExporterOptions.ExporMeshUv2)
                aTexcoord1 = ExportAccessor(SchemaExtensions.FlipTexCoordArrayVAndCopy(meshObj.uv2));

            if (meshObj.colors.Length != 0)
                aColor0 = ExportAccessor(meshObj.colors);

            if (meshObj.boneWeights != null && meshObj.boneWeights.Any())
            {
                // var weights = meshObj.boneWeights.Select(y => new Vector4(y.weight0, y.weight1, y.weight2, y.weight3));
                var weights = meshObj.boneWeights.Select(y => new Vector4(0, 0, 0, 0));
                var joints = meshObj.boneWeights.Select(y => new UShort4((ushort)y.boneIndex0, (ushort)y.boneIndex1, (ushort)y.boneIndex2, (ushort)y.boneIndex3));
                aWeights0 = this.exporter.accessorExporter.Export(weights);
                aJoints0 = this.exporter.accessorExporter.Export(joints);
                // var i = 0;
                // foreach (var bonew in meshObj.boneWeights)
                // {
                //     var sum = (bonew.weight0 + bonew.weight1 + bonew.weight2 + bonew.weight3);
                //     if (sum > 1.01 || sum < 0.99)
                //     {
                //         Debug.Log("boneWeight" + i + " => sum=" + sum);
                //     }
                //     i++;
                // }
            }

            GLTF.Schema.MaterialId lastMaterialId = null;

            for (var submesh = 0; submesh < meshObj.subMeshCount; submesh++)
            {
                var primitive = new GLTF.Schema.MeshPrimitive();

                var triangles = meshObj.GetTriangles(submesh);
                primitive.Indices = ExportAccessor(SchemaExtensions.FlipFacesAndCopy(triangles), true);

                primitive.Attributes = new Dictionary<string, GLTF.Schema.AccessorId>();
                primitive.Attributes.Add(GLTF.Schema.SemanticProperties.POSITION, aPosition);

                if (aNormal != null)
                    primitive.Attributes.Add(GLTF.Schema.SemanticProperties.NORMAL, aNormal);
                if (aTangent != null)
                    primitive.Attributes.Add(GLTF.Schema.SemanticProperties.TANGENT, aTangent);
                if (aTexcoord0 != null)
                    primitive.Attributes.Add(GLTF.Schema.SemanticProperties.TexCoord(0), aTexcoord0);
                if (aTexcoord1 != null)
                    primitive.Attributes.Add(GLTF.Schema.SemanticProperties.TexCoord(1), aTexcoord1);
                if (aColor0 != null)
                    primitive.Attributes.Add(GLTF.Schema.SemanticProperties.Color(0), aColor0);
                if (aWeights0 != null)
                    primitive.Attributes.Add(GLTF.Schema.SemanticProperties.Weight(0), aWeights0);
                if (aJoints0 != null)
                    primitive.Attributes.Add(GLTF.Schema.SemanticProperties.Joint(0), aJoints0);

                if (submesh < materialsObj.Length)
                {
                    primitive.Material = this.exporter.materialExporter.Export(materialsObj[submesh]);
                    lastMaterialId = primitive.Material;
                }
                else
                {
                    primitive.Material = lastMaterialId;
                }

                prims[submesh] = primitive;
            }

            this.exporter.meshToPrims[meshObj] = prims;

            return prims;
        }


        private GLTF.Schema.AccessorId ExportAccessor(int[] arr, bool isIndices)
        {
            return this.exporter.accessorExporter.Export(arr, isIndices);
        }
        private GLTF.Schema.AccessorId ExportAccessor(Vector2[] arr)
        {
            return this.exporter.accessorExporter.Export(arr);
        }
        private GLTF.Schema.AccessorId ExportAccessor(Vector3[] arr)
        {
            return this.exporter.accessorExporter.Export(arr);
        }
        private GLTF.Schema.AccessorId ExportAccessor(Vector4[] arr)
        {
            return this.exporter.accessorExporter.Export(arr);
        }
        private GLTF.Schema.AccessorId ExportAccessor(Color[] arr)
        {
            return this.exporter.accessorExporter.Export(arr);
        }
    }
}

#endif