// fbx2obj.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "assimploader.h"

int main(int argc, char** argv)
{
	AssimpLoader loader;

	if (argc < 3)
	{
		loader.Load("model.fbx");
		loader.Save("model");
	}
	else
	{
		loader.Load(argv[1]);
		loader.Save(argv[2]);
	}

	getchar();

    return 0;
}

