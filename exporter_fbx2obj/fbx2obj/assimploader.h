#pragma once

#include <string>
#include <vector>
#include <Assimp/Importer.hpp>
#include <Assimp/scene.h>
#include <Assimp/postprocess.h>

struct Vector3
{
	float x;
	float y;
	float z;

	Vector3()
	{
		x = 0.0f, y = 0.0f, z = 0.0f;
	}

	Vector3(float _x, float _y, float _z)
	{
		x = _x; y = _y; z = _z;
	}
};

struct Vector2
{
	float u;
	float v;

	Vector2()
	{
		u = 0.0f; v = 0.0f;
	}

	Vector2(float _u, float _v)
	{
		u = _u; v = _v;
	}
};

struct Triangle
{
	unsigned int a;
	unsigned int b;
	unsigned int c;

	Triangle(unsigned int _a, unsigned int _b, unsigned int _c)
	{
		a = _a;
		b = _b;
		c = _c;
	}
};

struct Mesh
{
	std::string name;
	std::vector<Vector3> vertices;
	std::vector<Vector2> uv;
	std::vector<Vector3> normals;
	std::vector<Triangle> indices;
	unsigned int materialID;

	Mesh()
	{

	}
};

struct Material
{
	std::string name;
	std::string texture;
	Vector3 ambient;
	Vector3 diffuse;
	Vector3 specular;
	Vector3 emissive;
	float shininess;

	Material()
	{

	}
};

struct Model
{
	std::vector<Material*> materials;
	std::vector<Mesh*> meshes;

	~Model()
	{
		for (Material* m : materials)
		{
			delete m;
		}

		for (Mesh* m : meshes)
		{
			delete m;
		}
	}
};

class AssimpLoader
{
private:
	Model* result;
	int materialCounter;
	int meshCounter;

	Mesh* InitMesh(const aiMesh* mesh, float factor);
	Material* InitMaterial(const aiMaterial* mat);
	void LoadFromScene(const std::string& path, const aiScene* scene, float factor);
	void SaveObj(const std::string& filename, const std::string& filenameMat);
	void SaveMtl(const std::string& filename);

public:
	AssimpLoader() { result = nullptr; materialCounter = 0; meshCounter = 0; }
	~AssimpLoader() { if (result) delete result; }
	void Load(const std::string& filename);
	void Save(const std::string& filename);
};