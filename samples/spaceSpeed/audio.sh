function reduceBitRate {
	for audio in *.mp3 ; do
		echo "======== Reducing bitrate of ${audio} ========"
		cp "$audio" "$audio-back"
		ffmpeg -y -i "$audio-back" -codec:a libmp3lame -qscale:a 8 "${audio}"
		rm "$audio-back"
	done
}

audiospritler --log debug --export mp3,ogg --output game/assets/sounds/sounds assetsSource/audio/sounds/*
#audiospritler --log debug --export mp3,ogg --output game/assets/sounds/music1 assetsSource/audio/music1/*
#audiospritler --log debug --export mp3,ogg --output game/assets/sounds/music2 assetsSource/audio/music2/*
##

cd game/assets/sounds
reduceBitRate
cd ../../..
