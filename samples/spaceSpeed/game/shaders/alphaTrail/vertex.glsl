/**
 * Example Vertex Shader
 * Sets the position of the vertex by setting gl_Position
 */

// Set the precision for data types used in this shader
precision highp float;
precision highp int;

// Default THREE.js uniforms available to both fragment and vertex shader
uniform mat4 modelMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat3 normalMatrix;
uniform vec3 boundingBox;
uniform vec3 nbRows;
// Default uniforms provided by ShaderFrog.
uniform vec3 cameraPosition;
uniform float time;
uniform sampler2D texture1;

// Default attributes provided by THREE.js. Attributes are only available in the
// vertex shader. You can pass them to the fragment shader using varyings
attribute vec3 position;
attribute vec3 normal;
attribute vec2 uv;
attribute vec2 uv2;

// Examples of variables passed from vertex to fragment shader
varying vec3 vPosition;
varying vec3 vNormal;
varying vec2 vUv;
varying vec2 vUv2;
varying vec4 vColor;

float truc(float nb, float modulo) {

  int floored = int(nb / modulo);
  ;
  float res = nb - (float(floored) * modulo);
  // res = 0.0;
  return (res);
}

float GetDistanceFromGrid(float position, float gridSize) {
  float mod1 = truc(position, gridSize);
  float res = mod1;
  float mod2 = abs((gridSize - mod1));
  if (abs(mod1) > mod2) {
    res = mod2;
  }
  return (res);
}

float GetDistanceFromGrid(vec3 position, vec3 gridSizes) {

  float res = 0.0;
  float x = GetDistanceFromGrid(position.x, gridSizes.x);
  float y = GetDistanceFromGrid(position.y, gridSizes.y);
  float z = GetDistanceFromGrid(position.z, gridSizes.z);
  vec3 resVec = vec3(x, y, z);
  float resLen = length(resVec);
  // vec3(GetDistanceFromGrid(position.x, gridSizes.x),
  // GetDistanceFromGrid(position.y, gridSizes.y),
  // GetDistanceFromGrid(position.z, gridSizes.z)).length;
  return (resLen);
}

void main() {

  // To pass variables to the fragment shader, you assign them here in the
  // main function. Traditionally you name the varying with vAttributeName
  vNormal = normal;
  vUv = uv;
  vUv2 = uv2;
  vPosition = position;
  vec4 worldPosition = modelViewMatrix * vec4(position, 1.0);
  float dist = GetDistanceFromGrid(worldPosition.xyz, vec3(0.5, 0.5, 0.5));
  /*if (dist < 0.5)
  {
          vColor = vec4(0.8, 0.8, 0.6, 0.85);
  }
  else
  {
          vColor = vec4(0, 0, 0, 0);
  }*/
  // This sets the position of the vertex in 3d space. The correct math is
  // provided below to take into account camera and object data.
  // gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}