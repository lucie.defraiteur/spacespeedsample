/**
 * Example Fragment Shader
 * Sets the color and alpha of the pixel by setting gl_FragColor
 */
#define M_PI 3.1415926535897932384626433832795
// Set the precision for data types used in this shader
precision highp float;
precision highp int;

// Default THREE.js uniforms available to both fragment and vertex shader
uniform vec4 mColor;
uniform float intensity;



void main() {

  gl_FragColor = vec4(mColor.xyz, 0.4 * intensity);
}