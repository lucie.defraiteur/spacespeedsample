module GAME {

    export class Gamepad {

        private _size = 200;

        private ctn: plume.Container;
        private visual1: plume.Graphics;
        private visual2: plume.Graphics;

        private mouse: plume.MouseHandler;
        private pointerId = -1;
        private acquired = false;

        private initialDownX = 0;
        private initialDownY = 0;

        private downX = 0;
        private downY = 0;

        private _x = 0;
        private _y = 0;

        private _enabled = true;
        visualFeedback = true;


        // container is full screen
        constructor(visualFeedback: boolean = true, private container: plume.Container) {
            let self = this;
            this.visualFeedback = visualFeedback;

            let bounds = getGame().bounds;
            this.mouse = getGame().inputManager.mouse;

            if (this.visualFeedback) {
                this.rebuildVisual();
            }
        }

        rebuildVisual() {
            if (this.ctn) {
                this.ctn.removeSelf();
            }

            let mainr = this._size;
            let indicatorr = 10;
            this.ctn = new plume.Container(mainr * 2, mainr * 2);
            this.ctn.visible = false;
            this.container.addChild(this.ctn);
            this.visual1 = new plume.Graphics(mainr * 2, mainr * 2);
            this.visual1.beginStroke(3, 0x48bed6);
            this.visual1.drawCircle(mainr, mainr, mainr);
            this.visual1.endStroke();
            this.ctn.addChild(this.visual1);
            this.visual2 = new plume.Graphics(indicatorr * 2, indicatorr * 2);
            this.visual2.beginStroke(3, 0x66e5ff);
            this.visual2.drawCircle(0, 0, indicatorr);
            this.visual2.endStroke();
            this.ctn.addChild(this.visual2);

        }

        update(dt: number) {

            if (!this._enabled) return;

            if (this.acquired) {
                let pointer = this.mouse.pointers[this.pointerId];
                if (pointer.isJustUp) {
                    this.release(pointer);
                } else {
                    this.updateActivePointer(pointer);
                }
            }

            this.tryAcquire();
        }

        private tryAcquire() {
            for (let i = 0; i < this.mouse.pointers.length; i++) {
                let pointer = this.mouse.pointers[i];
                if (pointer.isJustDown) {
                    this.pointerId = i;
                    this.acquire(pointer);
                }
            }
        }

        private tryAcquireOnStart() {
            for (let i = 0; i < this.mouse.pointers.length; i++) {
                let pointer = this.mouse.pointers[i];
                if (pointer.isJustDown || pointer.isDown) {
                    this.pointerId = i;
                    this.acquire(pointer);
                }
            }
        }

        get x(): number {
            return this._x;
        }
        get y(): number {
            return this._y;
        }




        private acquire(pointer: plume.PointerInput) {
            this.acquired = true;
            if (this.visualFeedback) {
                this.ctn.visible = true
            }

            this.downX = pointer.position.x;
            this.downY = pointer.position.y;

            this.initialDownX = pointer.position.x;
            this.initialDownY = pointer.position.y;

            this._x = 0;
            this._y = 0;

            if (this.visualFeedback) {
                this.updateCtnPosition();
            }
        }

        private release(pointer: plume.PointerInput) {
            this.acquired = false;
            this.pointerId = -1;
            if (this.visualFeedback) {
                this.ctn.visible = false;
            }
            this._x = 0;
            this._y = 0;
        }

        private updateActivePointer(pointer: plume.PointerInput) {
            let offsetx = pointer.position.x - this.downX;
            let offsety = pointer.position.y - this.downY;
            offsetx = plume.Mathf.clamp(offsetx, -this._size, this._size);
            offsety = plume.Mathf.clamp(offsety, -this._size, this._size);


            this._x = offsetx;
            this._y = offsety;
            if (Math.abs(offsetx) == this._size) {
                this.downX = pointer.position.x - offsetx;
            }
            if (Math.abs(offsety) == this._size) {
                this.downY = pointer.position.y - offsety;
            }
            // update down position toward current position so that after x ms it simulate we are in standbyif()
            // this.downX = plume.Mathf.lerp(this.downX, pointer.position.x, 0.5);
            // this.downY = plume.Mathf.lerp(this.downY, pointer.position.y, 0.5);
            if (this.visualFeedback) {

                this.visual2.x = this.ctn.width / 2 + offsetx;
                this.visual2.y = this.ctn.height / 2 + offsety;
                this.updateCtnPosition();
            }
        }

        private updateCtnPosition() {
            this.ctn.x = this.initialDownX - this.ctn.width / 2 - this.container.x //- 100;
            this.ctn.y = this.initialDownY - this.ctn.height / 2 - this.container.y //- 100;
        }

        destroy() {
            // getGame().removeUpdatable(this);
        }

        get enabled(): boolean {
            return this._enabled;
        }
        set enabled(v: boolean) {
            if (v) {
                this.tryAcquireOnStart();
            }
            this._enabled = v;
        }


        get size(): number {
            return this._size;
        }

        set size(size: number) {
            this._size = size;
        }



        get active(): boolean {
            return this.acquired;
        }


    }

}