module GAME {
    export function getGame(): Game {
        return Game.get();
    }
    export function getEngine(): plume.Engine {
        return plume.Engine.get();
    }
	export function ArrayOf<T>() : T[]
	{
		return ([]);
	}
}

module GAME.collections {
    export function one<T>(array: Array<T>): T {
        let item = array[Math.floor(Math.random() * array.length)];
        return item;
	}
	
}

