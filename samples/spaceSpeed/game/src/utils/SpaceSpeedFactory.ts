
// singletons
module GAME.sing {
	export let loader: plume.Loader3d;
	export let engine: plume.Engine;
}

module GAME {
	export class RigidBodyDefinition {

	}





}

module GAME.factory {

	export function newContainer(fullsize: boolean = true): plume.Container {
		if (fullsize) {
			return new plume.Container(Game.get().bounds.width, Game.get().bounds.height);
		}
		return new plume.Container();
	}

	// export function newText(text: string, size: number = 256, color?: number, fontname?: string): plume.BitmapText {
	// 	let font = (fontname == null ? "arcade" : fontname);
	// 	let c = (color == null ? Colors.Green : color);
	// 	let t = new plume.BitmapText(text, font, size);
	// 	t.tint = c;
	// 	return t;
	// }

	/*export function newText(text: string, size: number = 256, color: number = 0, fontname?: string): plume.Text {
		let font = (fontname == null ? "Arial" : fontname);
		// let c = (color == null ? 0 : color);
		let t = new plume.Text(text, font, size, color);
		// t.tint = c;
		return t;
	}*/
	export function newText(text: string, size: number = 256, color?: number, fontname?: string): plume.BitmapText {
		let font = (fontname == null ? "arcade" : fontname);
		let c = (color == null ? 0x9900aa : color);
		let t = new plume.BitmapText(text, font, size);
		t.tint = c;
		return t;
	}
	export function newButton(width: number, height: number): ImageButton {
		let linewidth = 5;

		let up = new plume.Graphics(width, height);
		up.beginStroke(linewidth, 0x00FF00);
		up.drawRect(0, 0, width, height);
		up.endStroke();

		let hover = new plume.Graphics(width, height);
		hover.beginStroke(linewidth, Colors.Red);
		hover.drawRect(0, 0, width, height);
		hover.endStroke();
		let pressed = new plume.Graphics(width, height);
		pressed.beginStroke(linewidth, Colors.Red);
		pressed.drawRect(0, 0, width, height);
		pressed.endStroke();

		let style = new plume.ImageButtonStyle(up, pressed, hover);
		let button = new ImageButton(style);
		return button;
	}

	export function blink(display: plume.DisplayObject) {
		let tween = plume.TweenManager.get().create(display, "blink");
		tween.to({ "alpha": 0.2 }, 750, plume.easing.easeInQuad);
		tween.to({ "alpha": 1 }, 750, plume.easing.easeOutQuad);
		tween.delay(450);
		tween.repeat(-1);
		tween.start();
		display.detachListener.once(function () {
			tween.stop();
		});
	}


	export function newSprite(asset: string): plume.Sprite {
		var sprite = plume.Sprite.fromFrame(asset);
		// var sprite = new Sprite(plume.Texture.fromFrame(asset));
		return sprite;
	}

	export function newRectangle(width: number, height: number, color: number): plume.Graphics {
		var graphic = new plume.Graphics(width, height);
		graphic.beginFill(color);
		graphic.drawRect(0, 0, width, height);
		graphic.endFill();
		return graphic;
	}

	export function newRoundRect(width: number, height: number, radius: number, color: number, alpha?: number): plume.Graphics {
		var graphic = new plume.Graphics(width, height);
		graphic.beginFill(color, alpha);
		graphic.drawRoundedRect(0, 0, width, height, radius);
		graphic.endFill();
		return graphic;
	}
	export function newHalfRoundRect(width: number, height: number, radius: number, color: number, alpha?: number): plume.Graphics {
		var graphic = new plume.Graphics(width, height);
		graphic.beginFill(color, alpha);
		graphic.drawRoundedRect(0, 0, width, height, radius);
		graphic.drawRect(0, 0, width / 2, height);
		graphic.endFill();
		graphic.setSize(width,height);
		return graphic;
	}

	export function newCircle(radius: number, color: number): plume.Graphics {
		var graphic = new plume.Graphics(radius * 2, radius * 2);
		graphic.beginFill(color);
		graphic.drawCircle(radius, radius, radius);
		graphic.endFill();
		return graphic;
	}

	export function createDebugBorder(obj: plume.DisplayObject) {
		let g = new plume.Graphics();
		g.beginStroke(1, 0xff0000);
		g.drawRect(0, 0, obj.width, obj.height);
		g.endStroke();
		g.x = obj.x;
		g.y = obj.y;
		return g;
	}

	export function fitObjIn(obj: plume.DisplayObject, target: plume.HasBounds, margin: number = 0) {
		var scaleX = target.width / (obj.width + margin * 2);
		var scaleY = target.height / (obj.height + margin * 2);

		var scale = Math.min(scaleX, scaleY);
		obj.scaleXY = scale;

		obj.x = target.x + (target.width / 2 - obj.width / 2)
		obj.y = target.y + (target.height / 2 - obj.height / 2)
	}

	export function newShadow(tint:number,alpha:number,flipX=true,width=492,height=51) {
		let halfRoundRect = factory.newHalfRoundRect(width,height,20,tint,alpha);
		// let halfRoundRect = factory.newHalfRoundRect(492,51,20,1,0.5);
		halfRoundRect.flipX=flipX;
		return halfRoundRect;
	}

	


}