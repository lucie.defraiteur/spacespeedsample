module GAME {
	export class CustomRenderTarget {
		scene: THREE.Scene = new THREE.Scene;
		root: THREE.Object3D;
		camera: THREE.Camera;

		public renderTarget: THREE.WebGLRenderTarget;
		lastTextureOutput: THREE.Texture = undefined;
		private _debugMode: boolean;
		renderTarget2: THREE.WebGLRenderTarget;
		switchRenderTarget: boolean = false;
		renderer: THREE.WebGLRenderer;
		depthTexture: THREE.Texture;
		public SetDebugMode(value: boolean) {
			this._debugMode = value;
		}

		public Render(drawPreviousFrames: boolean = true) {
			/*	plume.Engine.get().renderer.setScissorTest(false);
					let w = window.innerWidth, h = window.innerHeight;
					plume.Engine.get().renderer.setViewport(0, 0, w, h);
					plume.Engine.get().renderer.setClearColor(0x0, 1);
					plume.Engine.get().renderer.clear();
			*/
			Gameplay.Main.camera.updateMatrixWorld(true);
			if (this.switchRenderTarget) {
				var clearColor = this.renderer.getClearColor();
				this.renderer.clearDepth();
				//this.renderer.setClearAlpha(0x000000);
				this.renderer.render(this.scene, Gameplay.Main.camera, this.renderTarget2, true);
			
				this.renderer.setClearColor(clearColor);
				this.lastTextureOutput = this.renderTarget2.texture;
				this.depthTexture = this.renderTarget2.depthTexture;
				this.switchRenderTarget = false;
			}
			else {
				var clearColor = this.renderer.getClearColor();
				this.renderer.clearDepth();
				//this.renderer.setClearAlpha(0x000000);
				this.renderer.render(this.scene, Gameplay.Main.camera, this.renderTarget, true);
				this.renderer.setClearColor(clearColor);
				this.lastTextureOutput = this.renderTarget.texture;
				this.depthTexture = this.renderTarget.depthTexture;
				this.switchRenderTarget = true;
				// console.log("clear color:" + clearColor.toArray());
			}

			//plume.Engine.get().renderer.setClearColor(0xFFFFFF);
			//plume.Engine.get().renderer.render(Gameplay.Main.scene, Gameplay.Main.camera, this.renderTarget, true);
			//this.lastTextureOutput = this.renderTarget.texture;


		}
		public Destroy()
		{
			this.renderTarget.dispose();
			this.renderTarget2.dispose();
		}

		public constructor(rootObject: THREE.Object3D, camera: THREE.Camera, resolution: THREE.Vector2 = new THREE.Vector2(1024, 1024), materialOverride: THREE.MeshMaterialType | THREE.MeshMaterialType[] = undefined, targetMaterial: THREE.MeshBasicMaterial = undefined) {
			var context = plume.Engine.get().renderer.context;

			this.scene = new THREE.Scene();
			this.camera = camera;
			this.root = rootObject;
			resolution = new THREE.Vector2(plume.Engine.get().renderer.domElement.width, plume.Engine.get().renderer.domElement.height);
			//console.error('resolution:' + resolution.toArray());
			this.renderTarget = new THREE.WebGLRenderTarget(resolution.x, resolution.y, {
				minFilter: THREE.LinearFilter,
				stencilBuffer: true,
				depthBuffer: true
			
			});
			this.renderTarget.depthBuffer = true;

			this.renderTarget2 = new THREE.WebGLRenderTarget(resolution.x, resolution.y, {
				minFilter: THREE.LinearFilter,
				stencilBuffer: true,
				depthBuffer: true,
			});
			this.renderTarget2.depthBuffer = true;
			this.SetDebugMode(true);
			this.renderer = plume.Engine.get().renderer;
			/*{
				
				var width = this.renderTarget.width;
				var height = this.renderTarget.height;
				var self = this;
				plume.Engine.get().addRenderable(() => {
					self.OnPostRender();
				}, false);

				plume.Engine.get().addRenderable(() => {
					self.OnPreRender();
				}, true);
			}*/

		}
	}
}