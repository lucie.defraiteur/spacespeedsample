module GAME {


	export class Materials {

		private static debugRedBuildingMaterial = new THREE.MeshLambertMaterial({ color: 0xFF0000, depthTest: true, transparent: true });
		static getDebugRedBuildingMaterial() {
			return this.debugRedBuildingMaterial;
			// return new THREE.MeshLambertMaterial({ color: 0xFF0000, depthTest: true, transparent: true });;
		}
	}


	export class Geometries {
		private static debugRedBuildingGeoms: { [index: string]: THREE.BoxBufferGeometry } = {};
		static getDebugRedBuildingGeom(size: THREE.Vector3) {
			var hash = size.toArray().toString();
			if (this.debugRedBuildingGeoms[hash] == undefined) {
				this.debugRedBuildingGeoms[hash] = new THREE.BoxBufferGeometry(size.x, size.y, size.z);

			}
			return (this.debugRedBuildingGeoms[hash]);
		}
		
		private static _debugBoxBufferGeometries = new plume.HashMap<THREE.BoxBufferGeometry>();
		static getDebugBoxBufferGeometry(width: number, height: number, depth: number) {
			let id = "" + width + height + depth;
			let geo = this._debugBoxBufferGeometries.get(id);
			if (geo == null) {
				geo = new THREE.BoxBufferGeometry(width, height, depth);
				this._debugBoxBufferGeometries.put(id, geo);
			} else {
				logger.debug("getDebugBoxBufferGeometry from cache")
			}
			return geo;
		}
	}

	export class Vector3Pool {
		private static _stack = new Array<THREE.Vector3>();
		static pop(): THREE.Vector3 {
			if (this._stack.length == 0) return new THREE.Vector3();

			let v = this._stack.pop();
			return v;
		}
		static push(v: THREE.Vector3) {
			this._stack.push(v);
		}
	}
}