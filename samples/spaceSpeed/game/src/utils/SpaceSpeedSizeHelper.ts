module GAME {

    let tmpVector: THREE.Vector3;

    export class SizeHelper {
        private static INST: SizeHelper;

        private game: Game;

        scale = 0;
        halfWidth = 0;
        halfHeight = 0;

        constructor() {
            SizeHelper.INST = this;

            tmpVector = new THREE.Vector3();
            getGame().scaleManager.resizeHandler.add(this.onResize, this);

            this.game = getGame();
            this.onResize();
        }

        static get() {
            if (SizeHelper.INST == null) {
                SizeHelper.INST = new SizeHelper();
            }

            return SizeHelper.INST;


        }

        private onResize() {
            let sm = this.game.scaleManager.getCurrentScaling();

            let bounds = gui.expandedBounds;
            let width = Math.round(bounds.width * sm.scale);
            let height = Math.round(bounds.height * sm.scale);
            this.halfHeight = height / 2;
            this.halfWidth = width / 2;
            this.scale = sm.scale;
        }


        static get2dCoordinate(mesh: THREE.Object3D, projection: THREE.Vector3): THREE.Vector3 {
            let sh = SizeHelper.get();
            let bounds = gui.expandedBounds;

            mesh.getWorldPosition(tmpVector);
            projection.copy(tmpVector);
            projection.project(getEngine().camera);

            let x = (projection.x * sh.halfWidth) + sh.halfWidth;
            let y = -(projection.y * sh.halfHeight) + sh.halfHeight;

            x = x / sh.scale;
            y = y / sh.scale;

            projection.x = bounds.x + x;
            projection.y = bounds.y + y;

            return projection;
        }


        static get2dCoordinate2(worldPosition: THREE.Vector3, projection: THREE.Vector3): THREE.Vector3 {
            let sh = SizeHelper.get();
            let bounds = gui.expandedBounds;

            projection.copy(worldPosition);
            projection.project(getEngine().camera);

            let x = (projection.x * sh.halfWidth) + sh.halfWidth;
            let y = -(projection.y * sh.halfHeight) + sh.halfHeight;

            x = x / sh.scale;
            y = y / sh.scale;

            projection.x = bounds.x + x;
            projection.y = bounds.y + y;

            return projection;
        }
    }
}