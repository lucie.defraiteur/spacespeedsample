module GAME {

	export const enum Sounds {
		track0 = "track0",
		track1 = "track1"
	}

	export class SoundManager {

		private static INSTANCE: SoundManager;
		private static audioManager: plume.AudioManager;

		public static ready = false;
		private static sound: plume.Sound;
		static enabled = true;

		private static curEngineName: string = null;
		private static engineLastRate: number = 0.5;
		private static currentGear = 1;
		private static lastGearChanged = performance.now();

		private static musicid = 0;
		private static motorid = 0;
		static currentSound: string;

		constructor() {
			SoundManager.INSTANCE = this;
			SoundManager.audioManager = plume.AudioManager.get();
			SoundManager.load();
		}

		private static load() {
			this.audioManager.loadSounds([
				{ type: 'soundsheet', name: 'sounds', file: "game/assets/sounds/sounds.json" }
			], function () {
				SoundManager.sound = plume.AudioManager.get().getSoundSprite("sounds");
				if (SoundManager.sound != null) {
					SoundManager.ready = true;
				}
			});
		}

		static get(): SoundManager {
			if (SoundManager.INSTANCE == null) {
				let sm = new SoundManager();
			}
			return SoundManager.INSTANCE;
		}

		static play(sound: string, doneCb?: () => void) {
			if (!this.ready) return;
			if (!this.enabled) return;
			
			this.sound.play(sound, function () {
				if (doneCb) doneCb();
			});
		}
		static pause()
		{
			this.sound._howl.pause();
			
		}
		static resume()
		{
			this.sound._howl.play();
		}
	
		static stopMusic() {
			if (this.musicid != 0) {
				this.sound.stop(this.musicid);
				this.musicid = 0;
			}
		}

		static playMusic(sound: string, doneCb?: () => void) {

			if (!this.ready) return;
			if (!this.enabled) return;

			if (this.musicid != 0) {
				this.sound.stop(this.musicid);
				this.musicid = 0;
			}

			this.musicid = this.sound.play(sound, function () {
				if (doneCb) {
					doneCb();
				}
			});
			this.currentSound = sound;
			this.sound.setVolume(0.5, this.musicid);
			this.sound.loop(true, this.musicid);
		}

		static stopEngine() {
			if (this.motorid != 0) {
				this.sound.stop(this.motorid);
				this.motorid = 0;
				this.curEngineName = null;
			}
		}


	}


}