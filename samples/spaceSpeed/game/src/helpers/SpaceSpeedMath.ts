declare var uheprng: any;
module GAME {
	const DegToRad: number = 1 / 180.0 * Math.PI;
	const RadToDeg: number = 1 / Math.PI * 180.0;
	export class MathHelper {
		static randomDirection(): number {
			return ((MathHelper.fit01int(MathHelper.Random(), 0, 1) * 2) - 1);
		}
		static _rng: any;
		static Clamp(value: number, min: number, max: number): number {
			value = Math.max(value, min);
			value = Math.min(value, max);
			return (value);
		}
		static Clamp01(value: number) {
			return (Math.min(Math.max(value, 0), 1));
		}
		static floatMod(n: number, mod: number): any {
			return (n % mod);
		}
		public static fit01(interpolation: number, min: number, max: number): number {
			return ((interpolation * (max - min)) + min);
		}
		public static fit01int(interpolation: number, min: number, max: number): number {
			max += 0.95;
			return (Math.floor((interpolation * (max - min)) + min));
		}
		public static toInterpolant(value: number, min: number, max: number)
		{
			return ((value - min) / (max - min));
		}
		public static interpolateIndex<T>(t: number, array: T[]): number {
			return (MathHelper.fit01int(t, 0, array.length - 1));
		}
		public static interpolateValue<T>(t: number, array: T[]): T {
			return (array[MathHelper.fit01int(t, 0, array.length - 1)]);
		}
		public static interpolateIndexValue<T>(t: number, array: T[]): { index: number, value: T } {
			var index = MathHelper.fit01int(t, 0, array.length - 1);
			var value = array[index];
			return ({ index: index, value: value });
		}
		public static randomIndex<T>(array: T[]): number {
			var index = MathHelper.fit01int(MathHelper.Random(), 0, array.length - 1);
			return (index);
		}
		public static randomIndexValue<T>(array: T[]): { index: number, value: T } {
			var randIndex = MathHelper.fit01int(MathHelper.Random(), 0, array.length - 1);
			return ({ index: randIndex, value: array[randIndex] });
		}
		public static randomValue<T>(array: T[]): T {
			var randIndex = MathHelper.fit01int(MathHelper.Random(), 0, array.length - 1);
			return (array[randIndex]);
		}
		public static randomInRange(min: number, max: number): number {
			return (MathHelper.fit01int(MathHelper.Random(), min, max));
		}
		public static fit(value: number, min: number, max: number, newMin: number, newMax: number): number {
			var interpolation: number = value / (max - min);
			return (MathHelper.fit01(interpolation, newMin, newMax));
		}
		public static sign(value: number): number {
			return (value >= 0 ? 1.0 : -1.0);
		}
		public static ToDeg(rad: number): number {
			return (rad * RadToDeg);
		}
		public static ToRad(deg: number): number {
			return (deg * DegToRad);
		}
		private static _randomSeed: number = MathHelper.fit01int(Math.random(), 0, 1541899);
		public static RandomSeed(seed: number = undefined) {
			if (seed != undefined) {
				this._randomSeed = seed;
				this._rng = new uheprng(seed);
			}
			return (this._randomSeed);
		}
		public static Random() {
			return (this._rng(10000.0) / 10000.0);
		}

		public static GetAngleRange(a0: number, a1: number, keepOrder: boolean = false): { a0: number, a1: number } {
			a0 = a0 % 360;
			if (a0 < 0) {
				a0 += 360;
			}
			a1 = a1 % 360;
			if (a1 < 0) {
				a1 += 360;
			}

			if (a0 > a1 && a0 - a1 >= 180) {
				a1 += 360;
			}
			if (a1 > a0 && a1 - a0 >= 180) {
				a0 += 360;
			}
			if (a0 > a1 && !keepOrder) {
				var tmp = a1;
				a1 = a0;
				a0 = tmp;
			}

			return ({ a0: a0, a1: a1 });
		}
		public static GetAngleDiff(from: number, to: number) {
			var range = this.GetAngleRange(from, to, true);
			return (range.a1 - range.a0);
		}

	}
	export class Vector3 {
		public static get Zero(): THREE.Vector3 {
			return (new THREE.Vector3());
		}
		public static get Forward(): THREE.Vector3 {
			return (new THREE.Vector3(0, 0, 1));
		}
		public static get Up(): THREE.Vector3 {
			return (new THREE.Vector3(0, 1, 0));
		}
		public static get Right(): THREE.Vector3 {
			return (new THREE.Vector3(1, 0, 0));
		}
		public static get One(): THREE.Vector3 {
			return (new THREE.Vector3(1, 1, 1));
		}
		public static fit01(interpolation: number, min: THREE.Vector3, max: THREE.Vector3) {
			var res = new THREE.Vector3().set(MathHelper.fit01(Math.random(), min.x, max.x), MathHelper.fit01(Math.random(), min.y, max.y), MathHelper.fit01(Math.random(), min.z, min.z));
			return (res);
		}
	}
	export class Vector2 {
		public static get Zero(): THREE.Vector2 {
			return (new THREE.Vector2());
		}
		public static get Up(): THREE.Vector2 {
			return (new THREE.Vector2(0, 1));
		}
		public static get Right(): THREE.Vector2 {
			return (new THREE.Vector2(1, 0));
		}
		public static get Left(): THREE.Vector2 {
			return (new THREE.Vector2(-1, 0));
		}
		public static get Down(): THREE.Vector2 {
			return (new THREE.Vector2(0, -1));
		}
	}
	export class MeshHelper {

		public static recomputeWorldMatrix(mesh: THREE.Object3D) {
			var resMat: THREE.Matrix4;
			var self = mesh;
			if (self.parent != undefined) {
				var currentMat = self.matrix;
				var current = self.parent;
				self.updateMatrix();
				while (current != undefined) {
					current.updateMatrix();

					//	current.RecomputeLocalMatrix();
					//}
					var mat = current.matrix;
					currentMat = mat.clone().multiply(currentMat);
					var current = current.parent;
				}
				resMat = (currentMat);
			}
			else {

				self.updateMatrix();
				resMat = (self.matrix);
			}
			(self as any).m_localToWorldMatrix = resMat;
			(self as any).m_worldToLocalMatrix = resMat.clone().getInverse(resMat);
		}
		public static localToWorldByObject3D: { [index: number]: THREE.Matrix4 } = {};
		public static worldToLocal(mesh: THREE.Object3D, position: THREE.Vector3, forceUpdateMatrix: boolean = false) {

			if (forceUpdateMatrix) {
				this.recomputeWorldMatrix(mesh);
			}
			var worldToLocal: THREE.Matrix4 = (mesh as any).m_worldToLocalMatrix;


			return (position.clone().applyMatrix4(worldToLocal));
		}
		public static localToWorld(mesh: THREE.Object3D, position: THREE.Vector3, forceUpdateMatrix: boolean = false) {
			if (forceUpdateMatrix) {
				this.recomputeWorldMatrix(mesh);
			}
			var localToWorld: THREE.Matrix4 = (mesh as any).m_localToWorldMatrix;
			return ((position.clone().applyMatrix4(localToWorld)));
		}
	}


}