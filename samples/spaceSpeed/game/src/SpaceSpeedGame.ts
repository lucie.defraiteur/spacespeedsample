/// <reference path="../../../../plume3d/export/latest/plume3d.core.d.ts" />
/// <reference path="../../../../plume3d/export/latest/plume3d.gui.d.ts" />
/// <reference path="../../../../plume3d/export/latest/plume3d.tween.d.ts" />
/// <reference path="../../../../integration/minisdk/export/vads.d.ts" />
/// <reference path="../../../../plume3d/export/latest/plume3d.cannon.d.ts" />
/// <reference path="ads/SpaceSpeedAdsHandler.ts" />

module GAME {

    export let adsHandler: AdsHandler;
    export let logger = plume.logger;
    export let layout = plume.layout;
    export let random = plume.Random;

    export let engine: plume.Engine;
    export let gui: plume.CanvasGui;
    export let Time = plume.Time; 


    export class Game extends vads.VAdsGame {

        public static onGameEnd: () => void;
        private static _INSTANCE: Game;

        public static Restart() {
            var canvas3d = document.getElementById("3d");
            getGame().switchScene(new GameplayScene());
        }

        parameters: GameParameters;

        constructor() {
            super(1334, 750, { input: { maxTouchPointer: 10 } });
            Game._INSTANCE = this;
        }

        static get(): Game {
            return Game._INSTANCE;
        }

        newAdsHandler() {
            adsHandler = new AdsHandler();
            return adsHandler;
        }

        onCreate() {
            super.onCreate();

            this.initialize(GAMECONFIG);

            engine = this.engine;
            gui = this.gui;

            let seed = Date.now();
            random.init(seed);

            this.parameters = new GameParameters();

            let assets: Array<plume.AssetDef> = [
                { type: 'spritesheet', name: 'game-0', file: "game/assets/images/game-0.png", atlas: "game/assets/images/game-0.json" },
            ];
            this.loadAssets(assets);
        }

        protected onAssetsLoaded() {
            plume.FontLoader.loadAll(fonts.fonts);
            plume.Fonts.clearYOffset("arcade");
        }

        protected goToGame() {
            let self = this;
            let scene = new SplashScene();
            this.switchScene(scene);
        }

    }

    export let game = new Game();
    game.start();

}
