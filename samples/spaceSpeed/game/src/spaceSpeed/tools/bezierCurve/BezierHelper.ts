module GAME {
	export class QuadraticBezierUtil {
		// p2 is a control point.
		public static GetPoint(p0: THREE.Vector3, p1: THREE.Vector3, p2: THREE.Vector3, t: number): THREE.Vector3 {
			var t = (t);
			var oneMinusT = 1.0 - t;
			return (p0.clone().multiplyScalar(oneMinusT * oneMinusT)
				.add(p1.clone().multiplyScalar(2.0 * oneMinusT * t))
				.add(p2.clone().multiplyScalar(t * t)));
		}
		public static GetFirstDerivative(p0: THREE.Vector3, p1: THREE.Vector3, p2: THREE.Vector3, t: number): THREE.Vector3 {
			t = (t);
			return (p1.clone().sub(p0).multiplyScalar(2.0 * (1.0 - t)).add(p2.clone().sub(p1).multiplyScalar(2.0 * t)));
		}
	}


	export class CubicBezierUtil {
		// p2 and p3 are control points.
		public static GetPoint(p0: THREE.Vector3, p1: THREE.Vector3, p2: THREE.Vector3, p3: THREE.Vector3, t: number): THREE.Vector3 {
			var t = (t);
			var oneMinusT = 1.0 - t;
			return (p0.clone().multiplyScalar(oneMinusT * oneMinusT * oneMinusT)
				.add(p1.clone().multiplyScalar(3.0 * oneMinusT * oneMinusT * t))
				.add(p2.clone().multiplyScalar(3.0 * oneMinusT * t * t)))
				.add(p3.clone().multiplyScalar(t * t * t));
		}
		public static GetFirstDerivative(p0: THREE.Vector3, p1: THREE.Vector3, p2: THREE.Vector3, p3: THREE.Vector3, t: number): THREE.Vector3 {
			t = (t);
			var oneMinusT = 1.0 - t;
			return (
				p1.clone().sub(p0).multiplyScalar(3.0 * oneMinusT * oneMinusT)
					.add(p2.clone().sub(p1).multiplyScalar(6.0 * oneMinusT * t))
					.add(p3.clone().sub(p2).multiplyScalar(3.0 * t * t))
			);
		}
		/*
		public static AlignedRmf(data0:VectorFrame[], data1: VectorFrame[]) {
			var frames: VectorFrame[] = [];
		
			var c1: number, c2: number;
			var v1: THREE.Vector3, v2: THREE.Vector3, riL: THREE.Vector3, tiL: THREE.Vector3, riN: THREE.Vector3, siN: THREE.Vector3;
			var x0: VectorFrame, x1: VectorFrame;

			// Start off with the standard tangent/axis/normal frame
			// associated with the curve just prior the Bezier interval.
	
			//console.log("T0:" + t0);
//			frames.push(frenetFrameFunction(t0, data));
			frames.push(data0[data0.length - 1]);
			for (var i = 0; i < data1.length; i++)
			{
				x0 = frames[frames.length - 1];
				x1 = data1[i];
				
				v1 = x1.origin.clone().sub(x0.origin);
				//console.log("V1:" + v1.toArray());
				c1 = v1.dot(v1);
				//console.log("C1:" + c1);
				//console.log("X0axis:" + x0.axis.toArray());
				riL = x0.axis.clone().sub(v1.clone().multiplyScalar(2 / c1 * v1.dot(x0.axis)));
				//console.log("RIL:" + riL.clone().toArray());
				tiL = x0.tangent.clone().sub(v1.clone().multiplyScalar(2 / c1 * v1.dot(x0.tangent)));

				// Then we reflection a second time, over a plane at x1
				// so that the frame tangent is aligned with the curve tangent:
				v2 = x1.tangent.clone().sub(tiL);
				//console.log("V2:" + v2.toArray());
				c2 = v2.dot(v2);
				///console.log("C2:" + c2);
				riN = riL.clone().sub(v2.clone().multiplyScalar(2 / c2 * v2.dot(riL)));
				siN = x1.tangent.clone().cross(riN);
				var oldNormal = x1.normal.clone();
				var realAxis = oldNormal.cross(siN);
				x1.realAxis = realAxis;
				x1.normal = siN;
				x1.axis = riN;

				//  console.log("TANGENT:" + x1.tangent.toArray());
				//	console.log("NORMAL:" + x1.normal.toArray());

				// we record that frame, and move on
				frames.push(x1);
			}

			// and before we return, we throw away the very first frame,
			// because it lies outside the Bezier interval.
			frames.splice(0, 1);

			return frames;
		}*/

		public static getRmfFollow(firstFrame: VectorFrame, steps: number, data: THREE.Vector3[], frenetFrameFunction: (t: number, points: THREE.Vector3[]) => VectorFrame) {
			var frames: VectorFrame[] = [];
			//steps += 1.0;
			var c1: number, c2: number,step: number = 1.0 / steps, t0, t1;
			var v1: THREE.Vector3, v2: THREE.Vector3, riL: THREE.Vector3, tiL: THREE.Vector3, riN: THREE.Vector3, siN: THREE.Vector3;
			var x0: VectorFrame, x1: VectorFrame;
			t0 = 0;
			// Start off with the standard tangent/axis/normal frame
			// associated with the curve just prior the Bezier interval.
	
			//console.log("T0:" + t0);
//			frames.push(frenetFrameFunction(t0, data));
			frames.push(firstFrame.clone());
			for (; t0 < 1.0; t0 += step) {
				//	console.log("STEP:" + step);
				// start with the previous, known frame
				x0 = frames[frames.length - 1];

				// get the next frame: we're going to throw away its axis and normal
				t1 = t0 + step;
				x1 = frenetFrameFunction(t1, data);
				// console.log("T1:" + t1);

				// First we reflect x0's tangent and axis onto x1, through
				// the plane of reflection at the point midway x0--x1
				v1 = x1.origin.clone().sub(x0.origin);
				//console.log("V1:" + v1.toArray());
				c1 = v1.dot(v1);
				//console.log("C1:" + c1);
				//console.log("X0axis:" + x0.axis.toArray());
				riL = x0.axis.clone().sub(v1.clone().multiplyScalar(2 / c1 * v1.dot(x0.axis)));
				//console.log("RIL:" + riL.clone().toArray());
				tiL = x0.tangent.clone().sub(v1.clone().multiplyScalar(2 / c1 * v1.dot(x0.tangent)));

				// Then we reflection a second time, over a plane at x1
				// so that the frame tangent is aligned with the curve tangent:
				v2 = x1.tangent.clone().sub(tiL);
				//console.log("V2:" + v2.toArray());
				c2 = v2.dot(v2);
				///console.log("C2:" + c2);
				riN = riL.clone().sub(v2.clone().multiplyScalar(2 / c2 * v2.dot(riL)));
				siN = x1.tangent.clone().cross(riN);
				var oldNormal = x1.normal.clone();
				var realAxis = oldNormal.cross(siN);
				x1.realAxis = realAxis;
				x1.normal = siN;
				x1.axis = riN;

				//  console.log("TANGENT:" + x1.tangent.toArray());
				//	console.log("NORMAL:" + x1.normal.toArray());

				// we record that frame, and move on
				frames.push(x1);

			}

			// and before we return, we throw away the very first frame,
			// because it lies outside the Bezier interval.
		

			return frames;
		}
		
		public static getRMF(steps: number, data: THREE.Vector3[], frenetFrameFunction: (t: number, points: THREE.Vector3[]) => VectorFrame) {
			var frames: VectorFrame[] = [];
			var c1: number, c2: number, step: number = 1.0 / steps, t0, t1;
			var v1: THREE.Vector3, v2: THREE.Vector3, riL: THREE.Vector3, tiL: THREE.Vector3, riN: THREE.Vector3, siN: THREE.Vector3;
			var x0: VectorFrame, x1: VectorFrame;

			// Start off with the standard tangent/axis/normal frame
			// associated with the curve just prior the Bezier interval.
			t0 = -step;
			//console.log("T0:" + t0);
			frames.push(frenetFrameFunction(t0, data));

			// start constructing RM frames
			for (; t0 < 1.0; t0 += step) {
				//	console.log("STEP:" + step);
				// start with the previous, known frame
				x0 = frames[frames.length - 1];

				// get the next frame: we're going to throw away its axis and normal
				t1 = t0 + step;
				x1 = frenetFrameFunction(t1, data);
				// console.log("T1:" + t1);

				// First we reflect x0's tangent and axis onto x1, through
				// the plane of reflection at the point midway x0--x1
				v1 = x1.origin.clone().sub(x0.origin);
				//console.log("V1:" + v1.toArray());
				c1 = v1.dot(v1);
				//console.log("C1:" + c1);
				//console.log("X0axis:" + x0.axis.toArray());
				riL = x0.axis.clone().sub(v1.clone().multiplyScalar(2 / c1 * v1.dot(x0.axis)));
				//console.log("RIL:" + riL.clone().toArray());
				tiL = x0.tangent.clone().sub(v1.clone().multiplyScalar(2 / c1 * v1.dot(x0.tangent)));

				// Then we reflection a second time, over a plane at x1
				// so that the frame tangent is aligned with the curve tangent:
				v2 = x1.tangent.clone().sub(tiL);
				//console.log("V2:" + v2.toArray());
				c2 = v2.dot(v2);
				///console.log("C2:" + c2);
				riN = riL.clone().sub(v2.clone().multiplyScalar(2 / c2 * v2.dot(riL)));
				siN = x1.tangent.clone().cross(riN);
				var oldNormal = x1.normal.clone();
				var realAxis = oldNormal.cross(siN);
				x1.realAxis = realAxis;
				x1.normal = siN;
				x1.axis = riN;

				//  console.log("TANGENT:" + x1.tangent.toArray());
				//	console.log("NORMAL:" + x1.normal.toArray());

				// we record that frame, and move on
				frames.push(x1);

			}

			// and before we return, we throw away the very first frame,
			// because it lies outside the Bezier interval.
			frames.splice(0, 1);

			return frames;
		}
		// je génère trois points,
		// un point qui est mon début de jeu, (A)
		// un point qui est le point d'après,  (B)
		// un point qui est encore le point d'après. (C)
		// je passe (A B C), ainsi j'obtient la premiere courbe A B,
		// le jeu avance jusqu'a B, alors je génère un nouveau point (D)
		// je passe (A B C D),  ainsi j'obtient la seconde courbe B C,
		// etc...
		// il faut connaitre un point d'avance a chaque fois.
		public static GenerateRandomCurves(count: number = 15, oldPts: THREE.Vector3[] = undefined): { res: THREE.Vector3[][], pts: THREE.Vector3[] } {
			var res: THREE.Vector3[][] = [];
			var pts: THREE.Vector3[] = [];
			if (oldPts != undefined) {
				pts = oldPts;
			}
			for (var i = 0; i < count; i++) {
				var scale = 2.0;
				var minDist = 25.0 * 2.0;
				var maxDist = 50.0 * 2.0;
				var radiusMin = 5.0 * 2.0;
				var radiusMax = 15.0 * 2.0;


				if (i == 0 && oldPts == undefined) {

					pts = this.GetRandomPoints(Vector3.Zero, 3, radiusMin, radiusMax, minDist, maxDist, Vector3.Forward);

					// la premiere courbe A B C, a besoin de trois points.
				}
				else if (pts.length == 3) {

					pts.push(this.GetRandomPoints(pts[pts.length - 1], 1, radiusMin, radiusMax, minDist, maxDist, Vector3.Forward)[0]);
				}
				else {
					// on retire le premier point de la liste.
					pts.splice(0, 1);
					// ensuite, on a besoin de 4 points, les points de la derniere courbe, et un nouveau.
					pts.push(this.GetRandomPoints(pts[pts.length - 1], 1, radiusMin, radiusMax, minDist, maxDist, Vector3.Forward)[0]);
				}
				//console.log("HEY???!!" + JSON.stringify(pts));
				// par contre ça va pas parceque on veut pas ajouter des vraies courbes les construire etc.. si on en garde que une.
				res.push(this.GetCubicBezierControlListPassingByPoints(pts, 1.0)[pts.length == 3 ? 0 : 1]);
			}
			return ({ res: res, pts: pts });
		}
		private static GetRandomPoints(start: THREE.Vector3, count: number, radiusMin: number, radiusMax: number, minDist: number, maxdist: number, direction: THREE.Vector3) {
			var res: THREE.Vector3[] = [];
			var upDir = new THREE.Vector3(0, 1, 0);
			var lastPt: THREE.Vector3 = start;
			var quat = QuaternionHelper.LookAtQuaternion(Vector3.Zero, direction, upDir);
			for (var i = 0; i < count; i++) {
				var randomAngle = MathHelper.fit01(MathHelper.Random(), 0, 360);
				var rad = MathHelper.ToRad(randomAngle);
				var randomDist: number = MathHelper.fit01(MathHelper.Random(), minDist, maxdist);
				var pt = new THREE.Vector3(Math.cos(rad), Math.sin(rad), 0).multiplyScalar(MathHelper.fit01(MathHelper.Random(), radiusMin, radiusMax));
				pt.applyQuaternion(quat);
				pt.add(direction.clone().multiplyScalar(randomDist));
				pt.add(start);
				start = pt;
				res.push(pt);
			}
			return (res);

		}
		// genre a chaque fois tu passe les points de celle d'avant,
		// et les points de celle d'après, et hop.
		public static GetCubicBezierControlListPassingByPoints(ptList: THREE.Vector3[], smooth_value: number = 1.0) {

			var res: THREE.Vector3[][] = [];
			//console.log("PT LIST LEN0:" + ptList.length);
			var firstVertex = ptList[0].clone().sub(ptList[1]).add(ptList[0]);
			//console.log("PT LIST LEN:" + ptList.length);
			var endVertex = ptList[ptList.length - 1].clone().sub(ptList[ptList.length - 2]).add(ptList[ptList.length - 1]);
			//console.log("oh???");
			var vertices = [firstVertex, ...ptList, endVertex];
			//console.log("???::" + JSON.stringify(vertices));
			for (var i = 1; i < vertices.length - 2; i++) {
				var lastVert = vertices[i - 1];
				var nextVert = vertices[i + 2];
				var p1 = vertices[i];
				var p2 = vertices[i + 1];
				var cubicControls = this.GetCubicControlsPassingByPoints(lastVert, p1, p2, nextVert, smooth_value);
				res.push(cubicControls);
			}
			//console.log("RES:" + JSON.stringify(res));
			return (res);
		}
		public static GetCubicControlsPassingByPoints(lastVertex: THREE.Vector3, p1: THREE.Vector3, p2: THREE.Vector3, nextVertex: THREE.Vector3, smooth_value: number = 1.0): THREE.Vector3[] {
			//http://www.antigrain.com/research/bezier_interpolation/index.html
			var p0 = lastVertex;
			var p3 = nextVertex;

			var xc1 = (p0.x + p1.x) / 2.0;
			var yc1 = (p0.y + p1.y) / 2.0;
			var zc1 = (p0.z + p1.z) / 2.0;

			var xc2 = (p1.x + p2.x) / 2.0;
			var yc2 = (p1.y + p2.y) / 2.0;
			var zc2 = (p1.z + p2.z) / 2.0;

			var xc3 = (p2.x + p3.x) / 2.0;
			var yc3 = (p2.y + p3.y) / 2.0;
			var zc3 = (p2.z + p3.z) / 2.0;


			var len1 = Math.sqrt((p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y) + (p1.z - p0.z) * (p1.z - p0.z));
			var len2 = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y) + (p2.z - p1.z) * (p2.z - p1.z));
			var len3 = Math.sqrt((p3.x - p2.x) * (p3.x - p2.x) + (p3.y - p2.y) * (p3.y - p2.y) + (p3.z - p2.z) * (p3.z - p2.z));

			var k1 = len1 / (len1 + len2);
			var k2 = len2 / (len2 + len3);

			var xm1 = xc1 + (xc2 - xc1) * k1;
			var ym1 = yc1 + (yc2 - yc1) * k1;
			var zm1 = zc1 + (zc2 - zc1) * k1;

			var xm2 = xc2 + (xc3 - xc2) * k2;
			var ym2 = yc2 + (yc3 - yc2) * k2;
			var zm2 = zc2 + (zc3 - zc2) * k2;
			var ctrl1 = new THREE.Vector3();
			var ctrl2 = new THREE.Vector3();
			// Resulting control points. Here smooth_value is mentioned
			// above coefficient K whose value should be in range [0...1].
			ctrl1.x = xm1 + (xc2 - xm1) * smooth_value + p1.x - xm1;
			ctrl1.y = ym1 + (yc2 - ym1) * smooth_value + p1.y - ym1;
			ctrl1.z = zm1 + (zc2 - zm1) * smooth_value + p1.z - zm1;

			ctrl2.x = xm2 + (xc2 - xm2) * smooth_value + p2.x - xm2;
			ctrl2.y = ym2 + (yc2 - ym2) * smooth_value + p2.y - ym2;
			ctrl2.z = zm2 + (zc2 - zm2) * smooth_value + p2.z - zm2;

			return ([p1, p2, ctrl1, ctrl2]);
		}

		public static GetQuadraticControlsPassingByPoints(lastVertex: THREE.Vector3, p1: THREE.Vector3, p2: THREE.Vector3, nextVertex: THREE.Vector3, smooth_value: number = 1.0): THREE.Vector3[] {
			//http://www.antigrain.com/research/bezier_interpolation/index.html
			var p0 = lastVertex;
			var p3 = nextVertex;

			var xc1 = (p0.x + p1.x) / 2.0;
			var yc1 = (p0.y + p1.y) / 2.0;
			var zc1 = (p0.z + p1.z) / 2.0;

			var xc2 = (p1.x + p2.x) / 2.0;
			var yc2 = (p1.y + p2.y) / 2.0;
			var zc2 = (p1.z + p2.z) / 2.0;

			var xc3 = (p2.x + p3.x) / 2.0;
			var yc3 = (p2.y + p3.y) / 2.0;
			var zc3 = (p2.z + p3.z) / 2.0;


			var len1 = Math.sqrt((p1.x - p0.x) * (p1.x - p0.x) + (p1.y - p0.y) * (p1.y - p0.y) + (p1.z - p0.z) * (p1.z - p0.z));
			var len2 = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y) + (p2.z - p1.z) * (p2.z - p1.z));
			var len3 = Math.sqrt((p3.x - p2.x) * (p3.x - p2.x) + (p3.y - p2.y) * (p3.y - p2.y) + (p3.z - p2.z) * (p3.z - p2.z));

			var k1 = len1 / (len1 + len2);
			var k2 = len2 / (len2 + len3);
			//var k3 = k1 + k2 / 2.0;

			var xm1 = xc1 + (xc2 - xc1) * k1;
			var ym1 = yc1 + (yc2 - yc1) * k1;
			var zm1 = zc1 + (zc2 - zc1) * k1;

			var xm2 = xc2 + (xc3 - xc2) * k2;
			var ym2 = yc2 + (yc3 - yc2) * k2;
			var zm2 = zc2 + (zc3 - zc2) * k2;
			var ctrl1 = new THREE.Vector3();
			var ctrl2 = new THREE.Vector3();
			// Resulting control points. Here smooth_value is mentioned
			// above coefficient K whose value should be in range [0...1].
			ctrl1.x = xm1 + (xc2 - xm1) * smooth_value + p1.x - xm1;
			ctrl1.y = ym1 + (yc2 - ym1) * smooth_value + p1.y - ym1;
			ctrl1.z = zm1 + (zc2 - zm1) * smooth_value + p1.z - zm1;

			ctrl2.x = xm2 + (xc2 - xm2) * smooth_value + p2.x - xm2;
			ctrl2.y = ym2 + (yc2 - ym2) * smooth_value + p2.y - ym2;
			ctrl2.z = zm2 + (zc2 - zm2) * smooth_value + p2.z - zm2;
			var ctrl: THREE.Vector3;// = new THREE.Vector3();
			ctrl = ctrl1.clone().sub(ctrl2).multiplyScalar(0.5).add(ctrl2);
			return ([p1, p2, ctrl]);
		}
	}

	export class VectorFrame {
		public static Lerp(vf0: VectorFrame, vf1: VectorFrame, t: number)
		{
			var normal = new THREE.Vector3().lerpVectors(vf0.normal, vf1.normal, t);
			var origin = new THREE.Vector3().lerpVectors(vf0.origin, vf1.origin, t);
			var tangent = new THREE.Vector3().lerpVectors(vf0.tangent, vf1.tangent, t);
			var axis = new THREE.Vector3().lerpVectors(vf0.axis, vf1.axis, t);

			var distance = MathHelper.fit01(t, vf0.distance, vf1.distance);
			var res = new VectorFrame();
			res.normal = normal;
			res.origin = origin;
			res.tangent = tangent;
			res.axis = axis;
			res.distance = distance;
			return (res);
		}

		realAxis: THREE.Vector3;
		distance: number;
		clone(): any {
			var res = new VectorFrame();
			res.normal = this.normal.clone();
			res.axis = this.axis.clone();
			res.origin = this.origin.clone();
			res.tangent = this.tangent.clone();
			res.distance = this.distance;
			return (res);
		}
		origin: THREE.Vector3; // point.
		tangent: THREE.Vector3; // derivative.
		normal: THREE.Vector3; // axis.cross(tangent);
		axis: THREE.Vector3; // (derivative(t + epsilon)).cross(derivative(t));
		public static GetQuadraticRMF(p0: THREE.Vector3, p1: THREE.Vector3, controlPoint: THREE.Vector3, steps: number = 100) {
			// attention control point au milieu ici.
			var func = function (t: number, data: THREE.Vector3[]) {
				var frame: VectorFrame = new VectorFrame();
				frame.origin = QuadraticBezierUtil.GetPoint(data[0], data[1], data[2], t);
				frame.tangent = QuadraticBezierUtil.GetFirstDerivative(data[0], data[1], data[2], t).normalize();
				frame.axis = QuadraticBezierUtil.GetFirstDerivative(data[0], data[1], data[2], t + 0.02).cross(frame.tangent).normalize();
				//console.log("AXIS:" + frame.axis.toArray());
				frame.normal = frame.axis.clone().cross(frame.tangent).normalize();
				//console.log("NORMAL:" + frame.normal.toArray());
				return (frame);
			}
			return (CubicBezierUtil.getRMF(steps, [p0, controlPoint, p1], func));
		}

		public static GetCubicRMF(p0: THREE.Vector3, p1: THREE.Vector3, controlPoint0: THREE.Vector3, controlPoint1: THREE.Vector3, steps: number = 100, startFrame: VectorFrame = undefined) {
			// attention control point au milieu ici.
			var func = function (t: number, data: THREE.Vector3[]) {
				var frame: VectorFrame = new VectorFrame();
				frame.origin = CubicBezierUtil.GetPoint(data[0], data[1], data[2], data[3], t);
				frame.tangent = CubicBezierUtil.GetFirstDerivative(data[0], data[1], data[2], data[3], t).normalize();
				frame.axis = CubicBezierUtil.GetFirstDerivative(data[0], data[1], data[2], data[3], t + 0.02).cross(frame.tangent).normalize();

				frame.normal = frame.axis.clone().cross(frame.tangent).normalize();
				return (frame);
			}
			if (startFrame != undefined)
			{
				return (CubicBezierUtil.getRmfFollow(startFrame, steps, [p0, controlPoint0, controlPoint1, p1], func));
			}
			return (CubicBezierUtil.getRMF(steps, [p0, controlPoint0, controlPoint1, p1], func));
		}
	}




}