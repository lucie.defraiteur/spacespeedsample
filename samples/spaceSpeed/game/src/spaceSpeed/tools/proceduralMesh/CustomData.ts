module GAME
{
	export type Data = number | string | number[] | string[] | THREE.Vector3[] | THREE.Color[] | THREE.Vector3 | THREE.Color | THREE.Vector2[] | THREE.Vector2;
	export type numberData = number;

	export class CustomData {
		public value: Data;
		public name: string;
		public constructor(name: string, value: Data) {
			this.name = name;
			this.value = value;
		}
		public cloneArray(array: any[]): any[] {
			var res: any[] = [];
			for (var i = 0; i < array.length; i++) {
				res.push(array[i]);
			}
			return (res);
		}
		public cloneThreeValueArray(array: THREE.Vector3[] | THREE.Vector2[] | THREE.Color[]): THREE.Vector2 | THREE.Vector3[] | THREE.Color[] {
			var res: any[] = [];
			for (var i = 0; i < array.length; i++) {
				var elem: THREE.Vector3 | THREE.Vector2 | THREE.Color = array[i].clone();
				res.push(elem);
			}
			return (res);
		}
		public clone(): CustomData {
			var res: CustomData = new CustomData(this.name, undefined);
			if (typeof this.value === "string") {
				res.value = "" + this.value;
			}
			else if (typeof this.value === "number") {
				res.value = this.value;
			}
			else if (this.value instanceof THREE.Vector3 || this.value instanceof THREE.Vector2 || this.value instanceof THREE.Color) {
				res.value = this.value.clone();
			}
			else if (this.value instanceof Array) {
				if (this.value.length == 0) {
					res.value = [];
				}
				else {
					var item = this.value[0];

					if (typeof item === "number" || typeof item == "string") {
						res.value = this.cloneArray(this.value);
					}
					else if (item instanceof THREE.Vector3 || item instanceof THREE.Vector2 || item instanceof THREE.Color) {
						res.value = this.cloneThreeValueArray(this.value as THREE.Vector3[] | THREE.Color[] | THREE.Vector2[]);
					}
				}
			}
			return (res);
		}
	}


}