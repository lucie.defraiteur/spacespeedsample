///<reference path="./Delegate.ts"/>
module GAME {
	export class LinkedListRemovedDelegate<T> extends Delegate<{ elem: LinkedList<T>, prev?: LinkedList<T>, next?: LinkedList<T>, removedRange?: boolean }>
	{

	}
	export class LinkedListInsertedDelegate<T> extends Delegate<{ elem: LinkedList<T>, prev?: LinkedList<T>, next?: LinkedList<T> }>
	{

	}
	export class LinkedListBase {
		constructor() {

		}
	}
	export class LinkedList<T> extends LinkedListBase {
		data: T = undefined;
		private _next: LinkedList<T> = undefined;
		private _prev: LinkedList<T> = undefined;
		private _onLinkedListRemovedEvent = new LinkedListRemovedDelegate<T>();
		private _onLinkedListInsertedEvent = new LinkedListInsertedDelegate<T>();
		public get OnLinkedListRemovedEvent()
		{
			return (this._onLinkedListRemovedEvent);
		}
		public get OnLinkedListInsertedEvent()
		{
			return (this._onLinkedListInsertedEvent);
		}
		notSet: boolean;
		get EndElement(): LinkedList<T> {
			var next: LinkedList<T> = this;
			while (next._next != undefined) {
				next = next._next;
			}
			return (next);
		}
		get FirstElement(): LinkedList<T> {
			var first: LinkedList<T> = this;
			while (first._prev != undefined) {
				first = first._prev;
			}
			return (first);
		}
		get Next(): LinkedList<T> {
			return (this._next);
		}
		get Prev(): LinkedList<T> {
			return (this._prev);
		}
		Remove(): LinkedList<T> {
			this._next._prev = this._prev;
			this._prev._next = this._next;
			if (this._onLinkedListRemovedEvent != undefined) {
				this._onLinkedListRemovedEvent.Invoke({ elem: this, prev: this._prev, next: this._next, removedRange: false });
			}
			return (this);
		}
		RemoveNextElements(): LinkedList<T> {
			var oldPrev = this;
			this._next = undefined;
			if (this._onLinkedListRemovedEvent != undefined) {
				this._onLinkedListRemovedEvent.Invoke({ elem: this._next, prev: this, next: undefined, removedRange: true });
			}
			return (this);
		}
		RemoveLastElements(): LinkedList<T> {
			this._prev = undefined;
			if (this._onLinkedListRemovedEvent != undefined) {
				this._onLinkedListRemovedEvent.Invoke({ elem: this._prev, prev: undefined, next: this, removedRange: true });
			}
			return (this);
		}

		static NewList<T>(): LinkedList<T> {
			return (new LinkedList<T>(undefined, true));
		}

		Insert(data: T): LinkedList<T> {
			if (this.notSet == true) {
				this.data = data;
				this.notSet = false;
				if (this._onLinkedListInsertedEvent != undefined) {
					this._onLinkedListInsertedEvent.Invoke({ elem: this });
				}
				return (this);
			}
			else {
				var old = this._next;
				this._next = new LinkedList<T>(data, false);
				this.Attach(this._next);
				old._prev = this._next;
				this._next._prev = this;
				this._next._next = old;
				if (this._onLinkedListInsertedEvent != undefined) {
					this._onLinkedListInsertedEvent.Invoke({ elem: this._next });
				}
				return (this._next);
			}
		}
		InsertBefore(data: T): LinkedList<T> {
			if (this.notSet == true) {
				this.data = data;
				this.notSet = false;
				return (this);
			}
			else {
				if (this._prev != undefined) {
					this._prev.Insert(data);
				}
				else {
					this._prev = new LinkedList<T>(data, false);
					this.Attach(this._prev);
					this._prev._next = this;
				}
				return (this._prev);
			}
		}
		private Attach(linkedList: LinkedList<T>)
		{
			linkedList._onLinkedListInsertedEvent = this._onLinkedListInsertedEvent;
			linkedList._onLinkedListRemovedEvent = this._onLinkedListRemovedEvent;
		}
		constructor(data: T, emptyList: boolean = false) {
			super();
			if (emptyList) {
				this.notSet = true;
			}
			this.data = data;
		}

	}

}