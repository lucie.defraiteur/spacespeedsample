module GAME {
	export class ScreenTakeHitEffect extends GameEntity {
		TakeBlackHit(): any {
			this.takingBlackHit = true;
			this.screenBloodMaterial.uniforms.mColor.value = new THREE.Vector4(0, 1.0, 0, 1.0);
			this.timeSinceTookDamage = 0.0;
			this.takingDamage = true;
		}

		private static screenBloodMaterial: THREE.RawShaderMaterial;
		private static screenBloodGeometry: THREE.Geometry;
		private screenBloodMaterial: THREE.RawShaderMaterial;
		private takingDamage: boolean = false;
		private takingBlackHit: boolean = false;
		private timeSinceTookDamage: number = 0;
		private TakeDamageDuration: number = 0.5;
		Start0(): void {
			if (ScreenTakeHitEffect.screenBloodMaterial == undefined) {
				var screenBloodMaterial = new THREE.RawShaderMaterial({
					fragmentShader: shaders.screen_fragment, vertexShader: shaders.screen_vertex, transparent: true,
					uniforms: {
						"mColor": { type: "c", value: new THREE.Vector4(1.0, 0.0, 0.0, 0.0) },
						"intensity": { value: 0.0 }
					}
				});
				ScreenTakeHitEffect.screenBloodMaterial = screenBloodMaterial;
				var geom = new THREE.Geometry();
				geom.vertices = [
					new THREE.Vector3(-1.0, -1.0, 1.0),
					new THREE.Vector3(1.0, -1.0, 1.0),
					new THREE.Vector3(1.0, 1.0, 1.0),
					new THREE.Vector3(-1.0, -1.0, 1.0),
					new THREE.Vector3(-1.0, 1.0, 1.0),
					new THREE.Vector3(1.0, 1.0, 1.0),
				];
				geom.faces = [
					new THREE.Face3(0, 1, 2),
					new THREE.Face3(5, 4, 3)
				]
				ScreenTakeHitEffect.screenBloodGeometry = geom;
				//		this.screenBloodMaterial = screenBloodMaterial;
			}

			this.screenBloodMaterial = ScreenTakeHitEffect.screenBloodMaterial;

			var mesh = new THREE.Mesh(ScreenTakeHitEffect.screenBloodGeometry, this.screenBloodMaterial);
			Gameplay.Main.scene.add(mesh);
			mesh.frustumCulled = false;

		}
		TakeHit() {
			this.takingDamage = true;
			this.timeSinceTookDamage = 0;
		}
		Update0(deltaTime: number): void {
			//throw new Error("Method not implemented.");
			if (this.takingDamage) {
				this.timeSinceTookDamage += deltaTime;
				var t = this.timeSinceTookDamage / this.TakeDamageDuration;
				t = t * 2;
				t = t - 1.0;
				if (t < 0) {
					t = 1.0 + t;
				}
				else {
					t = 1.0 - t;
				}
				t = plume.easing.easeInOutCubic(t);
				this.screenBloodMaterial.uniforms.intensity.value = t;
				if (this.takingBlackHit) {
					
				}
				if (this.timeSinceTookDamage > this.TakeDamageDuration) {
					this.takingDamage = false;
					if (this.takingBlackHit) {
						this.screenBloodMaterial.uniforms.mColor.value = new THREE.Vector4(1.0, 0, 0, 1.0);
					}
					this.takingBlackHit = false;
					this.screenBloodMaterial.uniforms.intensity.value = 0;

				}
				this.screenBloodMaterial.needsUpdate = true;
			}
		}
		Render0(i: number): void {
			//throw new Error("Method not implemented.");
		}
		Destroy0(): void {
			
			//throw new Error("Method not implemented.");
		}


	}
}