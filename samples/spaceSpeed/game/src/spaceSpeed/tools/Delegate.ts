module GAME {
	export abstract class Delegate<T>
	{
		private _listeners: ((args: T, instance?: any) => void)[] = [];
		private _instances: any[] = [];
		public AddListener(instance: any, listener: (args: T, instance?: any) => void) {
			this._listeners.push(listener);
			this._instances.push(instance);
		}
		public RemoveListener(instance: any, listener: (args: T, instance?: any) => void) {
			var index = this._listeners.indexOf(listener);
			while (index >= 0) {
				this._instances.splice(index, 1);
				this._listeners.splice(index, 1);
				index = this._listeners.indexOf(listener);
			}
		}
		public Invoke(args: T): void {
			for (var i = 0; i < this._listeners.length; i++) {
				this._listeners[i].call(this._instances[i], args, this._instances[i]);
			}
		}
		public RemoveAllListeners(): void {
			this._listeners = [];
			this._instances = [];
		}
	}
}