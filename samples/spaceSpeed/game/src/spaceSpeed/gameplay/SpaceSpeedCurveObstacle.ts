/// <reference path="../tools/bezierCurve/BezierCurve.ts"/>
module GAME {
	export interface CurveObstacleInfo {
		columnIndex: number;
		rowIndex: number;
		actualAngle: number;
	}
	export interface PoolItem {
		__itemInfo__:
		{
			used: boolean, index: number | string, key: number, pool: Pool<any, any, any>
		}
		OnPoolItemReset(args: any): void;
		OnPoolItemRecycle(): void;
	}
	export class Pool<T extends number | string, U extends GameEntity & PoolItem, V> extends GameEntity {
		Start0(): void { }
		Update0(deltaTime: number): void { }
		Render0(i: number): void { }
		Destroy0(): void {
			var keys = Object.keys(this._map);
			for (var i = 0; i < keys.length; i++) {
				for (var j = 0; j < this._map[keys[i]].length; j++) {
					this._map[keys[i]][j].Destroy();
				}
			}
			this._map = undefined;
			this._inactiveElements = undefined;
		}

		private _map: { [index: string]: U[] } = {};
		private _inactiveElements: { [index: string]: { [index: number]: U } } = {};

		public poolLength: number = 4;
		private _usedItems: number = 0;
		public get usedItems(): number {
			return (this._usedItems);
		}
		public get availableItems(): number {
			return (this.poolLength - this._usedItems);
		}

		public GetOne(index: T, args: V) {
			if (this._map[index] == undefined) {
				this._map[index] = [];
				this._inactiveElements[index] = {};
			}
			var keys = Object.keys(this._inactiveElements[index]);
			for (var i = 0; i < keys.length; i++) {
				var res = this._inactiveElements[index][i];
				if (res != undefined) {
					this._inactiveElements[index][i] = undefined;
					res.__itemInfo__.used = true;
					this._usedItems++;
					res.OnPoolItemReset(args);
					return (res);
				}
			}
			var key = this._map[index].length;
			var generated = this.generator(index, args);
			generated.__itemInfo__ = { used: true, index: index, key: key, pool: this };
			this._map[index].push(generated);
			this._usedItems++;
			return (generated);
		}
		public Recycle(elem: U) {
			elem.__itemInfo__.used = false;
			this._usedItems--;
			this._inactiveElements[elem.__itemInfo__.index][elem.__itemInfo__.key] = elem;
			elem.OnPoolItemRecycle();
		}

		public constructor(parent: EntityContainer, private generator: (index: string | number, args: V) => U) {
			super(parent);
		}
	}

	export interface CurveObstacleArgs { parent: BezierCurve, position: number, angle: number, mode: number }
	export class CurveObstacle extends BezierCurveObject implements PoolItem {

		__itemInfo__: {
			used: boolean; index: string | number; key: number;
			pool: Pool<number, CurveObstacle, CurveObstacleArgs>
		};
		private _physicallyActive: boolean = true;
		rotationDir: any;
		rootObj: THREE.Object3D;
		angle: number;
		SetPhysicallyActive(value: boolean): any {
			if (value != this._physicallyActive) {
				this._physicallyActive = value;
				if (value) {
					for (var i = 0; i < this.bodies.length; i++) {
						this.UpdateObjects();
						Gameplay.Main.simulation.addRigidBody(this.bodies[i].body, this.bodies[i].physicMesh, false);
					}
				}
				else {
					for (var i = 0; i < this.bodies.length; i++) {
						Gameplay.Main.simulation.removeRigidBody(this.bodies[i].body);
					}
				}
			}
		}
		public static DepthMaterial: THREE.RawShaderMaterial = new THREE.RawShaderMaterial({ vertexShader: shaders.depth_vertex, fragmentShader: shaders.depth_fragment });
		curveInfo: { curve: BezierCurve; tangent: THREE.Vector3; normal: THREE.Vector3; origin: THREE.Vector3; position: number; angle: number; };


		public angularSpeed: number = 60;
		public rotationMode: number = 0;
		public yRot0: number = 0;
		public yRot1: number = 0;
		type: SpaceSpeedBlockType2;

		public OnCurveUpdate(curve: BezierCurve): void {

		}
		Start0(): void {

		}
		Update0(deltaTime: number): void {
			if (this.__itemInfo__ == undefined || this.__itemInfo__.used) {
				var follower = Gameplay.Main.follower;
				var pos = follower.position;

				if (this._visible == true) {
					if (follower.position - this.position > 0.5) {
						//console.log("HIDING MYSELF:" + this.position);
						var ok = true;
						if (this.collidedYet == true) {
							ok = false;
						}

						if (ok) {
							if (!((Gameplay.Main.parent as GameplayScene).hud.isGameOver)) {
								Gameplay.Main.score += 1;
								(Gameplay.Main.parent as GameplayScene).hud.ScoreText.text = "" + Gameplay.Main.score;
							}
						}
						//this.SetVisible(false);
						if (this.__itemInfo__ != undefined) {
							this.__itemInfo__.pool.Recycle(this);
						}
					}

				}
				else {
					if (this.position - follower.position < 30.0 && this.position - follower.position > 0.0) {
						//console.log("SHOWING MYSELF.");

						this.SetVisible(true);
					}
				}
				if (this._visible == true) {

					if (Math.abs(follower.position - this.position) < 1.0) {
						this.SetPhysicallyActive(true);
					}

					switch (this.type) {
						case SpaceSpeedBlockType2.Bars:

							for (var i = 0; i < this.bodies.length; i++) {
								var visual = this.bodies[i].visualMesh;
								visual.rotateOnAxis(Vector3.Forward, MathHelper.ToRad(this.angularSpeed * deltaTime * this.rotationDir));
							}
							this.UpdateObjects();
							break;
						case SpaceSpeedBlockType2.Circles:
							switch (this.rotationMode) {
								case 0:
									{
										for (var i = 0; i < this.bodies.length; i++) {
											var visual = this.bodies[i].visualMesh;
											visual.rotateOnAxis(Vector3.Up, MathHelper.ToRad(this.angularSpeed * deltaTime * this.rotationDir));
										}
										this.UpdateObjects();
										break;
									}
								case 1:
									{
										//console.error("CASE 1");
										//angularSpeed = -this.angularSpeed;
										for (var i = 0; i < this.bodies.length; i++) {
											var angularSpeed = this.angularSpeed;
											var visual = this.bodies[i].visualMesh;
											var info = <CurveObstacleInfo>(visual as any).obstacleInfo;
											if (Math.floor(info.columnIndex / 2) % 2 == 1) {
												angularSpeed *= -1;
											}
											visual.rotateOnAxis(Vector3.Up, MathHelper.ToRad(angularSpeed * deltaTime * this.rotationDir));
										}
										this.UpdateObjects();
										break;
									}
								case 2:
									{
										//console.error("CASE 1");
										//angularSpeed = -this.angularSpeed;
										for (var i = 0; i < this.bodies.length; i++) {
											var angularSpeed = this.angularSpeed;
											var visual = this.bodies[i].visualMesh;
											var info = <CurveObstacleInfo>(visual as any).obstacleInfo;
											if (info.columnIndex % 2 == 1) {
												angularSpeed *= -1;
											}
											visual.rotateOnAxis(Vector3.Up, MathHelper.ToRad(angularSpeed * deltaTime * this.rotationDir));
										}
										this.UpdateObjects();
										break;
									}
							}

							break;
						case SpaceSpeedBlockType2.Cross:
							switch (this.rotationMode) {
								case 0:
									{
										for (var i = 0; i < this.bodies.length; i++) {
											var visual = this.bodies[i].visualMesh;
											visual.rotateOnAxis(Vector3.Up, MathHelper.ToRad(this.angularSpeed * deltaTime * this.rotationDir));
										}
										this.UpdateObjects();
										break;
									}

								case 3:
									{
										//console.error("CASE 1");
										//angularSpeed = -this.angularSpeed;
										for (var i = 0; i < this.bodies.length; i++) {
											var angularSpeed = this.angularSpeed;
											var visual = this.bodies[i].visualMesh;
											var info = <CurveObstacleInfo>(visual as any).obstacleInfo;
											if (Math.floor(info.columnIndex / 2) % 2 == 0) {
												//angularSpeed = 0;
											}
											else {
												var diff = angularSpeed * deltaTime;
												info.actualAngle += diff;
												var maxAngle = 75.0;
												if (info.actualAngle >= (maxAngle)) {

													diff = -diff;// 180.0;
												}
												if (info.actualAngle >= maxAngle * 2.0) {
													info.actualAngle = 0;
												}

												visual.rotateOnAxis(Vector3.Up, MathHelper.ToRad(-diff));
											}

										}
										this.UpdateObjects();
										break;
									}

								case 4:
									{
										//console.error("CASE 1");
										//angularSpeed = -this.angularSpeed;
										for (var i = 0; i < this.bodies.length; i++) {
											var angularSpeed = this.angularSpeed;
											var visual = this.bodies[i].visualMesh;
											var info = <CurveObstacleInfo>(visual as any).obstacleInfo;
											if (Math.floor(info.columnIndex / 2) % 2 == 1) {
												//angularSpeed = 0;
											}
											else {
												var diff = angularSpeed * deltaTime;
												info.actualAngle += diff;
												var maxAngle = 75.0;
												if (info.actualAngle >= (maxAngle)) {

													diff = -diff;// 180.0;
												}
												if (info.actualAngle >= maxAngle * 2.0) {
													info.actualAngle = 0;
												}

												visual.rotateOnAxis(Vector3.Up, MathHelper.ToRad(diff));
											}

										}
										this.UpdateObjects();
										break;
									}
							}

							break;
						default:

							break;

					}
				}
			}
		}
		Render0(i: number): void {

		}
		Destroy0(): void {
			//	
			/*	for (var i = 0; i < this.bodies.length; i++) {
					if (this.bodies[i].physicMesh.parent != undefined) {
						this.bodies[i].physicMesh.parent.remove(this.bodies[i].physicMesh);
					}
					if (this.bodies[i].visualMesh.parent != undefined) {
						this.bodies[i].visualMesh.parent.remove(this.bodies[i].visualMesh);
					}
				}*/
		}
		private _visible: boolean = true;
		SetVisible(value: boolean) {

			if (this._visible != value) {

				this.visualRoot.visible = value;
				this._visible = value;
				if (!value) {
					for (var i = 0; i < this.bodies.length; i++) {

						Gameplay.Main.follower.customScene.remove(this.bodies[i].physicMesh);
					}
					this.SetPhysicallyActive(false);
				}
				else {
					//console.log("SET AS VISIBLE.");
					for (var i = 0; i < this.bodies.length; i++) {

						Gameplay.Main.follower.customScene.add(this.bodies[i].physicMesh);
					}
					this.UpdateObjects();
				}
			}
		
		}
		visualRoot: THREE.Object3D;
		bodies: { body: CANNON.Body, visualMesh: THREE.Mesh, physicMesh: THREE.Mesh, initialRotation: THREE.Quaternion }[] = [];
		UpdateObjects(forceUpdate: boolean = false) {
			var doneObjs: { [index: string]: boolean } = {};

			for (var i = 0; i < this.bodies.length; i++) {
				//console.error("ITERATION:" + i);
				var clone = this.bodies[i].physicMesh;
				var obj = this.bodies[i].visualMesh;
				//customScene.add(clone);
				/*var parents: THREE.Object3D[] = [obj, obj.parent];
				//var current: THREE.Object3D = obj;
				
				parents = parents.reverse();
				for (var k = 0; k < parents.length; k++) {
					if (!(doneObjs[parents[k].uuid])) {
						parents[k].updateMatrixWorld(false);
						doneObjs[parents[k].uuid] = true;
					}
				}*/
				if (forceUpdate) {
					obj.updateMatrixWorld(true);
				}
				var worldPos = obj.getWorldPosition(new THREE.Vector3());
				var worldQuat = obj.getWorldQuaternion(new THREE.Quaternion());
				clone.position.copy(worldPos);
				clone.quaternion.copy(worldQuat);
				if (forceUpdate)
				{
					clone.updateMatrixWorld(true);
				}
				//clone.matrixWorldNeedsUpdate = true;
				var body = this.bodies[i].body;
				Gameplay.Main.simulation.updatePositionAndRotation(body, worldPos, worldQuat);
			}
		}
		OnPoolItemReset(args: CurveObstacleArgs) {
			for (var i = 0; i < this.bodies.length; i++) {
				this.bodies[i].visualMesh.quaternion.copy(this.bodies[i].initialRotation.clone());

				(this.bodies[i].visualMesh as any).obstacleInfo.actualAngle = 0;
			}
			this.collidedYet = false;
			this.rotationMode = args.mode;
			this.rotationDir = MathHelper.randomDirection();
			this.position = args.position;
			this.angle = args.angle;
			Gameplay.Main.scene.add(this.rootObj);
			this.SetParent(args.parent);
			var curveInfo = (this.parent as BezierCurve).PlaceOnCurve(this.rootObj, this.position, this.angle);
			this.SetParent(curveInfo.curve);
			this.curveInfo = curveInfo;
			this.SetVisible(false);
			

		}
		OnPoolItemRecycle(): void {
			this.SetVisible(false);
			this.RemoveFromParent();
		}
		constructor(root: THREE.Object3D, position: number, angle: number, mode: number, type: SpaceSpeedBlockType2, parent: BezierCurve) {
			super(parent);

			this.rootObj = root;
			this.type = type;
			this.angle = angle;
			var meshes: THREE.Mesh[] = [];
			var customScene = Gameplay.Main.follower.customScene;
			var doneObjs: { [index: string]: boolean } = {};
			this.visualRoot = root;
			root.traverse((obj: THREE.Object3D) => {
				if (obj instanceof THREE.Mesh) {
					var clone = obj.clone();
					clone.material = CurveObstacle.DepthMaterial;
					customScene.add(clone);
					var parents: THREE.Object3D[] = [];
					var current: THREE.Object3D = obj;
					while (current != undefined) {
						parents.push(current);
						current = current.parent;
					}
					parents = parents.reverse();
					for (var i = 0; i < parents.length; i++) {
						if (!(doneObjs[parents[i].uuid])) {
							parents[i].updateMatrixWorld(true);
							doneObjs[parents[i].uuid] = true;
						}
					}
					var worldPos = obj.getWorldPosition(new THREE.Vector3());
					var worldQuat = obj.getWorldQuaternion(new THREE.Quaternion());

					clone.position.copy(worldPos);
					clone.quaternion.copy(worldQuat);

					var body = plume.CannonBodyBuilder.newTriMesh(obj.geometry);
					Gameplay.Main.simulation.addRigidBody(body, clone, false);

					(body as any).obstacle = this;
					this.bodies.push({ body: body, physicMesh: clone, visualMesh: obj, initialRotation: obj.quaternion.clone() });
				}
			})
			this.SetVisible(false);


			this.OnPoolItemReset({ parent: parent, angle: angle, position: position, mode: mode });



		}
		public collidedYet: boolean = false;

	}

}