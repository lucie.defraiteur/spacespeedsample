module GAME {
	export interface IGameEntity {
		Start(): void;
		Update(deltaTime: number): void;
		Render(i: number): void;
		Destroy(): void;
	}
	export interface EntityContainer {
		entities: IGameEntity[];
		parent?: EntityContainer;
		Root(): EntityContainer;
	}
	export abstract class GameEntity implements IGameEntity {
		RemoveFromParent()
		{
			this.SetParent(undefined);
		}
		public SetParent(parent: EntityContainer) {
			if (this.parent != undefined) {
				var index = this.parent.entities.indexOf(this);
				if (index != -1) {
					this.parent.entities.splice(index, 1);
				}
				this.parent = undefined;
			}
			this.parent = parent;
			if (this.parent != undefined) {
				this.parent.entities.push(this);
			}
		}
		public Root(): EntityContainer {
			var current: EntityContainer = this;
			while (current.parent != undefined) {
				current = current.parent;
			}
			return (current);
		}
		abstract Start0(): void
		Start(): void {
			this.Start0();
			for (var i = 0; i < this.entities.length; i++) {
				this.entities[i].Start();
			}
		}
		abstract Update0(deltaTime: number): void;
		Update(deltaTime: number): void {
			this.Update0(deltaTime);
			for (var i = 0; i < this.entities.length; i++) {
				this.entities[i].Update(deltaTime);
			}
		}
		abstract Render0(i: number): void;
		Render(i: number): void {
			this.Render0(i);
			for (var i = 0; i < this.entities.length; i++) {
				this.entities[i].Render(i);
			}
		}
		abstract Destroy0(): void;
		Destroy(): void {
			this.Destroy0();
			var slice = this.entities.slice();
			for (var i = 0; i < slice.length; i++) {
				slice[i].Destroy();
			}
			this.entities = [];
			var index = this.parent.entities.indexOf(this);
			if (index != -1) {
				this.parent.entities.splice(index, 1);
			}
			this.parent = undefined;

		}
		parent: EntityContainer = undefined;
		entities: IGameEntity[] = [];
		constructor(parent: EntityContainer) {
			this.parent = parent;
			if (this.parent != undefined) {
				this.parent.entities.push(this);
			}
		}
	}
	export interface Updatable {
		Update(dt: number): void;
	}
}