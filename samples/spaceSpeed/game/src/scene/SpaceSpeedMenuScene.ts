
module GAME {

    export class MenuScene extends plume.GuiScene {


        private static _firstBuild = true;

        private started = false;

        private engine: plume.Engine;
        private scene: THREE.Scene;

        private stars: Array<THREE.Mesh> = [];

        private arwingMoveStart = 0;
        private arwingMoveDuration = 0;
        private arwing: THREE.Mesh;
        private arwingCurveIndex = 0;
        private arwingCurve: THREE.CubicBezierCurve3;
		_versionText: plume.BitmapText;

        constructor() {
            super();

            let self = this;

            this.scene = new THREE.Scene();
            this.engine = getEngine();
            this.engine.scene = this.scene;

            let logo = factory.newText("Warp Zone Infinite", 120);
            logo.y = 200;
            this.addChild(logo);
            layout.centerOnXInParent(logo);

            let playButton = factory.newButton(300, 100);
            playButton.setTextColored("Play", 128);
            playButton.y = 600;
            this.addChild(playButton);
			layout.centerOnXInParent(playButton);
			let creditsButton = factory.newButton(200, 65);
            creditsButton.setTextColored("Credits", 55);
			creditsButton.y = 25;
			
            this.addChild(creditsButton);
	//		layout.centerOnXInParent(creditsButton);
			creditsButton.x = Game.get().width - creditsButton.width - 25;
			this._versionText = factory.newText(Game.get().config.gameVersion, 56);
			this.addChild(this._versionText);
			this._versionText.x = (Game.get().width) - (this._versionText.width / 2.0 + 15);
			this._versionText.y = (Game.get().height) - 56;

         

            playButton.onClick(function () {
				//game.deviceManager.fullscreenManager.requestFullScreen();
                self._startGame();
			});
			creditsButton.onClick(function(){
				self._showCredits();
			})

			this._create3dscene();
        }

        update(ts: number) {
            super.update(ts);

        }

        render(interpolation: number) {
            super.render(interpolation);
        }

        protected onShow() {
            // score
        }

        private _firstInit() {
        }

        private _create3dscene() {
            // fly mode ?
            let light = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.6);
            this.scene.add(light);
            let camera = this.engine.camera;
            camera.position.set(0, 0, 10);
            camera.lookAt(0, 0, 0);
            camera.far = 1000;
            camera.updateProjectionMatrix();
        }

        private _startGame() {
			
            if (this.started) return;
			
            this.started = true;
			var scene = new GameplayScene();
			getGame().switchScene(scene);
			scene.start();
			
		}
		private _showCredits(): any {
			var scene = new CreditsScene();
			getGame().switchScene(scene);
		}

    }


}