
module GAME {

    export class CreditsScene extends plume.GuiScene {

        private static _firstBuild = true;

        private started = false;

        private engine: plume.Engine;
        private scene: THREE.Scene;

        private stars: Array<THREE.Mesh> = [];

        private arwingMoveStart = 0;
        private arwingMoveDuration = 0;
        private arwing: THREE.Mesh;
        private arwingCurveIndex = 0;
        private arwingCurve: THREE.CubicBezierCurve3;
		_versionText: plume.BitmapText;
		private textSpeed: number = 75.0;
		private textOrigin: number = 0.0;
		text: plume.BitmapText;
		textDisplacement: number = 0;
        constructor() {
            super();

            let self = this;

            this.scene = new THREE.Scene();
            this.engine = getEngine();
            this.engine.scene = this.scene;

            let logo = factory.newText("Warp Zone Infinite", 85);
            logo.y = 100;
            this.addChild(logo);
            layout.centerOnXInParent(logo);

           /* let playButton = factory.newButton(300, 100);
            playButton.setTextColored("Play", 128);
            playButton.y = 600;
            this.addChild(playButton);
			layout.centerOnXInParent(playButton);*/
			let creditsButton = factory.newButton(200, 65);
            creditsButton.setTextColored("Back", 55);
			creditsButton.y = 25;
			
            this.addChild(creditsButton);
	//		layout.centerOnXInParent(creditsButton);
			creditsButton.x = 25;
			this._versionText = factory.newText(Game.get().config.gameVersion, 56);
			this.addChild(this._versionText);
			this._versionText.x = (Game.get().width) - (this._versionText.width / 2.0 + 15);
			this._versionText.y = (Game.get().height) - 56;

			creditsButton.onClick(function(){
				self._showMenu();
			})
			let textParent = factory.newContainer(true);
			textParent.mask = new plume.Mask();
			textParent.mask.rectangle(0, logo.y + logo.height, Game.get().width, 500);
			let text = factory.newText(`
Publisher - Gamebuilt (C) 2018








Technical Artist / Level Designer / Game Developper
						- 
Gonzague Defraiteur


-----------------------------


Procedural Environnement Artist
-
Gonzague Defraiteur



-----------------------------


Effects / Shaders
-
Gonzague Defraiteur


-----------------------------


Icons Designs
-
Simon Talviste


-----------------------------


Core Systems Developpper
-
Mathieu Barbier


-----------------------------

Musics
-

-- bensound-dubstep 

Music by BENSOUND
http://www.bensound.com/royalty-free-...
Creative Commons — Attribution 3.0 Unported— CC BY 3.0 
http://creativecommons.org/licenses/b...
Music promoted by Audio Library 
https://youtu.be/18D3KxOtnZc


-- Reatch - Reatch - Funk City - 01 Funk City

Funk City by Reatch https://soundcloud.com/reatch
Creative Commons — Attribution 3.0 Unported— CC BY 3.0 
http://creativecommons.org/licenses/b...
Music promoted by Audio Library
https://youtu.be/J5JZNdb50B8



			
			
			`, 55, 0xAAAAAA);
			this.text = text;
			this.text.align = "center";
		
			/*text.mask = new plume.Mask();
			text.mask.rectangle(0, 0, Game.get().width, 400);
			*/
			
			text.y = 200 + logo.height + 350;
			this.textOrigin = text.y;
			this.addChild(textParent);
			textParent.addChild(text);
            layout.centerOnXInParent(text);
            
			this._create3dscene();
        }

        update(ts: number) {
            super.update(ts);
			var dt = ts / 1000.0;
			this.textDisplacement += dt * this.textSpeed;
			this.text.y = this.textOrigin - this.textDisplacement;
			if (this.textDisplacement > this.text.height + 200)
			{
				this.textDisplacement = 0;
			}
			
        }

        render(interpolation: number) {
            super.render(interpolation);
		}
		private _showMenu(): any {
			var scene = new MenuScene();
			getGame().switchScene(scene);
		}

        protected onShow() {
            // score
        }

        private _firstInit() {
        }

        private _create3dscene() {
            // fly mode ?
            let light = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.6);
            this.scene.add(light);
            let camera = this.engine.camera;
            camera.position.set(0, 0, 10);
            camera.lookAt(0, 0, 0);
            camera.far = 1000;
            camera.updateProjectionMatrix();
        }

        private _startGame() {
			
            if (this.started) return;
			
            this.started = true;
			var scene = new GameplayScene();
			getGame().switchScene(scene);
			scene.start();
			
        }

    }


}