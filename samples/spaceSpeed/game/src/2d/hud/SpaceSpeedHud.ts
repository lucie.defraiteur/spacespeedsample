module GAME {


	export class Hud extends plume.Container {


		private static INSTANCE: Hud = null;
		_leftText: plume.BitmapText;
		_rightText: plume.BitmapText;
		_button: plume.ImageButton;
		lifeLeftText: plume.BitmapText;
		ScoreText: plume.BitmapText;
		ScoreText2: plume.BitmapText;
		ScoreText3: plume.BitmapText;
		speedText: plume.BitmapText;
		static get() {
			if (this.INSTANCE == null) {
				this.INSTANCE = new Hud();
			}
			return this.INSTANCE;
		}

		private _hover: plume.Container;
		private tweenManager: plume.TweenManager;



		protected shadowTab: plume.Sprite;
		gameArea: number;


		protected _gameOver: boolean;

		protected bound3D: plume.Rectangle;

		protected tmpPosHud: THREE.Vector3;

		protected btnAdd: plume.Sprite;

		protected stop: boolean;
		private gameOverDuration: number = 3.0;
		private gameOverColor: number = 0xEE1122;
		private standardColor: number = 0xCCAA55;
		public isGameOver: boolean = false;
		private timeSinceGameover: number = 0.0;
		private tiltPerSecond: number = 2.0;
		private timeSinceLastTilt: number = 0.0;
		private tiltValue: boolean = true;
		private get tiltDuration(): number {
			return (1.0 / this.tiltPerSecond);
		}
		public maxSpeedColor = 0xEF2211;
		public middleSpeedColor = 0xEF9922;
		public minSpeedColor = 0x99FFAA;

		myRank: number = 0;
		myScore: number = 0;

		constructor() {
			super();

			this.btnAdd = null;
			this._gameOver = false;
			Hud.INSTANCE = this;
			this.tmpPosHud = new THREE.Vector3();
			this.bound3D = gui.expandedBounds;





			this.tweenManager = new plume.TweenManager();

			let bounds = gui.expandedBounds;

			this._hover = new plume.Container(bounds.width, bounds.height);
			this._hover.x = bounds.x;
			this._hover.y = bounds.y;
			this.addChild(this._hover);

			this._leftText = factory.newText("((", 96);

			this._rightText = factory.newText("))", 96);
			var gameplay = Gameplay.Main;


			var xOffset = 50;
			var yOffset = -100;









			this._button = plume.ImageButton.fromFrames(Assets.BUTTONS_BUTTON_PAUSE, Assets.BUTTONS_BUTTON_PAUSE, Assets.BUTTONS_BUTTON_PAUSE);
			this.addChild(this._button);
			this._button.x = (this._button.width / 2.0);
			this._button.y = (200 - (this._button.height / 2.0));
			this._button.onStateOver = function () {

			}
			this._button.interactive = true;
			
			this._button.onClick(function () {
			
				GameplayScene.get().paused = !(GameplayScene.get().paused);
				//SoundManager.p
				if (GameplayScene.get().paused)
				{
					SoundManager.pause();
				}
				else
				{
					SoundManager.resume();
				}
			})

			var icon = factory.newSprite(Assets.ICONS_SHIELD);
			this.addChild(icon);
			icon.x = ((Game.get().width) - 100) - (icon.width / 2.0);
			icon.y = (100 - (icon.height / 2.0));
			var lifeLeftText = factory.newText("5", 75, 0xDDDD99);
			this.lifeLeftText = lifeLeftText;
			lifeLeftText.x = icon.x + (icon.width / 2.0) - (lifeLeftText.width / 2.0);
			lifeLeftText.y = icon.y + (icon.height / 2.0) - (lifeLeftText.height / 2.0);
			this.addChild(lifeLeftText);


			var icon = factory.newSprite(Assets.ICONS_SPEED);
			this.addChild(icon);
			icon.scaleXY = 0.7;
			icon.scaleX = 0.8;
			icon.x = ((Game.get().width) - 100) - (icon.width / 2.0);
			icon.y = (200 - (icon.height / 2.0));
			
			var speedText = factory.newText("100%", 55 * icon.scaleXY, 0x99FFAA);
			this.speedText = speedText;
			speedText.x = icon.x + (icon.width / 2.0) - (speedText.width / 2.0) + 20 * icon.scaleXY;
			speedText.y = icon.y + (icon.height / 2.0) - (speedText.height / 2.0) + 5 * icon.scaleXY;
			this.addChild(speedText);


			var ScoreText = factory.newText("0", 100, 0xCCAA55);
			ScoreText.x = ((Game.get().width / 2.0)) - (ScoreText.width / 2.0);
			ScoreText.y = (100 - (ScoreText.height / 2.0));

			this.addChild(ScoreText);
			this.ScoreText = ScoreText;



			var ScoreText2 = factory.newText("80", 140, 0x8877CC);
			ScoreText2.x = ((Game.get().width / 2.0)) - (ScoreText2.width / 2.0);
			ScoreText2.y = (((Game.get().height / 2.0)) - (ScoreText2.height / 2.0));


			this.addChild(ScoreText2);
			this.ScoreText2 = ScoreText2;
			this.ScoreText2.visible = false;

			var ScoreText3 = factory.newText("!", 140, 0xBBCC22);
			ScoreText3.x = ((Game.get().width / 2.0) + (ScoreText2.width / 2.0));
			ScoreText3.y = (((Game.get().height / 2.0)) - (ScoreText3.height / 2.0));

			this.addChild(ScoreText3);
			this.ScoreText3 = ScoreText3;
			this.ScoreText3.visible = false;
			var self = this;
			this._rightText.visible = true;
			this._leftText.visible = true;
			window.setTimeout(function () { self._rightText.visible = false; self._leftText.visible = false; }, 5000);

			this._leftText.x = 45 - (this._rightText.width / 2.0);
			this._rightText.x = Game.get().width - (45 + this._rightText.width / 2.0);
			this._leftText.y = (Game.get().height) - 200;
			this._rightText.y = (Game.get().height) - 200;

			this.addChild(this._leftText);
			this.addChild(this._rightText);

		}
		get hover(): plume.Container {
			return this._hover;
		}
		protected move(dest: number, current: number, delta: number) {
			return current + (dest - current) * 7 * delta * 0.001;
		}
		public CallGameOver(score: number)
		{
			this.timeSinceGameover = 0;
			this.timeSinceLastTilt = 0;
			this.tiltValue = true;
			this.ScoreText2.visible = true;
			this.ScoreText3.visible = true;
			this.isGameOver = true;
			this.ScoreText2.text = "" + score;
			this.ScoreText3.x = ((Game.get().width / 2.0) + (this.ScoreText2.width / 2.0) + 10);
			this.lifeLeftText.text = "0";
			this.lifeLeftText.tint = this.gameOverColor;
			
		}
		update(delta: number) {
			if (this.isGameOver && (!(GameplayScene.get().paused))) {
				this.timeSinceGameover += (delta / 1000.0);
				this.timeSinceLastTilt += (delta / 1000.0);
				while (this.timeSinceLastTilt >= this.tiltDuration)
				{
					this.timeSinceLastTilt -= this.tiltDuration;
					this.tiltValue = !this.tiltValue;
	//				this.ScoreText2.visible = this.tiltValue;
					this.ScoreText3.visible = this.tiltValue;
				}
				if (this.timeSinceGameover >= this.gameOverDuration)
				{
					this.isGameOver = false;
					this.ScoreText2.visible = false;
					this.ScoreText3.visible = false;
					this.tiltValue = true;
					this.lifeLeftText.tint = this.standardColor;
					Gameplay.Main.lifeLeft = 5;
					Gameplay.Main.score = 0;
					this.ScoreText.text = "" + 0;
					this.lifeLeftText.text = "5";
				}
			}
		}
	}
}