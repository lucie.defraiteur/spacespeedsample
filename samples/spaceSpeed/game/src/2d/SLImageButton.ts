module GAME {

    export class ImageButton extends plume.ImageButton {

        private _textG: plume.BitmapText;
        private _textR: plume.BitmapText;

        protected setStateTexture() {
            super.setStateTexture();

            this._textG.visible = false;
            this._textR.visible = false;

            if (this._down || this._over) this._textR.visible = true;
            else this._textG.visible = true;
        }

        setTextColored(text: string, size: number) {
            if (this._textG != null) this._textG.removeSelf();
            if (this._textR != null) this._textR.removeSelf();

            this._textG = this.addText(text, 0x00FF00, size);
            this._textR = this.addText(text, Colors.Red, size);

            this._textR.visible = false;
        }

        private addText(text: string, color: number, size: number) {
            let t = factory.newText(text, size, color);
            this.addChild(t);
            layout.centerOnX(t, this, false);
            layout.centerOnY(t, this, false);
            return t;
        }
    }

}