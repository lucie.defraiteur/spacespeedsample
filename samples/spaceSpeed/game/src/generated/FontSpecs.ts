// this file is auto-generated.
module GAME.fonts {

	export var fonts : Array<plume.FontSpec> = [];
	var face0 : plume.FontSpec = {
		face: "arcade",
		size: 256,
		lineHeight: 256,
		chars: [
			{
				id: 97,
				x: 2,
				y: 2,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 98,
				x: 124,
				y: 2,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 99,
				x: 2,
				y: 91,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 100,
				x: 246,
				y: 2,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 101,
				x: 368,
				y: 2,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 102,
				x: 490,
				y: 2,
				width: 87,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 100
			}
			,
			{
				id: 103,
				x: 368,
				y: 91,
				width: 120,
				height: 121,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 104,
				x: 124,
				y: 124,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 105,
				x: 2,
				y: 180,
				width: 36,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 48
			}
			,
			{
				id: 106,
				x: 40,
				y: 180,
				width: 69,
				height: 154,
				xoffset: 0,
				yoffset: 0,
				xadvance: 82
			}
			,
			{
				id: 107,
				x: 246,
				y: 124,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 108,
				x: 2,
				y: 302,
				width: 36,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 48
			}
			,
			{
				id: 109,
				x: 579,
				y: 2,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 110,
				x: 701,
				y: 2,
				width: 103,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 116
			}
			,
			{
				id: 111,
				x: 806,
				y: 2,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 112,
				x: 579,
				y: 91,
				width: 120,
				height: 121,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 113,
				x: 701,
				y: 91,
				width: 120,
				height: 121,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 114,
				x: 490,
				y: 124,
				width: 87,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 99
			}
			,
			{
				id: 115,
				x: 823,
				y: 91,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 116,
				x: 490,
				y: 213,
				width: 87,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 99
			}
			,
			{
				id: 117,
				x: 351,
				y: 214,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 118,
				x: 823,
				y: 180,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 119,
				x: 111,
				y: 246,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 120,
				x: 579,
				y: 214,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 121,
				x: 701,
				y: 214,
				width: 120,
				height: 121,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 122,
				x: 579,
				y: 303,
				width: 120,
				height: 87,
				xoffset: 0,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 65,
				x: 823,
				y: 269,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 66,
				x: 701,
				y: 337,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 67,
				x: 823,
				y: 391,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 68,
				x: 233,
				y: 303,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 69,
				x: 111,
				y: 335,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 70,
				x: 355,
				y: 303,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 71,
				x: 477,
				y: 392,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 72,
				x: 599,
				y: 459,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 73,
				x: 2,
				y: 424,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 74,
				x: 233,
				y: 425,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 75,
				x: 355,
				y: 425,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 76,
				x: 477,
				y: 514,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 77,
				x: 107,
				y: 457,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 78,
				x: 229,
				y: 547,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 79,
				x: 351,
				y: 547,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 80,
				x: 2,
				y: 579,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 81,
				x: 721,
				y: 513,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 82,
				x: 599,
				y: 581,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 83,
				x: 473,
				y: 636,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 84,
				x: 124,
				y: 579,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 85,
				x: 843,
				y: 513,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 86,
				x: 229,
				y: 669,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 87,
				x: 351,
				y: 669,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 88,
				x: 2,
				y: 701,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 89,
				x: 124,
				y: 701,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 90,
				x: 721,
				y: 635,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 33,
				x: 595,
				y: 703,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 59,
				x: 965,
				y: 2,
				width: 36,
				height: 103,
				xoffset: 0,
				yoffset: 17,
				xadvance: 51
			}
			,
			{
				id: 37,
				x: 473,
				y: 758,
				width: 103,
				height: 87,
				xoffset: 0,
				yoffset: 17,
				xadvance: 133
			}
			,
			{
				id: 58,
				x: 965,
				y: 107,
				width: 36,
				height: 87,
				xoffset: 0,
				yoffset: 17,
				xadvance: 51
			}
			,
			{
				id: 63,
				x: 843,
				y: 635,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 42,
				x: 477,
				y: 335,
				width: 52,
				height: 53,
				xoffset: 17,
				yoffset: 34,
				xadvance: 133
			}
			,
			{
				id: 40,
				x: 948,
				y: 635,
				width: 69,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 41,
				x: 945,
				y: 196,
				width: 69,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 43,
				x: 229,
				y: 791,
				width: 103,
				height: 87,
				xoffset: 0,
				yoffset: 17,
				xadvance: 116
			}
			,
			{
				id: 45,
				x: 2,
				y: 546,
				width: 87,
				height: 19,
				xoffset: 0,
				yoffset: 51,
				xadvance: 102
			}
			,
			{
				id: 61,
				x: 233,
				y: 246,
				width: 87,
				height: 53,
				xoffset: 0,
				yoffset: 34,
				xadvance: 102
			}
			,
			{
				id: 46,
				x: 531,
				y: 335,
				width: 36,
				height: 36,
				xoffset: 34,
				yoffset: 102,
				xadvance: 82
			}
			,
			{
				id: 44,
				x: 965,
				y: 318,
				width: 52,
				height: 53,
				xoffset: 17,
				yoffset: 102,
				xadvance: 82
			}
			,
			{
				id: 47,
				x: 334,
				y: 791,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 34,
				x: 721,
				y: 459,
				width: 87,
				height: 52,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 39,
				x: 599,
				y: 392,
				width: 36,
				height: 52,
				xoffset: 0,
				yoffset: 0,
				xadvance: 89
			}
			,
			{
				id: 64,
				x: 2,
				y: 823,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 35,
				x: 124,
				y: 880,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 36,
				x: 945,
				y: 373,
				width: 69,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 38,
				x: 2,
				y: 945,
				width: 87,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 108
			}
			,
			{
				id: 91,
				x: 965,
				y: 495,
				width: 52,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 78
			}
			,
			{
				id: 93,
				x: 246,
				y: 880,
				width: 52,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 82
			}
			,
			{
				id: 48,
				x: 717,
				y: 757,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 116
			}
			,
			{
				id: 49,
				x: 578,
				y: 825,
				width: 103,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 116
			}
			,
			{
				id: 50,
				x: 439,
				y: 847,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 51,
				x: 300,
				y: 913,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 52,
				x: 822,
				y: 757,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 53,
				x: 91,
				y: 1002,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 54,
				x: 683,
				y: 879,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 55,
				x: 561,
				y: 947,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 56,
				x: 422,
				y: 969,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 57,
				x: 213,
				y: 1035,
				width: 120,
				height: 120,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
			,
			{
				id: 32,
				x: 0,
				y: 0,
				width: 0,
				height: 0,
				xoffset: 0,
				yoffset: 0,
				xadvance: 133
			}
		]
	}
	fonts.push(face0)
	
}
