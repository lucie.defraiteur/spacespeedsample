// this file is auto-generated (grunt assets).
module GAME {
	export const enum Assets {
		ARCADE = "arcade.png",
		BUTTONS_BUTTON_JUMP = "buttons/button_jump.png",
		BUTTONS_BUTTON_MOVE = "buttons/button_move.png",
		BUTTONS_BUTTON_PAUSE = "buttons/button_pause.png",
		BUTTONS_BUTTON_SHOOT = "buttons/button_shoot.png",
		ICONS_SHIELD = "icons/shield.png",
		ICONS_SPEED = "icons/speed.png",
		SPLASHSCREEN = "splashScreen.jpg",
		SPLASHSCREEN_OLD = "splashscreen_old.png",
	}
}
