/// <reference path="threejs/r96/index.d.ts" />

declare namespace THREEEXT.Math {
    export function lerp(x: number, y: number, t: number): number;
}
declare namespace THREEEXT {
    export class Vector3 {
        static lerpVectors(v1: THREE.Vector3, v2: THREE.Vector3, alpha: number): THREE.Vector3;
	}

}