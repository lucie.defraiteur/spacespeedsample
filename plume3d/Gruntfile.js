/* global module */

module.exports = function (grunt) {
    'use strict';

    // Create 2 tasks for each target
    var allSubproject = ['core', 'gui', 'tween', 'trail', 'ammo', 'cannon', 'gltf', 'particle'];
    var allCompileTask = [];
    var allWatchTask = [];
    var tscConfig = {};

    allCompileTask.push("glsl:all");

    for (var k in allSubproject) {
        var name = allSubproject[k];
        tscConfig["compile-" + name] = {
            options: {
                project: "src/" + name
            }
        }
        tscConfig["watch-" + name] = {
            options: {
                project: "src/" + name,
                watch: true
            }
        }
        allCompileTask.push("tsc:compile-" + name);
        allWatchTask.push("tsc:watch-" + name);
    }

    // task to build tslint rules
    tscConfig["compile-tslint-rules"] = {
        options: {
            project: "tools/tslint/rules/src"
        }
    }

    //grunt.log.writeln('tscConfig == ' + JSON.stringify(tscConfig));

    grunt.initConfig({
        tsc: tscConfig,
        clean: {
            pre: ['build', 'bin', 'obj']
        },
        concurrent: {
            all: {
                tasks: allWatchTask,
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        tslint: {
            options: {
                configuration: "tslint.json",
            },
            core: {
                src: ["src/**/*.ts"]
            }
        },
        copy: {
            all: {
                expand: true,
                cwd: 'build/latest',
                src: ['**/*.js', '!**/*.min.js', '**/*.d.ts', '**/*.js.map'],
                dest: 'export/latest',
            },
        },
    });

    var watch = grunt.option('watch');

    grunt.task.registerTask('dev', ['clean:pre'].concat(allCompileTask).concat(['tslint:core', 'concurrent:all'])); // compile all project and watch
    grunt.task.registerTask('default', ['clean:pre'].concat(allCompileTask).concat(['tslint:core']));
    grunt.task.registerTask('release', ['default', 'copy:all']); // final build saved in 'export' (export is commited)

    // if grunt --watch, default is dev
    if (watch != null) {
        grunt.task.registerTask('default', ['dev']);
    }

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks("grunt-tslint");

    grunt.loadTasks('../build/grunt');


    grunt.config('glsl', {
        all: {
            inputs: ["shaders/core", "shaders/particle", "shaders/trail"],
            namespace: ["plume.core", "plume.particle", "plume.trail"],
            output: ["src/core/generated", "src/particle/generated", "src/trail/generated"]
        }
    });

    grunt.config('watch', {
        glsl: {
            files: ["shaders/**"],
            tasks: ["glsl:all"]
        },
    });
};
