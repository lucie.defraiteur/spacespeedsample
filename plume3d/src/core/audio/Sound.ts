﻿module plume {

    export class Sound extends AbstractSound {

        constructor(howl: Howl) {
            super(howl);
        }

        play(sprite: string, onEndCallback?: Function): number {
            return super.play0(sprite, onEndCallback);
        }
    }
}