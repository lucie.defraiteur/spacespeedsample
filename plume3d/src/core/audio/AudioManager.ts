﻿///<reference path="../../../vendor/howler.d.ts"/>

module plume {

    export class AudioManager {

        protected static _INSTANCE: AudioManager;

        protected _soundSprites: { [name: string]: Sound; } = {};
        protected _musics: { [name: string]: Music; } = {};

        protected _enableMusics: boolean = true;
        protected _enableSoundsSprites: boolean = true;

        protected _musicWasEnable = true;
        protected _soundWasEnable = true;

        musicStateChangeHandlers: Handler<boolean> = new Handler<boolean>();

        constructor(protected _visibilityManager: VisibilityManager) {
            AudioManager._INSTANCE = this;
            this._visibilityManager.addVisibilityChangeHandler(this.onVisibilityChange.bind(this));
        }

        // Allow to load sound after preloading
        loadSounds(sounds: Array<AssetDef>, onComplete: (succes: boolean) => any) {
            let loader = new Loader2d(sounds);
            loader.load(function (complete) {
                onComplete(complete);
            });
        }

        registerSoundsheet(name: string, data: string) {
            let sprites = JSONS.safeJson(data);
            if (sprites == null) return;

            // Copy urls property from json def to "src" property for howler
            sprites.src = sprites.urls;

            let howl = new Howl(sprites);
            if (this.getSpriteCount(sprites.sprite) <= 1) {
                // Single sprite: assume it is music
                logger.debug("Registering music: " + name);
                this.addMusic(name, new Music(howl, sprites.sprite));
            } else {
                logger.debug("Registering sound: " + name);
                this.addSoundSprite(name, new Sound(howl));
            }
        }

        protected getSpriteCount(spriteObj: { [name: string]: Array<number> }): number {
            if (spriteObj == null) return 0;
            let count = 0;
            for (let k in spriteObj) {
                count++;
            }
            return count;
        }

        mute() {
            Howler.mute(true);
        }

        unmute() {
            Howler.mute(false);
        }

        volume(volume: number) {
            Howler.volume(volume);
        }

        protected addMusic(name: string, music: Music) {
            if (!this._enableMusics) {
                music.mute();
            }
            this._musics[name] = music;
        }

        getMusic(fileName: string): Music {
            let music = this._musics[fileName];
            if (!music) {
                logger.error("Can't find music with name: " + fileName);
                return null;
            }
            return music;
        }

        muteMusics() {
            logger.debug("muting musics ");
            for (let key in this._musics) {
                this._musics[key].mute();
            }
            this._enableMusics = false;
            this.musicStateChangeHandlers.fire(false);
        }

        unmuteMusics() {
            logger.debug("unmuting musics ");
            for (let key in this._musics) {
                this._musics[key].unmute();
            }
            this._enableMusics = true;
            this.musicStateChangeHandlers.fire(true);
        }

        volumeMusics(volume: number) {
            for (let key in this._musics) {
                this._musics[key].setVolume(volume);
            }
        }

        get enableMusics(): boolean {
            return this._enableMusics;
        }

        set enableMusics(e: boolean) {
            if (e) {
                this.unmuteMusics();
            } else {
                this.muteMusics();
            }
        }


        protected addSoundSprite(name: string, sound: Sound) {
            this._soundSprites[name] = sound;
            if (!this._enableSoundsSprites) {
                sound.mute();
            }
        }

        getSoundSprite(fileName: string): Sound {
            let sound = this._soundSprites[fileName];
            if (!sound) {
                logger.error("Can't find sound sprite with name: " + fileName + ". Sprite may not be loaded yet or load failed.");
                return null;
            }
            return sound;
        }

        muteSoundSprites(key: string, mute: boolean) {
            let s = this._soundSprites[key];
            if (s != null) {
                if (mute) s.mute();
                else s.unmute();
            }
        }

        muteSoundsSprites() {
            for (let key in this._soundSprites) {
                this._soundSprites[key].mute();
            }
            this._enableSoundsSprites = false;
        }

        unmuteSoundsSprites() {
            for (let key in this._soundSprites) {
                this._soundSprites[key].unmute();
            }

            this._enableSoundsSprites = true;
        }

        volumeSoundsSprites(volume: number) {
            for (let key in this._soundSprites) {
                this._soundSprites[key].setVolume(volume);
            }
        }

        get enableSoundsSprites(): boolean {
            return this._enableSoundsSprites;
        }

        set enableSoundsSprites(e: boolean) {
            if (e) {
                this.unmuteSoundsSprites();
            } else {
                this.muteSoundsSprites();
            }
        }


        protected onVisibilityChange(visible: boolean) {
            if (visible) {
                // Restore audio parameters
                this.enableMusics = this._musicWasEnable;
                this.enableSoundsSprites = this._soundWasEnable;
            } else {
                // Save audio parameters
                this._musicWasEnable = this._enableMusics;
                this._soundWasEnable = this._enableSoundsSprites;
                this.muteMusics();
                this.muteSoundsSprites();
            }
        }

        static get(): AudioManager {
            return AudioManager._INSTANCE;
        }
    }
}