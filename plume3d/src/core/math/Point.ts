module plume {

    //
    // TODO merge with 3js ?
    //
    export class Point {

        private _x: number;
        private _y: number;

        constructor(x: number = 0, y: number = 0) {
            this._x = x;
            this._y = y;
        }

        copy(): Point {
            return new Point(this._x, this._y);
        }

        equals(point: Point): boolean {
            if (point == null) return false;
            return (this.x == point.x && this.y == point.y);
        }

        // Accessor

        get x(): number {
            return this._x;
        }

        set x(x: number) {
            this._x = x;
        }

        get y(): number {
            return this._y;
        }

        set y(y: number) {
            this._y = y;
        }

        toString(): string {
            return this._x + "," + this._y;
        }

        static distance(p1: Point, p2: Point) {
            let xs = 0;
            let ys = 0;

            xs = p2.x - p1.x;
            xs = xs * xs;

            ys = p2.y - p1.y;
            ys = ys * ys;

            return Math.sqrt(xs + ys);
        }

    }

}