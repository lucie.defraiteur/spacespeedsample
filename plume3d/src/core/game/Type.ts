// type definition only used for comoile time checks
module plume {

    export interface AssetDef {
        type: "img" | "spritesheet" | "font" | "soundsheet" | "file",
        name: string,
        file: string,
        atlas?: string
    }

    export interface Updatable {
        update(dt: number): any;
    }

    export interface Renderable {
        render(interpolation: number): any;
    }

    export type EasingFunction = (t: number) => number;

    export interface EffectRenderer { // can be THREE.EffectComposer
        render(dt: number): void;
    }

    // export interface Updatable {
    //     update(dt: number): void;
    // }

    export type StatEntry = {
        start: number,
        sum: number,
        count: number,
    }

    export type GltfObject = {
        animations: Array<THREE.AnimationClip>,
        scene: THREE.Scene,
        scenes: Array<THREE.Scene>,
        cameras: Array<THREE.Camera>,
        asset: Object,
        parser: GLTFParser,
    }
    export type GLTFParser = {
        json: gltfspec.GlTf;
    }
}
