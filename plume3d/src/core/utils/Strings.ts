﻿
module plume {
    
    export class Strings {

        static ellipsis(text: string, maxLength: number, ellipsChar?: string): string {
            if (text == null) return text;
            if (text.length <= maxLength) return text;

            let shorten = text.substring(0, maxLength);
            if (ellipsChar != null) {
                shorten += ellipsChar;
            }
            return shorten;
        }

        static capitalize(text: string) {
            if (text == null) return null;
            if (text.length <= 1) return text.toUpperCase();
            return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
        }

        static interpolate(textIn: string, params: { [key: string]: any }): string {
            let out = textIn.replace(/{([^{}]*)}/g, function (found: string, key: string): string {
                let r = params[key];
                return r;
            });
            return out;
        }
        
        static pad(num: number, size: number) {
            let s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        }

        static pads(s: string, size: number) {
            while (s.length < size) s = "0" + s;
            return s;
        }

        static pad2(num: number): string {
            return Strings.pad(num, 2)
        }

        static randomUUID() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }

    }


}
