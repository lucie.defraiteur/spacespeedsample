module plume {

    export class RecyclePool<T> {

        private _factory: () => T;

        private _count: number;
        private _data: T[];

        constructor(factory: () => T, capacity: number) {
            this._factory = factory;

            this._count = 0;
            this._data = new Array(capacity);
            for (let i = 0; i < capacity; ++i) {
                this._data[i] = factory();
            }
        }

        get length() {
            return this._count;
        }

        get data() {
            return this._data;
        }

        reset() {
            this._count = 0;
        }

        resize(size: number) {
            if (size > this._data.length) {
                for (let i = this._data.length; i < size; ++i) {
                    this._data[i] = this._factory();
                }
            }
        }

        // Virtualy add a new element in the array
        // Resize the array if not enough capacity
        add(): T {
            if (this._count >= this._data.length) {
                this.resize(this._data.length * 2);
            }
            return this._data[this._count++];
        }

        // Remove virtually an element (move removed element to end of array)
        remove(idx: number): T;
        remove(element: T): T;
        remove(idxOrElem: T | number): T {
            let idx = (typeof idxOrElem == "number" ? idxOrElem : this._data.indexOf(idxOrElem));
            if (idx >= this._count) {
                return null;
            }

            let last = this._count - 1;
            let tmp = this._data[idx];
            this._data[idx] = this._data[last];
            this._data[last] = tmp;
            this._count -= 1;

            return tmp;
        }
    }

}