
module plume.easing {

    export function linear(t: number) { return t };

    export function easeInQuad(t: number) { return t * t };
    export function easeInCubic(t: number) { return t * t * t };
    export function easeInQuint(t: number) { return t * t * t * t };

    export function easeOutQuad(t: number) { return 1 - easeInQuad(1 - t) };
    export function easeOutCubic(t: number) { return 1 - easeInCubic(1 - t) };
    export function easeOutQuint(t: number) { return 1 - easeInQuint(1 - t) };

    export function easeInOutQuad(t: number) { return (t < 0.5 ? easeInQuad(t * 2) / 2 : 1 - easeInQuad((1 - t) * 2) / 2) };
    export function easeInOutCubic(t: number) { return (t < 0.5 ? easeInCubic(t * 2) / 2 : 1 - easeInCubic((1 - t) * 2) / 2) };
    export function easeInOutQuint(t: number) { return (t < 0.5 ? easeInQuint(t * 2) / 2 : 1 - easeInQuint((1 - t) * 2) / 2) };

    export function easeOutElastic(t: number) { return Math.pow(2, -10 * t) * Math.sin((t - 0.3 / 4) * (2 * Math.PI) / 0.3) + 1 };

}