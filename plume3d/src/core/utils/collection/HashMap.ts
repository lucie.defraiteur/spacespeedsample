﻿

module plume {

    export class HashMap<V> {

        protected _data: { [key: string]: V } = {};
        protected _size: number = 0;

        isEmpty(): boolean {
            return this._size == 0;
        }

        size(): number {
            return this._size;
        }

        clear() {
            this._data = {};
            this._size = 0;
        }

        containsKey(key: string): boolean {
            return (this._data[key] != null);
        }

        get(key: string) {
            let v = this._data[key];
            return v;
        }

        put(key: string, value: V): V {
            let previous = this._data[key];
            this._data[key] = value;
            if (previous == null) {
                this._size++;
            }
            return previous;
        }

        remove(key: string): V {
            let previous = this._data[key];
            if (previous != null) {
                delete this._data[key];
                this._size--;
            }
            return previous;
        }

        removeValues(value: V) {
            let keys = [];
            for (let key in this._data) {
                let v = this._data[key];
                if (v != null && v == value) keys.push(key);
            }
            for (let i = 0; i < keys.length; i++) {
                this.remove(keys[i]);
            }
        }

        keys(): Array<string> {
            let keys: Array<string> = [];
            for (let key in this._data) {
                keys.push(key);
            }
            return keys;
        }

        values(): Array<V> {
            let values: Array<V> = [];
            for (let key in this._data) {
                let v = this._data[key];
                if (v != null) values.push(v);
            }
            return values;
        }

        forEach(callback: (key: string, value: V) => any, thisArg: any): void {
            for (let key in this._data) {
                let v = this._data[key];
                if (v != null) {
                    let result = callback.call(thisArg, key, v);
                    if (result === false) {
                        break;
                    }
                }
            }
        }

        computeIfAbsent(key: string, mappingFunction: (key: string) => V): V {
            let found = this.get(key);
            if (found != null) {
                return found;
            }

            let v = mappingFunction(key);
            this.put(key, v);
            return v;
        }
    }

}