﻿module plume {

    export interface Listener<T> {
        (event: T): any;
    }

    type HandlerContext<T> = {
        listener: Listener<T>,
        context: any,
    }

    export class Handler<T> {

        private _handlers: HandlerContext<T>[] = [];
        private _onces: HandlerContext<T>[] = [];

        get length(): number {
            return (this._handlers.length + this._onces.length);
        }

        add(listener: Listener<T>, context?: any): void {
            this._handlers.push({
                listener: listener,
                context: context
            });
        }

        once(listener: Listener<T>, context?: any): void {
            this._onces.push({
                listener: listener,
                context: context
            });
        }

        remove(listener: Listener<T>) {
            let index = -1;
            for (let i = 0; i < this._handlers.length; i++) {
                if (this._handlers[i].listener == listener) {
                    index = i;
                    break;
                }
            }
            if (index > -1) {
                this._handlers.splice(index, 1);
            }
        }

        clear() {
            this._handlers = [];
            this._onces = [];
        }

        fire(event: T) {
            this._handlers.forEach((h) => {
                h.listener.apply(h.context, [event]);
            });

            this._onces.forEach((h) => {
                h.listener.apply(h.context, [event]);
            });
            this._onces = [];
        }
    }

    export class ContextualCallback {
        callback: Function;
        context: any;

        constructor(callback: Function, context?: any) {
            this.callback = callback;
            this.context = context;
        }

        execute(...args: any[]) {
            this.callback.apply(this.context, args);
        }

        // Helper to clean an array of ContextualCallback
        static removeFromArray(fn: Function, array: Array<ContextualCallback>): boolean {
            let index = -1;
            for (let i = 0; i < array.length; i++) {
                if (array[i].callback == fn) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                array.splice(index, 1);
                return true;
            }
            return false;
        }
    }

}

