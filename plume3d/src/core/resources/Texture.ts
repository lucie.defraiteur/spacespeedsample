/// <reference path="../utils/collection/HashMap.ts" />

module plume {

    export type Rec = { x: number, y: number, width: number, height: number };
    export enum TextureType { IMG, CANVAS }

    export class Texture {

        static textures: HashMap<Texture> = new HashMap<Texture>();

        private _impl: HTMLImageElement | HTMLCanvasElement;
        private _type: TextureType = TextureType.IMG;

        private _loaded = false;
        private _loadedHadler: Handler<boolean>;

        private _trim: Rec;

        constructor(public name: string, public x: number, public y: number, public width: number, public height: number) {
        }

        setTrim(dx: number, dy: number, sourceW: number, sourceH: number) {
            this._trim =  { x: dx, y: dy, width: sourceW, height: sourceH };
        }

        getTrim(): Rec {
            return this._trim;
        }

        getFrameTexture(x: number, y: number, width: number, height: number, nameId?: string) {
            if (nameId && Texture.containsTexture(nameId))
                return Texture.getTexture(nameId);
            let trim = this._trim;
            if (trim) {
                x += trim.x;
                y += trim.y;
            }
            let texture = new Texture(nameId ? nameId : 'useless', x, y, width, height);
            texture.setImpl(this._impl);
            texture.setTrim(0, 0, width, height);
            if (nameId)
                Texture.addTexture(texture);
            return texture;
        }

        get trimmed(): boolean {
            return this._trim != null;
        }
        set trimmed(t: boolean) {
            throw new Error("trimmed is read-only");
        }

        static addTexture(texture: Texture) {
            Texture.textures.put(texture.name, texture);
        }

        static getTexture(name: string): Texture {
            let texture = Texture.textures.get(name);
            if (texture == null) {
                throw new Error("Texture " + name + " not loaded.");
            }
            return texture;
        }

        // Like getTexture, but do not throw exception
        static containsTexture(name: string): Texture {
            let texture = Texture.textures.get(name);
            return texture;
        }

        static fromFrame(frameId: string): Texture {
            let texture = Texture.getTexture(frameId);
            return texture;
        }

        static fromImage(url: string): Texture {
            let texture = Texture.containsTexture(url);
            if (texture != null) {
                return texture;
            }

            let img = new PImage(url, url);
            texture = img.getTexture();
            return texture;
        }

        static fromFrames(frameIds: Array<string>): Array<Texture> {
            let textureArray: Array<Texture> = [];
            frameIds.forEach(function (frameId) {
                textureArray.push(Texture.fromFrame(frameId));
            });
            return textureArray;
        }

        setLoaded(loaded: boolean) {
            this._loaded = loaded;

            if (loaded && this._loadedHadler != null) {
                this._loadedHadler.fire(true);
            }
        }

        isLoaded(): boolean {
            return this._loaded;
        }

        setImpl(impl: HTMLImageElement | HTMLCanvasElement) {
            this._impl = impl;
        }

        getImpl() {
            return this._impl;
        }

        addLoadedHandler(cb: () => any) {
            if (this._loaded) {
                cb();
                return;
            }

            if (this._loadedHadler == null) this._loadedHadler = new Handler();

            this._loadedHadler.once(cb);
        }
    }
}