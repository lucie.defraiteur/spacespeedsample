module plume {

    export class AbstractLoadingResource {

        protected _success: boolean = false;
        protected _failed: boolean = false;

        protected _onSuccess: () => any;
        protected _onFailed: () => any;

        protected onSuccess0() {
            this._success = true;
            if (this._onSuccess != null) this._onSuccess();
        }

        protected onFailed0() {
            this._failed = true;
            if (this._onFailed != null) this._onFailed();
        }

        onSuccess(cb: () => any) {
            this._onSuccess = cb;
            if (this._success) {
                cb();
            }
        }

        onFailed(cb: () => any) {
            this._onFailed = cb;
            if (this._failed) {
                cb();
            }
        }

        isSuccess(): boolean {
            return this._success;
        }
        isFailed(): boolean {
            return this._failed;
        }
        isTerminated(): boolean {
            return (this._success == true || this._failed == true);
        }

    }

}