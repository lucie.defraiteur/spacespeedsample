module plume {

    export class LoaderProxy {

        private static _INST: LoaderProxy;

        static get(): LoaderProxy {
            if (this._INST == null) {
                this._INST = new LoaderProxy();
            }
            return this._INST;
        }

        static set(factory: LoaderProxy) {
            this._INST = factory;
        }

        // XHR
        loadXHR(url: string, onsucess: (response: string) => any, onerror: (error: string) => any): void {
            // console.log("<> will do xhr <> " + url);
            let request = new HttpRequest();
            request.get(url, onsucess, onerror);
        }

        // Image
        loadImage(image: HTMLImageElement, src: string) {
            // console.log("<> will load image <> " + src);
            image.src = src;
        }

    }


}