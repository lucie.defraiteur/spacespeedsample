/// <reference path="FileResource.ts" />

module plume {

    export class Soundsheet extends FileResource {

        constructor(private _name: string, _file: string) {
            super(_file);
        }

        onSuccess0() {
            super.onSuccess0();

            AudioManager.get().registerSoundsheet(this._name, this.data);
        }

        getAssetDef(): AssetDef {
            return {
                type: "soundsheet",
                name: this._name,
                file: this.file
            }
        }

    }

}