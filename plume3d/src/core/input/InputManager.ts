﻿
module plume {


    export class InputManager implements Updatable {

        keyboard: KeyboardHandler;
        mouse: MouseHandler;
        desktop: boolean;

        canvas: HTMLCanvasElement;

        _pointerDirty: boolean = false;

        private _maxTouchPointer = 1;
        private _disabled: boolean = false;

        mouseEventInterpretors : Array<MouseEventInterpretor> = [];
        noCursor = false;

        constructor(game: Game) {
            this._maxTouchPointer = (game.options.input == null ? 1 : (game.options.input.maxTouchPointer == null ? 1 : game.options.input.maxTouchPointer));
            this.desktop = game.deviceManager.os.desktop;
            this.keyboard = new KeyboardHandler(game, this);
            this.mouse = new MouseHandler(game, this);
        }

        update(dt: number) {
            if (!this._disabled) {
                this.keyboard.update();
                this.mouse.update();

                // delegate mouse cursor check to Gui or others
                for(let i=0; i<this.mouseEventInterpretors.length;i++) {
                    this.mouseEventInterpretors[i].onUpdate();
                    //cf this._triggerMouseHit();
                }
            }
        }

        triggerImmediateClick() {
            // delegate mouse cursor check to Gui or others
            for(let i=0; i<this.mouseEventInterpretors.length;i++) {
                this.mouseEventInterpretors[i].onImmediate();
            }
            // for (let i = 0; i < this.mouse.pointers.length; i++) {
            //     let pointer = this.mouse.pointers[i];
            //     if (pointer.isActive()) {
            //         pointer._triggerImmediateClick();
            //     }
            // }

        }

        // 
        // Go through the scene and dispatch down, up, enter, out event
        // 

        private _triggerMouseHit() {
            // TODO hover, mouse-in, ect in GUI
        }

        get maxTouchPointer(): number {
            return this._maxTouchPointer;
        }

        enable() {
            this._disabled = false;
        }

        disable() {
            this._disabled = true;

            // we need to update pointer (else isJustUp will be true for many gameloop)
            // better way ?
            this.mouse.update();
        }

        hideCursor() {
            this.noCursor = true;
            // Game.get().renderer.canvas.style.cursor = "none";
        }
    }


    

}
