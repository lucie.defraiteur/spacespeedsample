module plume {

    const EPS = 0.000001;
    const enum STATE { NONE = - 1, ROTATE = 0, DOLLY = 1, PAN = 2, TOUCH_ROTATE = 3, TOUCH_DOLLY_PAN = 4 }

    //From threejs example
    export class OrbitControls {

        // Set to false to disable this control
        public enabled = true;

        // "target" sets the location of focus, where the object orbits around
        public target = new THREE.Vector3();

        // How far you can dolly in and out ( PerspectiveCamera only )
        public minDistance = 0;
        public maxDistance = Infinity;

        // How far you can zoom in and out ( OrthographicCamera only )
        public minZoom = 0;
        public maxZoom = Infinity;

        // How far you can orbit vertically, upper and lower limits.
        // Range is 0 to Math.PI radians.
        public minPolarAngle = 0; // radians
        public maxPolarAngle = Math.PI; // radians

        // How far you can orbit horizontally, upper and lower limits.
        // If set, must be a sub-interval of the interval [ - Math.PI, Math.PI ].
        public minAzimuthAngle = - Infinity; // radians
        public maxAzimuthAngle = Infinity; // radians

        // Set to true to enable damping (inertia)
        // If damping is enabled, you must call controls.update() in your animation loop
        public enableDamping = false;
        public dampingFactor = 0.25;

        // This option actually enables dollying in and out; left as "zoom" for backwards compatibility.
        // Set to false to disable zooming
        public enableZoom = true;
        public zoomSpeed = 1.0;

        // Set to false to disable rotating
        public enableRotate = true;
        public rotateSpeed = 1.0;

        // Set to false to disable panning
        public enablePan = true;
        public panSpeed = 1.0;
        public screenSpacePanning = false; // if true, pan in screen-space
        public keyPanSpeed = 7.0;	// pixels moved per arrow key push

        // Set to true to automatically rotate around the target
        // If auto-rotate is enabled, you must call controls.update() in your animation loop
        public autoRotate = false;
        public autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60

        // Set to false to disable use of the keys
        public enableKeys = true;

        // The four arrow keys
        public keys = { LEFT: 37, UP: 38, RIGHT: 39, BOTTOM: 40 };

        // Mouse buttons
        public mouseButtons = { ORBIT: THREE.MOUSE.LEFT, ZOOM: THREE.MOUSE.MIDDLE, PAN: THREE.MOUSE.RIGHT };

        // for reset
        public target0 = this.target.clone();
        public position0 = this.object.position.clone();
        public zoom0 = this.object.zoom;

        constructor(public object: THREE.PerspectiveCamera, public domElement?: HTMLElement, private handleMouseEvent = true) {
            if (domElement == null) {
                this.domElement = window.document.body;
            }

            if (this.handleMouseEvent) {
                this._onContextMenuBound = this._onContextMenu.bind(this);
                this._onMouseDownBound = this._onMouseDown.bind(this);
                this._onMouseWheelBound = this._onMouseWheel.bind(this);
                this._onMouseMoveBound = this._onMouseMove.bind(this);
                this._onMouseUpBound = this._onMouseUp.bind(this);
                this._onTouchStartBound = this._onTouchStart.bind(this);
                this._onTouchMoveBound = this._onTouchMove.bind(this);
                this._onTouchEndBound = this._onTouchEnd.bind(this);

                this.domElement.addEventListener('contextmenu', this._onContextMenuBound, false);
                this.domElement.addEventListener('mousedown', this._onMouseDownBound, false);
                this.domElement.addEventListener('wheel', this._onMouseWheelBound, false);
                this.domElement.addEventListener('touchstart', this._onTouchStartBound, false);
                this.domElement.addEventListener('touchend', this._onTouchEndBound, false);
                this.domElement.addEventListener('touchmove', this._onTouchMoveBound, false);
            }

            // force an update at start
            this.update();
        }


        //
        // public methods
        //

        public getPolarAngle() {
            return this._spherical.phi;
        }

        public getAzimuthalAngle() {
            return this._spherical.theta;
        }

        public saveState() {
            this.target0.copy(this.target);
            this.position0.copy(this.object.position);
            this.zoom0 = this.object.zoom;
        }

        public reset() {
            this.target.copy(this.target0);
            this.object.position.copy(this.position0);
            this.object.zoom = this.zoom0;

            this.object.updateProjectionMatrix();
            this._DispatchEvent(this._changeEvent);

            this.update();

            this._state = STATE.NONE;
        }

        private _DispatchEvent(e: any) {
        }

        // this method is exposed, but perhaps it would be better if we can make it private...
        public update() {

            let offset = new THREE.Vector3();

            // so camera.up is the orbit axis
            let quat = new THREE.Quaternion().setFromUnitVectors(this.object.up, new THREE.Vector3(0, 1, 0));
            let quatInverse = quat.clone().inverse();

            let lastPosition = new THREE.Vector3();
            let lastQuaternion = new THREE.Quaternion();


            let position = this.object.position;
            offset.copy(position).sub(this.target);

            // rotate offset to "y-axis-is-up" space
            offset.applyQuaternion(quat);

            // angle from z-axis around y-axis
            this._spherical.setFromVector3(offset);

            if (this.autoRotate && this._state === STATE.NONE) {
                this._rotateLeft(this._getAutoRotationAngle());
            }

            this._spherical.theta += this._sphericalDelta.theta;
            this._spherical.phi += this._sphericalDelta.phi;

            // restrict theta to be between desired limits
            this._spherical.theta = Math.max(this.minAzimuthAngle, Math.min(this.maxAzimuthAngle, this._spherical.theta));

            // restrict phi to be between desired limits
            this._spherical.phi = Math.max(this.minPolarAngle, Math.min(this.maxPolarAngle, this._spherical.phi));
            this._spherical.makeSafe();

            this._spherical.radius *= this._scale;

            // restrict radius to be between desired limits
            this._spherical.radius = Math.max(this.minDistance, Math.min(this.maxDistance, this._spherical.radius));

            // move target to panned location
            this.target.add(this._panOffset);

            offset.setFromSpherical(this._spherical);

            // rotate offset back to "camera-up-vector-is-up" space
            offset.applyQuaternion(quatInverse);
            position.copy(this.target).add(offset);
            this.object.lookAt(this.target);

            if (this.enableDamping === true) {
                this._sphericalDelta.theta *= (1 - this.dampingFactor);
                this._sphericalDelta.phi *= (1 - this.dampingFactor);
                this._panOffset.multiplyScalar(1 - this.dampingFactor);
            } else {
                this._sphericalDelta.set(0, 0, 0);
                this._panOffset.set(0, 0, 0);
            }

            this._scale = 1;

            // update condition is:
            // min(camera displacement, camera rotation in radians)^2 > EPS
            // using small-angle approximation cos(x/2) = 1 - x^2 / 8

            if (this._zoomChanged ||
                lastPosition.distanceToSquared(this.object.position) > EPS ||
                8 * (1 - lastQuaternion.dot(this.object.quaternion)) > EPS) {
                this._DispatchEvent(this._changeEvent);
                lastPosition.copy(this.object.position);
                lastQuaternion.copy(this.object.quaternion);
                this._zoomChanged = false;
                return true;
            }
            return false;
        }

        public dispose() {
            if (this.handleMouseEvent) {
                this.domElement.removeEventListener('contextmenu', this._onContextMenuBound, false);
                this.domElement.removeEventListener('mousedown', this._onMouseDownBound, false);
                this.domElement.removeEventListener('wheel', this._onMouseWheelBound, false);
                this.domElement.removeEventListener('touchstart', this._onTouchStartBound, false);
                this.domElement.removeEventListener('touchend', this._onTouchEndBound, false);
                this.domElement.removeEventListener('touchmove', this._onTouchMoveBound, false);

                document.removeEventListener('mousemove', this._onMouseMoveBound, false);
                document.removeEventListener('mouseup', this._onMouseUpBound, false);
            }
        }



        //
        // internals
        //
        private _onContextMenuBound: () => void;
        private _onMouseDownBound: () => void;
        private _onMouseWheelBound: () => void;
        private _onMouseMoveBound: () => void;
        private _onMouseUpBound: () => void;
        private _onTouchStartBound: () => void;
        private _onTouchMoveBound: () => void;
        private _onTouchEndBound: () => void;

        private _changeEvent = { type: 'change' };
        private _startEvent = { type: 'start' };
        private _endEvent = { type: 'end' };

        private _state = STATE.NONE;

        private _EPS = 0.000001;

        // current position in spherical coordinates
        private _spherical = new THREE.Spherical();
        private _sphericalDelta = new THREE.Spherical();

        private _scale = 1;
        private _panOffset = new THREE.Vector3();
        private _zoomChanged = false;

        private _rotateStart = new THREE.Vector2();
        private _rotateEnd = new THREE.Vector2();
        private _rotateDelta = new THREE.Vector2();

        private _panStart = new THREE.Vector2();
        private _panEnd = new THREE.Vector2();
        private _panDelta = new THREE.Vector2();

        private _dollyStart = new THREE.Vector2();
        private _dollyEnd = new THREE.Vector2();
        private _dollyDelta = new THREE.Vector2();

        private _getAutoRotationAngle() {
            return 2 * Math.PI / 60 / 60 * this.autoRotateSpeed;
        }

        private _getZoomScale() {
            return Math.pow(0.95, this.zoomSpeed);
        }

        private _rotateLeft(angle: number) {
            this._sphericalDelta.theta -= angle;
        }

        private _rotateUp(angle: number) {
            this._sphericalDelta.phi -= angle;
        }

        private _panLeft(distance: number, objectMatrix: THREE.Matrix4) {
            let v = new THREE.Vector3();
            v.setFromMatrixColumn(objectMatrix, 0); // get X column of objectMatrix
            v.multiplyScalar(- distance);
            this._panOffset.add(v);
        }

        private _panUp(distance: number, objectMatrix: THREE.Matrix4) {
            let v = new THREE.Vector3();
            if (this.screenSpacePanning === true) {
                v.setFromMatrixColumn(objectMatrix, 1);
            } else {
                v.setFromMatrixColumn(objectMatrix, 0);
                v.crossVectors(this.object.up, v);
            }
            v.multiplyScalar(distance);
            this._panOffset.add(v);
        }

        // deltaX and deltaY are in pixels; right and down are positive
        private _pan0(deltaX: number, deltaY: number) {

            let offset = new THREE.Vector3();

            // perspective
            let position = this.object.position;
            offset.copy(position).sub(this.target);
            let targetDistance = offset.length();

            // half of the fov is center to top of screen
            targetDistance *= Math.tan((this.object.fov / 2) * Math.PI / 180.0);

            // we use only clientHeight here so aspect ratio does not distort speed
            this._panLeft(2 * deltaX * targetDistance / this.domElement.clientHeight, this.object.matrix);
            this._panUp(2 * deltaY * targetDistance / this.domElement.clientHeight, this.object.matrix);
        }

        public dollyIn(dollyScale: number, update = true) {
            this._scale /= dollyScale;
            if (update) {
                this.update();
            }
        }
        public dollyOut(dollyScale: number, update = true) {
            this._scale *= dollyScale;
            if (update) {
                this.update();
            }
        }

        //
        // event callbacks - update the object state
        //
        public startRotate(x: number, y: number) {
            this._rotateStart.set(x, y);
        }

        public startDolly(x: number, y: number) {
            this._dollyStart.set(x, y);
        }

        public startPan(x: number, y: number) {
            this._panStart.set(x, y);
        }

        public rotate(x: number, y: number) {
            this._rotateEnd.set(x, y);
            this._rotateDelta.subVectors(this._rotateEnd, this._rotateStart).multiplyScalar(this.rotateSpeed);
            let element = this.domElement;
            this._rotateLeft(2 * Math.PI * this._rotateDelta.x / element.clientHeight); // yes, height
            this._rotateUp(2 * Math.PI * this._rotateDelta.y / element.clientHeight);
            this._rotateStart.copy(this._rotateEnd);
            this.update();
        }

        public dolly(x: number, y: number) {
            this._dollyEnd.set(x, y);
            this._dollyDelta.subVectors(this._dollyEnd, this._dollyStart);
            if (this._dollyDelta.y > 0) {
                this.dollyIn(this._getZoomScale(), false);
            } else if (this._dollyDelta.y < 0) {
                this.dollyOut(this._getZoomScale(), false);
            }
            this._dollyStart.copy(this._dollyEnd);
            this.update();
        }

        public pan(x: number, y: number) {
            this._panEnd.set(x, y);
            this._panDelta.subVectors(this._panEnd, this._panStart).multiplyScalar(this.panSpeed);
            this._pan0(this._panDelta.x, this._panDelta.y);
            this._panStart.copy(this._panEnd);
            this.update();
        }

        //
        // event handlers - FSM: listen for events and reset state
        //
        private _handleMouseDownRotate(event: MouseEvent) {
            this.startRotate(event.clientX, event.clientY);
        }
        private _handleMouseDownDolly(event: MouseEvent) {
            this.startDolly(event.clientX, event.clientY);
        }
        private _handleMouseDownPan(event: MouseEvent) {
            this.startPan(event.clientX, event.clientY);
        }
        private _handleMouseMoveRotate(event: MouseEvent) {
            this.rotate(event.clientX, event.clientY);
        }
        private _handleMouseMoveDolly(event: MouseEvent) {
            this.dolly(event.clientX, event.clientY);
        }
        private _handleMouseMovePan(event: MouseEvent) {
            this.pan(event.clientX, event.clientY);
        }
        private _handleMouseUp(event: MouseEvent) {
        }

        private _handleMouseWheel(event: MouseWheelEvent) {
            if (event.deltaY < 0) {
                this.dollyOut(this._getZoomScale(), false);
            } else if (event.deltaY > 0) {
                this.dollyIn(this._getZoomScale(), false);
            }
            this.update();
        }

        private _onMouseDown(event: MouseEvent) {
            if (this.enabled === false) return;

            // event.preventDefault();
            switch (event.button) {
                case this.mouseButtons.ORBIT:
                    if (this.enableRotate === false) return;
                    this._handleMouseDownRotate(event);
                    this._state = STATE.ROTATE;
                    break;

                case this.mouseButtons.ZOOM:
                    if (this.enableZoom === false) return;
                    this._handleMouseDownDolly(event);
                    this._state = STATE.DOLLY;
                    break;

                case this.mouseButtons.PAN:
                    if (this.enablePan === false) return;
                    this._handleMouseDownPan(event);
                    this._state = STATE.PAN;
                    break;

            }

            if (this._state !== STATE.NONE) {
                document.addEventListener('mousemove', this._onMouseMoveBound, false);
                document.addEventListener('mouseup', this._onMouseUpBound, false);
                this._DispatchEvent(this._startEvent);
            }
        }

        private _onMouseMove(event: MouseEvent) {
            if (this.enabled === false) return;

            // event.preventDefault();
            switch (this._state) {
                case STATE.ROTATE:
                    if (this.enableRotate === false) return;
                    this._handleMouseMoveRotate(event);
                    break;

                case STATE.DOLLY:
                    if (this.enableZoom === false) return;
                    this._handleMouseMoveDolly(event);
                    break;

                case STATE.PAN:
                    if (this.enablePan === false) return;
                    this._handleMouseMovePan(event);
                    break;

            }
        }

        private _onMouseUp(event: MouseEvent) {

            if (this.enabled === false) return;

            this._handleMouseUp(event);

            document.removeEventListener('mousemove', this._onMouseMoveBound, false);
            document.removeEventListener('mouseup', this._onMouseUpBound, false);

            this._DispatchEvent(this._endEvent);

            this._state = STATE.NONE;
        }

        private _onMouseWheel(event: MouseWheelEvent) {

            if (this.enabled === false || this.enableZoom === false || (this._state !== STATE.NONE && this._state !== STATE.ROTATE)) return;

            // event.preventDefault();
            // event.stopPropagation();

            this._DispatchEvent(this._startEvent);
            this._handleMouseWheel(event);
            this._DispatchEvent(this._endEvent);
        }

        private _onContextMenu(event: MouseEvent) {
            if (this.enabled === false) return;
            event.preventDefault();
        }

        private _onTouchStart(event: TouchEvent) {
            if (this.enabled === false) return;
            event.preventDefault();

            switch (event.touches.length) {
                case 1:	// one-fingered touch: rotate
                    if (this.enableRotate === false) return;
                    this._handleTouchStartRotate(event);
                    this._state = STATE.TOUCH_ROTATE;
                    break;
                    case 2:	// two-fingered touch: dolly-pan
                    if (this.enableZoom === false && this.enablePan === false) return;
                    this._handleTouchStartDollyPan(event);
                    this._state = STATE.TOUCH_DOLLY_PAN;
                    break;
                default:
                    this._state = STATE.NONE;
            }

            if (this._state !== STATE.NONE) {
                this._DispatchEvent(this._startEvent);
            }
        }

        private _onTouchMove(event: TouchEvent) {
            if (this.enabled === false) return;
            event.preventDefault();
            event.stopPropagation();

            switch (event.touches.length) {
                case 1: // one-fingered touch: rotate
                    if (this.enableRotate === false) return;
                    if (this._state !== STATE.TOUCH_ROTATE) return; // is this needed?
                    this._handleTouchMoveRotate(event);
                    break;
                case 2: // two-fingered touch: dolly-pan
                    if (this.enableZoom === false && this.enablePan === false) return;
                    if (this._state !== STATE.TOUCH_DOLLY_PAN) return; // is this needed?
                    this._handleTouchMoveDollyPan(event);
                    break;
                default:
                    this._state = STATE.NONE;
            }
        }

        private _onTouchEnd(event: TouchEvent) {
            if (this.enabled === false) return;

            this._handleTouchEnd(event);
            this._DispatchEvent(this._endEvent);
            this._state = STATE.NONE;
        }


        private _handleTouchStartRotate(event: TouchEvent) {
            this._rotateStart.set(event.touches[0].pageX, event.touches[0].pageY);
        }

        private _handleTouchStartDollyPan(event: TouchEvent) {
            if (this.enableZoom) {
                let dx = event.touches[0].pageX - event.touches[1].pageX;
                let dy = event.touches[0].pageY - event.touches[1].pageY;
                let distance = Math.sqrt(dx * dx + dy * dy);
                this._dollyStart.set(0, distance);
            }

            if (this.enablePan) {
                let x = 0.5 * (event.touches[0].pageX + event.touches[1].pageX);
                let y = 0.5 * (event.touches[0].pageY + event.touches[1].pageY);
                this._panStart.set(x, y);
            }
        }

        private _handleTouchMoveRotate(event: TouchEvent) {
            this._rotateEnd.set(event.touches[0].pageX, event.touches[0].pageY);
            this._rotateDelta.subVectors(this._rotateEnd, this._rotateStart).multiplyScalar(this.rotateSpeed);

            let element = this.domElement;
            //let element = this.domElement === document ? this.domElement.body : this.domElement;
            this._rotateLeft(2 * Math.PI * this._rotateDelta.x / element.clientHeight); // yes, height
            this._rotateUp(2 * Math.PI * this._rotateDelta.y / element.clientHeight);
            this._rotateStart.copy(this._rotateEnd);
            this.update();
        }

        private _handleTouchMoveDollyPan(event: TouchEvent) {
            if (this.enableZoom) {
                let dx = event.touches[0].pageX - event.touches[1].pageX;
                let dy = event.touches[0].pageY - event.touches[1].pageY;
                let distance = Math.sqrt(dx * dx + dy * dy);

                this._dollyEnd.set(0, distance);
                this._dollyDelta.set(0, Math.pow(this._dollyEnd.y / this._dollyStart.y, this.zoomSpeed));
                this.dollyIn(this._dollyDelta.y);

                this._dollyStart.copy(this._dollyEnd);
            }

            if (this.enablePan) {
                let x = 0.5 * (event.touches[0].pageX + event.touches[1].pageX);
                let y = 0.5 * (event.touches[0].pageY + event.touches[1].pageY);

                // this._panEnd.set(x, y);
                // this._panDelta.subVectors(this._panEnd, this._panStart).multiplyScalar(this.panSpeed);
                // this.pan(this._panDelta.x, this._panDelta.y);
                // this._panStart.copy(this._panEnd);
                this.pan(x,y);
            }

            this.update();
        }

        private _handleTouchEnd(event: TouchEvent) {
        }
    }

}