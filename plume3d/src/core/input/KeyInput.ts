﻿
module plume {

    export class KeyInput extends AbstractInput {

        keycode: number;
        event: KeyboardEvent;

        constructor(keycode: number) {
            super();
            this.keycode = keycode;
        }

        keyDown(e: KeyboardEvent) {
            if (this.isDown) return;

            this.event = e;

            super.onDown();
        }

        keyUp(e: KeyboardEvent) {
            if (this.isUp) return;

            this.event = e;

            super.onUp();
        }

    }

}
