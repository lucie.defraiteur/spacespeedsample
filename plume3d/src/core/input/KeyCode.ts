﻿
module plume {

    export class KeyCode {
        static readonly SHIFT: number = 17;
        static readonly CTRL: number = 17;
        static readonly ALT: number = 18;
        static readonly SPACEBAR: number = 32;
        static readonly LEFT: number = 37;
        static readonly PAGEUP: number = 33;
        static readonly PAGEDOWN: number = 34;
        static readonly END: number = 35;
        static readonly HOME: number = 36;
        static readonly UP: number = 38;
        static readonly RIGHT: number = 39;
        static readonly DOWN: number = 40;
        static readonly INSERT: number = 45;
        static readonly DELETE: number = 46;

        static readonly F1: number = 112;
        static readonly F2: number = 113;
        static readonly F3: number = 114;
        static readonly F4: number = 115;
        static readonly F5: number = 116;
        static readonly F6: number = 117;
        static readonly F7: number = 118;
        static readonly F8: number = 119;
        static readonly F9: number = 120;
        static readonly F10: number = 121;
        static readonly F11: number = 122;
        static readonly F12: number = 123;

        static fromChar(c: string): number {
            return c.charCodeAt(0) - 32;
        }
    }

}
