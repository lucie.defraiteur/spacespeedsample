﻿
module plume {

    export class AbstractInput implements Updatable {

        isDown: boolean = false;
        isUp: boolean = true;

        isJustDown: boolean = false;
        isJustUp: boolean = false;

        timeDown: number = 0;
        timeUp: number = 0;
        downDuration: number = 0;

        protected justUpdated: boolean = false;
        private _duration: number = 0;

        constructor() {
        }

        update() {

            if (this.isDown) {
                this.downDuration = Time.now() - this.timeDown;
            }

            if (this.justUpdated) {
                this.isJustDown = this.isDown;
                this.isJustUp = this.isUp;
            } else {
                this.isJustDown = false;
                this.isJustUp = false;
            }

            this.justUpdated = false;
        }

        protected onDown() {

            if (this.isDown) return;

            this.justUpdated = true;
            this.isDown = true;
            this.isUp = false;
            this.timeDown = Time.now();
            this.downDuration = 0;
        }

        protected onUp() {
            if (this.isUp) return;

            this.justUpdated = true;
            this.isDown = false;
            this.isUp = true;
            this.timeUp = Time.now();
        }

    }

}
