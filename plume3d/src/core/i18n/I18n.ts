/// <reference path="../utils/Jsons.ts" />
module plume {

    export class I18N {

        static availableLocales: Array<string> = [];
        static defaultLocale: string = "en";
        static locale: string = "en";
        static translations: any = [];

        public static setAvailableLocales(locales: Array<string>) {
            I18N.availableLocales = locales;
        }

        public static registerTranslation(locale: string, translation: any) {
            if (I18N.translations[locale] == null) {
                I18N.translations[locale] = {};
            }
            JSONS.merge(translation, I18N.translations[locale]);
        }

        public static setDefaultLocale(locale: string) {
            I18N.defaultLocale = locale;
        }

        public static setLocale(locale: string) {
            if (I18N.availableLocales.indexOf(locale) >= 0) {
                I18N.locale = locale;
            } else {
                I18N.locale = I18N.defaultLocale;
            }
        }

        public static isAvailableLocale(locale: string): boolean {
            return I18N.availableLocales.indexOf(locale) >= 0;
        }

        public static get(key: string, params?: { [key: string]: any }): string {
            let value = I18N.translations[I18N.locale][key];
            if (value) {
                if (params != null) return Strings.interpolate(value, params);
                return value;
            } else {
                let en = I18N.translations[I18N.defaultLocale][key];
                if (en) {
                    if (params != null) return Strings.interpolate(en, params);
                    return en;
                }
                return key;
            }
        }


        public static getPluralForm(n: number, local?: string): number {
            if (local == null) local = I18N.locale;

            // n : actual number
            let plural = 2;
            if (local == "fr") {
                plural = (n > 1 ? 1 : 0);
            } else if (local == "lv") {
                plural = (n == 0 ? 0 : (n % 10 == 1 && n % 100 != 11 ? 1 : 2));
            } else if (local == "pl") {
                plural = (n == 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
            } else if (local == "ru") {
                plural = (n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
            } else {
                // english rule
                plural = (n == 1 ? 0 : 1);
            }
            return plural;
        }

        public static getPlural(n: number, keyBase: string, params?: { [key: string]: any }): string {
            if (params == null) {
                // default, put "n" as parameters
                params = { "n": n };
            }

            // n : actual number
            // keyBase: ref key to get "exetnded form". ie if key is "N_POINT" we will look in dictionnaries N_POINT_F0, N_POINT_F1 or N_POINT_F2
            let plural = I18N.getPluralForm(n);
            let key = keyBase + "_F" + plural;
            let str = I18N.get(key, params);

            return str;
        }

        /*
        Plural rule #1 (2 forms)
        Families: Germanic (Danish, Dutch, English, Faroese, Frisian, German, Norwegian, Swedish), Finno-Ugric (Estonian, Finnish, Hungarian), Language isolate (Basque), Latin/Greek (Greek), Semitic (Hebrew), Romanic (Italian, Portuguese, Spanish, Catalan), Vietnamese
        F0 => is 1: 1
        F1 => everything else: 0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, ...
        
        Plural rule #2 (2 forms)
        Families: Romanic (French, Brazilian Portuguese)
        F0 => is 0 or 1: 0, 1
        F1 => everything else: 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, ...
        
        Families: Baltic (Latvian)
        F0 => is 0: 0
        F1 => ends in 1, excluding 11: 1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191, 201, 221, 231, 241, 251, 261, 271, 281, 291, …
        F2 => everything else: 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, ...
        
        Families: Slavic (Belarusian, Bosnian, Croatian, Serbian, Russian, Ukrainian)
        F0 => ends in 1, excluding 11: 1, 21, 31, 41, 51, 61, 71, 81, 91, 101, 121, 131, 141, 151, 161, 171, 181, 191, 201, 221, 231, 241, 251, 261, 271, 281, 291, …
        F1 => ends in 2-4, excluding 12-14: 2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54, 62, 63, 64, 72, 73, 74, 82, 83, 84, 92, 93, 94, 102, 103, 104, 122, 123, 124, 132, 133, 134, 142, 143, 144, 152, 153, 154, 162, 163, 164, 172, 173, 174, 182, 183, …
        F2 => everything else: 0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 25, 26, 27, 28, 29, ...
        
        Plural rule #9 (3 forms)
        Families: Slavic (Polish)
        F0 => is 1: 1
        F1 => ends in 2-4, excluding 12-14: 2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54, 62, 63, 64, 72, 73, 74, 82, 83, 84, 92, 93, 94, 102, 103, 104, 122, 123, 124, 132, 133, 134, 142, 143, 144, 152, 153, 154, 162, 163, 164, 172, 173, 174, 182, 183, …
        F2 => everything else: 0, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, ...
        */

    }

}