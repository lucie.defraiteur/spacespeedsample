module plume {

    export class SceneUtils {

        private static _tmp_mat4: THREE.Matrix4 = new THREE.Matrix4();

        static detach(child: THREE.Object3D, parent?: THREE.Object3D, scene?: THREE.Scene) {
            if (parent == null) parent = child.parent;
            if (scene == null) scene = Engine.get().scene;

            child.applyMatrix(parent.matrixWorld);
            parent.remove(child);
            scene.add(child);
        }

        static attach(child: THREE.Object3D, parent: THREE.Object3D, scene?: THREE.Scene) {
            if (scene == null) scene = Engine.get().scene;

            child.applyMatrix(this._tmp_mat4.getInverse(parent.matrixWorld));

            scene.remove(child);
            parent.add(child);
        }

        static move(object: THREE.Object3D, newParent: THREE.Object3D) {
            this.detach(object);
            this.attach(object, newParent);
        }

    }

}