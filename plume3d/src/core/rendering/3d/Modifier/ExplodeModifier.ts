module plume {
    // Make all faces use unique vertices so that each face can be separated from others
    export class ExplodeModifier {
        modify(geometry: THREE.Geometry) {
            let vertices = [];

            for (let i = 0, il = geometry.faces.length; i < il; i++) {

                let n = vertices.length;

                let face = geometry.faces[i];

                let a = face.a;
                let b = face.b;
                let c = face.c;

                let va = geometry.vertices[a];
                let vb = geometry.vertices[b];
                let vc = geometry.vertices[c];

                vertices.push(va.clone());
                vertices.push(vb.clone());
                vertices.push(vc.clone());

                face.a = n;
                face.b = n + 1;
                face.c = n + 2;

            }

            geometry.vertices = vertices;
        }
    }
}