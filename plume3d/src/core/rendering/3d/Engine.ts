/// <reference path="../../../../vendor/three.d.ts" />

// TODO how to complete three.d.ts ??
window["THREEEXT"] = THREE;

module plume {

    export type EngineOptions = {
        canvasId?: string,
        antialias?: boolean,
        alpha?: boolean,
    }

    export class Engine {

        private static _INSTANCE: Engine;

        canvas: HTMLCanvasElement;
        game: Game;

        private _options: EngineOptions;

        // current scene & camera
        private _scene: THREE.Scene;
        private _camera: THREE.PerspectiveCamera | THREE.OrthographicCamera;
        private _renderer: THREE.WebGLRenderer;
        private _effectComposer: EffectRenderer;
        private _loader: Loader3d;

        private _prerender: Array<ContextualCallback> = [];
        private _postrender: Array<ContextualCallback> = [];

        private _textureCache: TextureCache;

        constructor(options?: EngineOptions) {

            let self = this;

            let now = performance.now();

            this.game = Game.get();
            this._options = this._initOption(options);
            this._initCanvas();

            Engine._INSTANCE = this;

            let width = window.innerWidth;
            let height = window.innerHeight;

            this._scene = new THREE.Scene();
            this._camera = new THREE.PerspectiveCamera(50, width / height);
            this._camera.position.z = 30;

            this._renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias: this._options.antialias, alpha: this._options.alpha });
            this._renderer.setPixelRatio(window.devicePixelRatio);
            this._renderer.setSize(width, height);
            this._renderer.setClearColor(0x000000, 1);

            this._textureCache = new TextureCache();
            this._loader = new Loader3d();

            this.game.scaleManager.resizeHandler.add(function () {
                let size = self.game.scaleManager.getWindowSize();
                self.resize(size.width, size.height);
            });
        }

        static get(): Engine {
            return Engine._INSTANCE;
        }

        private _initOption(o: EngineOptions): EngineOptions {
            if (o == null) o = {};

            if (o.antialias == null) o.antialias = true;
            if (o.alpha == null) o.alpha = true;

            return o;
        }

        private _initCanvas() {
            if (this._options.canvasId != null) {
                this.canvas = document.getElementById(this._options.canvasId) as HTMLCanvasElement;
            } else {
                this.canvas = document.createElement("canvas");
                this.canvas.style.position = "absolute";
                this.canvas.id = "canvas3D";
                document.body.appendChild(this.canvas);
            }
        }

        render(dt: number, interpolation: number) {
            if (this._effectComposer == null) {
                for (let i = 0; i < this._prerender.length; i++) {
                    this._prerender[i].execute(dt);
                }
                this._renderer.render(this._scene, this._camera);
                for (let i = 0; i < this._postrender.length; i++) {
                    this._postrender[i].execute(dt);
                }
            } else {
                this._effectComposer.render(dt);
            }

        }

        resize(width: number, height: number) {
            (this._camera as any).aspect = width / height;
            this._camera.updateProjectionMatrix();
            this._renderer.setSize(width, height);
        }


        addRenderable(renderable: (dt: number) => void, prerender: boolean, ctx?: any) {
            let cb = new ContextualCallback(renderable, ctx);
            if (prerender) {
                this._prerender.push(cb);
            } else {
                this._postrender.push(cb);
            }
            return cb;
        }

        removeRenderable(renderable: (dt: number) => void): boolean {
            let removed = ContextualCallback.removeFromArray(renderable, this._postrender);
            if (removed) {
                return true;
            }

            removed = ContextualCallback.removeFromArray(renderable, this._prerender);
            return removed;
        }

        get renderer(): THREE.WebGLRenderer {
            return this._renderer;
        }

        get scene(): THREE.Scene {
            return this._scene;
        }
        set scene(s: THREE.Scene) {
            this._scene = s;
        }

        get camera(): THREE.PerspectiveCamera | THREE.OrthographicCamera {
            return this._camera;
        }
        set camera(camera: THREE.PerspectiveCamera | THREE.OrthographicCamera) {
            this._camera = camera;
        }

        get effectComposer(): EffectRenderer {
            return this._effectComposer;
        }
        set effectComposer(v: EffectRenderer) {
            this._effectComposer = v;
        }

        get loader(): Loader3d {
            return this._loader;
        }
        set loader(v: Loader3d) {
            this._loader = v;
        }

        get textureCache(): TextureCache {
            return this._textureCache;
        }
    }

}