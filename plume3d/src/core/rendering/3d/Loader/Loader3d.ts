

module plume {

    export class Loader3d {

        public static dracoLib = 'build/lib/draco/';

        private _manager: THREE.LoadingManager;
        private _textureLoaderManager: TextureLoaderManager;
        private _total = 0;
        private _loaded = 0;

        private _ressourceByName = new HashMap<any>();
        private _textureByName = new HashMap<THREE.Texture>();

        private _loadedHandler = new Handler<boolean>();

        constructor(onloaded?: () => void) {
            let self = this;
            this._manager = new THREE.LoadingManager();
            this._textureLoaderManager = new TextureLoaderManager(this._manager);

            if (onloaded != null) {
                self._loadedHandler.once(onloaded);
            }
        }

        private _fbxloader() {
            let fbxloader0 = new THREE.FBXLoader(this._manager);
            return fbxloader0;
        }
        private _objloader() {
            let objloader0 = new THREE.OBJLoader2(this._manager);
            objloader0.setLogging(false, false);
            return objloader0;
        }
        private _gltfloader() {
            if (THREE["GLTFLoader"] != null) {
                // TODO, need to be removed
                let gltfloader0 = new THREE["GLTFLoader"](this._manager);
                let dracoloader = THREE["DRACOLoader"];
                if (dracoloader) {
                    dracoloader.setDecoderPath(Loader3d.dracoLib);
                    gltfloader0.setDRACOLoader(new dracoloader());
                }
                return gltfloader0;
            } else {
                // Good one
                let gltfloader0 = new plume["gltf"]["GLTFLoader"](this._manager, this._textureLoaderManager);
                return gltfloader0;
            }
        }

        setAvailableCompressionAndQuanlity(textures: Array<TextureCompressionAndQuality>): ProgressiveTextureLoaderManager {
            // TODO we might want to do 2d loading pass in custom order
            // (ie: loading texture near camera)

            if (textures == null || textures.length == 0) return null;

            // check there is at least 1 low res or 1 compressed texture
            let hasCompressionOrResolution = false;
            for (let i = 0; i < textures.length; i++) {
                let t = textures[i];
                if (t.quality != TextureQuality.NORMAL || t.format != TextureCompression.NORMAL) {
                    hasCompressionOrResolution = true;
                    break;
                }
            }

            if (!hasCompressionOrResolution) {
                logger.warn("No compressed or low res textures registered.");
                return null;
            }

            this._total++;
            let tl = new ProgressiveTextureLoaderManager(this._manager);
            tl.initialize(textures, this);
            this._textureLoaderManager = tl;
            this._total--; // bof bof

            return tl;
        }

        reset() {
            this._total = 0;
            this._loaded = 0;
        }

        onLoaded(cb: () => void) {
            if (this.isLoaded()) {
                cb();
                return;
            }

            this._loadedHandler.once(cb);
        }

        isLoaded(): boolean {
            return (this._loaded == this._total);
        }

        getModel(name: string): any {
            return this._ressourceByName.get(name);
        }

        getTexture(name: string): THREE.Texture {
            return this._textureByName.get(name);
        }

        getCubeTexture(name: string): THREE.CubeTexture {
            return this._textureByName.get(name) as THREE.CubeTexture;
        }

        loadTexture(url: string, name: string): THREE.Texture {
            let self = this;
            this._total++;

            let texture = this._textureLoaderManager.load(url, function (texture: THREE.Texture) {
                self._textureByName.put(name, texture);
                self._loaded++;
                self._checkLoadingComplete();
            });
            return texture;
        }

        loadCubeTexture(url: Array<string>, name: string): THREE.Texture {
            let self = this;
            this._total++;

            // TODO / test: use this._textureLoaderManager to have progressive loading
            let loader = new THREE.CubeTextureLoader(this._manager);
            let texture = loader.load(url, function (texture: THREE.CubeTexture) {
                self._textureByName.put(name, texture);
                self._loaded++;
                self._checkLoadingComplete();
            });
            return texture;
        }

        loadObj(objUrl: string, mtlUrl: string, name: string) {
            let self = this;
            this._total++;

            let objLoader = this._objloader();
            let callbackOnLoad = function (event: any) {
                // event.detail.loaderRootNode
                // event.detail.modelName
                self._ressourceByName.put(name, event.detail.loaderRootNode);
                self._loaded++;
                self._checkLoadingComplete();
            };
            let onLoadMtl = function (materials: THREE.Material) {
                objLoader.setModelName(name); // important ??
                objLoader.setMaterials(materials);
                objLoader.load(objUrl, callbackOnLoad, null, null, null, false);
            };
            objLoader.loadMtl(mtlUrl, null, onLoadMtl);
        }

        loadFbx(objUrl: string, name: string) {
            let self = this;
            this._total++;

            this._fbxloader().load(objUrl, function (object) {
                self._ressourceByName.put(name, object);
                self._loaded++;
                self._checkLoadingComplete();
            });
        }

        loadGltf(objUrl: string, name: string, configHandler?: (gltfloader: any) => void) {
            let self = this;
            this._total++;

            let loader = this._gltfloader();
            if (configHandler != null) {
                configHandler(loader);
            }

            loader.load(objUrl, function (gltf: GltfObject) {
                self._ressourceByName.put(name, gltf);
                self._loaded++;
                self._checkLoadingComplete();
            }, function (xhr: any) {
                //logger.debug((xhr.loaded / xhr.total * 100) + '% loaded');
            }, function (error: any) {
                logger.error('An error happened', error);
            });
        }

        addModel(mesh: THREE.Mesh, name: string) {
            this._ressourceByName.put(name, mesh);
        }

        get manager(): THREE.LoadingManager {
            return this._manager;
        }
        get progressiveLoader(): TextureLoaderManager {
            return this._textureLoaderManager;
        }

        private _checkLoadingComplete() {
            if (this.isLoaded()) {
                this._loadedHandler.fire(true);
            }
        }
    }

}