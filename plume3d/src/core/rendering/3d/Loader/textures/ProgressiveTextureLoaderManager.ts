/// <reference path="TextureLoaderManager.ts" />

module plume {

    // should be in sync with grunt asset & texture script
    export const enum TextureQuality {
        NORMAL = 1,
        LOW = 2,
    }

    // should be in sync with grunt asset & texture script
    export const enum TextureCompression {
        NORMAL = 1,
        ASTC = 2,
        PVR = 4,
        S3TC = 8,
        ETC = 16,
    }

    export type TextureCompressionAndQuality = {
        path: string,
        quality: number,
        format: number,
    }

    // private
    type ImageDataHolder = {
        path: string,
        name: string,
        ext: string,
    }

    // Entry point of texture loading
    // It can delegate to TextureLoader or CompressedTextureLoader and handle low/high res resolution loading
    export class ProgressiveTextureLoaderManager extends TextureLoaderManager {

        static readonly GLEXTENSION_BY_TEXTURECOMPRESSION = {
            2: "WEBGL_compressed_texture_astc",
            4: "WEBGL_compressed_texture_pvrtc",
            8: "WEBGL_compressed_texture_s3tc",
            16: "WEBGL_compressed_texture_etc1",
        }

        // should be in sync with grunt asset & texture script
        static readonly IMAGEEXT_BY_TEXTURECOMPRESSION = {
            2: "ASTC",
            4: "PVRX",
            8: "S3TC",
            16: "ETCX",
        }

        // should be in sync with grunt asset & texture script
        static readonly IMAGEFORMAT_BY_TEXTURECOMPRESSION = {
            2: "ASTC",
            4: "PVRTC",
            8: "S3TC",
            16: "ETC1",
        }

        private _phase = 1; // only 1 loading phase for now (ie: low res then normal res)
        private _infoByPath = new plume.HashMap<TextureCompressionAndQuality>();
        private _imageDataByPath = new plume.HashMap<ImageDataHolder>();

        private _nextPhaseUrls = new Array<{ original: string, next: string }>();
        private _compressionFormat: TextureCompression = null;

        public progressiveLoadingEnabled = true;

        initialize(texturesInfos: Array<TextureCompressionAndQuality>, loader: Loader3d) {
            let self = this;

            let compressionFormat = 1;
            let renderer = Engine.get().renderer;

            for (let i = 0; i < texturesInfos.length; i++) {
                let info = texturesInfos[i];
                this._infoByPath.put(info.path, info);

                compressionFormat = (compressionFormat | info.format);

                let dotIdx = info.path.lastIndexOf(".");
                let slashIdx = info.path.lastIndexOf("/");
                let imageData: ImageDataHolder = {
                    path: info.path.substring(0, slashIdx),
                    name: info.path.substring(slashIdx + 1, dotIdx),
                    ext: info.path.substring(dotIdx + 1),
                }
                this._imageDataByPath.put(info.path, imageData);
            }

            loader.onLoaded(function () {
                self._onFirstLoadComplete();
            });

            // We suppose that for all compressed image we have the same compressed version available
            // (ie: we have ASTC & S3TC for all compressed image, not ASTC & S3TC for 1 image, ASTC & PVR for another image, etc.)
            let allFormats = [TextureCompression.ASTC, TextureCompression.S3TC, TextureCompression.PVR, TextureCompression.ETC];
            for (let i = 0; i < allFormats.length; i++) {
                let f = allFormats[i];
                let supported = this._isSupported(f, compressionFormat);
                if (supported) {
                    // logger.debug("Using compression format " + f);
                    this._compressionFormat = f;
                    break;
                }
            }
        }

        private _isSupported(format: TextureCompression, compressionFormat: number) {
            // if ((compressionFormat & TextureCompression.ASTC) == TextureCompression.ASTC) {
            if ((compressionFormat & format) == format) {
                // Format used by texture
                // Does renderer support it ?
                let extension = ProgressiveTextureLoaderManager.GLEXTENSION_BY_TEXTURECOMPRESSION[format];
                return (Engine.get().renderer.extensions.get(extension) != null);
            } else {
                // Format not used
                return false;
            }
        }

        // Override TextureLoaderManager to handle:
        // - low res loading
        // - compressed texture loading
        load(url: string, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture {
            let realUrl = url;
            let compressed = false;

            let info = this._infoByPath.get(url);
            if (info != null) {
                let lowRes = this._getLowRes(info);
                let extension = this._getCompressionExt(info);
                let imagedata = this._imageDataByPath.get(url);

                realUrl = imagedata.path + "/" + imagedata.name;

                if (lowRes) {
                    realUrl += "_LOW";
                }

                if (extension != null) {
                    realUrl += "_" + extension + ".ktx";
                    compressed = true;
                } else {
                    realUrl += "." + imagedata.ext;
                }

                if (lowRes) {
                    let nextPhaseUrl = imagedata.path + "/" + imagedata.name;
                    if (extension != null) {
                        nextPhaseUrl += "_" + extension + ".ktx";
                    } else {
                        nextPhaseUrl += "." + imagedata.ext;
                    }
                    this._nextPhaseUrls.push({ original: url, next: nextPhaseUrl }); // Track for 2d load
                }

            } else {
                // No compression/resolution data, ignore
            }

            let texture = this._load(realUrl, compressed, onLoad, onError);
            this.cache.add(url, texture);
            return texture;
        }

        getCompressionFormat(): TextureCompression {
            return this._compressionFormat;
        }
        getCompressionFormatAsStr(): string {
            if (this._compressionFormat == null) return "";
            return ProgressiveTextureLoaderManager.IMAGEFORMAT_BY_TEXTURECOMPRESSION[this._compressionFormat];
        }

        private _getLowRes(textureInfo: TextureCompressionAndQuality) {
            if (this._phase > 1) return false; // phase 2 => HR texture
            return ((textureInfo.quality & TextureQuality.LOW) == TextureQuality.LOW); // phase 1 => do we have low-res texture ?
        }

        private _getCompressionExt(textureInfo: TextureCompressionAndQuality) {
            if (this._compressionFormat == null) return null; // No browser support

            let supported = ((textureInfo.format & this._compressionFormat) == this._compressionFormat);
            if (!supported) return null; // No texture for the supported compression format

            let extension = ProgressiveTextureLoaderManager.IMAGEEXT_BY_TEXTURECOMPRESSION[this._compressionFormat];
            return extension;
        }

        private _load(url: string, isCompressed: boolean, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture {
            if (isCompressed) {
                // KTX file
                let loader = new KTXLoader(this.manager);
                let texture = loader.load(url, onLoad, onError);
                return texture;
            } else {
                // Regular image
                let loader = new TextureLoader(this.manager);
                let texture = loader.load(url, onLoad, onError);
                return texture;
            }
        }

        private _onFirstLoadComplete() {
            if (!this.progressiveLoadingEnabled) return; // HD texture loading explicitely disabled by user

            // console.log("Upgrading textures..");

            // First load complete, upgrade to higher resolution
            this._phase++;

            for (let i = 0; i < this._nextPhaseUrls.length; i++) {
                this._upgradeTexture(this._nextPhaseUrls[i]);
            }
            this._nextPhaseUrls = [];
        }

        private _upgradeTexture(data: { original: string, next: string }) {
            let texture = this.cache.get(data.original);
            if (texture == null) {
                logger.warn("No mapping found for texture " + data.original);
                return;
            }

            let extension = data.next.substring(data.next.length - 3);
            if (extension == "ktx") {
                // Compressed texture
                let loader = new KTXLoader(this.manager);
                loader.swap(data.next, texture, function () {
                    // logger.debug("HR version of " + data.original + " loaded (" + data.next + ")");
                });

            } else {
                // Normal texture
                let loader = new TextureLoader(this.manager);
                loader.swap(data.next, texture, function () {
                    // logger.debug("HR version of " + data.original + " loaded (" + data.next + ")");
                });
            }
        }

    }

}