module plume {

    // Entry point of texture loading
    export class TextureLoaderManager {

        constructor(public manager?: THREE.LoadingManager, public cache?: TextureCache) {
            if (manager == null) {
                this.manager = THREE.DefaultLoadingManager;
            }
            if (cache == null) {
                this.cache = Engine.get().textureCache;
            }
        }

        load(url: string, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture {
            // Default, load regular image
            let loader = new TextureLoader(this.manager);
            let texture = loader.load(url, onLoad, onError);
            this.cache.add(url, texture);
            return texture;
        }
    }


    export class TextureCache {
        private _textureByUrl = new HashMap<THREE.Texture>();

        constructor() {
        }

        add(url: string, texture: THREE.Texture) {
            this._textureByUrl.put(url, texture);
        }

        get(url: string) {
            return this._textureByUrl.get(url);
        }
    }

}