module plume {

    export class DynamicTexture {

        private _texture: THREE.Texture;
        private _loaded: boolean;

        private _context2d: CanvasRenderingContext2D;
        private _width = 0;
        private _height = 0;

        heightRatio = 3.65;

        constructor(private canvas: HTMLCanvasElement) {
            this._context2d = canvas.getContext("2d");
            this._width = this.canvas.width;
            this._height = this.canvas.height;
            this._texture = new THREE.Texture(canvas);
        }

        static fromSize(width: number, height: number): DynamicTexture {
            let canvas = document.createElement("canvas");
            canvas.width = width;
            canvas.height = height;
            return new DynamicTexture(canvas);
        }

        _debug() {
            this.canvas.style.zIndex = "15";
            this.canvas.style.position = "absolute";
            document.body.appendChild(this.canvas);
        }

        update() {
            this._texture.needsUpdate = true;
        }

        get texture(): THREE.Texture {
            return this._texture;
        }
        get context2d(): CanvasRenderingContext2D {
            return this._context2d;
        }

        clear() {
            this.context2d.clearRect(0, 0, this._width, this._height);
        }

        // shortcut to draw text
        drawText(text: string, fontName: string, fontWeight: string, fontSize: number, color: string, bgColor: string = null, x: number = null, y: number = null, update = true) {
            let font = fontWeight + " " + fontSize + "px" + " " + fontName;
            if (bgColor != null) {
                this._context2d.fillStyle = bgColor;
                this._context2d.fillRect(0, 0, this._width, this._height);
            }

            this._context2d.font = font;
            this._context2d.textBaseline = "alphabetic";
            if (x == null) {
                // center
                let textSize = this._context2d.measureText(text);
                x = (this._width - textSize.width) / 2;
            }
            if (y == null) {
                // center
                let textHeight = fontSize / this.heightRatio;
                y = (this._width - textHeight) / 2;
            }

            this._context2d.font = font;
            this._context2d.textBaseline = "alphabetic";
            this._context2d.fillStyle = color;
            this._context2d.fillText(text, x, y);

            if (update) {
                this.update();
            }
        }
    }

}