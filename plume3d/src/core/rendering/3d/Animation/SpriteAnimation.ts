module plume {

    export class SpriteAnimation implements Updatable {
        private _curIndex: number = 0;
        private _playing: boolean = false;

        private _startTime: number = 0;
        private _lastIndex: number = -1;

        private _repeats: number = 1;
        private _totalFrames: number;
        private _tilesCount: number;

        private _onEnd: Handler<any>;

        fps: number = 1000 / 60;
        animationSpeed: number = 1;
        destroyOnEnd = true;

        constructor(private _texture: THREE.Texture, private _xframes: number, private _yframes: number) {
            // ! texture updated by ref !
            this._tilesCount = _xframes * _yframes;

            this._texture.wrapS = _texture.wrapT = THREE.RepeatWrapping;
            this._texture.repeat.set(1 / this._xframes, 1 / this._yframes);

            Game.get().addUpdatable(this);
        }

        start() {
            this._playing = true;

            this._totalFrames = this._tilesCount;
            this._curIndex = 0;

            this._startTime = performance.now();

            this._SetFrameIndex(0);

            // compute animation end
            if (this._repeats < 0) {
                this._lastIndex = -1;
            } else {
                this._lastIndex = this._curIndex + this._totalFrames * this._repeats;
            }
        }

        stop() {
            this._playing = false;
        }

        destroy() {
            this.stop();
            Game.get().removeUpdatable(this);
        }

        update(dt: number) {
            if (!this._playing) return;

            let frameEllapsed = Math.floor((performance.now() - this._startTime) / this.fps);
            frameEllapsed = Math.floor(frameEllapsed * this.animationSpeed);

            this._SetActiveFrame(frameEllapsed);
        }

        onEnd(cb: () => void) {
            if (this._onEnd == null) {
                this._onEnd = new Handler<any>();
            }
            this._onEnd.add(cb);
        }

        private _SetActiveFrame(index: number) {
            if (index == null) return;

            let newIndex = index % (this._totalFrames);
            if (newIndex == this._curIndex) return;

            this._curIndex = newIndex;

            this._SetFrameIndex(this._curIndex);

            if (this._lastIndex > 0) {
                index = Math.abs(index);
                if (index >= this._lastIndex) {
                    // time to stop
                    this._SetFrameIndex(this._tilesCount - 1);
                    this._OnAnimationEnded();
                }
            }
        }

        private _SetFrameIndex(index: number) {
            let currentColumn = index % this._xframes;
            let currentRow = Math.floor(index / this._xframes);
            this._texture.offset.x = currentColumn / this._xframes;
            this._texture.offset.y = currentRow / this._yframes;
        }

        private _OnAnimationEnded() {
            this._playing = false;
            if (this._onEnd != null) this._onEnd.fire({});
            if (this.destroyOnEnd) {
                this.destroy();
            }
        }

        repeat(repeats: number = 0) {
            this._repeats = repeats;
        }

        isPlaying(): boolean {
            return this._playing;
        }

        ////////////// static helper

        static onPlane(texture: THREE.Texture, xframes: number, yframes: number) {
            let tex = texture.clone();
            tex.needsUpdate = true;
            let mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), new THREE.MeshBasicMaterial({ map: tex, transparent: true, side: THREE.DoubleSide }));
            let spriteAnimator = new plume.SpriteAnimation(tex, xframes, yframes);
            spriteAnimator.onEnd(function () {
                if (mesh.parent) mesh.parent.remove(mesh);
            });
            return {
                mesh: mesh,
                animator: spriteAnimator,
                texture: tex,
            }
        }

        // Use sprite : always facing the camera
        static onSprite(texture: THREE.Texture, xframes: number, yframes: number) {
            let tex = texture.clone();
            tex.needsUpdate = true;
            let mesh = new THREE.Sprite(new THREE.SpriteMaterial({ map: tex, transparent: true, side: THREE.DoubleSide }));
            let spriteAnimator = new plume.SpriteAnimation(tex, xframes, yframes);
            spriteAnimator.onEnd(function () {
                if (mesh.parent) mesh.parent.remove(mesh);
            });
            return {
                mesh: mesh,
                animator: spriteAnimator,
                texture: tex,
            }
        }
    }
}