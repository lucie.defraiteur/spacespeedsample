module plume {

    export class Renderer {

        public static _applyStyle(canvas: HTMLCanvasElement) {
            let self = this;
            let vendors = [
                '-webkit-',
                '-khtml-',
                '-moz-',
                '-ms-',
                ''
            ];

            vendors.forEach(function (vendor) {
                canvas.style[vendor + 'user-select'] = "none";
            });
            canvas.style['-webkit-touch-callout'] = "none";
            canvas.style['-webkit-tap-highlight-color'] = 'rgba(0, 0, 0, 0)';
        }
    }

}