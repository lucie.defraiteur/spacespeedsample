module plume {

    export interface Gui {
        update(dt: number): void;
        render(dt: number, interpolation: number): void;
    }

}