﻿module plume {


    export class DeviceManager {

        readyCallback: any;

        os: OS = new OS();
        fullscreenManager = new FullScreenManager();
        visibilityManager = new VisibilityManager();

        whenReady(callback: () => void) {
            let ready = document.readyState == 'complete';
            this.readyCallback = callback;
            if (ready) {
                this.os.init();
                this.readyCallback();
            } else {
                document.addEventListener('DOMContentLoaded', this._checkReady);
                window.addEventListener('load', this._checkReady);
            }
        }

        private _checkReady = () => {
            if (!document.body) {
                window.setTimeout(this._checkReady, 20);
            } else {

                this.os.init();

                document.removeEventListener('DOMContentLoaded', this._checkReady);
                window.removeEventListener('load', this._checkReady);
                this.readyCallback();
            }
        }
    }


}