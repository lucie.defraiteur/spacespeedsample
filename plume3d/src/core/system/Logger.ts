module plume {

    // Keep only one instance for the game.
    // Ie: use logger.debug(...)
    export class Logger {

        protected level = 0;
        protected listeners: Handler<{ level: string, message: any, error?: any }>;

        setLevel(level: number) {
            this.level = level;
        }

        debug(message: any) {
            if (this.level > 0) return;
            console.debug(message)
            if (this.listeners != null) this.listeners.fire({ level: "debug", message: message });
        }

        info(message: any) {
            if (this.level > 1) return;
            console.info(message)
            if (this.listeners != null) this.listeners.fire({ level: "info", message: message });
        }

        warn(message: any) {
            if (this.level > 2) return;
            console.warn(message)
            if (this.listeners != null) this.listeners.fire({ level: "warn", message: message });
        }

        error(message: any, error?: any) {
            if (this.level > 3) return;

            console.error(message, error);
            if (this.listeners != null) this.listeners.fire({ level: "error", message: message, error: error });
        }

        addListener(i: Listener<{ level: string, message: any, error?: any }>) {
            if (this.listeners == null) this.listeners = new Handler();

            this.listeners.add(i);
        }
    }

    export let logger: Logger = new Logger();


    //
    // Use it when no access to devtools
    //

    let _debugConsole: HTMLDivElement;
    let _debugconsoleChildren: Array<HTMLDivElement> = [];

    export function enableDomConsole() {
        let div = document.createElement("div");
        div.id = "plume-logger";
        div.style.background = "white";
        div.style.fontSize = "10px";
        div.style.fontFamily = "Consolas, monaco, monospace";
        div.style.position = "absolute";
        div.style.top = "0";
        div.style.zIndex = "11";
        document.body.appendChild(div);
        let top = document.createElement("div");
        div.appendChild(top);
        _debugconsoleChildren.push(top);
        _debugConsole = div;

        logger.addListener(function (e) {
            append(e.level, e.message, e.error);
        });
    }

    function append(level: string, message: string, error?: any) {
        let top = _debugconsoleChildren[0];
        let line = document.createElement("div");
        line.innerHTML = "<b>" + level + "</b>: " + message + (error == null ? "" : ", " + error);
        _debugConsole.insertBefore(line, top);
        _debugconsoleChildren.splice(0, 0, line);
        top = line;
        if (_debugconsoleChildren.length > 8) {
            let last = _debugconsoleChildren[_debugconsoleChildren.length - 1];
            _debugconsoleChildren.splice(_debugconsoleChildren.length - 1, 1);
            _debugConsole.removeChild(last);
        }
    }
}