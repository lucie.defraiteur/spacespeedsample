module plume.Datef {

    export let oneday = 24 * 60 * 60 * 1000;
    export let serverTimeDiff: number = 0;

    // allow to fix client time by setting server diff time
    export function now() {
        let browserDate = Date.now();
        return browserDate - serverTimeDiff;
    }

}