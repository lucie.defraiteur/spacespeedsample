﻿
module plume {

    export class TimeManager implements Updatable {

        _now: number;

        private _timers: Array<Timer> = [];

        private static _TIME_MANAGER: TimeManager;

        private _inUpdateLoop = false;
        private _timersToAdd: Array<Timer> = [];
        private _timersToRemove: Array<Timer> = [];

        constructor() {
            TimeManager._TIME_MANAGER = this;
        }

        static get() {
            if (TimeManager._TIME_MANAGER == null) {
                TimeManager._TIME_MANAGER = new TimeManager();
            }
            return TimeManager._TIME_MANAGER;
        }

        update(dt: number) {
            this._inUpdateLoop = true;
            this._now = Time.now();


            let self = this;
            let lastExpired = -1;
            for (let i = this._timers.length - 1; i >= 0; i--) {
                let t = this._timers[i];
                let notRunning = t.update(this._now);
                if (notRunning && t.hasExpired()) {
                    //this._timers.slice(i, 1);
                    this._timersToRemove.push(t);
                }
            }


            this._inUpdateLoop = false;

            if (this._timersToRemove.length > 0) {
                this._timersToRemove.forEach(function (timer) {
                    self.remove(timer);
                });
                this._timersToRemove.length = 0;
            }

            if (this._timersToAdd.length > 0) {
                this._timersToAdd.forEach(function (timer) {
                    self._pushTimer(timer);
                });
                this._timersToAdd.length = 0;
            }
        }



        private _pushTimer(timer: Timer) {
            this._timers.push(timer);
        }

        remove(timer: Timer) {

            if (this._inUpdateLoop) {
                this._timersToRemove.push(timer);
                return;
            }

            let index = this._timers.indexOf(timer);
            if (index >= 0) {
                this._timers.splice(index, 1);
            }
        }

        removeAll() {
            for (let i = 0; i < this._timers.length; i++) {
                this._timers[i]._expired = true;
            }
            this._timers = [];
        }

        cancelAll() {
            for (let i = this._timers.length - 1; i >= 0; i--) {
                this._timers[i].cancel();
            }
        }


        createTimer(delay: number, callback?: Function, thisArg?: any, argArray?: any[]): Timer {
            let timer = new Timer(this, delay, callback, thisArg, argArray);
            if (this._inUpdateLoop) {
                this._timersToAdd.push(timer);
            } else {
                this._pushTimer(timer);
            }
            return timer;
        }

        createAndStartTimer(delay: number, callback?: Function, thisArg?: any, argArray?: any[]): Timer {
            let timer = this.createTimer(delay, callback, thisArg, argArray);
            timer.start();
            return timer;
        }

        static createAndStartTimer(delay: number, callback?: Function, thisArg?: any, argArray?: any[]): Timer {
            return TimeManager.get().createAndStartTimer(delay, callback, thisArg, argArray);
        }

        get activeTimersCount(): number {
            return this._timers.length;
        }

        get activeTimers(): string {
            return this._timers.map(function (elem) {
                return elem.id;
            }).join();
        }

    }

}

