module plume {

    export type SimulationStats = {
        avg: number;
    }

    export interface Simulation {
        running: boolean;
        stats: SimulationStats;
        update(dt: number): void;
    }

}