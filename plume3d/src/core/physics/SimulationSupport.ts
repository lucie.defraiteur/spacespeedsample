module plume {

    export class SimulationSupport {

        protected _running = false;

        update(dt: number) {
            if (!this._running) return;
        }

        setGravity(x: number, y: number, z: number) {
        }

        protected onStart() {
        }
        protected onStop() {
        }

        get running(): boolean {
            return this._running;
        }
        set running(v: boolean) {
            if (this._running == v) return;

            this._running = v;

            if (this._running) this.onStart();
            else this.onStop();
        }

    }

    export type BodyParameter = {
        mass: number
    }
}