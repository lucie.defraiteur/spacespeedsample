/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    // THREE.QuaternionKeyframeTrack.prototype.InterpolantFactoryMethodSmooth = function (result) {
    //     return new THREE.QuaternionLinearInterpolant(this.times, this.values, this.getValueSize(), result);
    // }

    export class GLTFAnimationImporter extends GLTFImporter {

        import() {
            if (this.parser.json.animations == null || this.parser.json.animations.length <= 0) return;

            for (let i = 0; i < this.parser.json.animations.length; i++) {
                let animationDef = this.parser.json.animations[i];
                let anim = this._loadAnimation(animationDef, i);
                if (animationDef.extras != null && (animationDef.extras.root != null || animationDef.extras.othersRoot != null)) {
                    // GB custom
                    // Anim can be shared
                    let root = (animationDef.extras.root == null ? this.parser._scenes[0] : this.parser._nodes[animationDef.extras.root]);
                    let relative = GLTFAnimationsHelper.convertToRelative(root, anim);
                    this.parser._animationsRelatives.push({
                        root: root,
                        clip: relative,
                    });

                    if (animationDef.extras.othersRoot != null) {
                        for (let i = 0; i < animationDef.extras.othersRoot.length; i++) {
                            let otherId = animationDef.extras.othersRoot[i];
                            let other = this.parser._nodes[otherId];
                            this.parser._animationsRelatives.push({
                                root: other,
                                clip: relative,
                            });
                        }
                    }
                } else {
                    // Regular "absolute" animation
                    this.parser._animations.push(anim);
                }
            }
        }

        private _loadAnimation(animationDef: gltfspec.Animation, index: number) {

            let tracks = [];
            for (let i = 0; i < animationDef.channels.length; i++) {

                let channel = animationDef.channels[i];
                let sampler = animationDef.samplers[channel.sampler];
                if (sampler == null) {
                    logger.warn("No sampler " + channel.sampler + " in " + JSON.stringify(animationDef));
                    continue;
                }

                let target = channel.target;
                let input = animationDef.parameters !== undefined ? animationDef.parameters[sampler.input] : sampler.input;
                let output = animationDef.parameters !== undefined ? animationDef.parameters[sampler.output] : sampler.output;

                let inputAccessor = this.parser._accessors[input];
                let outputAccessor = this.parser._accessors[output];
                let node = this.parser._nodes[target.node];
                if (node == null) {
                    logger.warn("Node " + target.node + " used in " + animationDef.name + " does not exists.");
                    continue;
                }

                node.updateMatrix();
                node.matrixAutoUpdate = true;

                let TypedKeyframeTrack: typeof THREE.KeyframeTrack;
                switch (PATH_PROPERTIES[target.path]) {
                    case PATH_PROPERTIES.weights:
                        TypedKeyframeTrack = THREE.NumberKeyframeTrack;
                        break;
                    case PATH_PROPERTIES.rotation:
                        TypedKeyframeTrack = THREE.QuaternionKeyframeTrack;
                        break;
                    case PATH_PROPERTIES.translation:
                    case PATH_PROPERTIES.scale:
                    default:
                        TypedKeyframeTrack = THREE.VectorKeyframeTrack;
                        break;
                }

                let targetName = node.name ? node.name : node.uuid;
                let interpolation = sampler.interpolation !== undefined ? INTERPOLATION[sampler.interpolation] : THREE.InterpolateLinear;
                let targetNames: string[] = [];

                if (PATH_PROPERTIES[target.path] === PATH_PROPERTIES.weights) {
                    // node can be THREE.Group here but
                    // PATH_PROPERTIES.weights(morphTargetInfluences) should be
                    // the property of a mesh object under group.
                    node.traverse(function (object) {
                        if (object["isMesh"] === true && object["morphTargetInfluences"]) {
                            targetNames.push(object.name ? object.name : object.uuid);
                        }
                    });
                } else {
                    targetNames.push(targetName);
                }

                // KeyframeTrack.optimize() will modify given 'times' and 'values'
                // buffers before creating a truncated copy to keep. Because buffers may
                // be reused by other tracks, make copies here.
                for (let j = 0; j < targetNames.length; j++) {
                    let track = new TypedKeyframeTrack(
                        targetNames[j] + '.' + PATH_PROPERTIES[target.path],
                        THREE.AnimationUtils.arraySlice(inputAccessor.array, 0, undefined),
                        THREE.AnimationUtils.arraySlice(outputAccessor.array, 0, undefined),
                        interpolation
                    );


                    // Here is the trick to enable custom interpolation.
                    // Overrides .createInterpolant in a factory method which creates custom interpolation.
                    if (sampler.interpolation === 'CUBICSPLINE') {
                        track.createInterpolant = function InterpolantFactoryMethodGLTFCubicSpline(result) {
                            // A CUBICSPLINE keyframe in glTF has three output values for each input value,
                            // representing inTangent, splineVertex, and outTangent. As a result, track.getValueSize()
                            // must be divided by three to get the interpolant's sampleSize argument.
                            return new GLTFCubicSplineInterpolant(this.times, this.values, this.getValueSize() / 3, result);
                        };
                        // Workaround, provide an alternate way to know if the interpolant type is cubis spline to track.
                        // track.getInterpolation() doesn't return valid value for custom interpolant.
                        // track.createInterpolant.isInterpolantFactoryMethodGLTFCubicSpline = true;
                    }
                    tracks.push(track);
                }

            }

            let name = animationDef.name !== undefined ? animationDef.name : 'animation_' + index;
            return new THREE.AnimationClip(name, -1, tracks);
        }

    }


    class GLTFCubicSplineInterpolant extends THREE.Interpolant {

        copySampleValue_(index: number) {
            // Copies a sample value to the result buffer. See description of glTF
            // CUBICSPLINE values layout in interpolate_() function below.
            let result = this.resultBuffer;
            let values = this.sampleValues;
            let valueSize = this.valueSize;
            let offset = index * valueSize * 3 + valueSize;

            for (let i = 0; i !== valueSize; i++) {
                result[i] = values[offset + i];
            }
            return result;
        }

        // GLTFCubicSplineInterpolant.prototype.beforeStart_ = GLTFCubicSplineInterpolant.prototype.copySampleValue_;
        // GLTFCubicSplineInterpolant.prototype.afterEnd_ = GLTFCubicSplineInterpolant.prototype.copySampleValue_;

        beforeStart_(index: number) {
            this.copySampleValue_(index);
        }
        afterEnd_(index: number) {
            this.copySampleValue_(index);
        }

        interpolate_(i1: number, t0: number, t: number, t1: number) {

            let result = this.resultBuffer;
            let values = this.sampleValues;
            let stride = this.valueSize;

            let stride2 = stride * 2;
            let stride3 = stride * 3;

            let td = t1 - t0;

            let p = (t - t0) / td;
            let pp = p * p;
            let ppp = pp * p;

            let offset1 = i1 * stride3;
            let offset0 = offset1 - stride3;

            let s0 = 2 * ppp - 3 * pp + 1;
            let s1 = ppp - 2 * pp + p;
            let s2 = - 2 * ppp + 3 * pp;
            let s3 = ppp - pp;

            // Layout of keyframe output values for CUBICSPLINE animations:
            //   [ inTangent_1, splineVertex_1, outTangent_1, inTangent_2, splineVertex_2, ... ]
            for (let i = 0; i !== stride; i++) {
                let p0 = values[offset0 + i + stride]; // splineVertex_k
                let m0 = values[offset0 + i + stride2] * td; // outTangent_k * (t_k+1 - t_k)
                let p1 = values[offset1 + i + stride]; // splineVertex_k+1
                let m1 = values[offset1 + i] * td; // inTangent_k+1 * (t_k+1 - t_k)
                result[i] = s0 * p0 + s1 * m0 + s2 * p1 + s3 * m1;
            }

            return result;
        }

    }

}