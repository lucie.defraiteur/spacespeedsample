/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFSkinImporter extends GLTFImporter {

        import() {
            if (this.parser.json.skins == null || this.parser.json.skins.length <= 0) return;

            for (let i = 0; i < this.parser.json.skins.length; i++) {
                let skinDef = this.parser.json.skins[i];
                this._loadSkin(skinDef, i);
            }
        }

        private _loadSkin(skinDef: gltfspec.Skin, index: number) {
            let skinEntry: SkinEntry = { joints: skinDef.joints, inverseBindMatrices: null };
            if (skinDef.inverseBindMatrices != null) {
                skinEntry.inverseBindMatrices = this.parser._accessors[skinDef.inverseBindMatrices] as THREE.BufferAttribute;
            }
            this.parser._skins[index] = skinEntry;
        }

    }
}