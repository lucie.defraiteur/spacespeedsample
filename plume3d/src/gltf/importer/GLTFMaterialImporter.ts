/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFMaterialImporter extends GLTFImporter {

        import() {
            if (this.parser.json.materials == null || this.parser.json.materials.length <= 0) return;

            for (let i = 0; i < this.parser.json.materials.length; i++) {
                let materialDef = this.parser.json.materials[i];
                this._loadMaterial(materialDef, i);
            }
        }

        private _loadMaterial(materialDef: gltfspec.Material, index: number) {
            let materialType: (typeof THREE.ShaderMaterial) | (typeof THREE.MeshStandardMaterial) | (typeof THREE.MeshBasicMaterial);
            let materialParams: any = {};
            let materialExtensions: ExtensionsType = materialDef.extensions || {};

            if (materialExtensions == "KHR_materials_pbrSpecularGlossiness") {
                let sgExtension = this.parser.extensions.KHR_materials_pbrSpecularGlossiness;
                materialType = sgExtension.getMaterialType(materialDef);
                sgExtension.extendParams(materialParams, materialDef, this.parser);

            } else if (materialExtensions == "KHR_materials_unlit") {
                let kmuExtension = this.parser.extensions.KHR_materials_unlit;
                materialType = kmuExtension.getMaterialType(materialDef);
                kmuExtension.extendParams(materialParams, materialDef, this.parser);

            } else {
                // Specification:
                // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#metallic-roughness-material
                materialType = THREE.MeshStandardMaterial;

                let metallicRoughness = materialDef.pbrMetallicRoughness || {};
                materialParams.color = new THREE.Color(1.0, 1.0, 1.0);
                materialParams.opacity = 1.0;

                if (Array.isArray(metallicRoughness.baseColorFactor)) {
                    let array = metallicRoughness.baseColorFactor;
                    materialParams.color.fromArray(array);
                    materialParams.opacity = array[3];
                }

                if (metallicRoughness.baseColorTexture !== undefined) {
                    this.parser.assignTexture(metallicRoughness.baseColorTexture, materialParams, 'map');
                }

                materialParams.metalness = metallicRoughness.metallicFactor !== undefined ? metallicRoughness.metallicFactor : 1.0;
                materialParams.roughness = metallicRoughness.roughnessFactor !== undefined ? metallicRoughness.roughnessFactor : 1.0;

                if (metallicRoughness.metallicRoughnessTexture !== undefined) {
                    let textureIndex = metallicRoughness.metallicRoughnessTexture.index;
                    this.parser.assignTexture(metallicRoughness.metallicRoughnessTexture, materialParams, 'metalnessMap');
                    this.parser.assignTexture(metallicRoughness.metallicRoughnessTexture, materialParams, 'roughnessMap');
                }
            }

            if (materialDef.doubleSided === true) {
                materialParams.side = THREE.DoubleSide;
            }

            let alphaMode = materialDef.alphaMode || ALPHA_MODES.OPAQUE;
            if (alphaMode === ALPHA_MODES.BLEND) {
                materialParams.transparent = true;
            } else {
                materialParams.transparent = false;
                if (alphaMode === ALPHA_MODES.MASK) {
                    materialParams.alphaTest = materialDef.alphaCutoff !== undefined ? materialDef.alphaCutoff : 0.5;
                }
            }

            if (materialDef.normalTexture !== undefined && materialType !== THREE.MeshBasicMaterial) {
                this.parser.assignTexture(materialDef.normalTexture, materialParams, 'normalMap');
                materialParams.normalScale = new THREE.Vector2(1, 1);
                if (materialDef.normalTexture.scale !== undefined) {
                    materialParams.normalScale.set(materialDef.normalTexture.scale, materialDef.normalTexture.scale);
                }
            }

            if (materialDef.occlusionTexture !== undefined && materialType !== THREE.MeshBasicMaterial) {
                this.parser.assignTexture(materialDef.occlusionTexture, materialParams, 'aoMap');
                if (materialDef.occlusionTexture.strength !== undefined) {
                    materialParams.aoMapIntensity = materialDef.occlusionTexture.strength;
                }
            }

            if (materialDef.emissiveFactor !== undefined && materialType !== THREE.MeshBasicMaterial) {
                materialParams.emissive = new THREE.Color().fromArray(materialDef.emissiveFactor);
            }

            if (materialDef.emissiveTexture !== undefined && materialType !== THREE.MeshBasicMaterial) {
                this.parser.assignTexture(materialDef.emissiveTexture, materialParams, 'emissiveMap');
            }

            let material: any; //THREE.ShaderMaterial | THREE.MeshStandardMaterial | THREE.MeshBasicMaterial;
            if (materialExtensions == "KHR_materials_pbrSpecularGlossiness") {
                material = this.parser.extensions.KHR_materials_pbrSpecularGlossiness.createMaterial(materialParams);
            } else {
                if (this.parser._factory.materialFactory != null) {
                    material = this.parser._factory.materialFactory(materialType, materialParams);
                } else {
                    material = new (materialType as any)(materialParams);
                }
            }

            if (materialDef.name !== undefined) material.name = materialDef.name;

            // Normal map textures use OpenGL conventions:
            // https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#materialnormaltexture
            if (material.normalScale) {
                material.normalScale.y = -material.normalScale.y;
            }

            // baseColorTexture, emissiveTexture, and specularGlossinessTexture use sRGB encoding.
            if (material.map) material.map.encoding = THREE.sRGBEncoding;
            if (material.emissiveMap) material.emissiveMap.encoding = THREE.sRGBEncoding;
            if (material.specularMap) material.specularMap.encoding = THREE.sRGBEncoding;

            assignExtrasToUserData(material, materialDef);

            if (this.parser._factory.materialProcessor != null) {
                material = this.parser._factory.materialProcessor(material);
            }

            // if (materialDef.extensions) addUnknownExtensionsToUserData(extensions, material, materialDef);
            this.parser._materials[index] = material;
        }

    }
}