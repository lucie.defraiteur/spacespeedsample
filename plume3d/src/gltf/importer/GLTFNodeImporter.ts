/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFNodeImporter extends GLTFImporter {

        import() {
            if (this.parser.json.nodes == null || this.parser.json.nodes.length <= 0) return;

            for (let i = 0; i < this.parser.json.nodes.length; i++) {
                let nodeDef = this.parser.json.nodes[i];
                this._loadNode(nodeDef, i);
            }
        }

        private _loadNode(nodeDef: gltfspec.Node, index: number) {

            // let json = this.parser.json;
            // let extensions = this.parser.extensions;
            let meshReferences = this.parser.json.meshReferences;
            let meshUses = this.parser.json.meshUses;
            // let nodeDef = json.nodes[nodeIndex];


            let node;

            // .isBone isn't in glTF spec. See .markDefs
            if (nodeDef.isBone === true) {
                node = new THREE.Bone();

            } else if (nodeDef.mesh !== undefined) {
                let mesh = this.parser._meshes[nodeDef.mesh];
                if (meshReferences[nodeDef.mesh] > 1) {
                    let instanceNum = meshUses[nodeDef.mesh]++;
                    // node = AnimUtils.clone(mesh);
                    node = mesh.clone();
                    node.name += '_instance_' + instanceNum;

                    // onBeforeRender copy for Specular-Glossiness
                    node.onBeforeRender = mesh.onBeforeRender;
                    for (let i = 0, il = node.children.length; i < il; i++) {
                        node.children[i].name += '_instance_' + instanceNum;
                        node.children[i].onBeforeRender = mesh.children[i].onBeforeRender;
                    }
                } else {
                    node = mesh;
                }

            } else if (nodeDef.camera !== undefined) {
                node = this.parser._cameras[nodeDef.camera];

            } else if (nodeDef.extensions) {
                if (nodeDef.extensions.KHR_lights_punctual != null) {
                    if (nodeDef.extensions.KHR_lights_punctual.light != null) {
                        let lights = this.parser.extensions.KHR_lights_punctual.lights;
                        node = lights[nodeDef.extensions.KHR_lights_punctual.light];
                    }
                }

            } else {
                node = new THREE.Object3D();
            }

            if (node == null) {
                logger.warn("construct default object3d");
                node = new THREE.Object3D();
            }

            if (nodeDef.name !== undefined) {
                node.name = THREE.PropertyBinding.sanitizeNodeName(nodeDef.name as string);
            }

            if (nodeDef.extensions != null && nodeDef.extensions.GB_lightmaps != null) {
                this.parser.extensions.GB_lightmaps.applyLightmap(node, nodeDef.extensions.GB_lightmaps);
            }

            assignExtrasToUserData(node, nodeDef);
            // if (nodeDef.extensions) addUnknownExtensionsToUserData(extensions, node, nodeDef);

            if (nodeDef.matrix !== undefined) {
                let matrix = new THREE.Matrix4();
                matrix.fromArray(nodeDef.matrix);
                node.applyMatrix(matrix);
            } else {
                if (nodeDef.translation !== undefined) {
                    node.position.fromArray(nodeDef.translation);
                }
                if (nodeDef.rotation !== undefined) {
                    node.quaternion.fromArray(nodeDef.rotation);
                }
                if (nodeDef.scale !== undefined) {
                    node.scale.fromArray(nodeDef.scale);
                }
            }


            if (nodeDef.extras != null && nodeDef.extras.gbunity) {
                this.parser.extensions.GB_Unity.applyToNode(node, nodeDef.extras.gbunity);
            }

            this.parser._nodes[index] = node;
        }
    }

    
}