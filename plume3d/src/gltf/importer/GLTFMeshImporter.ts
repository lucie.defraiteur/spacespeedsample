/// <reference path="GLTFImporter.ts" />

module plume.gltf {

    export class GLTFMeshImporter extends GLTFImporter {

        private _primitiveCache: Array<PrimitiveCacheData> = [];
        private _cache: plume.HashMap<any> = new plume.HashMap();

        import() {
            if (this.parser.json.meshes == null || this.parser.json.meshes.length <= 0) return;

            for (let i = 0; i < this.parser.json.meshes.length; i++) {
                let meshDef = this.parser.json.meshes[i];
                this._loadMesh(meshDef, i);
            }
        }

        private _loadMesh(meshDef: gltfspec.Mesh, index: number) {

            let primitives = meshDef.primitives;
            let originalMaterials: THREE.Material[] = [];

            for (let i = 0; i < primitives.length; i++) {
                originalMaterials[i] = (primitives[i].material === undefined ? createDefaultMaterial() : this.parser._materials[primitives[i].material]);
            }

            let geometries = this._loadGeometries(primitives);
            let isMultiMaterial = (geometries.length == 1 && geometries[0].groups.length > 0);
            let meshes = [];

            for (let i = 0; i < geometries.length; i++) {
                let geometry = geometries[i];
                let primitive = primitives[i];

                // 1. create Mesh
                let mesh: any;
                let material = isMultiMaterial ? originalMaterials : originalMaterials[i];
                if (primitive.mode === WEBGL_CONSTANTS.TRIANGLES ||
                    primitive.mode === WEBGL_CONSTANTS.TRIANGLE_STRIP ||
                    primitive.mode === WEBGL_CONSTANTS.TRIANGLE_FAN ||
                    primitive.mode === undefined) {

                    // .isSkinnedMesh isn't in glTF spec. See .markDefs()
                    mesh = (meshDef.isSkinnedMesh === true ? new THREE.SkinnedMesh(geometry, material as any) : new THREE.Mesh(geometry, material as any));
                    if (primitive.mode === WEBGL_CONSTANTS.TRIANGLE_STRIP) {
                        mesh.drawMode = THREE.TriangleStripDrawMode;
                    } else if (primitive.mode === WEBGL_CONSTANTS.TRIANGLE_FAN) {
                        mesh.drawMode = THREE.TriangleFanDrawMode;
                    }

                } else if (primitive.mode === WEBGL_CONSTANTS.LINES) {
                    mesh = new THREE.LineSegments(geometry, material as any);
                } else if (primitive.mode === WEBGL_CONSTANTS.LINE_STRIP) {
                    mesh = new THREE.Line(geometry, material as any);
                } else if (primitive.mode === WEBGL_CONSTANTS.LINE_LOOP) {
                    mesh = new THREE.LineLoop(geometry, material as any);
                } else if (primitive.mode === WEBGL_CONSTANTS.POINTS) {
                    mesh = new THREE.Points(geometry, material as any);
                } else {
                    throw new Error('THREE.GLTFLoader: Primitive mode unsupported: ' + primitive.mode);
                }

                if (Object.keys((mesh.geometry as THREE.BufferGeometry).morphAttributes).length > 0) {
                    updateMorphTargets(mesh as THREE.Mesh, meshDef);
                }

                mesh.name = meshDef.name || ('mesh_' + index);

                if (geometries.length > 1) mesh.name += '_' + i;

                assignExtrasToUserData(mesh, meshDef);

                meshes.push(mesh);


                // 2. update Material depending on Mesh and BufferGeometry
                let materials = isMultiMaterial ? mesh.material as THREE.Material[] : [mesh.material as THREE.Material];
                let useVertexColors = geometry.attributes.color !== undefined;
                let useFlatShading = geometry.attributes.normal === undefined;
                let useSkinning = (mesh as THREE.SkinnedMesh).isSkinnedMesh === true;
                let useMorphTargets = Object.keys(geometry.morphAttributes).length > 0;
                let useMorphNormals = useMorphTargets && geometry.morphAttributes.normal !== undefined;

                for (let j = 0, jl = materials.length; j < jl; j++) {
                    let material = materials[j];
                    if (mesh instanceof THREE.Points) {
                        let cacheKey = 'PointsMaterial:' + material["uuid"];
                        let pointsMaterial = this._cache.get(cacheKey);
                        if (!pointsMaterial) {
                            pointsMaterial = new THREE.PointsMaterial();
                            THREE.Material.prototype.copy.call(pointsMaterial, material);
                            pointsMaterial.color.copy(material["color"]);
                            pointsMaterial.map = material["map"];
                            pointsMaterial.lights = false; // PointsMaterial doesn't support lights yet
                            this._cache.put(cacheKey, pointsMaterial);
                        }
                        material = pointsMaterial;

                    } else if (mesh instanceof THREE.Line) {

                        let cacheKey = 'LineBasicMaterial:' + material["uuid"];
                        let lineMaterial = this._cache.get(cacheKey);
                        if (!lineMaterial) {
                            lineMaterial = new THREE.LineBasicMaterial();
                            THREE.Material.prototype.copy.call(lineMaterial, material);
                            lineMaterial.color.copy(material["color"]);
                            lineMaterial.lights = false; // LineBasicMaterial doesn't support lights yet
                            this._cache.put(cacheKey, lineMaterial);
                        }
                        material = lineMaterial;
                    }

                    // Clone the material if it will be modified
                    if (useVertexColors || useFlatShading || useSkinning || useMorphTargets) {

                        let cacheKey = 'ClonedMaterial:' + material["uuid"] + ':';
                        if (material["isGLTFSpecularGlossinessMaterial"]) cacheKey += 'specular-glossiness:';
                        if (useSkinning) cacheKey += 'skinning:';
                        if (useVertexColors) cacheKey += 'vertex-colors:';
                        if (useFlatShading) cacheKey += 'flat-shading:';
                        if (useMorphTargets) cacheKey += 'morph-targets:';
                        if (useMorphNormals) cacheKey += 'morph-normals:';

                        let cachedMaterial = this._cache.get(cacheKey);
                        if (!cachedMaterial) {
                            cachedMaterial = material["isGLTFSpecularGlossinessMaterial"]
                                ? this.parser.extensions.KHR_materials_pbrSpecularGlossiness.cloneMaterial(material as any)
                                : material.clone();

                            if (useSkinning) cachedMaterial.skinning = true;
                            if (useVertexColors) cachedMaterial.vertexColors = THREE.VertexColors;
                            if (useFlatShading) cachedMaterial.flatShading = true;
                            if (useMorphTargets) cachedMaterial.morphTargets = true;
                            if (useMorphNormals) cachedMaterial.morphNormals = true;

                            this._cache.put(cacheKey, cachedMaterial);
                        }
                        material = cachedMaterial;
                    }

                    materials[j] = material;

                    // workarounds for mesh and geometry
                    if (material["aoMap"] && geometry.attributes.uv2 === undefined && geometry.attributes.uv !== undefined) {
                        console.log('THREE.GLTFLoader: Duplicating UVs to support aoMap.');
                        geometry.addAttribute('uv2', new THREE.BufferAttribute(geometry.attributes.uv.array, 2));
                    }

                    if (material["isGLTFSpecularGlossinessMaterial"]) {
                        // for GLTFSpecularGlossinessMaterial(ShaderMaterial) uniforms runtime update
                        mesh.onBeforeRender = this.parser.extensions.KHR_materials_pbrSpecularGlossiness.refreshUniforms;
                    }
                }

                mesh.material = isMultiMaterial ? materials : materials[0];
            }

            if (meshes.length === 1) {
                this.parser._meshes[index] = meshes[0];
            } else {
                let group = new THREE.Group();
                for (let i = 0, il = meshes.length; i < il; i++) {
                    group.add(meshes[i]);
                }
                this.parser._meshes[index] = group;
            }
        }

        private _loadGeometries(primitives: Array<gltfspec.MeshPrimitive>): Array<THREE.BufferGeometry> {
            let geometries: Array<THREE.BufferGeometry> = [];
            for (let i = 0; i < primitives.length; i++) {
                let primitive = primitives[i];
                // See if we've already created this geometry
                let cached = getCachedGeometry(this._primitiveCache, primitive);
                if (cached != null) {
                    // Use the cached geometry if it exists
                    geometries.push(cached.geometry);
                } else if (primitive.extensions && primitive.extensions["KHR_draco_mesh_compression"]) {
                    // Use DRACO geometry if available
                    let geometry = this.parser.extensions.KHR_draco_mesh_compression.decodePrimitive(primitive, this.parser);
                    addPrimitiveAttributes(geometry, primitive, this.parser._accessors); // TODO check
                    if (this.parser._factory.geometryProcessor != null) {
                        geometry = this.parser._factory.geometryProcessor(geometry);
                    }

                    // Cache this geometry
                    this._primitiveCache.push({ primitive: primitive, geometry: geometry });
                    geometries.push(geometry);
                } else {
                    // Otherwise create a new geometry
                    let geometry = new THREE.BufferGeometry();
                    addPrimitiveAttributes(geometry, primitive, this.parser._accessors);
                    if (this.parser._factory.geometryProcessor != null) {
                        geometry = this.parser._factory.geometryProcessor(geometry);
                    }

                    // Cache this geometry
                    this._primitiveCache.push({ primitive: primitive, geometry: geometry });
                    geometries.push(geometry);
                }
            }

            return geometries;
        }


    }
}