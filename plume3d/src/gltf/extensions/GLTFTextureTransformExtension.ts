module plume.gltf {

    export type GLTFExtTextureTransformNodeExtension = {
        scale?: Array<number>;
        offset?: Array<number>;
        texCoord?: number;
    }

    export class GLTFTextureTransformExtension {

        name: ExtensionsType;

        constructor() {
            this.name = "EXT_texture_transform";
        }

        apply(texture: THREE.Texture, params: GLTFExtTextureTransformNodeExtension): THREE.Texture {
            texture = texture.clone();
            if (params.scale != null) {
                // texture.repeat.set(1 / params.scale[0], 1 / params.scale[1]);
                texture.repeat.set(params.scale[0], params.scale[1]);
            }
            if (params.offset != null) {
                texture.offset.set(params.offset[0], params.offset[1]);
            }

            texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
            texture.needsUpdate = true;
            return texture;
        }
    }
}