module plume.gltf {

    export let BINARY_EXTENSION_HEADER_MAGIC = 'glTF';
    let BINARY_EXTENSION_HEADER_LENGTH = 12;
    let BINARY_EXTENSION_CHUNK_TYPES = { JSON: 0x4E4F534A, BIN: 0x004E4942 };

    export class GLTFBinaryExtension {

        name: ExtensionsType;
        content: string;
        header: { magic: string, version: number, length: number };
        body: ArrayBuffer;

        constructor(data: ArrayBuffer) {

            this.name = "KHR_binary_glTF";
            this.content = null;
            this.body = null;

            let headerView = new DataView(data, 0, BINARY_EXTENSION_HEADER_LENGTH);
            this.header = {
                magic: THREE.LoaderUtils.decodeText(new Uint8Array(data.slice(0, 4))),
                version: headerView.getUint32(4, true),
                length: headerView.getUint32(8, true)
            };

            if (this.header.magic !== BINARY_EXTENSION_HEADER_MAGIC) {
                throw new Error('Unsupported glTF-Binary header.');
            } else if (this.header.version < 2.0) {
                throw new Error('Legacy binary file detected. Use LegacyGLTFLoader instead.');
            }

            let chunkView = new DataView(data, BINARY_EXTENSION_HEADER_LENGTH);
            let chunkIndex = 0;

            while (chunkIndex < chunkView.byteLength) {

                let chunkLength = chunkView.getUint32(chunkIndex, true);
                chunkIndex += 4;

                let chunkType = chunkView.getUint32(chunkIndex, true);
                chunkIndex += 4;

                if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.JSON) {
                    let contentArray = new Uint8Array(data, BINARY_EXTENSION_HEADER_LENGTH + chunkIndex, chunkLength);
                    this.content = THREE.LoaderUtils.decodeText(contentArray);
                } else if (chunkType === BINARY_EXTENSION_CHUNK_TYPES.BIN) {
                    let byteOffset = BINARY_EXTENSION_HEADER_LENGTH + chunkIndex;
                    this.body = data.slice(byteOffset, byteOffset + chunkLength);
                }

                // Clients must ignore chunks with unknown types.
                chunkIndex += chunkLength;
            }

            if (this.content === null) {
                throw new Error('JSON content not found.');
            }
        }
    }
}