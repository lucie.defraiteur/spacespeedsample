module plume.gltf {

    export class GLTFLightsExtension {

        name: ExtensionsType;
        lights: Array<THREE.Light>;

        constructor(json: gltfspec.GlTf) {
            this.name = "KHR_lights_punctual";
            this.lights = [];

            let extension = (json.extensions && json.extensions.KHR_lights_punctual) || {};
            let lightDefs = extension.lights || [];

            for (let i = 0; i < lightDefs.length; i++) {

                let lightDef = lightDefs[i];
                let lightNode;

                let color = new THREE.Color(0xffffff);
                if (lightDef.color !== undefined) color.fromArray(lightDef.color);

                let range = lightDef.range !== undefined ? lightDef.range : 0;

                switch (lightDef.type) {

                    case 'directional':
                        lightNode = new THREE.DirectionalLight(color);
                        lightNode.target.position.set(0, 0, 1);
                        lightNode.add(lightNode.target);
                        break;

                    case 'point':
                        lightNode = new THREE.PointLight(color);
                        lightNode.decay = 2;
                        lightNode.distance = range;
                        break;

                    case 'spot':
                        lightNode = new THREE.SpotLight(color);
                        lightNode.distance = range;
                        lightNode.decay = 2;
                        // Handle spotlight properties.
                        lightDef.spot = lightDef.spot || {};
                        lightDef.spot.innerConeAngle = lightDef.spot.innerConeAngle !== undefined ? lightDef.spot.innerConeAngle : 0;
                        lightDef.spot.outerConeAngle = lightDef.spot.outerConeAngle !== undefined ? lightDef.spot.outerConeAngle : Math.PI / 4.0;
                        lightNode.angle = lightDef.spot.outerConeAngle;
                        lightNode.penumbra = 1.0 - lightDef.spot.innerConeAngle / lightDef.spot.outerConeAngle;
                        lightNode.target.position.set(0, 0, 1);
                        lightNode.add(lightNode.target);
                        break;

                    default:
                        throw new Error('GLTFLoader: Unexpected light type, "' + lightDef.type + '".');
                }

                if (lightDef.intensity !== undefined) lightNode.intensity = lightDef.intensity;

                lightNode.name = lightDef.name || ('light_' + i);

                this.lights.push(lightNode);
            }
        }
    }
}