module plume.gltf {

    export type GLTFLightMapRootExtension = {
        texture: {
            index: number;
        }
    }
    export type GLTFLightMapNodeExtension = {
        lightmap: number;
        uvScale: Array<number>;
        uvOffset: Array<number>;
    }

    export class GLTFLightMapsExtension {

        parser: GLTFParser;

        name: ExtensionsType;
        lightmaps: Array<GLTFLightMapRootExtension>;

        private _initialized = false;
        private _processedGeometries: Array<string> = [];
        private _processedMaterial: plume.HashMap<THREE.Material> = new plume.HashMap();

        constructor(json: gltfspec.GlTf) {
            this.name = "GB_lightmaps";
            this.lightmaps = [];

            let extension = (json.extensions && json.extensions.GB_lightmaps) || {};
            let lightmapDefs = extension.lightmaps || [];

            for (let i = 0; i < lightmapDefs.length; i++) {
                let lightmapDef: GLTFLightMapRootExtension = lightmapDefs[i];
                this.lightmaps.push(lightmapDef);
            }
        }

        private _initTextures() {
            if (this._initialized) return;

            this._initialized = true;

            for (let i = 0; i < this.lightmaps.length; i++) {
                let textureIdx = this.lightmaps[i].texture.index;
                let texture = this.parser._textures[textureIdx];
                // texture.encoding = THREE.GammaEncoding;
            }
        }

        applyLightmap(node: THREE.Object3D, lightmapDef: GLTFLightMapNodeExtension) {
            let textureIdx = this._getTexture(lightmapDef);
            if (textureIdx == null) {
                return;
            }

            if (!this._initialized) {
                this._initTextures();
            }

            if (node instanceof THREE.Mesh) {
                this._applyLightmapOnMesh(node, lightmapDef);
            } else if (node instanceof THREE.Group) {
                // TODO check..
                for (let c = 0; c < node.children.length; c++) {
                    let child = node.children[c];
                    if (child instanceof THREE.Mesh) {
                        this._applyLightmapOnMesh(child, lightmapDef);
                    } else {
                        logger.warn("Can not apply lightmap on node " + node.type + " (" + node.name + ")");
                    }
                }
            } else {
                logger.warn("Can not apply lightmap on node " + node.type + " (" + node.name + ")");
            }
        }

        private _applyLightmapOnMesh(node: THREE.Mesh, lightmapDef: GLTFLightMapNodeExtension) {
            let textureIdx = this._getTexture(lightmapDef);
            if (textureIdx == null) {
                return;
            }

            let material = node.material;
            if (material instanceof THREE.MeshStandardMaterial || material instanceof THREE.MeshLambertMaterial || material instanceof THREE.MeshPhongMaterial || material instanceof THREE.ShaderMaterial || material instanceof THREE.RawShaderMaterial) {
                // if (material instanceof THREE.MeshStandardMaterial || material instanceof THREE.MeshLambertMaterial || material instanceof THREE.MeshPhongMaterial) {
                let texture = this.parser._textures[textureIdx];
                let key = material.uuid + "-" + textureIdx;
                let mat = this._processedMaterial.get(key);
                if (mat == null) {
                    // copy material and assign texture
                    material = material.clone();
                    (material as any).lightMap = texture;
                    node.material = material;
                    this._processedMaterial.put(key, mat);
                } else {
                    node.material = (mat as any);
                }

                this._setUV2(node, lightmapDef.uvScale, lightmapDef.uvOffset);
            } else {
                logger.warn("Can not apply lightmap on material " + node.material);
            }
        }

        private _getTexture(lightmapDef: GLTFLightMapNodeExtension) {
            let idx = lightmapDef.lightmap;
            if (idx >= this.lightmaps.length) {
                logger.warn("Trying to get data from unregistered lightmap " + idx);
                return null;
            }

            let lightmap = this.lightmaps[idx];
            return lightmap.texture.index;
        }

        private _setUV2(node: THREE.Mesh, uvScale: Array<number>, uvOffset: Array<number>) {
            let geometry = (node.geometry as THREE.BufferGeometry);
            let key = geometry.uuid + "#" + uvScale.join(",") + "#" + uvOffset.join(",");
            let idx = this._processedGeometries.indexOf(key);
            if (idx >= 0) {
                // reuse uv2 already computed
                return;
            } else {
                // this._processedGeometries.push(key);
            }

            // else, we nned to clone geometry to set new uv2
            geometry = geometry.clone();
            node.geometry = geometry;

            let uv2 = geometry.getAttribute("uv2") as THREE.BufferAttribute;
            if (uv2 != null) {
                // Use uv2 from unity (fix to match atlas)
                this._fixUv2(node, uvScale, uvOffset);
            } else {
                // No uv2, use uv1 but fix to match atlas
                this._copyUvToUv2(node, uvScale, uvOffset);
            }
        }

        private _fixUv2(node: THREE.Mesh, uvScale: Array<number>, uvOffset: Array<number>) {
            let geometry = (node.geometry as THREE.BufferGeometry);
            let uv = geometry.getAttribute("uv") as THREE.BufferAttribute;
            let uv2 = geometry.getAttribute("uv2") as THREE.BufferAttribute;
            for (let i = 0; i < uv.count; i++) {
                // (1 - uv2.getY(i)) => flip to be in unity coordinate
                // 1 - (unityy) => flip to be in three coordinate
                let x = uv2.getX(i) * uvScale[0] + uvOffset[0];
                let y = 1 - ((1 - uv2.getY(i)) * uvScale[1] + uvOffset[1]);
                uv2.setXY(i, x, y);
            }
        }

        private _copyUvToUv2(node: THREE.Mesh, uvScale: Array<number>, uvOffset: Array<number>) {
            let geometry = (node.geometry as THREE.BufferGeometry);
            let uv = geometry.getAttribute("uv") as THREE.BufferAttribute;
            let uv2 = new THREE.BufferAttribute(new Float32Array(uv.count * 2), 2);
            for (let i = 0; i < uv.count; i++) {
                let x = uv.getX(i) * uvScale[0] + uvOffset[0];
                let y = 1 - ((1 - uv.getY(i)) * uvScale[1] + uvOffset[1]);
                uv2.setXY(i, x, y);
            }
            geometry.addAttribute("uv2", uv2);
        }

        // private _printUV(u: THREE.BufferAttribute) {
        //     let res = "";
        //     for (let i = 0; i < u.count; i++) {
        //         let x = u.getX(i).toFixed(4);
        //         let y = u.getY(i).toFixed(4);
        //         res += "[" + x + "," + y + "], ";
        //     }
        //     return res;
        // }
    }
}