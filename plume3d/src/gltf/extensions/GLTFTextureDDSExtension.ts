module plume.gltf {

    export class GLTFTextureDDSExtension {

        name: ExtensionsType;
        ddsLoader: THREE.DDSLoader;

        constructor() {
            if (!THREE.DDSLoader) {
                throw new Error('THREE.GLTFLoader: Attempting to load .dds texture without importing THREE.DDSLoader');
            }

            this.name = "MSFT_texture_dds";
            this.ddsLoader = new THREE.DDSLoader();
        }
    }
}