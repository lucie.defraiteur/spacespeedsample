module plume.gltf {

    export class GLTFParser {

        // private _buffersByUri: plume.HashMap<ArrayBuffer>;
        // private _texturesByUri: plume.HashMap<THREE.Texture>;

        _textureLoaderManager: plume.TextureLoaderManager;
        private _fileLoader: THREE.FileLoader;

        private _onCompleteCb: (glTF: GltfObject) => void;
        private _externalResources = 0;
        private _loadedResources = 0;
        private _errorResources = 0;

        _factory: GLTFElementFactory;

        _buffers: Array<ArrayBuffer> = [];
        _images: Array<THREE.Texture> = [];
        _textures: Array<THREE.Texture> = [];
        _accessors: Array<THREE.BufferAttribute | THREE.InterleavedBufferAttribute> = [];
        _bufferViews: Array<ArrayBuffer> = [];
        _materials: Array<THREE.Material> = [];
        _meshes: Array<THREE.Mesh | THREE.SkinnedMesh | THREE.Group> = [];
        _cameras: Array<THREE.Camera> = [];
        _animations: Array<THREE.AnimationClip> = [];
        _animationsRelatives: Array<{ root: THREE.Object3D, clip: THREE.AnimationClip }> = [];
        _nodes: Array<THREE.Object3D> = [];
        _scenes: Array<THREE.Scene> = [];
        _skins: Array<SkinEntry> = [];

        constructor(public json: gltfspec.GlTf, public extensions: Extensions, public options: GltfParserOptions) {

            this._factory = (options.factory == null ? {} : options.factory);
            this._textureLoaderManager = options.textureLoaderManager;
            //this._textureLoaderManager.setCrossOrigin(this.options.crossOrigin);

            this._fileLoader = new THREE.FileLoader(this.options.manager);
            this._fileLoader.setResponseType('arraybuffer');
        }

        parse(onLoad: (glTF: GltfObject) => void, onError: (event: ErrorEvent | Error) => void) {
            let self = this;
            this._onCompleteCb = onLoad;

            //
            // Start by  loading all remote data
            //

            // Add fake assets
            this._externalResources++;

            //
            // Buffers
            let buffers = this.json.buffers;
            if (buffers != null && buffers.length > 0) {
                for (let i = 0; i < buffers.length; i++) {
                    let bufferDef = buffers[i];
                    bufferDef.index = i;

                    if (bufferDef.type && bufferDef.type !== 'arraybuffer') {
                        logger.error("GLTFLoader: " + bufferDef.type + " buffer type is not supported.");
                        continue;
                    }

                    // If present, GLB container is required to be the first buffer.
                    if (bufferDef.uri == null && bufferDef.index == 0) {
                        self._buffers[bufferDef.index] = this.extensions.KHR_binary_glTF.body;
                        continue;
                    }

                    if (bufferDef.uri == null) {
                        logger.warn("GLTFLoader: buffer without uri.");
                        continue;
                    }

                    let url = resolveURL(bufferDef.uri, this.options.path);
                    this._externalResources++;

                    this._fileLoader.load(url, function (response) {
                        self._loadedResources++;
                        // self._buffersByUri.put(bufferDef.uri, response as ArrayBuffer);
                        self._buffers[bufferDef.index] = response as ArrayBuffer;
                        self._onCheckDataLoaded();
                    }, function () {
                        // progress
                    }, function (error) {
                        logger.error("Failed to load " + bufferDef.uri);
                        self._errorResources++;
                        self._onCheckDataLoaded();
                    });
                }
            }

            // Images
            let images = this.json.images;
            if (images != null && images.length > 0) {
                for (let i = 0; i < images.length; i++) {
                    let imageDef = images[i];
                    imageDef.index = i;
                    if (imageDef.uri) {
                        let url = resolveURL(imageDef.uri, this.options.path);
                        this._externalResources++;

                        this._textureLoaderManager.load(url, function (response) {
                            self._loadedResources++;
                            // self._texturesByUri.put(imageDef.uri, response);
                            self._images[imageDef.index] = response;
                            self._onCheckDataLoaded();
                        }, function (error) {
                            logger.error("Failed to load " + imageDef.uri);
                            self._errorResources++;
                            self._onCheckDataLoaded();
                        });
                    } else {
                        // Embedded image loaded in GLTFEmbeddedImageImporter (once bufferview ready)
                    }
                }
            }

            if (this.extensions.KHR_draco_mesh_compression != null) {
                // Using draco compression, load module
                this._externalResources++;
                this.extensions.KHR_draco_mesh_compression.initialize(function () {
                    self._loadedResources++;
                    self._onCheckDataLoaded();
                });
            }

            // Remove fake assets
            this._externalResources--;
            this._onCheckDataLoaded();
        }

        private _onCheckDataLoaded() {
            let totalLoaded = this._errorResources + this._loadedResources;
            if (totalLoaded >= this._externalResources) {
                this._onExternalDataLoaded();
            }
        }

        private _onExternalDataLoaded() {
            this._markDefs();

            // To build final gltf object we need
            // scenes
            // animations
            // cameras

            new GLTFBufferViewImporter(this).import();
            new GLTFEmbeddedImageImporter(this).import();
            new GLTFTextureImporter(this).import();
            new GLTFAccessorImporter(this).import();
            new GLTFMaterialImporter(this).import();
            new GLTFMeshImporter(this).import();
            new GLTFCameraImporter(this).import();
            new GLTFSkinImporter(this).import();
            new GLTFNodeImporter(this).import();
            new GLTFSceneImporter(this).import();
            new GLTFAnimationImporter(this).import();

            let gtfl: GltfObject = {
                scene: this._scenes[0],
                scenes: this._scenes,
                cameras: this._cameras,
                animations: this._animations,
                animationsRelatives: this._animationsRelatives,
                asset: this.json.asset,
                parser: this,
                userData: {}
            };

            this._onCompleteCb(gtfl);
        }


        /**
         * Marks the special nodes/meshes in json for efficient parse.
         */
        private _markDefs() {

            let nodeDefs = this.json.nodes || [];
            let skinDefs = this.json.skins || [];
            let meshDefs = this.json.meshes || [];

            let meshReferences = {};
            let meshUses = {};

            // Nothing in the node definition indicates whether it is a Bone or an
            // Object3D. Use the skins' joint references to mark bones.
            for (let skinIndex = 0, skinLength = skinDefs.length; skinIndex < skinLength; skinIndex++) {
                let joints = skinDefs[skinIndex].joints;
                for (let i = 0, il = joints.length; i < il; i++) {
                    nodeDefs[joints[i]].isBone = true;
                }
            }

            // Meshes can (and should) be reused by multiple nodes in a glTF asset. To
            // avoid having more than one THREE.Mesh with the same name, count
            // references and rename instances below.
            //
            // Example: CesiumMilkTruck sample model reuses "Wheel" meshes.
            for (let nodeIndex = 0, nodeLength = nodeDefs.length; nodeIndex < nodeLength; nodeIndex++) {
                let nodeDef = nodeDefs[nodeIndex];
                if (nodeDef.mesh !== undefined) {
                    if (meshReferences[nodeDef.mesh] === undefined) {
                        meshReferences[nodeDef.mesh] = meshUses[nodeDef.mesh] = 0;
                    }
                    meshReferences[nodeDef.mesh]++;

                    // Nothing in the mesh definition indicates whether it is
                    // a SkinnedMesh or Mesh. Use the node's mesh reference
                    // to mark SkinnedMesh if node has skin.
                    if (nodeDef.skin !== undefined) {
                        meshDefs[nodeDef.mesh].isSkinnedMesh = true;
                    }
                }
            }

            this.json.meshReferences = meshReferences;
            this.json.meshUses = meshUses;
        }

        assignTexture(textureInfo: gltfspec.TextureInfo, materialParams: Object, textureName: string) {
            let textureIndex = textureInfo.index;
            let texture = this._textures[textureIndex];
            if (textureInfo.extensions != null) {
                if (textureInfo.extensions["EXT_texture_transform"] != null) {
                    texture = this.extensions.EXT_texture_transform.apply(texture, textureInfo.extensions["EXT_texture_transform"]);
                    //console.log("apply texture transform to " + textureName + " / " + JSON.stringify(textureInfo) + " / " + texture.image.src);
                }
            }
            materialParams[textureName] = texture;
        };


        getBufferView(index: number) {
            return this._bufferViews[index];
        }

    }

}