module plume {

    export const enum MinMaxCurveMode {
        Constant, Curve, TwoCurves, TwoConstants
    }

    export class MinMaxCurve {

        mode: MinMaxCurveMode;

        constant: number;
        constantMin: number;
        constantMax: number;

        evaluate(time: number = 0, rndRatio: number = 0) {
            switch (this.mode) {
                case MinMaxCurveMode.Constant:
                    return this.constant;
                case MinMaxCurveMode.TwoConstants:
                    return Mathf.lerp(this.constantMin, this.constantMax, rndRatio);
            }
            logger.warn("MinMaxCurve mode not implemented " + this.mode);
            return null;
        }

        static constant(v: number): MinMaxCurve {
            let m = new MinMaxCurve();
            m.mode = MinMaxCurveMode.Constant;
            m.constant = v;
            return m;
        }

        static randomBetweenTwoConstant(min: number, max: number): MinMaxCurve {
            let m = new MinMaxCurve();
            m.mode = MinMaxCurveMode.TwoConstants;
            m.constantMin = min;
            m.constantMax = max;
            return m;
        }

    }

}