module plume {

    export class Particle {

        position = new THREE.Vector3();
        velocity = new THREE.Vector3();

        remainingLifetime: number;
        animatedVelocity = new THREE.Vector3();
        totalVelocity = new THREE.Vector3();

        constructor(public particleSystem: Emitter) {
        }

    }

    // this.particleSystem = particleSystem;
    // this.position = vec3.new(0, 0, 0);
    // this.velocity = vec3.new(0, 0, 0);
    // this.animatedVelocity = vec3.new(0, 0, 0);
    // this.ultimateVelocity = vec3.new(0, 0, 0);
    // this.angularVelocity = vec3.new(0, 0, 0);
    // this.axisOfRotation = vec3.new(0, 0, 0);
    // this.rotation = vec3.new(0, 0, 0);
    // this.startSize = vec3.new(0, 0, 0);
    // this.size = vec3.zero();
    // this.startColor = color4.new(1, 1, 1, 1);
    // this.color = color4.create();
    // this.randomSeed = 0; // uint
    // this.remainingLifetime = 0.0;
    // this.startLifetime = 0.0;
    // this.emitAccumulator0 = 0.0;
    // this.emitAccumulator1 = 0.0;
    // this.frameIndex = 0.0;

    // angularVelocity	The angular velocity of the particle.
    // angularVelocity3D	The 3D angular velocity of the particle.
    // animatedVelocity	The animated velocity of the particle.
    // axisOfRotation	Mesh particles will rotate around this axis.
    // position	The position of the particle.
    // randomSeed	The random seed of the particle.
    // remainingLifetime	The remaining lifetime of the particle.
    // rotation	The rotation of the particle.
    // rotation3D	The 3D rotation of the particle.
    // startColor	The initial color of the particle. The current color of the particle is calculated procedurally based on this value and the active color modules.
    // startLifetime	The starting lifetime of the particle.
    // startSize	The initial size of the particle. The current size of the particle is calculated procedurally based on this value and the active size modules.
    // startSize3D	The initial 3D size of the particle. The current size of the particle is calculated procedurally based on this value and the active size modules.
    // totalVelocity	The total velocity of the particle.
    // velocity	The velocity of the particle.
}