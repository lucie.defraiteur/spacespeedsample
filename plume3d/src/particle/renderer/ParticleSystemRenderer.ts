
module plume {

    let _uvs = [
        0, 0, // bottom-left
        1, 0, // bottom-right
        0, 1, // top-left
        1, 1  // top-right
    ];

    export class ParticleSystemRenderer {

        material: THREE.Material;
        renderMode = ParticleRenderMode.Billboard;

        particleSystem: Emitter;

        model: ParticleContainer;

        private _vertexDataTmp: ParticleContainerVertexDataType;
        private _vertexDataFlag: ParticleContainerVertexDataFlag;

        constructor() {
            this._vertexDataFlag = {
                position: true,
                uv: true,
            }

            this._vertexDataTmp = {
                position: new THREE.Vector3(),
                uv: new THREE.Vector2(),
            }
        }

        initialize(particleSystem: Emitter) {
            this.particleSystem = particleSystem;

            let main = particleSystem.main;

            this.model = new ParticleContainer(main.maxParticles);
            this.particleSystem.add(this.model);
        }

        updateRenderData() {
            let idx = 0;
            let particles = this.particleSystem.getParticles();
            for (let i = 0; i < particles.length; i++) {
                let p = particles.data[i];

                for (let j = 0; j < 4; j++) { // four verts per particle.
                    if (this._vertexDataFlag.position) {
                        this._vertexDataTmp.position.copy(p.position);
                    }
                    if (this._vertexDataFlag.uv) {
                        this._vertexDataTmp.uv.x = _uvs[2 * j];
                        this._vertexDataTmp.uv.y = _uvs[2 * j + 1];
                    }

                    this.model.setVertexData(idx, this._vertexDataTmp);

                    idx++;
                }
            }
        }

        private _initializeMaterial() {

        }
    }


    //     Properties
    // activeVertexStreamsCount	The number of currently active custom vertex streams.
    // alignment	Control the direction that particles face.
    // cameraVelocityScale	How much are the particles stretched depending on the Camera's speed.
    // enableGPUInstancing	Enables GPU Instancing on platforms where it is supported.
    // lengthScale	How much are the particles stretched in their direction of motion.
    // maskInteraction	Specifies how the Particle System Renderer interacts with SpriteMask.
    // maxParticleSize	Clamp the maximum particle size.
    // mesh	Mesh used as particle instead of billboarded texture.
    // meshCount	The number of meshes being used for particle rendering.
    // minParticleSize	Clamp the minimum particle size.
    // normalDirection	How much are billboard particle normals oriented towards the camera.
    // pivot	Modify the pivot point used for rotating particles.
    // renderMode	How particles are drawn.
    // sortingFudge	Biases particle system sorting amongst other transparencies.
    // sortMode	Sort particles within a system.
    // trailMaterial	Set the material used by the Trail module for attaching trails to particles.
    // velocityScale	How much are the particles stretched depending on "how fast they move".
    //
    // Public Methods
    // BakeMesh	Creates a snapshot of ParticleSystemRenderer and stores it in mesh.
    // BakeTrailsMesh	Creates a snapshot of ParticleSystem Trails and stores them in mesh.
    // GetActiveVertexStreams	Query which vertex shader streams are enabled on the ParticleSystemRenderer.
    // GetMeshes	Get the array of meshes to be used as particles.
    // SetActiveVertexStreams	Enable a set of vertex shader streams on the ParticleSystemRenderer.
    // SetMeshes	Set an array of meshes to be used as particles when the ParticleSystemRenderer.renderMode is set to ParticleSystemRenderMode.Mesh.
}