/// <reference path="ParticleModule.ts" />

module plume {

    export class ParticleEmissionModule extends ParticleModule {

        enabled = true;
        rateOverTime: MinMaxCurve;

        constructor() {
            super();

            this.rateOverTime = MinMaxCurve.constant(10);
        }
    }

    //     Properties
    // burstCount	The current number of bursts.
    // enabled	Enable/disable the Emission module.
    // rateOverDistance	The rate at which new particles are spawned, over distance.
    // rateOverDistanceMultiplier	Change the rate over distance multiplier.
    // rateOverTime	The rate at which new particles are spawned, over time.
    // rateOverTimeMultiplier	Change the rate over time multiplier.
}