/// <reference path="ParticleModule.ts" />

module plume {

    export class ParticleMainModule extends ParticleModule {

        maxParticles = 1000;
        simulationSpeed = 1;
        duration = 5; // in seconds
        loop = true;
        
        startSpeed: MinMaxCurve;
        startDelay: MinMaxCurve;

        constructor() {
            super();

            this.startSpeed = MinMaxCurve.constant(1);
            this.startDelay = MinMaxCurve.constant(0);
        }
    }


    //     Properties
    // customSimulationSpace	Simulate particles relative to a custom transform component.
    // duration	The duration of the particle system in seconds.
    // emitterVelocityMode	Control how the Particle System calculates its velocity, when moving in the world.
    // flipRotation	Makes some particles spin in the opposite direction.
    // gravityModifier	Scale applied to the gravity, defined by Physics.gravity.
    // gravityModifierMultiplier	Change the gravity mulutiplier.
    // loop	Is the particle system looping?
    // maxParticles	The maximum number of particles to emit.
    // playOnAwake	If set to true, the particle system will automatically start playing on startup.
    // prewarm	When looping is enabled, this controls whether this particle system will look like it has already simulated for one loop when first becoming visible.
    // scalingMode	Control how the particle system's Transform Component is applied to the particle system.
    // simulationSpace	This selects the space in which to simulate particles. It can be either world or local space.
    // simulationSpeed	Override the default playback speed of the Particle System.
    // startColor	The initial color of particles when emitted.
    // startDelay	Start delay in seconds.
    // startDelayMultiplier	Start delay multiplier in seconds.
    // startLifetime	The total lifetime in seconds that each new particle will have.
    // startLifetimeMultiplier	Start lifetime multiplier.
    // startRotation	The initial rotation of particles when emitted.
    // startRotation3D	A flag to enable 3D particle rotation.
    // startRotationMultiplier	Start rotation multiplier.
    // startRotationX	The initial rotation of particles around the X axis when emitted.
    // startRotationXMultiplier	Start rotation multiplier around the X axis.
    // startRotationY	The initial rotation of particles around the Y axis when emitted.
    // startRotationYMultiplier	Start rotation multiplier around the Y axis.
    // startRotationZ	The initial rotation of particles around the Z axis when emitted.
    // startRotationZMultiplier	Start rotation multiplier around the Z axis.
    // startSize	The initial size of particles when emitted.
    // startSize3D	A flag to enable specifying particle size individually for each axis.
    // startSizeMultiplier	Start size multiplier.
    // startSizeX	The initial size of particles along the X axis when emitted.
    // startSizeXMultiplier	Start rotation multiplier along the X axis.
    // startSizeY	The initial size of particles along the Y axis when emitted.
    // startSizeYMultiplier	Start rotation multiplier along the Y axis.
    // startSizeZ	The initial size of particles along the Z axis when emitted.
    // startSizeZMultiplier	Start rotation multiplier along the Z axis.
    // startSpeed	The initial speed of particles when emitted.
    // startSpeedMultiplier	A multiplier of the initial speed of particles when emitted.
    // stopAction	Configure whether the GameObject will automatically disable or destroy itself, when the Particle System is stopped and all particles have died.
    // useUnscaledTime	When true, use the unscaled delta time to simulate the Particle System. Otherwise, use the scaled delta time.
}