module plume {

    export class ParticleModule {

        particleSystem: Emitter;

        initialize(particleSystem: Emitter) {
            this.particleSystem = particleSystem;
        }
    }

}