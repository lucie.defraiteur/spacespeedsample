/// <reference path="../GUIScene.ts" />
module plume {

    export interface AssetDef {
        type: "img" | "spritesheet" | "font" | "soundsheet" | "file",
        name: string,
        file: string,
        atlas?: string
    }

    export const enum PreloadingProgressAlgo {
        Assets = 1,
        AssetsSmooth = 2,
        FakeDuration = 3
    }

    export class H5PreLoadingScene extends GuiScene {

        public static retryMax = 5;

        protected loader: Loader2d;
        protected assetList: Array<AssetDef>;
        protected callback: Function;

        private _loadingStatus: LoaderResources;

        progressListener: Handler<number> = new Handler();

        // Used to compute progress
        private _timeCounted = 0;
        protected startLoadTime: number = 0;

        // options
        optDelayBeforeCompleteFire = 0;
        optProgressAlgo: number = PreloadingProgressAlgo.Assets;
        optEstimateLoadingTime = 2000;
        optOnProgress: (progress: number) => void;
        estimatedProgress = -1;
        lastEstimatedProgress = -1;

        constructor(assetList: Array<AssetDef>, callback: Function) {
            super();

            this.assetList = assetList;
            this.callback = callback;
        }

        protected onLoadComplete() {
            let self = this;
            let now = performance.now();
            let ellapsed = now - this.startLoadTime;
            logger.debug("Loading time: " + ellapsed);
            //this.game.renderer.backgroundColor = this.gameBackgroundColor;
            //Analytics.sendTiming(AnalyticsTiming.ASSETS, ellapsed);

            for (let i = 0; i < this.assetList.length; i++) {
                let def = this.assetList[i];
                if (def.type != "spritesheet") continue;

                let split = def.atlas.split("/");
                let filename = split[split.length - 1];
                let jsIndex = filename.indexOf('.json');
                if (jsIndex != -1) {
                    let fakeFrame = "fake-" + def.name + ".json";
                    // if (Texture.hasInCache(fakeFrame)) {
                    //     let sprite = Sprite.fromFrame(fakeFrame);
                    //     self.addChild(sprite);
                    // }
                }
            }

            if (this.optDelayBeforeCompleteFire > 0) {
                TimeManager.get().createAndStartTimer(this.optDelayBeforeCompleteFire, () => {
                    self.progressListener.fire(100);
                    self.callback();
                });
            } else {
                self.progressListener.fire(100);
                self.callback();
            }
        }

        addProgressListener(fn: (progress: number) => void, context?: any) {
            this.progressListener.add(fn, context);
        }

        onShow() {
            let self = this;

            this.startLoadTime = performance.now();
            this.loader = new Loader2d(this.assetList, H5PreLoadingScene.retryMax);
            this.loader.load(function (success) {
                if (success) {
                    self.onLoadComplete();
                } else {
                    logger.error("Failed to load all assets :/");
                }
            });

            this._loadingStatus = this.loader.getStatus();
        }

        update(delta: number) {
            super.update(delta);

            if (this.startLoadTime <= 0) return;

            this.lastEstimatedProgress = this.estimatedProgress;
            this._updateProgress(delta);

            if (this.optOnProgress != null) {
                this.optOnProgress(this.estimatedProgress);
            }

            if (this._loadingStatus != null) {
                let newstatus = this.loader.getStatus();
                if (newstatus.loaded != this._loadingStatus.loaded) {
                    this._loadingStatus = newstatus;
                }
            }

            if (this.lastEstimatedProgress != this.estimatedProgress) {
                this.progressListener.fire(this.estimatedProgress * 100);
            }
        }

        render(interpolation: number) {
        }

        private _updateProgress(delta: number) {

            if (this.optProgressAlgo == PreloadingProgressAlgo.AssetsSmooth) {
                this._updateProgress_AssetsSmooth(delta);
            } else if (this.optProgressAlgo == PreloadingProgressAlgo.FakeDuration) {
                this._updateProgress_FakeDuration(delta);
            } else {
                this._updateProgress_Assets(delta);
            }
        }

        private _updateProgress_AssetsSmooth(delta: number) {
            let totalAssets = (this._loadingStatus.loading + this._loadingStatus.loaded + this._loadingStatus.failed);
            let progress = (this._loadingStatus.loaded + 1) / totalAssets;
            progress = Math.min(progress, 1);
            let ellapsed = this._timeCounted / this.optEstimateLoadingTime;
            if (ellapsed < progress) {
                this._timeCounted += delta;
            }
            this.estimatedProgress = Math.min(ellapsed, 1);
        }

        private _updateProgress_FakeDuration(delta: number) {
            let ellapsed = performance.now() - this.startLoadTime;
            let progress = (ellapsed) / this.optEstimateLoadingTime;
            this.estimatedProgress = Math.min(progress, 1);
        }

        private _updateProgress_Assets(delta: number) {
            let totalAssets = (this._loadingStatus.loading + this._loadingStatus.loaded + this._loadingStatus.failed);
            let progress = (this._loadingStatus.loaded) / totalAssets;
            this.estimatedProgress = Math.min(progress, 1);
        }


    }

}