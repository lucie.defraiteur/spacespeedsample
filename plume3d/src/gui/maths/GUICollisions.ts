module plume {

    export class Collisions {

        constructor() {
        }

        static overlapRectangles(r1: Rectangle, r2: Rectangle): boolean {
            return r1.left < r2.right && r1.right > r2.left && r1.top < r2.bottom && r1.bottom > r2.top;
        }

        static pointInPolygon2(points: Array<number>, x: number, y: number) {
            let polyCornersCount = points.length;
            let i = 0;
            let j = points.length - 2;
            let oddNodes = false;
            for (i = 0; i < polyCornersCount; i = i + 2) {
                if (points[i + 1] < y && points[j + 1] >= y
                    || points[j + 1] < y && points[i + 1] >= y) {
                    if (points[i] + (y - points[i + 1]) / (points[j + 1] - points[i + 1]) * (points[j] - points[i]) < x) {
                        oddNodes = !oddNodes;
                    }
                }
                j = i;
            }
            return oddNodes;
        }

    }
}