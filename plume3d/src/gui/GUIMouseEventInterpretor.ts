module plume {

    export class GuiMouseEventInterpretor implements MouseEventInterpretor {

        private _inputManager: InputManager;
        private _mouseHandler: MouseHandler;
        private _desktop: boolean;
        private _clickHandlerCount = 0;

        rootContainer: Container;

        constructor() {
            this._inputManager = Game.get().inputManager;
            this._mouseHandler = Game.get().inputManager.mouse;
            this._desktop = Game.get().deviceManager.os.desktop;
            this.rootContainer = CanvasGui.get().layers.root;
        }

        onUpdate(): void {
            this._triggerMouseHit();
        }

        onImmediate(): void {
            if (this._clickHandlerCount <= 0) {
                return;
            }
            
            for (let i = 0; i < this._mouseHandler.pointers.length; i++) {
                let pointer = this._mouseHandler.pointers[i];
                if (pointer.isActive()) {
                    this._triggerImmediateClick0(pointer);
                }
            }
        }




        //
        // In update loop
        //
        private _triggerMouseHit() {

            let checkInteractiveElement = this._inputManager._pointerDirty;
            this._inputManager._pointerDirty = false;

            if (DisplayObject.interactiveDirty) {
                //console.log("Invalidate interactive (root)");
                this._clickHandlerCount = 0;
                this._invalidateInteractive(this.rootContainer);
                DisplayObject.interactiveDirty = false;
                checkInteractiveElement = true;
                //this.rootContainer._outputToConsole(0, ["hasInteractiveChild", "interactive", "x", "y"]);
                //this.rootContainer._outputToConsole(0, []);
            }

            if (!checkInteractiveElement) {
                //console.log("checkInteractiveElement");
                return;
            }

            for (let i = 0; i < this._mouseHandler.pointers.length; i++) {
                let pointer = this._mouseHandler.pointers[i];
                this._triggerMouseHit0(pointer);
            }
        }

        // Tag scene graph element without interactive children
        // Count immadiate click hanlders
        private _invalidateInteractive(container: Container) {
            // mark container without children interactive
            container.hasInteractiveChild = false;
            // go through all children
            let children = container.getChildrenCount();
            for (let c = 0; c < children; c++) {
                let child = container.getChildAt(c);
                if (child instanceof Container) {
                    // go down
                    this._invalidateInteractive(child);
                    if (child.hasInteractiveChild || child.interactive) {
                        container.hasInteractiveChild = true;
                    }
                } else {
                    if (child.interactive) {
                        // mark with children interactive
                        container.hasInteractiveChild = true;
                    }
                }

                if (child.hasClickHandler()) {
                    this._clickHandlerCount++;
                }
            }
        }

        private _triggerMouseHit0(pointer: PointerInput) {
            if (!pointer.mouseChanged()) return;

            let collector = new Array<DisplayObject>();
            GuiMouseEventInterpretor._collectInteractiveHit(pointer.position, this.rootContainer, collector);

            if (collector.length > 0) {
                let target = collector[collector.length - 1];
                pointer.interactiveElement = target;

                if (pointer.isJustDown) {
                    pointer._lastDownTarget = target;
                    if (target.hasDownHandler()) target.downHandler.fire(true);
                } else if (pointer.isJustUp) {
                    if (target.hasUpHandler()) target.upHandler.fire(true);
                }

                if (target != pointer._previousHit) {
                    // over new target
                    if (target.hasOverHandler()) target.overHandler.fire(true);
                    // leave previous
                    if (pointer._previousHit != null) {
                        if (pointer._previousHit.hasOutHandler()) pointer._previousHit.outHandler.fire();
                    }
                }

                pointer._previousHit = target;

            } else {
                pointer.interactiveElement = null;
                if (pointer._previousHit != null) {
                    if (pointer._previousHit.hasOutHandler()) pointer._previousHit.outHandler.fire();
                }
                pointer._previousHit = null;
            }

            if (this._desktop && !this._inputManager.noCursor) {
                let canvas = Game.get().engine.canvas;
                if (pointer._previousHit == null || !pointer._previousHit.showCursor) {
                    canvas.style.cursor = "default";
                } else {
                    canvas.style.cursor = "pointer";
                }
            }
        }


        private _triggerImmediateClick0(pointer: PointerInput) {
            let collector = new Array<DisplayObject>();
            GuiMouseEventInterpretor._collectInteractiveHit(pointer.position, this.rootContainer, collector);
            if (collector.length > 0) {
                let target = collector[collector.length - 1];
                if (target.hasClickHandler()) {
                    if (!pointer.wasSwipe()) {
                        target.clickHandler.fire(true);
                    }
                }
            }
        }


        static _collectInteractiveHit(position: Point, container: Container, collector: Array<DisplayObject>) {
            //if(1==1) return;

            // container not visible, skip
            if (!container.visible) return;

            //console.log("position : " + position);

            // add container if hit and interactive
            if (container.interactive) {
                let containerContainsPoint = container.hitPoint(position);
                if (containerContainsPoint) collector.push(container);
            }

            if (!container.hasInteractiveChild) return;

            // loop through children
            let children = container.getChildrenCount();
            for (let c = 0; c < children; c++) {
                let child = container.getChildAt(c);
                if (child instanceof Container) {
                    this._collectInteractiveHit(position, child, collector);
                } else {
                    if (child.visible && child.interactive) {
                        if (child.hitPoint(position)) {
                            collector.push(child);
                        }
                    }
                }
            }
        }
    }




}