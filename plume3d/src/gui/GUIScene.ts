﻿module plume {
    export class GuiScene {

        game: Game;
        gui: CanvasGui;
        container: Container;

        private _hideHandler: Handler<{}>;

        constructor() {
            this.game = Game.get();
            this.gui = CanvasGui.get();
            this.container = new Container(this.game.bounds.width, this.game.bounds.height);
            this.container.debugName = "Scene";
        }

        addChild(child: DisplayObject) {
            this.container.addChild(child);
        }

        removeChild(child: DisplayObject) {
            this.container.removeChild(child);
        }

        update(dt?: number) {
        }

        render(dt?: number, interpolation?: number) {
        }

        //SHOULD NEVER BE OVERRIDED  
        show(animateIn: boolean = true) {
            let self = this;
            if (animateIn == true) {
                this.animateIn(() => {
                    this.game.inputManager.enable();
                    DisplayObject.interactiveDirty = true;
                    self.onShow();
                });
            } else {
                this.game.inputManager.enable();
                DisplayObject.interactiveDirty = true;
                this.onShow();
            }
        }

        //TO be overrided for scene transition IN
        protected animateIn(callback: Function) {
            callback();
        }

        //called when scene is ready (after animation)
        protected onShow() {
            //TO BE OVERRIDED 
        }

        //SHOULD NEVER BE OVERRIDED   
        hide(callback: Function, animateOut: boolean = true) {
            Game.get().inputManager.disable();

            if (this._hideHandler != null) {
                this._hideHandler.fire({});
                this._hideHandler.clear();
            }

            let self = this;
            if (animateOut == true) {
                this.animateOut(() => {
                    self.onHide();
                    self.dispose();
                    callback();
                });
            } else {
                this.onHide();
                this.dispose();
                callback();
            }
        }

        //TO be overrided for scene transition OUT
        protected animateOut(callback: Function) {
            callback();
        }

        //called when scene is hided (after animation)
        protected onHide() {
            //TO BE OVERRIDED
        }

        protected dispose() {
            //TO BE OVERRIDED
        }

        get hideHandler(): Handler<{}> {
            if (this._hideHandler == null) {
                this._hideHandler = new Handler();
            }
            return this._hideHandler;
        }
    }

}
