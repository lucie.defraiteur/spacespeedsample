module plume {

    export interface HasWidth {
        width: number;
    }
    export interface HasHeight {
        height: number;
    }
    export interface HasSize extends HasWidth, HasHeight {
    }

    export interface HasX {
        x: number;
    }
    export interface HasY {
        y: number;
    }
    export interface HasCoordinate extends HasX, HasY {
    }
    export interface HasBounds extends HasCoordinate, HasSize {
    }

}