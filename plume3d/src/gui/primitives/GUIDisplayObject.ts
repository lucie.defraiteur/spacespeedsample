﻿/// <reference path="../../../build/latest/plume3d.core.d.ts" />

module plume {

    export class DisplayObject {

        _parent: Container | null; // protected child only be touch by H5Container
        _root = false;

        showCursor: boolean = true;
        debugName: string;

        protected _debug: boolean;
        protected _visible: boolean = true;


        // protected member to manager interactive element
        protected _hitArea: Rectangle;
        protected _interactive: boolean = false;
        protected _hasInteractiveChild: boolean = false;
        static _interactiveDirty: boolean = false;
        protected _downHandler: Handler<boolean>;
        protected _upHandler: Handler<boolean>;
        protected _overHandler: Handler<boolean>;
        protected _outHandler: Handler<boolean>;
        protected _clickHandler: Handler<any>;

        // called when removed from parent
        protected _detachListener: Handler<boolean>;

        // id
        protected _id: number;
        protected _debugId: string;
        protected static _idCount: number = 0;

        protected _x = 0;
        protected _y = 0;
        protected _width = 0;
        protected _height = 0;
        protected _rotation = 0;
        protected _alpha = 1;
        protected _scaleX = 1;
        protected _scaleY = 1;
        protected _pivotX = 0;
        protected _pivotY = 0;
        protected _flipX = false;
        protected _flipY = false;
        protected _tint = 0xFFFFFF;

        protected _mask: Mask;

        protected _worldHitArea: Array<number> | null;

        protected _transformDirty = false;
        protected _transform: Matrix;
        protected _hitTransform: Matrix;
        protected _worldTransform: Matrix;
        protected _worldAlpha: number = 1;

        protected _cachedAsBitmap = false;
        protected _cachedTexture: Texture;

        constructor() {
            this._x = 0;
            this._y = 0;
            this._scaleX = 1;
            this._scaleY = 1;
            this._id = (DisplayObject._idCount++);
            this._debugId = "DisplayObject-" + this._id;
            this._transform = new Matrix();
            this._worldTransform = new Matrix();
            this._hitTransform = new Matrix();
        }

        addDebugBorder(color: string = "#00FFBB") {
            this.debug = true;
        }

        set tint(tint: number) {
            if (tint == null) this._tint = 0xFFFFFF;
            else this._tint = tint;
        }

        get tint(): number {
            return this._tint;
        }

        draw(context: CanvasRenderingContext2D) {

            if (!this.visible) {
                return;
            }

            this.computeWorldTransform();

            // Update world alpha
            let palpha = this.getParentAlpha();
            this._worldAlpha = this._alpha * palpha;
            context.globalAlpha = this._worldAlpha;

            context.save();


            this._worldTransform.applyToContext(context);
            // this._transform.transformContext(context);

            if (this._mask != null) {
                this._mask._applyMask(context);
            }

            if (this.cachedAsBitmap) {
                if (this._cachedTexture == null) {
                    this.createCachedTexture();
                }
                this.drawCachedTexture(context);
            } else {
                this.draw0(context);
            }


            context.restore();
        }

        protected computeWorldTransform() {

            // let dirty = false;

            // Reset and update local transform if dirty
            if (this._transformDirty) {
                // dirty = true;
                this._transformDirty = false;

                this._transform.reset();

                // Compute transform (transformation will be done in the reverse order (read bottom to top))
                // We do:
                // Flip x/y
                // Apply pivot
                // Rotate
                // Scale
                // Undo pivot
                // Translate

                if (this._x != 0 || this._y != 0) this._transform.translate(this._x, this._y);

                if (this._rotation != 0 || this._scaleX != 1 || this._scaleY != 1 || this._flipX || this._flipY) {
                    if (this._pivotX != 0 || this._pivotY != 0) this._transform.translate(this._pivotX, this._pivotY);
                    if (this._rotation != 0) this._transform.rotate(this._rotation);
                    if (this._scaleX != 1 || this._scaleY != 1) this._transform.scale(this._scaleX, this._scaleY);
                    if (this._pivotX != 0 || this._pivotY != 0) this._transform.translate(-this._pivotX, -this._pivotY);
                    if (this._flipX || this._flipY) {
                        this._transform.translate(this._width * (this._flipX ? 1 : 0), this._height * (this._flipY ? 1 : 0));
                        this._transform.scale(this._flipX ? -1 : 1, this._flipY ? -1 : 1);
                    }
                }
            }

            // Update world transform
            let pwt = this.getParentWorldTransform();
            let newWt = new Matrix();
            newWt.setTransform(pwt.a, pwt.b, pwt.c, pwt.d, pwt.e, pwt.f);
            newWt.transform(this._transform.a, this._transform.b, this._transform.c, this._transform.d, this._transform.e, this._transform.f);

            let pht = this.getParentHitTransform();
            let newHt = new Matrix();
            if (pht)
                newHt.setTransform(pht.a, pht.b, pht.c, pht.d, pht.e, pht.f);
            if (!this._root) {
                newHt.transform(this._transform.a, this._transform.b, this._transform.c, this._transform.d, this._transform.e, this._transform.f);
            }

            // Compute interaction limit
            if (this._interactive) {
                let worldChanged = !newHt.isEqual(this._hitTransform);
                if (worldChanged || this._worldHitArea == null) {
                    this.updateWorldHitArea(newHt);
                }
            }
            this._worldTransform = newWt;
            this._hitTransform = newHt;


            // if (dirty && this._debugId == "root") {
            //     console.log("ROOT dirty => ");
            //     console.log("Transform => " + this._transform);
            //     console.log("World Transform => " + this._worldTransform);
            //     console.log("Hit Transform => " + this._hitTransform);
            // }
            // if (dirty && this._debugId == "blue-square") {
            //     console.log("blue dirty => ");
            //     console.log("Transform => " + this._transform);
            //     console.log("World Transform => " + this._worldTransform);
            //     console.log("Hit Transform => " + this._hitTransform);
            //     console.log("Hit area: " + this._worldHitArea);
            // }
        }

        protected getParentWorldTransform(): Matrix {
            if (this._parent == null) return new Matrix();
            return this._parent._worldTransform;
        }
        protected getParentAlpha(): number {
            if (this._parent == null) return 1;
            return this._parent._worldAlpha;
        }

        protected getParentHitTransform(): Matrix {
            if (this._root) {
                return this._hitTransform;
            }
            if (this._parent)
                return this._parent._hitTransform;
            else
                return null;
        }

        // compute absolute coordiantes
        protected updateWorldHitArea(worldTransform: Matrix) {
            let transformedPoints: Array<number>;

            if (this._hitArea != null) {
                transformedPoints = worldTransform.applyToArray([this._hitArea.x, this._hitArea.y, this._hitArea.x + this._hitArea.width, this._hitArea.y, this._hitArea.x + this._hitArea.width, this._hitArea.y + this._hitArea.height, this._hitArea.x, this._hitArea.y + this._hitArea.height]);
            } else {
                if (this._width <= 0 || this.height <= 0) {
                    logger.warn("Cannot compute hit area on no-size object. Check bounds of #" + this.getDebugId() + ". Size:" + this._width + "x" + this._height);
                    return;
                }
                transformedPoints = worldTransform.applyToArray([0, 0, this._width, 0, this._width, this._height, 0, this._height]);
            }

            // logger.debug("New hit area: " + transformedPoints);

            transformedPoints.push(transformedPoints[0]);
            transformedPoints.push(transformedPoints[1]);

            this._worldHitArea = transformedPoints;
        }

        protected draw0(context: CanvasRenderingContext2D) {
        }

        protected createCachedTexture() {
        }
        protected drawCachedTexture(context: CanvasRenderingContext2D) {
            context.drawImage(this._cachedTexture.getImpl(), this._cachedTexture.x, this._cachedTexture.y, this._cachedTexture.width, this._cachedTexture.height, 0, 0, this._cachedTexture.width, this._cachedTexture.height);
        }

        onDetach() {
            if (this._detachListener != null) {
                this._detachListener.fire(true);
            }
        }

        removeSelf(): boolean {
            if (this._parent == null) return false;

            let removed = this._parent.removeChild(this);
            return removed != null;
        }

        getDebugId() {
            return this._debugId;
        }

        setDebugId(id: string, appendDynId: boolean = false) {
            this._debugId = id;
            if (appendDynId) {
                this._debugId += this._id;
            }
        }

        hitPoint(point: Point): boolean {
            if (this._worldHitArea != null && this._worldHitArea.length > 0) {
                return Collisions.pointInPolygon2(this._worldHitArea, point.x, point.y);
            }
            else {
                if (this._transformDirty) {
                    // Not draw yet
                    return false;
                } else {
                    logger.error("Using containsPoint without worldHitArea. Check object bounds of #" + this.getDebugId() + ". Size:" + this._width + "x" + this._height + ", Hit area: " + this._hitArea);
                    return this.containsPoint(point);
                }
            }
        }

        containsPoint(point: Point): boolean {
            return false;
            //return point.x > x && point.x < x + this.width && point.y > y && point.y < y + this.height
        }

        get interactive(): boolean {
            return this._interactive;
        }

        set interactive(interactive: boolean) {
            DisplayObject.interactiveDirty = true;
            this._worldHitArea = null;
            this._interactive = interactive;
            this._transformDirty = true;
        }

        get hitArea(): Rectangle {
            return this._hitArea;
        }

        set hitArea(rectangle: Rectangle) {
            this._hitArea = rectangle;
            this._worldHitArea = null; // clear world hit area to update it
        }

        get hasInteractiveChild(): boolean {
            return this._hasInteractiveChild;
        }

        set hasInteractiveChild(b: boolean) {
            this._hasInteractiveChild = b;
        }

        get mask(): Mask {
            return this._mask;
        }

        set mask(mask: Mask) {
            this._mask = mask;
        }

        static get interactiveDirty(): boolean {
            return this._interactiveDirty;
        }

        static set interactiveDirty(b: boolean) {
            this._interactiveDirty = b;
        }

        get visible(): boolean {
            return this._visible;
        }

        set visible(visible: boolean) {
            this._visible = visible;
        }

        // Interaction

        onClick(fn: () => void, context?: any, once?: boolean) {
            this.interactive = true;
            if (once != null && once == true) this.clickHandler.once(fn, context);
            else this.clickHandler.add(fn, context);
        }

        hasDownHandler(): boolean {
            return this._downHandler != null && this._downHandler.length > 0;
        }

        hasUpHandler(): boolean {
            return this._upHandler != null && this._upHandler.length > 0;
        }

        hasOverHandler(): boolean {
            return this._overHandler != null && this._overHandler.length > 0;
        }

        hasOutHandler(): boolean {
            return this._outHandler != null && this._outHandler.length > 0;
        }

        hasClickHandler(): boolean {
            return this._clickHandler != null && this._clickHandler.length > 0;
        }

        get downHandler(): Handler<boolean> {
            if (this._downHandler == null) this._downHandler = new Handler<boolean>();
            return this._downHandler;
        }

        get upHandler(): Handler<boolean> {
            if (this._upHandler == null) this._upHandler = new Handler<boolean>();
            return this._upHandler;
        }

        get overHandler(): Handler<boolean> {
            if (this._overHandler == null) this._overHandler = new Handler<boolean>();
            return this._overHandler;
        }

        get outHandler(): Handler<boolean> {
            if (this._outHandler == null) this._outHandler = new Handler<boolean>();
            return this._outHandler;
        }

        get clickHandler(): Handler<any> {
            if (this._clickHandler == null) this._clickHandler = new Handler<any>();
            return this._clickHandler;
        }

        get detachListener(): Handler<boolean> {
            if (this._detachListener == null) this._detachListener = new Handler<boolean>();
            return this._detachListener;
        }



        // Accessors

        setPivot(x: number, y: number) {
            this._pivotX = x;
            this._pivotY = y;
            this._transformDirty = true;
        }

        setPivotToCenter() {
            if (this._width <= 0 || this._height <= 0) throw new Error("Cannot set pivot to center on display object without size");

            this._pivotX = this._width / 2;
            this._pivotY = this._height / 2;
            this._transformDirty = true;
        }

        set x(x: number) {
            this._x = x;
            this._transformDirty = true;
        }

        get x(): number {
            return this._x;
        }

        set y(y: number) {
            this._y = y;
            this._transformDirty = true;
        }

        get y(): number {
            return this._y;
        }

        set alpha(alpha: number) {
            this._alpha = alpha

            if (this._alpha > 1) this._alpha = 1;
            else if (this._alpha < 0) this.alpha = 0;
        }

        get alpha(): number {
            return this._alpha;
        }

        set scaleX(x: number) {
            this._scaleX = x;
            this._transformDirty = true;
        }

        get scaleX(): number {
            return this._scaleX;
        }

        set scaleY(y: number) {
            this._scaleY = y;
            this._transformDirty = true;
        }

        get scaleY(): number {
            return this._scaleY;
        }

        set scaleXY(scale: number) {
            this._scaleX = scale;
            this._scaleY = scale;
            this._transformDirty = true;
        }

        get scaleXY(): number {
            return this._scaleX;
        }

        set debug(debug: boolean) {
            this._debug = debug;
        }

        get debug(): boolean {
            return this._debug;
        }

        set pivotX(x: number) {
            this._pivotX = x;
            this._transformDirty = true;
        }

        get pivotX(): number {
            return this._pivotX;
        }

        set pivotY(y: number) {
            this._pivotY = y;
            this._transformDirty = true;
        }

        get pivotY(): number {
            return this._pivotY
        }

        set flipX(flipX: boolean) {
            this._flipX = flipX;
            this._transformDirty = true;
        }

        get flipX(): boolean {
            return this._flipX;
        }

        set flipY(flipY: boolean) {
            this._flipY = flipY;
            this._transformDirty = true;
        }

        get flipY(): boolean {
            return this._flipY;
        }

        set angle(deg: number) {
            this.rotation = deg * Mathf.DEG2RAD;
        }

        get angle(): number {
            return this._rotation * Mathf.RAD2DEG;
        }

        set width(width: number) {
            if (this._width == 0) return;
            this.scaleX = width / this._width;
        }

        get width(): number {
            return this._width * this._scaleX;
        }

        set height(height: number) {
            if (this._height == 0) return;
            this.scaleY = height / this._height;
        }

        get height(): number {
            return this._height * this._scaleY;
        }

        get parent(): Container | null {
            return this._parent;
        }

        set parent(container: Container | null) {
            throw new Error("parent is readonly");
        }

        set rotation(rotation: number) {
            this._rotation = rotation;
            this._transformDirty = true;
        }

        get rotation(): number {
            return this._rotation;
        }

        get cachedAsBitmap(): boolean {
            return this._cachedAsBitmap;
        }

        set cachedAsBitmap(cachedAsBitmap: boolean) {
            this._cachedAsBitmap = cachedAsBitmap;
        }

        get top(): number {
            return this.y;
        }

        get bottom(): number {
            return this.y + this.height;
        }

        get left(): number {
            return this.x;
        }

        get right(): number {
            return this.x + this.width;
        }



    }
}
