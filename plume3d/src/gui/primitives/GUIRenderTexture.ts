module plume {

    export class RenderTexture {

        render(display: DisplayObject): string {
            let canvas: HTMLCanvasElement = document.createElement("canvas");
            let context: CanvasRenderingContext2D = canvas.getContext("2d");
            canvas.width = display.x + display.width;
            canvas.height = display.y + display.height;

            let parent = display._parent;
            display._parent = null;

            display.draw(context);

            display._parent = parent;

            let base4 = canvas.toDataURL("image/png");
            return base4;
        }

    }


}