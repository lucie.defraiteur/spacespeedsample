﻿/// <reference path="GUIContainer.ts" />

module plume {

    export class Graphics extends DisplayObject {

        private _ops: Array<(context: CanvasRenderingContext2D) => any> = new Array();

        constructor(width: number = 0, height: number = 0) {
            super();
            this._debugId = "Graphics-" + this._id;

            this._width = width;
            this._height = height;
        }

        customDraw(cb: (context: CanvasRenderingContext2D) => any): Graphics {
            this._ops.push(cb);
            return this;
        }

        beginFill(color: number, alpha: number = 1): Graphics {
            this._ops.push((context) => {
                context.fillStyle = Colors.toString(color, alpha);
                context.beginPath();
            });
            return this;
        }

        beginStroke(lineWidth: number, color: number, alpha = 1): Graphics {
            let self = this;
            this._ops.push((context) => {
                context.lineWidth = lineWidth;
                context.strokeStyle = Colors.toString(color, alpha);
                context.beginPath();
            });

            return this;
        }

        endStroke(): Graphics {
            this._ops.push((context) => {
                context.stroke();
            });
            return this;
        }

        endFill(): Graphics {
            this._ops.push((context) => {
                context.fill();
            });
            return this;
        }

        moveTo(x: number, y: number): Graphics {
            this._ops.push((context) => {
                context.moveTo(x, y);
            });
            return this;
        }

        lineTo(x: number, y: number): Graphics {
            this._ops.push((context) => {
                context.lineTo(x, y);
            });
            return this;
        }

        quadraticCurveTo(cpX: number, cpY: number, toX: number, toY: number): Graphics {
            this._ops.push((context) => {
                context.quadraticCurveTo(cpX, cpY, toX, toY);
            });
            return this;
        }

        bezierCurveTo(cpX: number, cpY: number, cpX2: number, cpY2: number, toX: number, toY: number): Graphics {
            this._ops.push((context) => {
                context.bezierCurveTo(cpX, cpY, cpX2, cpY2, toX, toY);
            });

            return this;
        }

        arcTo(x1: number, y1: number, x2: number, y2: number, radius: number): Graphics {
            this._ops.push((context) => {
                context.arcTo(x1, y1, x2, y2, radius);
            });
            return this;
        }

        arc(cx: number, cy: number, radius: number, startAngle: number = 0, endAngle: number = 2 * Math.PI, anticlockwise?: boolean): Graphics {
            this._ops.push((context) => {
                context.arc(cx, cy, radius, startAngle, endAngle, anticlockwise);
            });
            return this;
        }

        drawRect(x: number, y: number, width: number, height: number): Graphics {
            let self = this;
            this._ops.push((context) => {
                context.rect(x, y, width, height);
            });
            return this;
        }

        drawRoundedRect(x: number, y: number, width: number, height: number, radius: number): Graphics {
            this._ops.push((context) => {
                context.moveTo(x + radius, y);
                context.lineTo(x + width - radius, y);
                context.quadraticCurveTo(x + width, y, x + width, y + radius);
                context.lineTo(x + width, y + height - radius);
                context.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                context.lineTo(x + radius, y + height);
                context.quadraticCurveTo(x, y + height, x, y + height - radius);
                context.lineTo(x, y + radius);
                context.quadraticCurveTo(x, y, x + radius, y);
            });
            return this;
        }

        drawCircle(x: number, y: number, radius: number): Graphics {
            let self = this;
            this._ops.push((context) => {
                context.arc(x, y, radius, 0, 2 * Math.PI);
            });
            return this;
        }

        clear(): Graphics {
            this._ops.length = 0;
            return this;
        }

        protected createCachedTexture() {
            let canvas = document.createElement("canvas");
            canvas.width = this._width;
            canvas.height = this._height;
            if (this._width == 0 || this._height == 0) {
                logger.warn("Caching graphics without size.")
            }

            let context = canvas.getContext("2d");
            this.draw0(context);

            this._cachedTexture = new Texture("graphics", 0, 0, this._width, this._height);
            this._cachedTexture.setImpl(canvas);
        }

        protected draw0(context: CanvasRenderingContext2D) {
            this._ops.forEach(f => {
                f(context);
            });
        }

        setSize(w: number, h: number) {
            this._width = w;
            this._height = h;
        }

    }
}
