module plume {

    export class Text extends DisplayObject {

        private _canvas: HTMLCanvasElement;

        private _font: string;
        private _align: string = "left";
        private _colorString: string;

        private _dirty: boolean = true;

        _heightRatio = 1.4;

        constructor(private _text: string, private _fontName: string, private _fontSize: number, private _color: number = 0xFFFFFF) {
            super();

            this._font = _fontSize + "px " + _fontName;
            this._debugId = "Text-" + this._id;
            this._colorString = Colors.toHexString(_color);
        }

        // Accessor

        set text(text: string) {
            if (text == this._text) return;

            this._text = text;
            this._dirty = true;
        }
        get text(): string {
            return this._text;
        }

        get width(): number {
            if (this._dirty) this._create();
            return this._width * this._scaleX;
        }
        get height(): number {
            if (this._dirty) this._create();
            return this._height * this._scaleY;
        }

        set align(align: string) {
            this._align = align;
            this._dirty = true;
        }
        get align(): string {
            return this._align;
        }

        set color(color: number) {
            if (color == null) this.color = 0xFFFFFF;
            else this.color = color;

            this._colorString = Colors.toHexString(color);
            this._dirty = true;
        }

        draw0(context: CanvasRenderingContext2D) {
            if (this._dirty) {
                this._create();
                this._dirty = false;
            }

            if (this._canvas == null) return;
            if (this._height == 0) return;
            if (this._width == 0) return;

            super.draw0(context);

            context.drawImage(this._canvas, 0, 0);
        }

        private _setFontStyle(context: CanvasRenderingContext2D) {
            context.textBaseline = "alphabetic";
            context.font = this._font;
            context.fillStyle = this._colorString;
        }

        // Take in account: maxWidth, line break, alignment
        private _create() {

            if (this.text == null || this.text == "") {
                this._width = 0;
                this._height = 0;
                this._canvas = null;
                return;
            }

            if (this._canvas != null) {
                this._canvas = null;
            }
            this._canvas = document.createElement("canvas");

            if (this._text.indexOf("\n") > 0) {
                this._createMultiline();
                return;
            }

            let context = this._canvas.getContext("2d");
            this._setFontStyle(context);

            let measure = context.measureText(this._text);
            let width = measure.width;
            let lineHeight = estimateTextHeight(this._fontName, this._fontSize);

            this._canvas.width = width;
            this._canvas.height = lineHeight * this._heightRatio;
            this._width = this._canvas.width;
            this._height = this._canvas.height;

            this._setFontStyle(context);
            context.fillText(this._text, 0, lineHeight);
        }

        private _createMultiline() {

            let context = this._canvas.getContext("2d");
            let split = this._text.split("\n");
            let lineHeight = estimateTextHeight(this._fontName, this._fontSize);
            let lineHeightBig = lineHeight * this._heightRatio;

            let maxw = 0;
            let lines: Array<{ x: number, y: number, width: number }> = [];
            this._setFontStyle(context);

            for (let i = 0; i < split.length; i++) {
                let line = split[i];

                let w = context.measureText(line).width;
                lines.push({
                    x: 0,
                    y: (i * lineHeightBig),
                    width: w
                });
                maxw = Math.max(maxw, w);
            }

            this._canvas.width = maxw;
            this._canvas.height = lineHeightBig * lines.length;
            this._width = this._canvas.width;
            this._height = this._canvas.height;


            for (let i = 0; i < lines.length; i++) {
                let line = lines[i];
                let text = split[i];
                let y = (i * lineHeightBig);
                let w = line.width;
                let x = 0;
                if (this._align == "center") {
                    x = (maxw - w) / 2;
                } else if (this.align == "right") {
                    x = maxw - w;
                }

                this._setFontStyle(context);

                // console.log("write " + text + " at " + x + "," + y);
                context.fillText(text, x, y + lineHeight);
            }

        }
    }

    let heightcache: HashMap<number>;
    function estimateTextHeight(fontName: string, fontSize: number): number {
        if (heightcache == null) {
            heightcache = new HashMap<number>();
        }

        let key = fontName + fontSize;
        let cached = heightcache.get(key);
        if (cached != null) return cached;

        let div = document.createElement("div");
        div.innerHTML = "(|MÃ‰qgy";
        div.style.position = 'absolute';
        div.style.top = '-9999px';
        div.style.left = '-9999px';
        div.style.fontFamily = fontName;
        div.style.fontWeight = 'normal';
        div.style.fontSize = fontSize + 'px';
        document.body.appendChild(div);

        let height = div.offsetHeight;
        heightcache.put(key, height);

        document.body.removeChild(div);

        return height;
    }

}

