/// <reference path="GuiAbstractProgressBar.ts" />
module plume {

    export class RoundProgressBar extends AbstractProgressBar {

        start: number = -Math.PI / 2;
        radius: number;
        centerX: number;
        centerY: number;
        private _inverted: boolean = false;

        constructor(_background: DisplayObject, _progress = 0) {
            super(_background, _progress);
            this.radius = _background.width / 2;
            this.centerX = this.radius;
            this.centerY = this.radius;
        }

        setInverted(inverted: boolean) {
            this._inverted = inverted;
        }

        // progress between 0 and 1
        updateProgress(progress: number) {

            if (progress < 0) progress = 0;
            else if (progress > 1) progress = 1;

            this._progress = progress;
        }

        update(dt: number) {
            super.update(dt);
        }

        render(interpolation: number) {
            super.render(interpolation);

            if (this._progress == this._lastProgress) return;

            this._background.mask = new Mask();
            if (this._progress > 0) {
                this._background.visible = true;

                if (!this._inverted) {
                    let progressToAngle = this._progress * 2 * Math.PI;
                    this._background.mask.moveTo(this.centerX, this.centerY);
                    this._background.mask.lineTo(this.radius, 0);
                    this._background.mask.arc(this.centerX, this.centerY, this.radius, this.start, this.start + progressToAngle);
                    this._background.mask.lineTo(this.centerX, this.centerY);
                } else {
                    let progressToAngle = this._progress * 2 * Math.PI;
                    this._background.mask.moveTo(this.centerX, this.centerY);
                    this._background.mask.lineTo(this.radius + this.radius * Math.cos(this.start + progressToAngle), this.radius + this.radius * Math.sin(this.start + progressToAngle));
                    this._background.mask.arc(this.centerX, this.centerY, this.radius, this.start + progressToAngle, 3 * Math.PI / 2);
                    this._background.mask.lineTo(this.centerX, this.centerY);
                }

            } else {
                this._background.visible = false;
            }

            this._lastProgress = this._progress;
        }

    }

}
