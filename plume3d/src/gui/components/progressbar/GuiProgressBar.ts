/// <reference path="GuiAbstractProgressBar.ts" />
module plume {

    export const enum ProgressBarWay {
        LeftToRight, RightToLeft, TopToBottom, BottomToTop
    }

    export class ProgressBar extends AbstractProgressBar {

        private _bgwidth = 0;
        private _bgheight = 0;
        private _inverted = false;

        constructor(_background: DisplayObject, private _way = ProgressBarWay.LeftToRight, _progress = 0) {
            super(_background, _progress);
            this._bgwidth = _background.width;
            this._bgheight = _background.height;
        }

        // progress between 0 and 1
        updateProgress(progress: number) {

            if (progress < 0) progress = 0;
            else if (progress > 1) progress = 1;

            this._progress = progress;
        }

        update(dt: number) {
            super.update(dt);
        }

        render(interpolation: number) {
            super.render(interpolation);

            if (this._progress == this._lastProgress) return;

            this._background.mask = new Mask();

            let mX, mY, mW, mH;

            if (this._way == ProgressBarWay.LeftToRight) {
                mX = 0;
                mY = 0;
                mW = this._bgwidth * this._progress;
                mH = this._bgheight;

                if (this._inverted) {
                    mX = mW;
                    mW = this._bgwidth - mW;
                }

            } else if (this._way == ProgressBarWay.RightToLeft) {
                let w = this._background.width * this._progress;
                mX = this._bgwidth - w;
                mY = 0;
                mW = w;
                mH = this._bgheight;

                if (this._inverted) {
                    mW = mX;
                    mX = 0;
                }
            }
            else if (this._way == ProgressBarWay.TopToBottom) {
                mX = 0;
                mY = 0;
                mW = this._bgwidth;
                mH = this._bgheight * this._progress;

                if (this._inverted) {
                    mY = mH;
                    mH = this._bgheight - mH;
                }

            } else if (this._way == ProgressBarWay.BottomToTop) {
                let h = this._bgheight * this._progress;
                mX = 0;
                mY = this._bgheight - h;
                mW = this._bgwidth;
                mH = h;


                if (this._inverted) {
                    mH = mY;
                    mY = 0;
                }
            }

            this._background.mask.rectangle(mX, mY, mW, mH);

            this._lastProgress = this._progress;
        }

        setInverted(inverted: boolean) {
            this._inverted = true;
        }

    }

}
