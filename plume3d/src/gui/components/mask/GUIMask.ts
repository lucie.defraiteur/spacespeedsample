module plume {

    // to do when required arcTo, bezierCurveTo, quadraticCurveTo
    const enum Action {
        MoveTo = -1, LineTo = -2, Rectangle = -3, Arc = -4
    }

    export class Mask {

        private _command: Array<number> = [];

        moveTo(x: number, y: number) {
            this._command.push(Action.MoveTo);
            this._command.push(x);
            this._command.push(y);
        }

        lineTo(x: number, y: number) {
            this._command.push(Action.LineTo);
            this._command.push(x);
            this._command.push(y);
        }

        rectangle(x: number, y: number, width: number, height: number) {
            this._command.push(Action.Rectangle);
            this._command.push(x);
            this._command.push(y);
            this._command.push(width);
            this._command.push(height);
        }

        arc(cx: number, cy: number, radius: number, startAngle: number = 0, endAngle: number = 2 * Math.PI) {
            this._command.push(Action.Arc);
            this._command.push(cx);
            this._command.push(cy);
            this._command.push(radius);
            this._command.push(startAngle);
            this._command.push(endAngle);
        }

        _applyMask(context: CanvasRenderingContext2D) {

            context.beginPath()

            for (let i = 0; i < this._command.length; i++) {
                let action = this._command[i];

                if (action == Action.MoveTo) {
                    context.moveTo(this._command[i + 1], this._command[i + 2]);
                    i += 2;
                } else if (action == Action.LineTo) {
                    context.lineTo(this._command[i + 1], this._command[i + 2]);
                    i += 2;
                } else if (action == Action.Rectangle) {
                    context.rect(this._command[i + 1], this._command[i + 2], this._command[i + 3], this._command[i + 4]);
                    i += 4;
                } else if (action == Action.Arc) {
                    context.arc(this._command[i + 1], this._command[i + 2], this._command[i + 3], this._command[i + 4], this._command[i + 5]);
                    i += 5;
                }
            }

            context.clip();
        }
    }

}