// module plume {

//     export class ScrollableList extends Container {

//         private _upOrLeft: Button;
//         private _downOrRight: Button;

//         private _itemSize: number;
//         private _items: Array<DisplayObject> = [];

//         private _scrollableContainer: ScrollableContainer;
//         private _contentBounds: Rectangle;

//         private _topItemOverlay: Graphics;
//         private _bottomItemOverlay: Graphics;

//         //
//         optShowItemOverlay = true;

//         constructor(private _itemWidth: number, private _itemHeight: number, private _visibleItems: number, private _vertical: boolean) {
//             super();

//             var self = this;
//             this._scrollableContainer = new ScrollableContainer();
//             this.addChild(this._scrollableContainer);

//             var visibleWidth = _itemWidth;
//             var visibleHeight = _itemHeight;

//             if (_vertical) {
//                 this._itemSize = _itemHeight;
//                 visibleHeight = _itemHeight * _visibleItems;
//                 this._scrollableContainer.scrollEnableOnX = false;
//             } else {
//                 this._itemSize = _itemWidth;
//                 visibleWidth = _itemWidth * _visibleItems;
//                 this._scrollableContainer.scrollEnableOnY = false;
//             }

//             this._scrollableContainer.setContainerSize(visibleWidth, visibleHeight);
//             this._scrollableContainer.addScrollHandler(this.onScroll, this);
//             this._scrollableContainer.wheelOnY = this._vertical;

//             this._contentBounds = new Rectangle(0, 0, visibleWidth, visibleHeight);
//             this.setSize(visibleWidth, visibleHeight);

//             this.updateVisibleItems(0);
//         }

//         getContentBounds(): Rectangle {
//             return this._contentBounds;
//         }

//         addItem(item: DisplayObject) {
//             var position = this._items.length;

//             // update x/y before adding to scrollable container to have accurate bounds
//             if (this._vertical) {
//                 item.x = 0;
//                 item.y = position * this._itemHeight;
//             } else {
//                 item.x = position * this._itemWidth;
//                 item.y = 0;
//             }

//             this._scrollableContainer.addDisplayObject(item);
//             this._items.push(item);

//             this.updateVisibleItems();
//         }

//         clear() {
//             this._scrollableContainer.clear();
//             this._items = [];

//             this.updateVisibleItems(0);
//         }

//         // keeping button right now, but better to remove them and do cleaner UI with scrolling indicator (ie: scrollbar or just cutting next item)
//         setControlButton(upOrLeft: Button, downOrRight: Button) {
//             var self = this;
//             this._upOrLeft = upOrLeft;
//             this._downOrRight = downOrRight;

//             this._upOrLeft.onClick(function () {
//                 self.moveUpOrLeft();
//             });

//             this._downOrRight.onClick(function () {
//                 self.moveDownOrRight();
//             });
//         }

//         moveUpOrLeft() {
//             if (this._vertical) this.moveUp();
//             else this.moveLeft();
//         }

//         moveDownOrRight() {
//             if (this._vertical) this.moveDown();
//             else this.moveRight();
//         }

//         moveUp() {
//             if (!this._vertical) return;
//             this.moveUp0();
//         }

//         moveDown() {
//             if (!this._vertical) return;
//             this.moveDown0();
//         }

//         moveLeft() {
//             if (this._vertical) return;
//             this.moveUp0();
//         }

//         moveRight() {
//             if (this._vertical) return;
//             this.moveDown0();
//         }

//         scrollToItem(topIndex: number) {
//             if (topIndex > (this._items.length - this._visibleItems)) {
//                 topIndex = this._items.length - this._visibleItems;
//             }

//             if (topIndex >= 0) {
//                 this.moveTo(topIndex);
//             }
//         }

//         getItems(): Array<DisplayObject> {
//             return this._items;
//         }

//         private onScroll(offsetX: number, offsetY: number, x: number, y: number) {
//             this.updateVisibleItems();
//         }

//         private moveUp0() {
//             var move = this.getMoveByCount();
//             this.moveBy(-move);
//         }
//         private moveDown0() {
//             var move = this.getMoveByCount();
//             this.moveBy(move);
//         }

//         private getMoveByCount() {
//             if (this._visibleItems > 3) return this._visibleItems - 2;
//             return 1;
//         }

//         private moveBy(count: number) {
//             var offset = count * this._itemSize;

//             if (this._vertical) this._scrollableContainer.moveByAbout(null, offset);
//             else this._scrollableContainer.moveByAbout(offset, null);

//             this.updateVisibleItems();
//         }

//         private moveTo(index: number) {
//             var position = index * this._itemSize;

//             if (this._vertical) this._scrollableContainer.moveTo(null, position);
//             else this._scrollableContainer.moveTo(position, null);

//             this.updateVisibleItems();
//         }


//         private updateVisibleItems(top: number = null) {

//             if (top == null) {
//                 var cb = this._scrollableContainer.getContentBound();
//                 top = (this._vertical ? cb.y : cb.x);
//             }

//             if (this._topItemOverlay != null) this._topItemOverlay.visible = false;
//             if (this._bottomItemOverlay != null) this._bottomItemOverlay.visible = false;

//             var bottom = top + this._visibleItems * this._itemSize;
//             for (var i = 0; i < this._items.length; i++) {
//                 var item = this._items[i];
//                 var l1 = this._vertical ? item.y : item.x;
//                 var l2 = this._vertical ? (item.y + this._itemSize) : (item.x + this._itemSize);

//                 if (l1 >= top && l2 <= bottom) {
//                     // fully visible
//                     item.visible = true;
//                 } else {
//                     if (l1 >= bottom || l2 <= top) {
//                         item.visible = false;
//                     } else {
//                         // cut
//                         item.visible = true;
//                         if ((l1 + this._itemSize / 2) < top) {
//                             this.addTopItemOverlay(item);
//                         } else if ((l2 - this._itemSize / 2) > bottom) {
//                             this.addBottomItemOverlay(item);
//                         }
//                     }
//                 }
//             }
//         }

//         private addTopItemOverlay(item: DisplayObject) {
//             if (!this.optShowItemOverlay) return;

//             if (this._topItemOverlay == null) {
//                 this._topItemOverlay = this.buildItemOverlay(item);
//                 this._scrollableContainer.addDisplayObject(this._topItemOverlay);
//             }

//             this._topItemOverlay.visible = true;
//             this._topItemOverlay.x = item.x;
//             this._topItemOverlay.y = item.y;
//         }

//         private addBottomItemOverlay(item: DisplayObject) {
//             if (!this.optShowItemOverlay) return;

//             if (this._bottomItemOverlay == null) {
//                 this._bottomItemOverlay = this.buildItemOverlay(item);
//                 this._scrollableContainer.addDisplayObject(this._bottomItemOverlay);
//             }

//             this._bottomItemOverlay.visible = true;
//             this._bottomItemOverlay.x = item.x;
//             this._bottomItemOverlay.y = item.y;
//         }

//         private buildItemOverlay(item: DisplayObject): Graphics {
//             var w = this._itemWidth;
//             var h = this._itemWidth;
//             if (item instanceof Container) {
//                 w = Math.min(this._itemWidth, item.width);
//                 h = Math.min(this._itemHeight, item.height);
//             }

//             var overlay = new Graphics();
//             overlay.beginFill(0x000000, 0.2);
//             overlay.drawRect(0, 0, w, h);
//             overlay.endFill();
//             overlay.showCursor = false;
//             return overlay;
//         }

//     }


// }   