// module plume {

//     export class TilingSprite extends Container {

//         private _tilePositionX = 0;
//         private _tilePositionY = 0;
//         private _dirty = true;

//         private _elementW = 0;
//         private _elementH = 0;
//         private _elementCountW = 0;
//         private _elementCountH = 0;

//         constructor(width: number, height: number, private objectFactory: () => DisplayObject) {
//             super(width, height);

//             this.mask = new Mask();
//             this.mask.rectangle(0, 0, width, height);
//         }

//         static fromFrame(width: number, height: number, name: string): TilingSprite {
//             let ts = new TilingSprite(width, height, function () {
//                 return Sprite.fromFrame(name);
//             });
//             return ts;
//         }

//         draw0(context: CanvasRenderingContext2D) {
//             if (this._dirty) {
//                 this.create();
//                 this._dirty = false;
//             }

//             super.draw0(context);
//         }

//         private create() {

//             if (this._elementCountW == 0) {
//                 // first init, fill container width all childs
//                 let elem = this.objectFactory();
//                 let w = elem.width;
//                 let h = elem.height;
//                 this._elementCountW = Math.ceil((this.width / w) + 1);
//                 this._elementCountH = Math.ceil((this.height / h) + 1);
//                 this.addChild(elem);

//                 let count = (this._elementCountW * this._elementCountH);
//                 for (let i = 1; i < count; i++) {
//                     this.addChild(this.objectFactory());
//                 }

//                 this._elementW = w;
//                 this._elementH = h;
//             }

//             // Move all elements
//             let startx = Maths.mod(this._tilePositionX, this._elementW) - this._elementW;
//             let starty = Maths.mod(this._tilePositionY, this._elementH) - this._elementH;
//             for (let x = 0; x < this._elementCountW; x++) {
//                 for (let y = 0; y < this._elementCountH; y++) {
//                     let idx = (x * this._elementCountH) + y;
//                     let child = this.getChildAt(idx);
//                     child.x = startx + (x * this._elementW);
//                     child.y = starty + (y * this._elementH);
//                 }
//             }
//         }

//         get tilePositionX(): number {
//             return this._tilePositionX;
//         }
//         set tilePositionX(v: number) {
//             this._tilePositionX = v;
//             this._dirty = true;
//         }
//         get tilePositionY(): number {
//             return this._tilePositionY;
//         }
//         set tilePositionY(v: number) {
//             this._tilePositionY = v;
//             this._dirty = true;
//         }

//     }

// }