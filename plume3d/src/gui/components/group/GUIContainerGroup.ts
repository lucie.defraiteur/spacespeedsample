﻿/// <reference path="../../primitives/GuiContainer.ts" />
module plume {

    export class ContainerGroup implements Updatable {

        private _root: Container;
        private _childs: HashMap<ContainerGroupData> = new HashMap<ContainerGroupData>();
        private _dirty: boolean = false;

        constructor() {
            this._root = new Container();
            this._root._root = true;
            this._root.setDebugId("root");
        }

        destroy() {
            this._childs.clear();
        }

        update() {
            if (this._dirty) {
                this._sortLayers();
                this._dirty = false;
            }
        }

        // create child if not there
        getChild(name: string): Container {
            let layer = this._ensureLayer(name);
            return layer.container;
        }

        addChild(name: string, index: number = 1): Container {
            let layer = this._ensureLayer(name);
            layer.zindex = index;
            return layer.container;
        }

        removeChild(name: string): Container {
            let layer = this._childs.remove(name);
            if (layer != null) {
                this._root.removeChild(layer.container);
            }
            return layer.container;
        }

        setChildIndex(name: string, zindex: number) {
            let layer = this._ensureLayer(name);
            layer.zindex = zindex;
            this._dirty = true;
        }

        private _sortLayers() {
            let layers = this._childs.values();
            let sorted = layers.sort(function (l1, l2) {
                return l1.zindex - l2.zindex;
            });

            for (let i = 0; i < sorted.length; i++) {
                let layer = sorted[i];
                let current = this._root.getChildIndex(layer.container);
                if (current != i) {
                    this._root.setChildIndex(layer.container, i);
                }
            }
        }

        private _ensureLayer(name: string): ContainerGroupData {
            let layer = this._childs.get(name);
            if (layer == null) {
                layer = new ContainerGroupData(name);
                this._root.addChild(layer.container);
                this._childs.put(name, layer);
                this._dirty = true;
            }
            return layer;
        }

        get root(): Container {
            return this._root;
        }

        set root(root: Container) {
            throw new Error("root is read-only");
        }

    }


    class ContainerGroupData {
        container: Container;
        name: string;
        zindex: number;

        constructor(name: string) {
            this.container = new Container();
            this.container.setDebugId(name);
            this.zindex = 1;
            this.name = name;
        }
    }
}
