/// <reference path="../../primitives/GUISprite.ts" />
module plume {

    export class Animation extends Sprite implements Updatable {

        static fromFrames(frameIds: Array<string>, id?: string): Animation {
            let animation = new Animation(Texture.fromFrames(frameIds), id);
            return animation;
        }

        static fromTextures(textures: Array<Texture>, id?: string): Animation {
            let animation = new Animation(textures, id);
            return animation;
        }

        public fps: number = 1000 / 60;

        private _curIndex: number = 0;
        private _textures: Array<Texture> = [];
        private _playing: boolean = false;

        private _startTime: number = 0;
        private _startFrame: number = 0;
        private _lastIndex: number = -1;

        private _from: number = 0;
        private _to: number = 0;

        onEnd: Function;

        private _repeats: number = 1;
        private _totalFrames: number;

        forward: boolean = true;
        animationSpeed: number = 1;

        paused = false;
        pausedDuration = 0;

        constructor(textures: Array<Texture>, id?: string) {
            super(textures[0]);

            let self = this;
            textures.forEach(function (t) {
                self._textures.push(t);
            });

            Game.get().addUpdatable(this);
            this.detachListener.once(this.destroy, this);

            if (id != null) this.setDebugId(id);
        }

        destroy() {
            this.stop();
            Game.get().removeUpdatable(this);
        }

        setTextures(textureIdList: Array<string>) {
            this.setTextures2(Texture.fromFrames(textureIdList));
        }

        setTextures2(textures: Array<Texture>) {
            let wasPlaying = this.isPlaying();
            if (wasPlaying == true) {
                this.stop();
            }
            this._textures = textures;
            this.gotoAndStop(0)
            if (wasPlaying == true) {
                this.play();
            }
        }

        setFrameIndex(index: number) {
            let t = this._textures[index];
            this.setTexture(t);
            if (this._playing == false) {
                this._playing = true;
            }
        }

        protected setActiveFrame(index: number) {
            if (index == null) return;

            let newIndex;
            if (index < 0) newIndex = (index % (this._totalFrames) + this._totalFrames) % this._totalFrames;
            else newIndex = index % (this._totalFrames);

            if (newIndex == this._curIndex) return;

            this._curIndex = newIndex;

            let frameIndex = this._from + (this._curIndex % this._totalFrames);
            this.setFrameIndex(frameIndex);

            if (this._lastIndex > 0) {
                index = Math.abs(index);
                if (index >= this._lastIndex) {
                    // time to stop
                    this.setFrameIndex(this._to);
                    this._onAnimationEnded();
                }
            }
        }

        private _onAnimationEnded() {
            this._playing = false;
            if (this.onEnd) this.onEnd();
        }

        gotoAndStop(frameName: number) {
            this.setFrameIndex(frameName);
            this._playing = false;
        };

        gotoAndPlay(frameName: number) {
            this.setFrameIndex(frameName);
        };

        update(simulationTimeStep: number) {
            if (this.paused) {
                this.pausedDuration += simulationTimeStep;
            }
            else if (this._playing) {
                let frameEllapsed = Math.floor((Time.now() - this._startTime) / this.fps);
                frameEllapsed = Math.floor(frameEllapsed * this.animationSpeed);

                let frame = this._startFrame;
                if (this.forward) {
                    frame += frameEllapsed;
                } else {
                    frame -= frameEllapsed;
                }
                this.setActiveFrame(frame);
            }
        }

        getTotalFrames(): number {
            return this._totalFrames;
        }

        getCurrentFrame(): number {
            return this._curIndex;
        }

        private _play0() {
            this._playing = true;
            this._startTime = Time.now();
            this._startFrame = this._curIndex;
        }

        play(from: number = 0, to: number = 0) {

            this._from = from;

            if (to > 0) this._to = to;
            else this._to = this._textures.length - 1;

            this._totalFrames = (this._to - this._from) + 1;
            this._curIndex = from;

            this.setFrameIndex(from);

            if (this._repeats < 0) {
                this._lastIndex = -1;
            } else {
                this._lastIndex = this._curIndex + this._totalFrames * this._repeats;
            }

            this._play0();
        }

        repeat(repeats: number = 0) {
            this._repeats = repeats;
        }

        isPlaying(): boolean {
            return this._playing;
        }

        stop() {
            this._playing = false;
        }


        pause() {
            this.paused = true;
        }

        resume() {
            this._startTime += this.pausedDuration;
            this.pausedDuration = 0;
            this.paused = false;
        }

    }
}