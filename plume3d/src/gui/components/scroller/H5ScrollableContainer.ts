
// module plume {

//     export class ScrollableContainer extends Container implements DragListener, Updatable {

//         applyVelocity = true;
//         velocityTheshold = 0.2;
//         dragDurationVelocityTheshold = 400;

//         wheelOnY = true;

//         scrollEnableOnX = true;
//         scrollEnableOnY = true;

//         private _dragManager: DragManager;

//         private _innerVelocity: Vector = new Vector();
//         private _dragStartPosition: Point = new Point();
//         private _dragStartTime: number = 0;

//         private _lastInnerPosition: Point = new Point();
//         private _inner: Container;
//         private _innerMin: Point;
//         private _innerMax: Point;
//         private _contentMask: Mask;

//         private _contentBounds: Rectangle;
//         private _game: Game;

//         private _scrollHandler: HandlerCollection = new HandlerCollection();

//         constructor() {
//             super();

//             var self = this;

//             var gb = Game.get().bounds;

//             this.interactive = true;
//             this.showCursor = false;

//             this._game = Game.get();
//             this._game.addUpdatable(this);

//             this._inner = new Container();
//             this.addChild(this._inner);

//             this._dragManager = new DragManager(this);
//             this._dragManager.setStartDragController(function (position: Point) {
//                 return self.canStartDrag(position);
//             });
//             this._game.inputManager.mouse.mouseWheelHandlers.add(this.onMouseWheel, this);

//             this.detachListener.once(function () {
//                 self.destroy();
//             });

//             // default, set size to game size
//             this.setContainerSize(gb.width, gb.height);
//         }

//         setContainerSize(width: number, height: number) {
//             this.setSize(width, height);

//             this._contentBounds = new Rectangle(0, 0, width, height);

//             this._contentMask = new Mask();
//             this._contentMask.rectangle(0, 0, width, height);
//             this.mask = this._contentMask;
//         }

//         onDragBegin(position: Point) {
//             this._dragStartPosition = position.copy();
//             this._dragStartTime = Date.now();
//             this._innerVelocity.x = 0;
//             this._innerVelocity.y = 0;
//         }

//         onDragMove(xOffset: number, yOffset: number, position: Point) {
//             // offset is negative
//             if (this.scrollEnableOnX) this._inner.x += xOffset;
//             if (this.scrollEnableOnY) this._inner.y += yOffset;

//             this.checkBoundLimit();
//         }

//         onDragEnd(position: Point) {
//             var moveX = position.x - this._dragStartPosition.x;
//             var moveY = position.y - this._dragStartPosition.y;
//             var duration = Date.now() - this._dragStartTime;

//             if (!this.scrollEnableOnX) moveX = 0;
//             if (!this.scrollEnableOnY) moveY = 0;

//             if (this.applyVelocity) {
//                 if (duration < this.dragDurationVelocityTheshold) {
//                     this._innerVelocity.x = moveX / duration;
//                     this._innerVelocity.y = moveY / duration;
//                 }
//             }
//         }

//         onMouseWheel(delta: number) {

//             var mousePosition = Game.get().inputManager.mouse.position;
//             if (!this.canStartDrag(mousePosition)) {
//                 return;
//             }

//             // simulate drag
//             var offsetX = 0;
//             var offsetY = 0;
//             var offset = -delta * 50;
//             if (this.wheelOnY) {
//                 offsetY = offset;
//             } else {
//                 offsetX = offset;
//             }

//             this.doFakeDrag(offsetX, offsetY, 1000);
//         }

//         addDisplayObject(child: DisplayObject, at?: number): DisplayObject {
//             var added;
//             if (at != null) {
//                 added = this._inner.addChildAt(child, at);
//             } else {
//                 added = this._inner.addChild(child);
//             }

//             this.computeInnerMinMax();
//             return added;
//         }

//         clear() {
//             this._inner.removeChildren();
//             this.computeInnerMinMax();
//         }

//         moveTo(x: number = null, y: number = null) {
//             if (x != null) this._inner.x = -x;
//             if (y != null) this._inner.y = -y;

//             this.checkBoundLimit();
//         }

//         moveBy(offsetx: number = null, offsety: number = null) {
//             if (offsetx != null) this._inner.x -= offsetx;
//             if (offsety != null) this._inner.y -= offsety;

//             this.checkBoundLimit();
//         }

//         // move for about x/y simulate a drag from "inner - offset" to "inner"
//         // movement is only the resulting animation based of displacement / acceleration (not real offset)
//         moveByAbout(offsetx: number = null, offsety: number = null) {

//             if (!this.canMove()) return;

//             if (offsetx == null) offsetx = 0;
//             if (offsety == null) offsety = 0;

//             this._inner.x += offsetx;
//             this._inner.y += offsety;

//             this.doFakeDrag(-offsetx, -offsety, 300);
//         }

//         update(ts: number) {

//             if (this._innerVelocity.x != 0 || this._innerVelocity.y != 0) {
//                 this._innerVelocity.x *= 0.95;
//                 this._innerVelocity.y *= 0.95;

//                 if (Math.abs(this._innerVelocity.x) < this.velocityTheshold) this._innerVelocity.x = 0;
//                 if (Math.abs(this._innerVelocity.y) < this.velocityTheshold) this._innerVelocity.y = 0;

//                 this._inner.x = this._inner.x + ts * this._innerVelocity.x;
//                 this._inner.y = this._inner.y + ts * this._innerVelocity.y;

//                 this.checkBoundLimit();
//             }

//         }

//         getContentBound(): Rectangle {
//             this._contentBounds.x = -this._inner.x;
//             this._contentBounds.y = -this._inner.y;

//             return this._contentBounds;
//         }

//         private doFakeDrag(offsetX: number, offsetY: number, duration: number) {
//             var start = new Point(this._inner.x, this._inner.y);
//             var end = start.copy();
//             end.x = end.x + offsetX;
//             end.y = end.y + offsetY;

//             this.onDragBegin(start);
//             this._dragStartTime = Date.now() - duration;
//             this.onDragMove(offsetX, offsetY, end);
//             this.onDragEnd(end);
//         }

//         private computeInnerMinMax() {
//             // compute inner width/height dynamically based on children added
//             var innerBounds = layout.computeBounds(this._inner, false);
//             this._inner.setSize(innerBounds.width, innerBounds.height);

//             var w = this._inner.width;
//             var h = this._inner.height;

//             this._innerMin = new Point(-w + this._contentBounds.width, -h + this._contentBounds.height);
//             this._innerMax = new Point(0, 0);
//         }

//         private canMove(): boolean {
//             if (this._inner.width <= this._contentBounds.width && this._inner.height <= this._contentBounds.height) {
//                 return false;
//             }
//             return true;
//         }

//         private canStartDrag(position: Point): boolean {

//             if (!this.canMove()) return false;

//             var inside = this.hitPoint(position);
//             return inside;
//         }

//         // called after each inner x/y change
//         private checkBoundLimit() {
//             if (this.scrollEnableOnX) {
//                 if (this._inner.x < this._innerMin.x) this._inner.x = this._innerMin.x;
//                 else if (this._inner.x > this._innerMax.x) this._inner.x = this._innerMax.x;
//             }

//             if (this.scrollEnableOnY) {
//                 if (this._inner.y < this._innerMin.y) this._inner.y = this._innerMin.y;
//                 else if (this._inner.y > this._innerMax.y) this._inner.y = this._innerMax.y;
//             }

//             var offsetX = this._lastInnerPosition.x - this._inner.x;
//             var offsetY = this._lastInnerPosition.y - this._inner.y;
//             this._lastInnerPosition.x = this._inner.x;
//             this._lastInnerPosition.y = this._inner.y;

//             if (offsetX != 0 || offsetY != 0) {
//                 this._scrollHandler.fire(offsetX, offsetY, -this._lastInnerPosition.x, -this._lastInnerPosition.y);
//             }
//         }

//         private destroy() {
//             this._dragManager.destroy();
//             this._game.inputManager.mouse.mouseWheelHandlers.remove(this.onMouseWheel);
//             this._game.removeUpdatable(this);
//         }

//         addScrollHandler(fn: (offsetX: number, offsetY: number, x: number, y: number) => any, context?: any) {
//             this._scrollHandler.add(fn, context);
//         }

//         removeScrollHandler(fn: Function) {
//             this._scrollHandler.remove(fn);
//         }

//     }

// }