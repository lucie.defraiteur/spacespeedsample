﻿/// <reference path="../../primitives/GUIContainer.ts" />
module plume {

    export class Button extends Container {

        protected _down: boolean;
        protected _over: boolean;
        protected _enabled: boolean = true;

        disableAlpha: number = 0.5;

        constructor() {
            super();

            this.interactive = true;

            this.downHandler.add(this.onStateDown, this);
            this.upHandler.add(this.onStateUp, this);
            this.overHandler.add(this.onStateOver, this);
            this.outHandler.add(this.onStateOut, this);

            this.enable();
        }

        enable() {
            this._enabled = true;
            this.interactive = true;
            this.alpha = 1;
        }

        disable() {
            this._enabled = false;
            this.interactive = false;
            this.alpha = this.disableAlpha;
        }

        onStateDown() {
            this._down = true;
        }

        onStateUp() {
            this._over = false; // for mobile, do not maintain "over" state when releasing button
            this._down = false;
        }

        onStateOver() {
            this._over = true;
        }

        onStateOut() {
            this._over = false;
            this._down = false;
        }

    }

}