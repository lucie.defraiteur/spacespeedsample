module plume {

    export class GuiScaling {

        private _scaleManager: ScaleManager;

        constructor() {
            this._scaleManager = Game.get().scaleManager;
        }

        init() {
            this._scaleManager.resizeHandler.add(this._onResize, this);
            this._onResize();
        }

        private _onResize() {
            let current = this._scaleManager.getCurrentScaling();

            let devicePixelRatio = window.devicePixelRatio || 1;
            let width = current.windowWidth;
            let height = current.windowHeight;

            let canvasGui = CanvasGui.get();
            let canvas = canvasGui.canvas;
            let root = canvasGui.layers.root;
            canvas.width = width * devicePixelRatio;
            canvas.height = height * devicePixelRatio;
            canvas.style.width = width + 'px';
            canvas.style.height = height + 'px';
            root.scaleXY = current.scale * devicePixelRatio;
            root.x = current.x * devicePixelRatio;
            root.y = current.y * devicePixelRatio;

            let game = Game.get();
            let marginX = current.x / current.scale;
            let marginY = current.y / current.scale;
            CanvasGui.get().expandedBounds = new Rectangle(-marginX, -marginY, game.width + 2 * marginX, game.height + 2 * marginY);
        }
    }

}