
module plume {

    export class Colors {

        static getRgb(color: number): Array<number> {
            let r = (color >> 16) & 0x0000FF;
            let g = (color >> 8) & 0x0000FF;
            let b = color & 0x0000FF;
            return [r, g, b];
        }

        static fromRgb(r: number, g: number, b: number): number {
            return r << 16 | g << 8 | b;
        }

        static fromHSV(h: number, s: number, v: number) {
            let r, g, b: number;
            let i = Math.floor(h * 6);
            let f = h * 6 - i;
            let p = v * (1 - s);
            let q = v * (1 - f * s);
            let t = v * (1 - (1 - f) * s);
            switch (i % 6) {
                case 0: r = v, g = t, b = p; break;
                case 1: r = q, g = v, b = p; break;
                case 2: r = p, g = v, b = t; break;
                case 3: r = p, g = q, b = v; break;
                case 4: r = t, g = p, b = v; break;
                case 5: r = v, g = p, b = q; break;
            }

            r = Math.round(r * 255);
            g = Math.round(g * 255);
            b = Math.round(b * 255);

            return this.fromRgb(r, g, b);
        }

        static toString(color: number, alpha: number = 1) {
            let red = ((color & 0xFF0000) >>> 16);
            let green = ((color & 0xFF00) >>> 8);
            let blue = (color & 0xFF);
            return 'rgba(' + red + ',' + green + ',' + blue + ',' + alpha + ')';
        }

        static toHexString(color: number) {
            return '#' + Strings.pads(color.toString(16), 6);
        }

        static fromString(color: string): number {
            if (color.length != 7) return 0;
            let ci = parseInt(color.replace("#", ""), 16);
            return ci;
        }
    }
}
