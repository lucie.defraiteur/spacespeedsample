import * as ts from "typescript";
import * as Lint from "tslint";

export class Rule extends Lint.Rules.AbstractRule {

    public static readonly PRIVATE_METHOD_FAIL = "Private method should start with _";
    public static readonly METHOD_FAIL = "Method should be camelCase";
    public static readonly PRIVATE_PROPERTY_FAIL = "Private property should start with _";
    public static readonly READONLY_PROPERTY_FAIL = "Read only property should be UPPERCASE";
    public static readonly PROPERTY_FAIL = "Property should be camelCase";

    public apply(sourceFile: ts.SourceFile): Lint.RuleFailure[] {
        return this.applyWithWalker(
            new NamingSchemeWalker(sourceFile, this.getOptions())
        );
    }
}

// Class, interface: PascaleCase (check in another rule)
// Method: camelCase (_ before private)
// Property: camelCase (_ before private), readonly are uppercase
// getter/setter: like property

class NamingSchemeWalker extends Lint.RuleWalker {


    protected visitMethodDeclaration(node: ts.MethodDeclaration): void {

        const name = node.name.getText();
        if (Lint.hasModifier(node.modifiers, ts.SyntaxKind.PrivateKeyword)) {
            if (!hasLeadingUnderscore(name)) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PRIVATE_METHOD_FAIL));
            }
        } else {
            if (!isLowerCase(name[0])) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.METHOD_FAIL));
            }
        }

        super.visitMethodDeclaration(node);
    }

    protected visitPropertyDeclaration(node: ts.PropertyDeclaration): void {

        const propname = node.name.getText();
        if (Lint.hasModifier(node.modifiers, ts.SyntaxKind.PrivateKeyword)) {
            if (!hasLeadingUnderscore(propname)) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PRIVATE_PROPERTY_FAIL));
            }
        } else if (Lint.hasModifier(node.modifiers, ts.SyntaxKind.ReadonlyKeyword)) {
            if (!isUpperCase(propname)) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.READONLY_PROPERTY_FAIL));
            }
        } else {
            if (!isLowerCase(propname[0])) {
                this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PROPERTY_FAIL));
            }
        }

        super.visitPropertyDeclaration(node);
    }

    protected visitGetAccessor(node: ts.AccessorDeclaration): void {
        if (!this._checkGetter(node.name.getText())) {
            this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PROPERTY_FAIL));
        }
        super.visitGetAccessor(node);
    }
    protected visitSetAccessor(node: ts.AccessorDeclaration): void {
        if (!this._checkGetter(node.name.getText())) {
            this.addFailure(this.createFailure(node.getStart(), node.getWidth(), Rule.PROPERTY_FAIL));
        }
        super.visitSetAccessor(node);
    }

    private _checkGetter(name: string): boolean {
        return isLowerCase(name[0]);
    }

}

export function isUpperCase(str: string): boolean {
    return str === str.toUpperCase();
}

export function isLowerCase(str: string): boolean {
    return str === str.toLowerCase();
}

export function hasLeadingUnderscore(name: string): boolean {
    const firstCharacter = name[0];
    return firstCharacter === "_";
}