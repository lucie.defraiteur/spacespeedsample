"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var plume;
(function (plume) {
    var CannonBodyBuilder = /** @class */ (function () {
        function CannonBodyBuilder() {
        }
        CannonBodyBuilder.newBodyFromMesh = function (mesh, params) {
            var geometry = mesh.geometry;
            if (geometry instanceof THREE.BoxBufferGeometry || geometry instanceof THREE.BoxGeometry) {
                geometry.computeBoundingBox();
                var bbox = geometry.boundingBox;
                var size = new THREE.Vector3();
                bbox.getSize(size).multiply(mesh.scale);
                return this.newBox(size.x, size.y, size.z, params);
            }
            else if (geometry instanceof THREE.SphereBufferGeometry || geometry instanceof THREE.SphereGeometry) {
                geometry.computeBoundingSphere();
                var s = geometry.boundingSphere;
                return this.newSphere(s.radius * mesh.scale.x, params);
            }
            else if (geometry instanceof THREE.BufferGeometry || geometry instanceof THREE.Geometry) {
                var s = this.newTriMeshShape(geometry);
                return this.newRigidBody(s, params);
            }
            plume.logger.warn("Unable to build physics body from mesh: " + mesh);
            return null;
        };
        CannonBodyBuilder.newBoxShape = function (w, h, d) {
            var geometry = new CANNON.Box(new CANNON.Vec3(w * 0.5, h * 0.5, d * 0.5));
            return geometry;
        };
        CannonBodyBuilder.newBox = function (w, h, d, params) {
            if (params === void 0) { params = { mass: 0 }; }
            var geometry = this.newBoxShape(w, h, d);
            return this.newRigidBody(geometry, params);
        };
        CannonBodyBuilder.newSphere = function (radius, params) {
            if (params === void 0) { params = { mass: 0 }; }
            var geometry = new CANNON.Sphere(radius);
            return this.newRigidBody(geometry, params);
        };
        // !! Cylinder in CANNON is oriented along the z axis (the shape will be rotated if fixRotation == true )
        CannonBodyBuilder.newCylinder = function (radiusTop, radiusBottom, height, numSegments, params, fixRotation) {
            if (params === void 0) { params = { mass: 0 }; }
            if (fixRotation === void 0) { fixRotation = true; }
            var geometry = new CANNON.Cylinder(radiusTop, radiusBottom, height, numSegments);
            if (fixRotation) {
                var quat = new CANNON.Quaternion();
                quat.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2);
                var translation = new CANNON.Vec3(0, 0, 0);
                geometry.transformAllPoints(translation, quat);
            }
            return this.newRigidBody(geometry, params);
        };
        CannonBodyBuilder.newPlane = function (rotation) {
            var shape = new CANNON.Plane();
            var body = this.newRigidBody(shape);
            if (rotation != null) {
                body.quaternion.set(rotation.x, rotation.y, rotation.z, rotation.w);
            }
            return body;
        };
        CannonBodyBuilder.newTriMeshShape = function (geometry) {
            if (geometry instanceof THREE.BufferGeometry) {
                var vertices = geometry.getAttribute("position").array;
                var index = geometry.index.array;
                var shape = new CANNON.Trimesh(vertices, index);
                return shape;
            }
            else {
                var vertices = [];
                for (var i = 0; i < geometry.vertices.length; i++) {
                    vertices[(i * 3) + 0] = (geometry.vertices[i].x);
                    vertices[(i * 3) + 1] = (geometry.vertices[i].y);
                    vertices[(i * 3) + 2] = (geometry.vertices[i].z);
                }
                var indices = [];
                for (var i = 0; i < geometry.faces.length; i++) {
                    indices.push(geometry.faces[i].a);
                    indices.push(geometry.faces[i].b);
                    indices.push(geometry.faces[i].c);
                }
                var shape = new CANNON.Trimesh(vertices, indices);
                return shape;
            }
        };
        CannonBodyBuilder.newTriMesh = function (geometry) {
            var shape = this.newTriMeshShape(geometry);
            return this.newRigidBody(shape);
        };
        CannonBodyBuilder.newRigidBody = function (collisionShape, params) {
            if (params === void 0) { params = { mass: 0 }; }
            var body = new CANNON.Body({
                mass: params.mass,
                type: params.type
            });
            body.addShape(collisionShape);
            return body;
        };
        return CannonBodyBuilder;
    }());
    plume.CannonBodyBuilder = CannonBodyBuilder;
})(plume || (plume = {}));
/// <reference path="../../build/latest/plume3d.core.d.ts" />
/// <reference path="../../vendor/cannon.d.ts" />
var plume;
(function (plume) {
    var CannonSimulation = /** @class */ (function (_super) {
        __extends(CannonSimulation, _super);
        function CannonSimulation(init) {
            var _this = _super.call(this) || this;
            _this.optFixedTimeStep = 1 / 60;
            _this.optMaxSubSteps = 10;
            _this.readyHandlers = new plume.Handler();
            _this._ready = false;
            _this._dynamicBodies = []; // only used to track idx
            _this._dynamicBodiesData = [];
            _this._bodies = [];
            _this._visuals = []; // Use for DEBUG
            _this._stats = { avg: 0, _timeTotal: 0, _flush: 0 };
            _this._world = new CANNON.World();
            _this._world.gravity.set(0, -10, 0);
            // this._world.broadphase = new CANNON.NaiveBroadphase();
            // this._world.solver.iterations = 10;
            // this._world.defaultContactMaterial.contactEquationStiffness = 1e7;
            // this._world.defaultContactMaterial.contactEquationRelaxation = 4;
            _this._stats._flush = performance.now();
            _this._stats._timeTotal = 0;
            if (init != null) {
                // init overload
                init(_this._world);
            }
            return _this;
        }
        CannonSimulation.prototype.update = function (dt) {
            _super.prototype.update.call(this, dt);
            if (!this._running)
                return;
            var sEllapsed = dt / 1000;
            var now = performance.now();
            this._world.step(this.optFixedTimeStep, sEllapsed, this.optMaxSubSteps);
            this._syncMesh();
            var after = performance.now();
            var statsEllapsed = (now - this._stats._flush);
            if (statsEllapsed >= 1000) {
                this._stats.avg = Math.round((this._stats._timeTotal / statsEllapsed) * 100) / 100;
                this._stats._flush = now;
                this._stats._timeTotal = 0;
            }
            this._stats._timeTotal += (after - now);
        };
        Object.defineProperty(CannonSimulation.prototype, "stats", {
            get: function () {
                return this._stats;
            },
            enumerable: true,
            configurable: true
        });
        CannonSimulation.prototype.setGravity = function (x, y, z) {
            this._world.gravity.set(x, y, z);
        };
        CannonSimulation.prototype.onStart = function () {
        };
        CannonSimulation.prototype.onStop = function () {
        };
        CannonSimulation.prototype.getWorld = function () {
            return this._world;
        };
        CannonSimulation.prototype.updatePositionAndRotation = function (body, position, rotation) {
            if (position != null) {
                body.position.set(position.x, position.y, position.z);
            }
            if (rotation != null) {
                body.quaternion.set(rotation.x, rotation.y, rotation.z, rotation.w);
            }
            // no scaling in cannonjs!
        };
        CannonSimulation.prototype.addRigidBody = function (body, mesh, dynamic, group, mask) {
            if (mesh != null) {
                // initialize position and rotation body from mesh
                this.updatePositionAndRotation(body, mesh.position, mesh.quaternion);
            }
            if (dynamic) {
                // add body to sync list
                this._dynamicBodies.push(body);
                this._dynamicBodiesData.push({
                    rigidbody: body,
                    mesh: mesh,
                    syncPosition: true,
                    syncRotation: true,
                });
            }
            if (group != null) {
                body.collisionFilterGroup = group;
            }
            if (mask != null) {
                body.collisionFilterMask = mask;
            }
            this._bodies.push(body);
            this._world.addBody(body);
            // logger.debug("dynamic bodies: " + this._dynamicBodies.length);
        };
        CannonSimulation.prototype.removeRigidBody = function (body) {
            var idx = this._dynamicBodies.indexOf(body);
            if (idx >= 0) {
                this._dynamicBodies.splice(idx, 1);
                this._dynamicBodiesData.splice(idx, 1);
            }
            idx = this._bodies.indexOf(body);
            if (idx >= 0) {
                this._bodies.splice(idx, 1);
            }
            else {
                // logger.warn("Body not registered");
            }
            this._world.removeBody(body);
        };
        CannonSimulation.prototype.setDynamicBodySyncProperty = function (body, syncPosition, syncRotation) {
            var idx = this._dynamicBodies.indexOf(body);
            var data = this._dynamicBodiesData[idx];
            data.syncPosition = syncPosition;
            data.syncRotation = syncRotation;
        };
        CannonSimulation.prototype.getRigidBodies = function () {
            return this._bodies;
        };
        CannonSimulation.prototype.removeRigidBodies = function () {
            for (var i = this._bodies.length - 1; i >= 0; i--) {
                var b = this._bodies[i];
                this.removeRigidBody(b);
            }
        };
        CannonSimulation.prototype._syncMesh = function () {
            for (var i = 0; i < this._dynamicBodiesData.length; i++) {
                var data = this._dynamicBodiesData[i];
                var body = data.rigidbody;
                var mesh = data.mesh;
                if (data.syncPosition) {
                    var p = body.position;
                    mesh.position.set(p.x, p.y, p.z);
                }
                if (data.syncRotation) {
                    var q = body.quaternion;
                    mesh.quaternion.set(q.x, q.y, q.z, q.w);
                }
            }
            for (var i = 0; i < this._visuals.length; i++) {
                var visual = this._visuals[i];
                var body = visual.userData;
                visual.position.set(body.position.x, body.position.y, body.position.z);
                if (body.quaternion) {
                    visual.quaternion.set(body.quaternion.x, body.quaternion.y, body.quaternion.z, body.quaternion.w);
                }
            }
        };
        CannonSimulation.prototype.destroy = function () {
            this.removeRigidBodies();
        };
        // FOR DEBUG ONLY!
        CannonSimulation.prototype.addVisual = function (body, material, scene) {
            var mesh = this.createVisual(body, material);
            mesh.userData = body;
            this._visuals.push(mesh);
            scene.add(mesh);
        };
        CannonSimulation.prototype.removeVisual = function (mesh) {
            var idx = this._visuals.indexOf(mesh);
            if (idx >= 0) {
                this._visuals.splice(idx, 1);
                mesh.parent.remove(mesh);
            }
        };
        CannonSimulation.prototype.removeVisuals = function () {
            for (var i = this._visuals.length - 1; i >= 0; i--) {
                var m = this._visuals[i];
                this.removeVisual(m);
            }
        };
        // FOR DEBUG ONLY!
        CannonSimulation.prototype.createVisual = function (body, material) {
            var obj = new THREE.Object3D();
            for (var l = 0; l < body.shapes.length; l++) {
                var shape = body.shapes[l];
                var mesh = void 0;
                switch (shape.type) {
                    case CANNON.Shape.types.SPHERE:
                        var sphereGeometry = new THREE.SphereGeometry(shape.radius, 8, 8);
                        mesh = new THREE.Mesh(sphereGeometry, material);
                        break;
                    case CANNON.Shape.types.PLANE:
                        var planeGeometry = new THREE.PlaneGeometry(10, 10, 4, 4);
                        mesh = new THREE.Object3D();
                        var submesh = new THREE.Object3D();
                        var ground = new THREE.Mesh(planeGeometry, material);
                        ground.scale.set(100, 100, 100);
                        submesh.add(ground);
                        mesh.add(submesh);
                        break;
                    case CANNON.Shape.types.BOX:
                        var boxShape = shape;
                        var boxGeometry = new THREE.BoxGeometry(boxShape.halfExtents.x * 2, boxShape.halfExtents.y * 2, boxShape.halfExtents.z * 2);
                        mesh = new THREE.Mesh(boxGeometry, material);
                        break;
                    case CANNON.Shape.types.CONVEXPOLYHEDRON:
                        var convexGeometry = new THREE.Geometry();
                        var convexShape = shape;
                        // Add vertices
                        for (var i = 0; i < convexShape.vertices.length; i++) {
                            var v = convexShape.vertices[i];
                            convexGeometry.vertices.push(new THREE.Vector3(v.x, v.y, v.z));
                        }
                        for (var i = 0; i < convexShape.faces.length; i++) {
                            var face = convexShape.faces[i];
                            // add triangles
                            var a = face[0];
                            for (var j = 1; j < face.length - 1; j++) {
                                var b = face[j];
                                var c = face[j + 1];
                                convexGeometry.faces.push(new THREE.Face3(a, b, c));
                            }
                        }
                        convexGeometry.computeBoundingSphere();
                        convexGeometry.computeFaceNormals();
                        mesh = new THREE.Mesh(convexGeometry, material);
                        break;
                    case CANNON.Shape.types.TRIMESH:
                        var geometry = new THREE.Geometry();
                        var trimeshShape = shape;
                        var v0 = new CANNON.Vec3();
                        var v1 = new CANNON.Vec3();
                        var v2 = new CANNON.Vec3();
                        for (var i = 0; i < trimeshShape.indices.length / 3; i++) {
                            trimeshShape.getTriangleVertices(i, v0, v1, v2);
                            geometry.vertices.push(new THREE.Vector3(v0.x, v0.y, v0.z), new THREE.Vector3(v1.x, v1.y, v1.z), new THREE.Vector3(v2.x, v2.y, v2.z));
                            var j = geometry.vertices.length - 3;
                            geometry.faces.push(new THREE.Face3(j, j + 1, j + 2));
                        }
                        geometry.computeBoundingSphere();
                        geometry.computeFaceNormals();
                        mesh = new THREE.Mesh(geometry, material);
                        break;
                    default:
                        throw "Visual type not recognized: " + shape.type;
                }
                var o = body.shapeOffsets[l];
                var q = body.shapeOrientations[l];
                mesh.position.set(o.x, o.y, o.z);
                mesh.quaternion.set(q.x, q.y, q.z, q.w);
                obj.add(mesh);
            }
            return obj;
        };
        return CannonSimulation;
    }(plume.SimulationSupport));
    plume.CannonSimulation = CannonSimulation;
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.cannon.js.map