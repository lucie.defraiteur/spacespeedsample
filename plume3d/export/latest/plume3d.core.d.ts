/// <reference path="../../vendor/howler.d.ts" />
/// <reference path="../../vendor/three.d.ts" />
declare module plume {
    class AbstractSound {
        _howl: Howl;
        protected muted: boolean;
        constructor(_howl: Howl);
        protected play0(sprite?: string, onEndCallback?: Function): number;
        pause(id?: number): void;
        stop(id?: number): void;
        mute(id?: number): void;
        unmute(id?: number): void;
        fade(from: number, to: number, duration: number, id?: number, callback?: Function): void;
        loop(loop: boolean, id?: number): void;
        seek(seek?: number, id?: number): Howl | number;
        rate(rate: number, id?: number): Howl;
        setVolume(volume: number, id?: number): void;
        getVolume(): number;
    }
}
declare module plume {
    class AudioManager {
        protected _visibilityManager: VisibilityManager;
        protected static _INSTANCE: AudioManager;
        protected _soundSprites: {
            [name: string]: Sound;
        };
        protected _musics: {
            [name: string]: Music;
        };
        protected _enableMusics: boolean;
        protected _enableSoundsSprites: boolean;
        protected _musicWasEnable: boolean;
        protected _soundWasEnable: boolean;
        musicStateChangeHandlers: Handler<boolean>;
        constructor(_visibilityManager: VisibilityManager);
        loadSounds(sounds: Array<AssetDef>, onComplete: (succes: boolean) => any): void;
        registerSoundsheet(name: string, data: string): void;
        protected getSpriteCount(spriteObj: {
            [name: string]: Array<number>;
        }): number;
        mute(): void;
        unmute(): void;
        volume(volume: number): void;
        protected addMusic(name: string, music: Music): void;
        getMusic(fileName: string): Music;
        muteMusics(): void;
        unmuteMusics(): void;
        volumeMusics(volume: number): void;
        enableMusics: boolean;
        protected addSoundSprite(name: string, sound: Sound): void;
        getSoundSprite(fileName: string): Sound;
        muteSoundSprites(key: string, mute: boolean): void;
        muteSoundsSprites(): void;
        unmuteSoundsSprites(): void;
        volumeSoundsSprites(volume: number): void;
        enableSoundsSprites: boolean;
        protected onVisibilityChange(visible: boolean): void;
        static get(): AudioManager;
    }
}
declare module plume {
    class Music extends AbstractSound {
        private _spriteName;
        constructor(howl: Howl, sprite: {
            [name: string]: Array<number>;
        });
        play(onEndCallback?: Function): number;
    }
}
declare module plume {
    class Sound extends AbstractSound {
        constructor(howl: Howl);
        play(sprite: string, onEndCallback?: Function): number;
    }
}
declare module plume {
    class Game {
        protected static instance: Game;
        mainLoop: MainLoop;
        options: GameOption;
        scaleManager: ScaleManager;
        audioManager: AudioManager;
        deviceManager: DeviceManager;
        inputManager: InputManager;
        timeManager: TimeManager;
        _engine: Engine;
        _gui: Gui;
        _simulation: Simulation;
        _lastTick: number;
        _updatables: Array<Updatable>;
        _renderables: Array<Renderable>;
        protected _bounds: Rectangle;
        private _doingUpdate;
        private _afterUpdate;
        private _trackStats;
        _renderStat: StatEntry;
        _updateStat: StatEntry;
        _deltaStat: StatEntry;
        constructor(width: number, height: number, options?: GameOption);
        start(): void;
        protected newAudioManager(): AudioManager;
        protected newScaleManager(): ScaleManager;
        static get(): Game;
        static options(): GameOption;
        protected start0(): void;
        initializeEngine(engine: Engine): void;
        initializeGui(gui: Gui): void;
        initializeSimulation(simulation: Simulation): void;
        onCreate(): void;
        update(time: number, timeEllapsed: number): void;
        render(time: number, timeEllapsed: number, interpolation: number): void;
        addRenderable(renderable: Renderable): void;
        removeRenderable(renderable: Renderable): void;
        addUpdatable(updatable: Updatable): void;
        removeUpdatable(updatable: Updatable): void;
        getGui(): Gui;
        engine: Engine;
        simulation: Simulation;
        trackStats: boolean;
        readonly width: number;
        readonly height: number;
        readonly bounds: Rectangle;
    }
}
declare module plume {
    interface GameOption {
        input?: InputOption;
        gameLoop?: LoopOptions;
    }
    interface InputOption {
        maxTouchPointer?: number;
    }
    interface LoopOptions {
        forceSetTimeout?: boolean;
        desiredFps?: number;
        smoothDelta?: boolean;
    }
}
declare module plume {
    class RequestAnimationFrame {
        private callback;
        private forceSetTimeOut;
        private _running;
        private _timeoutId;
        private _tick;
        private _setTimeoutSleep;
        private _onSetTimeoutCbBound;
        private _onRafCbBound;
        constructor(callback: Function, forceSetTimeOut: boolean, desiredFps?: number);
        start(): void;
        stop(): void;
        private _onSetTimeoutCb;
        private _onRafCb;
    }
    class MainLoop {
        private updateCb;
        private renderCb;
        private _started;
        private _raf;
        private _lastTime;
        private _deltaHistoryIndex;
        private _deltaHistoryCount;
        private _deltaHistory;
        private _targetDeltaTime;
        private _timestep;
        private _frameDelta;
        constructor(updateCb: (time: number, timeEllapsed: number) => void, renderCb: (time: number, timeEllapsed: number, interpolation: number) => void);
        start(): void;
        private _tick;
        private _ticksmooth;
        stop(): void;
    }
}
declare namespace plume {
}
declare module plume {
    class ScaleManager {
        protected _game: Game;
        protected _pendingResize: boolean;
        enable: boolean;
        resizeHandler: Handler<{}>;
        canvasToResize: Array<HTMLCanvasElement>;
        protected _scalingStategy: ScalingStrategy;
        protected _currentScaling: ScalingStrategyResult;
        protected _windowSizeProvider: () => ({
            width: number;
            height: number;
        });
        constructor(game: Game);
        protected init(): void;
        resize(): void;
        setWindowSizeProvider(provider: () => ({
            width: number;
            height: number;
        })): void;
        setScalingStategy(scalingStategy: ScalingStrategy): void;
        getAbsoluteGameSize(): {
            width: number;
            height: number;
            marginX: number;
            marginY: number;
        };
        getWindowSize(): {
            width: number;
            height: number;
        };
        getCurrentScaling(): ScalingStrategyResult;
        offsetX: number;
        offsetY: number;
        scale: number;
    }
    interface ScalingStrategyResult {
        x: number;
        y: number;
        scale: number;
        windowWidth: number;
        windowHeight: number;
    }
    interface ScalingStrategy {
        getScaling(sourceWidth: number, sourceHeight: number, targetWidth: number, targetHeight: number): ScalingStrategyResult;
    }
    class FitScalingStrategy implements ScalingStrategy {
        getScaling(sourceWidth: number, sourceHeight: number, targetWidth: number, targetHeight: number): ScalingStrategyResult;
    }
    class FillScalingStrategy implements ScalingStrategy {
        getScaling(sourceWidth: number, sourceHeight: number, targetWidth: number, targetHeight: number): ScalingStrategyResult;
    }
}
declare module plume {
    interface AssetDef {
        type: "img" | "spritesheet" | "font" | "soundsheet" | "file";
        name: string;
        file: string;
        atlas?: string;
    }
    interface Updatable {
        update(dt: number): any;
    }
    interface Renderable {
        render(interpolation: number): any;
    }
    type EasingFunction = (t: number) => number;
    interface EffectRenderer {
        render(dt: number): void;
    }
    type StatEntry = {
        start: number;
        sum: number;
        count: number;
    };
    type GltfObject = {
        animations: Array<THREE.AnimationClip>;
        scene: THREE.Scene;
        scenes: Array<THREE.Scene>;
        cameras: Array<THREE.Camera>;
        asset: Object;
        parser: GLTFParser;
    };
    type GLTFParser = {
        json: gltfspec.GlTf;
    };
}
declare module plume.core.shaders {
    let test1_fragment: string;
    let test1_vertex: string;
}
declare module plume {
    class JSONS {
        static merge(source: {}, dest: {}): void;
        static safeJson(data: string, defaultValue?: any): any;
    }
}
declare module plume {
    class I18N {
        static availableLocales: Array<string>;
        static defaultLocale: string;
        static locale: string;
        static translations: any;
        static setAvailableLocales(locales: Array<string>): void;
        static registerTranslation(locale: string, translation: any): void;
        static setDefaultLocale(locale: string): void;
        static setLocale(locale: string): void;
        static isAvailableLocale(locale: string): boolean;
        static get(key: string, params?: {
            [key: string]: any;
        }): string;
        static getPluralForm(n: number, local?: string): number;
        static getPlural(n: number, keyBase: string, params?: {
            [key: string]: any;
        }): string;
    }
}
declare module plume {
    class AbstractInput implements Updatable {
        isDown: boolean;
        isUp: boolean;
        isJustDown: boolean;
        isJustUp: boolean;
        timeDown: number;
        timeUp: number;
        downDuration: number;
        protected justUpdated: boolean;
        private _duration;
        constructor();
        update(): void;
        protected onDown(): void;
        protected onUp(): void;
    }
}
declare module plume {
    class InputManager implements Updatable {
        keyboard: KeyboardHandler;
        mouse: MouseHandler;
        desktop: boolean;
        canvas: HTMLCanvasElement;
        _pointerDirty: boolean;
        private _maxTouchPointer;
        private _disabled;
        mouseEventInterpretors: Array<MouseEventInterpretor>;
        noCursor: boolean;
        constructor(game: Game);
        update(dt: number): void;
        triggerImmediateClick(): void;
        private _triggerMouseHit;
        readonly maxTouchPointer: number;
        enable(): void;
        disable(): void;
        hideCursor(): void;
    }
}
declare module plume {
    class KeyCode {
        static readonly SHIFT: number;
        static readonly CTRL: number;
        static readonly ALT: number;
        static readonly SPACEBAR: number;
        static readonly LEFT: number;
        static readonly PAGEUP: number;
        static readonly PAGEDOWN: number;
        static readonly END: number;
        static readonly HOME: number;
        static readonly UP: number;
        static readonly RIGHT: number;
        static readonly DOWN: number;
        static readonly INSERT: number;
        static readonly DELETE: number;
        static readonly F1: number;
        static readonly F2: number;
        static readonly F3: number;
        static readonly F4: number;
        static readonly F5: number;
        static readonly F6: number;
        static readonly F7: number;
        static readonly F8: number;
        static readonly F9: number;
        static readonly F10: number;
        static readonly F11: number;
        static readonly F12: number;
        static fromChar(c: string): number;
    }
}
declare module plume {
    class KeyInput extends AbstractInput {
        keycode: number;
        event: KeyboardEvent;
        constructor(keycode: number);
        keyDown(e: KeyboardEvent): void;
        keyUp(e: KeyboardEvent): void;
    }
}
declare module plume {
    class KeyboardHandler implements Updatable {
        private _enable;
        keys: Array<KeyInput>;
        keysByCode: HashMap<KeyInput>;
        keyUpHandlers: Handler<KeyInput>;
        keyDownHandlers: Handler<KeyInput>;
        propagatedKeys: Array<number>;
        propagateAllKeys: boolean;
        constructor(game: Game, inputManager: InputManager);
        update(): void;
        enable(): void;
        disable(): void;
        key(code: number | string): KeyInput;
        isDown(code: number | string): boolean;
        isUp(code: number | string): boolean;
        isJustDown(code: number | string): boolean;
        isJustUp(code: number | string): boolean;
        private _keydown;
        private _keyup;
        private _stopEvent;
        private _ensureKey;
        private _getInput;
    }
}
declare module plume {
    class MouseHandler implements Updatable {
        private _enable;
        private _touchEndFired;
        private _element;
        game: Game;
        inputManager: InputManager;
        pointer: PointerInput;
        pointers: Array<PointerInput>;
        mouseWheelHandlers: Handler<{
            delta: number;
            event: WheelEvent;
        }>;
        propagateAllEvents: boolean;
        wheelDelta: number;
        private _lastWheel;
        constructor(game: Game, inputManager: InputManager);
        update(): void;
        enableOnElement(element: HTMLElement | Window): void;
        enable(): void;
        disable(): void;
        readonly isDown: boolean;
        readonly isUp: boolean;
        readonly isJustDown: boolean;
        readonly isJustUp: boolean;
        readonly downDuration: number;
        readonly downPosition: Point;
        readonly position: Point;
        readonly upPosition: Point;
        private _mousedown;
        private _mousemove;
        private _mouseup;
        private _mouseout;
        private _contextmenu;
        private _mousewheel;
        private _getPointerOrFirstFree;
        private _touchstart;
        private _touchmove;
        private _touchend;
        private _stopEvent;
        private _debugTouch;
        private _touchcancel;
        private _touchleave;
    }
    interface MouseEventInterpretor {
        onUpdate(): void;
        onImmediate(): void;
    }
}
declare module plume {
    class PointerInput extends AbstractInput {
        scaleManager: ScaleManager;
        inputManager: InputManager;
        identifier: number;
        downPosition: Point;
        position: Point;
        upPosition: Point;
        diffPosition: Point;
        mouseActive: boolean;
        interactiveElement: any;
        _previousHit: any;
        _lastDownTarget: any;
        private _touchInProgress;
        private _touchEndTime;
        private _touchEndDebounce;
        private _prevMousePosition;
        private _prevMouseBState;
        private _lastMouseEvent;
        private _lastX;
        private _lastY;
        constructor(game: Game, inputManager: InputManager);
        update(): void;
        isActive(): boolean;
        mouseDown(e: MouseEvent): void;
        mouseMove(e: MouseEvent): void;
        mouseUp(e: MouseEvent): void;
        touchDown(e: Touch): void;
        touchMove(e: Touch): void;
        touchUp(e: Touch): void;
        private _getXValue;
        private _getYValue;
        private _acquirePointer;
        private _releasePointer;
        mouseChanged(): boolean;
        wasSwipe(): boolean;
        getLastMouseEvent(): MouseEvent;
    }
}
declare module plume {
    class OrbitControls {
        object: THREE.PerspectiveCamera;
        domElement?: HTMLElement;
        private handleMouseEvent;
        enabled: boolean;
        target: THREE.Vector3;
        minDistance: number;
        maxDistance: number;
        minZoom: number;
        maxZoom: number;
        minPolarAngle: number;
        maxPolarAngle: number;
        minAzimuthAngle: number;
        maxAzimuthAngle: number;
        enableDamping: boolean;
        dampingFactor: number;
        enableZoom: boolean;
        zoomSpeed: number;
        enableRotate: boolean;
        rotateSpeed: number;
        enablePan: boolean;
        panSpeed: number;
        screenSpacePanning: boolean;
        keyPanSpeed: number;
        autoRotate: boolean;
        autoRotateSpeed: number;
        enableKeys: boolean;
        keys: {
            LEFT: number;
            UP: number;
            RIGHT: number;
            BOTTOM: number;
        };
        mouseButtons: {
            ORBIT: THREE.MOUSE;
            ZOOM: THREE.MOUSE;
            PAN: THREE.MOUSE;
        };
        target0: THREE.Vector3;
        position0: THREE.Vector3;
        zoom0: number;
        constructor(object: THREE.PerspectiveCamera, domElement?: HTMLElement, handleMouseEvent?: boolean);
        getPolarAngle(): number;
        getAzimuthalAngle(): number;
        saveState(): void;
        reset(): void;
        private _DispatchEvent;
        update(): boolean;
        dispose(): void;
        private _onContextMenuBound;
        private _onMouseDownBound;
        private _onMouseWheelBound;
        private _onMouseMoveBound;
        private _onMouseUpBound;
        private _onTouchStartBound;
        private _onTouchMoveBound;
        private _onTouchEndBound;
        private _changeEvent;
        private _startEvent;
        private _endEvent;
        private _state;
        private _EPS;
        private _spherical;
        private _sphericalDelta;
        private _scale;
        private _panOffset;
        private _zoomChanged;
        private _rotateStart;
        private _rotateEnd;
        private _rotateDelta;
        private _panStart;
        private _panEnd;
        private _panDelta;
        private _dollyStart;
        private _dollyEnd;
        private _dollyDelta;
        private _getAutoRotationAngle;
        private _getZoomScale;
        private _rotateLeft;
        private _rotateUp;
        private _panLeft;
        private _panUp;
        private _pan0;
        dollyIn(dollyScale: number, update?: boolean): void;
        dollyOut(dollyScale: number, update?: boolean): void;
        startRotate(x: number, y: number): void;
        startDolly(x: number, y: number): void;
        startPan(x: number, y: number): void;
        rotate(x: number, y: number): void;
        dolly(x: number, y: number): void;
        pan(x: number, y: number): void;
        private _handleMouseDownRotate;
        private _handleMouseDownDolly;
        private _handleMouseDownPan;
        private _handleMouseMoveRotate;
        private _handleMouseMoveDolly;
        private _handleMouseMovePan;
        private _handleMouseUp;
        private _handleMouseWheel;
        private _onMouseDown;
        private _onMouseMove;
        private _onMouseUp;
        private _onMouseWheel;
        private _onContextMenu;
        private _onTouchStart;
        private _onTouchMove;
        private _onTouchEnd;
        private _handleTouchStartRotate;
        private _handleTouchStartDollyPan;
        private _handleTouchMoveRotate;
        private _handleTouchMoveDollyPan;
        private _handleTouchEnd;
    }
}
declare module plume {
    class Mathf {
        static readonly PI2: number;
        static readonly DEG2RAD: number;
        static readonly RAD2DEG: number;
        static clamp(value: number, min: number, max: number): number;
        static clamp01(value: number): number;
        static mod(a: number, n: number): number;
        static lerp(v0: number, v1: number, t: number): number;
        static inverseLerp(a: number, b: number, value: number): number;
        static sign(v: number): 1 | -1;
        static repeat(t: number, length: number): number;
        static pingPong(t: number, length: number): number;
        static map(num: number, inMin: number, inMax: number, outMin: number, outMax: number): number;
        static smoothDamp(current: number, target: number, currentVelocity: number, smoothTime: number, maxSpeed: number, deltaTime: number): {
            smoothValue: number;
            newVelocity: number;
        };
        static smoothDampAngle(current: number, target: number, currentVelocity: number, smoothTime: number, maxSpeed: number, deltaTime: number): {
            smoothValue: number;
            newVelocity: number;
        };
        static deltaAngle(current: number, target: number): number;
    }
}
declare module plume {
    class Point {
        private _x;
        private _y;
        constructor(x?: number, y?: number);
        copy(): Point;
        equals(point: Point): boolean;
        x: number;
        y: number;
        toString(): string;
        static distance(p1: Point, p2: Point): number;
    }
}
declare module plume {
    class Rectangle {
        private _left;
        private _top;
        private _width;
        private _height;
        constructor(left: number, top: number, width: number, height: number);
        getCenter(): Point;
        containsPoint(point: Point): boolean;
        containsPoint2(px: number, py: number): boolean;
        overlap(rectangle: Rectangle): boolean;
        toString(): string;
        copy(): Rectangle;
        x: number;
        left: number;
        right: number;
        y: number;
        top: number;
        bottom: number;
        width: number;
        height: number;
    }
}
declare module plume {
    class HttpRequest {
        private _xhrObject;
        get(url: string, connectionReceivedHandler: (response: string) => any, asyncSendFailResult: (error: string) => any): void;
        private _initHTTPRequest;
        post(url: string, data: string, connectionReceivedHandler: (data: string) => any, asyncSendFailResult: (error: string) => any): void;
    }
}
declare module plume {
    type SimulationStats = {
        avg: number;
    };
    interface Simulation {
        running: boolean;
        stats: SimulationStats;
        update(dt: number): void;
    }
}
declare module plume {
    class SimulationSupport {
        protected _running: boolean;
        update(dt: number): void;
        setGravity(x: number, y: number, z: number): void;
        protected onStart(): void;
        protected onStop(): void;
        running: boolean;
    }
    type BodyParameter = {
        mass: number;
    };
}
declare module plume {
    class RecyclePool<T> {
        private _factory;
        private _count;
        private _data;
        constructor(factory: () => T, capacity: number);
        readonly length: number;
        readonly data: T[];
        reset(): void;
        resize(size: number): void;
        add(): T;
        remove(idx: number): T;
        remove(element: T): T;
    }
}
declare module plume {
    class EphemeralPool {
        vectors3: RecyclePool<THREE.Vector3>;
        quaternions: RecyclePool<THREE.Quaternion>;
        constructor();
        clean(): void;
        v3(x?: number, y?: number, z?: number): THREE.Vector3;
        q(x?: number, y?: number, z?: number, w?: number): THREE.Quaternion;
    }
    let ephemeralPool: EphemeralPool;
}
declare module plume {
    class Renderer {
        static _applyStyle(canvas: HTMLCanvasElement): void;
    }
}
declare module plume {
    interface Gui {
        update(dt: number): void;
        render(dt: number, interpolation: number): void;
    }
}
declare module plume {
    type EngineOptions = {
        canvasId?: string;
        antialias?: boolean;
        alpha?: boolean;
    };
    class Engine {
        private static _INSTANCE;
        canvas: HTMLCanvasElement;
        game: Game;
        private _options;
        private _scene;
        private _camera;
        private _renderer;
        private _effectComposer;
        private _loader;
        private _prerender;
        private _postrender;
        private _textureCache;
        constructor(options?: EngineOptions);
        static get(): Engine;
        private _initOption;
        private _initCanvas;
        render(dt: number, interpolation: number): void;
        resize(width: number, height: number): void;
        addRenderable(renderable: (dt: number) => void, prerender: boolean, ctx?: any): ContextualCallback;
        removeRenderable(renderable: (dt: number) => void): boolean;
        readonly renderer: THREE.WebGLRenderer;
        scene: THREE.Scene;
        camera: THREE.PerspectiveCamera | THREE.OrthographicCamera;
        effectComposer: EffectRenderer;
        loader: Loader3d;
        readonly textureCache: TextureCache;
    }
}
declare module plume {
    class AnimUtils {
        static clone(source: THREE.Object3D): THREE.Object3D;
    }
}
declare module plume {
    class SpriteAnimation implements Updatable {
        private _texture;
        private _xframes;
        private _yframes;
        private _curIndex;
        private _playing;
        private _startTime;
        private _lastIndex;
        private _repeats;
        private _totalFrames;
        private _tilesCount;
        private _onEnd;
        fps: number;
        animationSpeed: number;
        destroyOnEnd: boolean;
        constructor(_texture: THREE.Texture, _xframes: number, _yframes: number);
        start(): void;
        stop(): void;
        destroy(): void;
        update(dt: number): void;
        onEnd(cb: () => void): void;
        private _SetActiveFrame;
        private _SetFrameIndex;
        private _OnAnimationEnded;
        repeat(repeats?: number): void;
        isPlaying(): boolean;
        static onPlane(texture: THREE.Texture, xframes: number, yframes: number): {
            mesh: THREE.Mesh;
            animator: SpriteAnimation;
            texture: THREE.Texture;
        };
        static onSprite(texture: THREE.Texture, xframes: number, yframes: number): {
            mesh: THREE.Sprite;
            animator: SpriteAnimation;
            texture: THREE.Texture;
        };
    }
}
declare module plume {
    class HeightMap {
        private _width;
        private _depth;
        private _options;
        optFilterR: number;
        optFilterG: number;
        optFilterB: number;
        private _geometry;
        private _loaded;
        private _loadedCb;
        constructor(_width: number, _depth: number, _options: {
            minHeight?: number;
            maxHeight?: number;
        });
        fromImage(url: string): void;
        fromPoints(w: number, z: number, heights: Array<number>): void;
        private _readImg;
        private _createGeometry;
        private _notifyLodead;
        createMesh(material: THREE.MeshMaterialType): THREE.Mesh;
        getGeometry(): THREE.PlaneBufferGeometry;
        isLoaded(): boolean;
        onLoaded(cb: () => void): void;
    }
}
declare namespace gltfspec {
    type GlTfId = number;
    /**
     * Indices of those attributes that deviate from their initialization value.
     */
    interface AccessorSparseIndices {
        /**
         * The index of the bufferView with sparse indices. Referenced bufferView can't have ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER target.
         */
        "bufferView": GlTfId;
        /**
         * The offset relative to the start of the bufferView in bytes. Must be aligned.
         */
        "byteOffset"?: number;
        /**
         * The indices data type.
         */
        "componentType": 5121 | 5123 | 5125 | number;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Array of size `accessor.sparse.count` times number of components storing the displaced accessor attributes pointed by `accessor.sparse.indices`.
     */
    interface AccessorSparseValues {
        /**
         * The index of the bufferView with sparse values. Referenced bufferView can't have ARRAY_BUFFER or ELEMENT_ARRAY_BUFFER target.
         */
        "bufferView": GlTfId;
        /**
         * The offset relative to the start of the bufferView in bytes. Must be aligned.
         */
        "byteOffset"?: number;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Sparse storage of attributes that deviate from their initialization value.
     */
    interface AccessorSparse {
        /**
         * Number of entries stored in the sparse array.
         */
        "count": number;
        /**
         * Index array of size `count` that points to those accessor attributes that deviate from their initialization value. Indices must strictly increase.
         */
        "indices": AccessorSparseIndices;
        /**
         * Array of size `count` times number of components, storing the displaced accessor attributes pointed by `indices`. Substituted values must have the same `componentType` and number of components as the base accessor.
         */
        "values": AccessorSparseValues;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A typed view into a bufferView.  A bufferView contains raw binary data.  An accessor provides a typed view into a bufferView or a subset of a bufferView similar to how WebGL's `vertexAttribPointer()` defines an attribute in a buffer.
     */
    interface Accessor {
        /**
         * The index of the bufferView.
         */
        "bufferView"?: GlTfId;
        /**
         * The offset relative to the start of the bufferView in bytes.
         */
        "byteOffset"?: number;
        /**
         * The datatype of components in the attribute.
         */
        "componentType": 5120 | 5121 | 5122 | 5123 | 5125 | 5126 | number;
        /**
         * Specifies whether integer data values should be normalized.
         */
        "normalized"?: boolean;
        /**
         * The number of attributes referenced by this accessor.
         */
        "count": number;
        /**
         * Specifies if the attribute is a scalar, vector, or matrix.
         */
        "type": "SCALAR" | "VEC2" | "VEC3" | "VEC4" | "MAT2" | "MAT3" | "MAT4" | string;
        /**
         * Maximum value of each component in this attribute.
         */
        "max"?: number[];
        /**
         * Minimum value of each component in this attribute.
         */
        "min"?: number[];
        /**
         * Sparse storage of attributes that deviate from their initialization value.
         */
        "sparse"?: AccessorSparse;
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * The index of the node and TRS property that an animation channel targets.
     */
    interface AnimationChannelTarget {
        /**
         * The index of the node to target.
         */
        "node"?: GlTfId;
        /**
         * The name of the node's TRS property to modify, or the "weights" of the Morph Targets it instantiates. For the "translation" property, the values that are provided by the sampler are the translation along the x, y, and z axes. For the "rotation" property, the values are a quaternion in the order (x, y, z, w), where w is the scalar. For the "scale" property, the values are the scaling factors along the x, y, and z axes.
         */
        "path": "translation" | "rotation" | "scale" | "weights" | string;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Targets an animation's sampler at a node's property.
     */
    interface AnimationChannel {
        /**
         * The index of a sampler in this animation used to compute the value for the target.
         */
        "sampler": GlTfId;
        /**
         * The index of the node and TRS property to target.
         */
        "target": AnimationChannelTarget;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Combines input and output accessors with an interpolation algorithm to define a keyframe graph (but not its target).
     */
    interface AnimationSampler {
        /**
         * The index of an accessor containing keyframe input values, e.g., time.
         */
        "input": GlTfId;
        /**
         * Interpolation algorithm.
         */
        "interpolation"?: "LINEAR" | "STEP" | "CUBICSPLINE" | string;
        /**
         * The index of an accessor, containing keyframe output values.
         */
        "output": GlTfId;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A keyframe animation.
     */
    interface Animation {
        /**
         * An array of channels, each of which targets an animation's sampler at a node's property. Different channels of the same animation can't have equal targets.
         */
        "channels": AnimationChannel[];
        /**
         * An array of samplers that combines input and output accessors with an interpolation algorithm to define a keyframe graph (but not its target).
         */
        "samplers": AnimationSampler[];
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Metadata about the glTF asset.
     */
    interface Asset {
        /**
         * A copyright message suitable for display to credit the content creator.
         */
        "copyright"?: string;
        /**
         * Tool that generated this glTF model.  Useful for debugging.
         */
        "generator"?: string;
        /**
         * The glTF version that this asset targets.
         */
        "version": string;
        /**
         * The minimum glTF version that this asset targets.
         */
        "minVersion"?: string;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A buffer points to binary geometry, animation, or skins.
     */
    interface Buffer {
        /**
         * The uri of the buffer.
         */
        "uri"?: string;
        /**
         * The length of the buffer in bytes.
         */
        "byteLength": number;
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A view into a buffer generally representing a subset of the buffer.
     */
    interface BufferView {
        /**
         * The index of the buffer.
         */
        "buffer": GlTfId;
        /**
         * The offset into the buffer in bytes.
         */
        "byteOffset"?: number;
        /**
         * The length of the bufferView in bytes.
         */
        "byteLength": number;
        /**
         * The stride, in bytes.
         */
        "byteStride"?: number;
        /**
         * The target that the GPU buffer should be bound to.
         */
        "target"?: 34962 | 34963 | number;
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * An orthographic camera containing properties to create an orthographic projection matrix.
     */
    interface CameraOrthographic {
        /**
         * The floating-point horizontal magnification of the view. Must not be zero.
         */
        "xmag": number;
        /**
         * The floating-point vertical magnification of the view. Must not be zero.
         */
        "ymag": number;
        /**
         * The floating-point distance to the far clipping plane. `zfar` must be greater than `znear`.
         */
        "zfar": number;
        /**
         * The floating-point distance to the near clipping plane.
         */
        "znear": number;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A perspective camera containing properties to create a perspective projection matrix.
     */
    interface CameraPerspective {
        /**
         * The floating-point aspect ratio of the field of view.
         */
        "aspectRatio"?: number;
        /**
         * The floating-point vertical field of view in radians.
         */
        "yfov": number;
        /**
         * The floating-point distance to the far clipping plane.
         */
        "zfar"?: number;
        /**
         * The floating-point distance to the near clipping plane.
         */
        "znear": number;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A camera's projection.  A node can reference a camera to apply a transform to place the camera in the scene.
     */
    interface Camera {
        /**
         * An orthographic camera containing properties to create an orthographic projection matrix.
         */
        "orthographic"?: CameraOrthographic;
        /**
         * A perspective camera containing properties to create a perspective projection matrix.
         */
        "perspective"?: CameraPerspective;
        /**
         * Specifies if the camera uses a perspective or orthographic projection.
         */
        "type": "perspective" | "orthographic" | string;
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Image data used to create a texture. Image can be referenced by URI or `bufferView` index. `mimeType` is required in the latter case.
     */
    interface Image {
        /**
         * The uri of the image.
         */
        "uri"?: string;
        /**
         * The image's MIME type.
         */
        "mimeType"?: "image/jpeg" | "image/png" | string;
        /**
         * The index of the bufferView that contains the image. Use this instead of the image's uri property.
         */
        "bufferView"?: GlTfId;
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Reference to a texture.
     */
    interface TextureInfo {
        /**
         * The index of the texture.
         */
        "index": GlTfId;
        /**
         * The set index of texture's TEXCOORD attribute used for texture coordinate mapping.
         */
        "texCoord"?: number;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A set of parameter values that are used to define the metallic-roughness material model from Physically-Based Rendering (PBR) methodology.
     */
    interface MaterialPbrMetallicRoughness {
        /**
         * The material's base color factor.
         */
        "baseColorFactor"?: number[];
        /**
         * The base color texture.
         */
        "baseColorTexture"?: TextureInfo;
        /**
         * The metalness of the material.
         */
        "metallicFactor"?: number;
        /**
         * The roughness of the material.
         */
        "roughnessFactor"?: number;
        /**
         * The metallic-roughness texture.
         */
        "metallicRoughnessTexture"?: TextureInfo;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    interface MaterialNormalTextureInfo {
        "index": any;
        "texCoord"?: any;
        /**
         * The scalar multiplier applied to each normal vector of the normal texture.
         */
        "scale"?: number;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    interface MaterialOcclusionTextureInfo {
        "index": any;
        "texCoord"?: any;
        /**
         * A scalar multiplier controlling the amount of occlusion applied.
         */
        "strength"?: number;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * The material appearance of a primitive.
     */
    interface Material {
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        /**
         * A set of parameter values that are used to define the metallic-roughness material model from Physically-Based Rendering (PBR) methodology. When not specified, all the default values of `pbrMetallicRoughness` apply.
         */
        "pbrMetallicRoughness"?: MaterialPbrMetallicRoughness;
        /**
         * The normal map texture.
         */
        "normalTexture"?: MaterialNormalTextureInfo;
        /**
         * The occlusion map texture.
         */
        "occlusionTexture"?: MaterialOcclusionTextureInfo;
        /**
         * The emissive map texture.
         */
        "emissiveTexture"?: TextureInfo;
        /**
         * The emissive color of the material.
         */
        "emissiveFactor"?: number[];
        /**
         * The alpha rendering mode of the material.
         */
        "alphaMode"?: "OPAQUE" | "MASK" | "BLEND" | string;
        /**
         * The alpha cutoff value of the material.
         */
        "alphaCutoff"?: number;
        /**
         * Specifies whether the material is double sided.
         */
        "doubleSided"?: boolean;
        [k: string]: any;
    }
    /**
     * Geometry to be rendered with the given material.
     */
    interface MeshPrimitive {
        /**
         * A dictionary object, where each key corresponds to mesh attribute semantic and each value is the index of the accessor containing attribute's data.
         */
        "attributes": {
            [k: string]: GlTfId;
        };
        /**
         * The index of the accessor that contains the indices.
         */
        "indices"?: GlTfId;
        /**
         * The index of the material to apply to this primitive when rendering.
         */
        "material"?: GlTfId;
        /**
         * The type of primitives to render.
         */
        "mode"?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | number;
        /**
         * An array of Morph Targets, each  Morph Target is a dictionary mapping attributes (only `POSITION`, `NORMAL`, and `TANGENT` supported) to their deviations in the Morph Target.
         */
        "targets"?: {
            [k: string]: GlTfId;
        }[];
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A set of primitives to be rendered.  A node can contain one mesh.  A node's transform places the mesh in the scene.
     */
    interface Mesh {
        /**
         * An array of primitives, each defining geometry to be rendered with a material.
         */
        "primitives": MeshPrimitive[];
        /**
         * Array of weights to be applied to the Morph Targets.
         */
        "weights"?: number[];
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A node in the node hierarchy.  When the node contains `skin`, all `mesh.primitives` must contain `JOINTS_0` and `WEIGHTS_0` attributes.  A node can have either a `matrix` or any combination of `translation`/`rotation`/`scale` (TRS) properties. TRS properties are converted to matrices and postmultiplied in the `T * R * S` order to compose the transformation matrix; first the scale is applied to the vertices, then the rotation, and then the translation. If none are provided, the transform is the identity. When a node is targeted for animation (referenced by an animation.channel.target), only TRS properties may be present; `matrix` will not be present.
     */
    interface Node {
        /**
         * The index of the camera referenced by this node.
         */
        "camera"?: GlTfId;
        /**
         * The indices of this node's children.
         */
        "children"?: GlTfId[];
        /**
         * The index of the skin referenced by this node.
         */
        "skin"?: GlTfId;
        /**
         * A floating-point 4x4 transformation matrix stored in column-major order.
         */
        "matrix"?: number[];
        /**
         * The index of the mesh in this node.
         */
        "mesh"?: GlTfId;
        /**
         * The node's unit quaternion rotation in the order (x, y, z, w), where w is the scalar.
         */
        "rotation"?: number[];
        /**
         * The node's non-uniform scale, given as the scaling factors along the x, y, and z axes.
         */
        "scale"?: number[];
        /**
         * The node's translation along the x, y, and z axes.
         */
        "translation"?: number[];
        /**
         * The weights of the instantiated Morph Target. Number of elements must match number of Morph Targets of used mesh.
         */
        "weights"?: number[];
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Texture sampler properties for filtering and wrapping modes.
     */
    interface Sampler {
        /**
         * Magnification filter.
         */
        "magFilter"?: 9728 | 9729 | number;
        /**
         * Minification filter.
         */
        "minFilter"?: 9728 | 9729 | 9984 | 9985 | 9986 | 9987 | number;
        /**
         * s wrapping mode.
         */
        "wrapS"?: 33071 | 33648 | 10497 | number;
        /**
         * t wrapping mode.
         */
        "wrapT"?: 33071 | 33648 | 10497 | number;
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * The root nodes of a scene.
     */
    interface Scene {
        /**
         * The indices of each root node.
         */
        "nodes"?: GlTfId[];
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * Joints and matrices defining a skin.
     */
    interface Skin {
        /**
         * The index of the accessor containing the floating-point 4x4 inverse-bind matrices.  The default is that each matrix is a 4x4 identity matrix, which implies that inverse-bind matrices were pre-applied.
         */
        "inverseBindMatrices"?: GlTfId;
        /**
         * The index of the node used as a skeleton root. When undefined, joints transforms resolve to scene root.
         */
        "skeleton"?: GlTfId;
        /**
         * Indices of skeleton nodes, used as joints in this skin.
         */
        "joints": GlTfId[];
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * A texture and its sampler.
     */
    interface Texture {
        /**
         * The index of the sampler used by this texture. When undefined, a sampler with repeat wrapping and auto filtering should be used.
         */
        "sampler"?: GlTfId;
        /**
         * The index of the image used by this texture.
         */
        "source"?: GlTfId;
        "name"?: any;
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
    /**
     * The root object for a glTF asset.
     */
    interface GlTf {
        /**
         * Names of glTF extensions used somewhere in this asset.
         */
        "extensionsUsed"?: string[];
        /**
         * Names of glTF extensions required to properly load this asset.
         */
        "extensionsRequired"?: string[];
        /**
         * An array of accessors.
         */
        "accessors"?: Accessor[];
        /**
         * An array of keyframe animations.
         */
        "animations"?: Animation[];
        /**
         * Metadata about the glTF asset.
         */
        "asset": Asset;
        /**
         * An array of buffers.
         */
        "buffers"?: Buffer[];
        /**
         * An array of bufferViews.
         */
        "bufferViews"?: BufferView[];
        /**
         * An array of cameras.
         */
        "cameras"?: Camera[];
        /**
         * An array of images.
         */
        "images"?: Image[];
        /**
         * An array of materials.
         */
        "materials"?: Material[];
        /**
         * An array of meshes.
         */
        "meshes"?: Mesh[];
        /**
         * An array of nodes.
         */
        "nodes"?: Node[];
        /**
         * An array of samplers.
         */
        "samplers"?: Sampler[];
        /**
         * The index of the default scene.
         */
        "scene"?: GlTfId;
        /**
         * An array of scenes.
         */
        "scenes"?: Scene[];
        /**
         * An array of skins.
         */
        "skins"?: Skin[];
        /**
         * An array of textures.
         */
        "textures"?: Texture[];
        "extensions"?: any;
        "extras"?: any;
        [k: string]: any;
    }
}
declare module plume {
    type Asset = {
        t: string;
        name: string;
        data: string;
    };
    class InlineLoadingManager {
        private static _INST;
        assetByName: plume.HashMap<Asset>;
        constructor();
        static get(): InlineLoadingManager;
        initialize(loader: Loader3d): void;
        private _getLocalUrl;
        private _onLoaded;
        private _arrayBufferToBase64;
    }
}
declare module plume {
    class Loader3d {
        static dracoLib: string;
        private _manager;
        private _textureLoaderManager;
        private _total;
        private _loaded;
        private _ressourceByName;
        private _textureByName;
        private _loadedHandler;
        constructor(onloaded?: () => void);
        private _fbxloader;
        private _objloader;
        private _gltfloader;
        setAvailableCompressionAndQuanlity(textures: Array<TextureCompressionAndQuality>): ProgressiveTextureLoaderManager;
        reset(): void;
        onLoaded(cb: () => void): void;
        isLoaded(): boolean;
        getModel(name: string): any;
        getTexture(name: string): THREE.Texture;
        getCubeTexture(name: string): THREE.CubeTexture;
        loadTexture(url: string, name: string): THREE.Texture;
        loadCubeTexture(url: Array<string>, name: string): THREE.Texture;
        loadObj(objUrl: string, mtlUrl: string, name: string): void;
        loadFbx(objUrl: string, name: string): void;
        loadGltf(objUrl: string, name: string, configHandler?: (gltfloader: any) => void): void;
        addModel(mesh: THREE.Mesh, name: string): void;
        readonly manager: THREE.LoadingManager;
        readonly progressiveLoader: TextureLoaderManager;
        private _checkLoadingComplete;
    }
}
declare module plume {
    class KTXLoader {
        manager?: THREE.LoadingManager;
        cache?: TextureCache;
        crossOrigin: string;
        constructor(manager?: THREE.LoadingManager, cache?: TextureCache);
        load(url: string, onLoad?: (texture: THREE.CompressedTexture) => void, onError?: (event: ErrorEvent) => void): THREE.CompressedTexture;
        swap(url: string, texture: THREE.CompressedTexture, onLoad?: (texture: THREE.CompressedTexture) => void, onError?: (event: ErrorEvent) => void): THREE.CompressedTexture;
        private _load0;
        private _parse;
    }
}
declare module plume {
    class TextureLoaderManager {
        manager?: THREE.LoadingManager;
        cache?: TextureCache;
        constructor(manager?: THREE.LoadingManager, cache?: TextureCache);
        load(url: string, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture;
    }
    class TextureCache {
        private _textureByUrl;
        constructor();
        add(url: string, texture: THREE.Texture): void;
        get(url: string): THREE.Texture;
    }
}
declare module plume {
    const enum TextureQuality {
        NORMAL = 1,
        LOW = 2
    }
    const enum TextureCompression {
        NORMAL = 1,
        ASTC = 2,
        PVR = 4,
        S3TC = 8,
        ETC = 16
    }
    type TextureCompressionAndQuality = {
        path: string;
        quality: number;
        format: number;
    };
    class ProgressiveTextureLoaderManager extends TextureLoaderManager {
        static readonly GLEXTENSION_BY_TEXTURECOMPRESSION: {
            2: string;
            4: string;
            8: string;
            16: string;
        };
        static readonly IMAGEEXT_BY_TEXTURECOMPRESSION: {
            2: string;
            4: string;
            8: string;
            16: string;
        };
        static readonly IMAGEFORMAT_BY_TEXTURECOMPRESSION: {
            2: string;
            4: string;
            8: string;
            16: string;
        };
        private _phase;
        private _infoByPath;
        private _imageDataByPath;
        private _nextPhaseUrls;
        private _compressionFormat;
        progressiveLoadingEnabled: boolean;
        initialize(texturesInfos: Array<TextureCompressionAndQuality>, loader: Loader3d): void;
        private _isSupported;
        load(url: string, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture;
        getCompressionFormat(): TextureCompression;
        getCompressionFormatAsStr(): string;
        private _getLowRes;
        private _getCompressionExt;
        private _load;
        private _onFirstLoadComplete;
        private _upgradeTexture;
    }
}
declare module plume {
    class TextureLoader {
        manager?: THREE.LoadingManager;
        cache?: TextureCache;
        crossOrigin: string;
        constructor(manager?: THREE.LoadingManager, cache?: TextureCache);
        load(url: string, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture;
        swap(url: string, texture: THREE.Texture, onLoad?: (texture: THREE.Texture) => void, onError?: (event: ErrorEvent) => void): THREE.Texture;
        private _load0;
    }
}
declare module plume {
    class DynamicTexture {
        private canvas;
        private _texture;
        private _loaded;
        private _context2d;
        private _width;
        private _height;
        heightRatio: number;
        constructor(canvas: HTMLCanvasElement);
        static fromSize(width: number, height: number): DynamicTexture;
        _debug(): void;
        update(): void;
        readonly texture: THREE.Texture;
        readonly context2d: CanvasRenderingContext2D;
        clear(): void;
        drawText(text: string, fontName: string, fontWeight: string, fontSize: number, color: string, bgColor?: string, x?: number, y?: number, update?: boolean): void;
    }
}
declare module plume {
    class ExplodeModifier {
        modify(geometry: THREE.Geometry): void;
    }
}
declare module plume {
    class FpsHelper implements Updatable {
        private _statsFlush;
        private _statsDiv;
        private _engine;
        private _game;
        optShowFps: boolean;
        optShowUpdateTime: boolean;
        optShowRenderTime: boolean;
        optShowCalls: boolean;
        optShowTriangles: boolean;
        optShowGeometries: boolean;
        optShowTextures: boolean;
        optShowPhysics: boolean;
        optPrintRenderableOnClick: boolean;
        private _customStats;
        constructor(showDiv?: boolean);
        showAll(): void;
        update(dt: number): void;
        destroy(): void;
        protected onFlushStats(): string;
        addCustom(name: string): StatEntry;
        getCustom(name: string): StatEntry;
        private _OnClick;
        readonly div: HTMLDivElement;
        private _CollectMeshData;
    }
}
declare module plume {
    class SceneUtils {
        private static _tmp_mat4;
        static detach(child: THREE.Object3D, parent?: THREE.Object3D, scene?: THREE.Scene): void;
        static attach(child: THREE.Object3D, parent: THREE.Object3D, scene?: THREE.Scene): void;
        static move(object: THREE.Object3D, newParent: THREE.Object3D): void;
    }
}
declare module plume {
    class AbstractLoadingResource {
        protected _success: boolean;
        protected _failed: boolean;
        protected _onSuccess: () => any;
        protected _onFailed: () => any;
        protected onSuccess0(): void;
        protected onFailed0(): void;
        onSuccess(cb: () => any): void;
        onFailed(cb: () => any): void;
        isSuccess(): boolean;
        isFailed(): boolean;
        isTerminated(): boolean;
    }
}
declare module plume {
    class FileResource extends AbstractLoadingResource {
        protected file: string;
        data: string;
        constructor(file: string);
        getAssetDef(): AssetDef;
    }
}
declare module plume {
    class HashMap<V> {
        protected _data: {
            [key: string]: V;
        };
        protected _size: number;
        isEmpty(): boolean;
        size(): number;
        clear(): void;
        containsKey(key: string): boolean;
        get(key: string): V;
        put(key: string, value: V): V;
        remove(key: string): V;
        removeValues(value: V): void;
        keys(): Array<string>;
        values(): Array<V>;
        forEach(callback: (key: string, value: V) => any, thisArg: any): void;
        computeIfAbsent(key: string, mappingFunction: (key: string) => V): V;
    }
}
declare module plume {
    type Rec = {
        x: number;
        y: number;
        width: number;
        height: number;
    };
    enum TextureType {
        IMG = 0,
        CANVAS = 1
    }
    class Texture {
        name: string;
        x: number;
        y: number;
        width: number;
        height: number;
        static textures: HashMap<Texture>;
        private _impl;
        private _type;
        private _loaded;
        private _loadedHadler;
        private _trim;
        constructor(name: string, x: number, y: number, width: number, height: number);
        setTrim(dx: number, dy: number, sourceW: number, sourceH: number): void;
        getTrim(): Rec;
        getFrameTexture(x: number, y: number, width: number, height: number, nameId?: string): Texture;
        trimmed: boolean;
        static addTexture(texture: Texture): void;
        static getTexture(name: string): Texture;
        static containsTexture(name: string): Texture;
        static fromFrame(frameId: string): Texture;
        static fromImage(url: string): Texture;
        static fromFrames(frameIds: Array<string>): Array<Texture>;
        setLoaded(loaded: boolean): void;
        isLoaded(): boolean;
        setImpl(impl: HTMLImageElement | HTMLCanvasElement): void;
        getImpl(): HTMLCanvasElement | HTMLImageElement;
        addLoadedHandler(cb: () => any): void;
    }
}
declare module plume {
    interface Font {
        size: number;
        lineHeight: number;
        lettersByCode: Array<Letter>;
        textureByCode: Array<Texture>;
        chars: Array<number>;
        resource: HTMLImageElement;
    }
    interface Letter {
        id: number;
        x: number;
        y: number;
        height: number;
        width: number;
        xoffset: number;
        yoffset: number;
        xadvance: number;
    }
    class HFont extends AbstractLoadingResource implements Font {
        private _name;
        private _img;
        private _def;
        private _atlas;
        private _image;
        chars: Array<number>;
        lettersByCode: Array<Letter>;
        size: number;
        lineHeight: number;
        resource: HTMLImageElement;
        textureByCode: Array<Texture>;
        constructor(_name: string, _img: string, _def: string);
        private _onAllLoaded;
        private _createFont;
        getAssetDef(): AssetDef;
    }
}
declare module plume {
    class PImage extends AbstractLoadingResource {
        private _name;
        private _src;
        private _texture;
        resource: HTMLImageElement;
        created: boolean;
        constructor(_name: string, _src: string);
        protected onSuccess0(): void;
        private _createTexture;
        getTexture(): Texture;
        getAssetDef(): AssetDef;
    }
}
declare module plume {
    type LoaderResources = {
        loading: number;
        loaded: number;
        failed: number;
    };
    class Loader2d {
        private _ressources;
        private _retryAttempt;
        private _state;
        private _completeCb;
        private _loadingResources;
        private _failedResources;
        private _successResources;
        private _terminated;
        private _attempt;
        private _timer;
        private _progressHandler;
        constructor(_ressources: Array<AssetDef>, _retryAttempt?: number);
        load(completeCb: (success: boolean) => any): void;
        getResourcesLoaded(): Array<AbstractLoadingResource>;
        private _checkLoaded;
        private _load0;
        private _retry;
        private _terminate;
        getStatus(): LoaderResources;
        getProgress(): number;
        readonly terminated: boolean;
        static loadFile(file: string, onload: (data: string) => void): void;
    }
}
declare module plume {
    class LoaderProxy {
        private static _INST;
        static get(): LoaderProxy;
        static set(factory: LoaderProxy): void;
        loadXHR(url: string, onsucess: (response: string) => any, onerror: (error: string) => any): void;
        loadImage(image: HTMLImageElement, src: string): void;
    }
}
declare module plume {
    class Resources {
        static images: {
            [name: string]: PImage;
        };
        static spritesheets: {
            [name: string]: Spritesheet;
        };
        static fonts: {
            [name: string]: Font;
        };
        static destroy(): typeof Resources;
        static addImage(name: string, file: string): PImage;
        static addFont(name: string, file: string, atlas: string): HFont;
        static addLoadedFont(name: string, font: Font): Font;
        static addSpriteSheet(name: string, file: string, atlas: string): Spritesheet;
        static addSoundsheet(name: string, file: string): Soundsheet;
        static addFile(name: string, file: string): FileResource;
    }
}
declare module plume {
    class Soundsheet extends FileResource {
        private _name;
        constructor(_name: string, _file: string);
        onSuccess0(): void;
        getAssetDef(): AssetDef;
    }
}
declare module plume {
    class Spritesheet extends AbstractLoadingResource {
        private _name;
        private _img;
        private _def;
        private _atlas;
        private _image;
        constructor(_name: string, _img: string, _def: string);
        private _onAllLoaded;
        private _createSpritesheet;
        getAssetDef(): AssetDef;
    }
}
declare module plume {
    class InlineLoader extends LoaderProxy {
        private _assetByName;
        assetExportName: string;
        loadXHR(url: string, onsucess: (response: string) => any, onerror: (error: string) => any): void;
        loadImage(image: HTMLImageElement, src: string): void;
        private _getAsset;
        private _ensureInititalized;
    }
}
declare module plume {
    class DeviceManager {
        readyCallback: any;
        os: OS;
        fullscreenManager: FullScreenManager;
        visibilityManager: VisibilityManager;
        whenReady(callback: () => void): void;
        private _checkReady;
    }
}
declare module plume {
    class FullScreenManager {
        private _element;
        protected _isInFullScreen: boolean;
        protected _changeHandler: Handler<{}>;
        fullscreenEnabled: boolean;
        requestFunction: Function;
        constructor();
        readonly isInFullScreen: boolean;
        readonly changeHandler: Handler<{}>;
        requestFullScreen(): void;
        exitFullscreen(): void;
        private _onFullscreenChange;
        private _hasFullscreenElement;
        toggleFullScreen(): void;
    }
}
declare module plume {
    class Logger {
        protected level: number;
        protected listeners: Handler<{
            level: string;
            message: any;
            error?: any;
        }>;
        setLevel(level: number): void;
        debug(message: any): void;
        info(message: any): void;
        warn(message: any): void;
        error(message: any, error?: any): void;
        addListener(i: Listener<{
            level: string;
            message: any;
            error?: any;
        }>): void;
    }
    let logger: Logger;
    function enableDomConsole(): void;
}
declare module plume {
    class OS {
        vita: boolean;
        kindle: boolean;
        android: boolean;
        chromeOS: boolean;
        iOS: boolean;
        iPhone: boolean;
        iPad: boolean;
        linux: boolean;
        macOS: boolean;
        windows: boolean;
        windowsPhone: boolean;
        desktop: boolean;
        iOSVersion: number;
        pixelRatio: number;
        init(): void;
    }
}
declare module plume {
    class VisibilityManager {
        private _isUnloading;
        private _isVisible;
        private _isExtendedVisible;
        private _hiddenPropertyName;
        private _visibilityEventName;
        private _visibilityChangeHandler;
        private _extendedVisibilityChangeHandler;
        private _beforeUnloadHandler;
        private _browserPrefixes;
        constructor();
        private _getHiddenPropertyName;
        private _getVisibilityEvent;
        private _getBrowserPrefix;
        private _handleVisibilityChange;
        private _handleExtendedVisibilityChange;
        private _onExtendedVisible;
        private _onExtendedHidden;
        private _handleOnBeforeUnload;
        addVisibilityChangeHandler(handler: (visible: boolean) => any): void;
        removeVisibilityChangeHandler(handler: (visible: boolean) => any): void;
        addExtendedVisibilityChangeHandler(handler: Listener<{
            isExtended: boolean;
            triggerProperty: string;
        }>): void;
        removeExtendedVisibilityChangeHandler(handler: Listener<{
            isExtended: boolean;
            triggerProperty: string;
        }>): void;
        addBeforeUnloadHandler(handler: (evt: Event) => any): void;
        removeBeforeUnloadHandler(handler: (evt: Event) => any): void;
        isVisible(): boolean;
        isUnloading(): boolean;
        hidden: boolean;
    }
}
declare module plume.Datef {
    let oneday: number;
    let serverTimeDiff: number;
    function now(): number;
}
declare module plume.Time {
    function _refresh(time: number): void;
    function now(): number;
    function ellapsed(): number;
}
declare module plume {
    class TimeManager implements Updatable {
        _now: number;
        private _timers;
        private static _TIME_MANAGER;
        private _inUpdateLoop;
        private _timersToAdd;
        private _timersToRemove;
        constructor();
        static get(): TimeManager;
        update(dt: number): void;
        private _pushTimer;
        remove(timer: Timer): void;
        removeAll(): void;
        cancelAll(): void;
        createTimer(delay: number, callback?: Function, thisArg?: any, argArray?: any[]): Timer;
        createAndStartTimer(delay: number, callback?: Function, thisArg?: any, argArray?: any[]): Timer;
        static createAndStartTimer(delay: number, callback?: Function, thisArg?: any, argArray?: any[]): Timer;
        readonly activeTimersCount: number;
        readonly activeTimers: string;
    }
}
declare module plume {
    class Timer {
        id: string;
        _delay: number;
        _callback: Function;
        _thisArg: any;
        _argArray: any[];
        _timeManager: TimeManager;
        _tick: number;
        _repeatCount: number;
        _running: boolean;
        _expired: boolean;
        _startTime: number;
        constructor(timeManager: TimeManager, delay: number, callback?: Function, thisArg?: any, argArray?: any[]);
        setRepeat(repeat: number): void;
        update(time: number): boolean;
        start(): void;
        cancel(): void;
        hasExpired(): boolean;
        getProgress(): number;
        getDuration(): number;
    }
}
declare module plume.easing {
    function linear(t: number): number;
    function easeInQuad(t: number): number;
    function easeInCubic(t: number): number;
    function easeInQuint(t: number): number;
    function easeOutQuad(t: number): number;
    function easeOutCubic(t: number): number;
    function easeOutQuint(t: number): number;
    function easeInOutQuad(t: number): number;
    function easeInOutCubic(t: number): number;
    function easeInOutQuint(t: number): number;
    function easeOutElastic(t: number): number;
}
declare module plume {
    class Random {
        private static _seed;
        static init(seed: number): void;
        static currentSeed(): number;
        static get(useSeed?: boolean): number;
        static inRange(min: number, max: number, useSeed?: boolean): number;
        static sign(useSeed?: boolean): number;
        static intInRange(min: number, max: number, useSeed?: boolean): number;
        private static _getSeededInt;
        private static _random0;
    }
}
declare module plume {
    class Strings {
        static ellipsis(text: string, maxLength: number, ellipsChar?: string): string;
        static capitalize(text: string): string;
        static interpolate(textIn: string, params: {
            [key: string]: any;
        }): string;
        static pad(num: number, size: number): string;
        static pads(s: string, size: number): string;
        static pad2(num: number): string;
        static randomUUID(): string;
    }
}
declare module plume {
    abstract class ValueSmoother<T> {
        state: number;
        data: any;
        startTime: number;
        duration: number;
        startValue: T;
        endValue: T;
        currentValue: T;
        easing: EasingFunction;
        reset(startValue: T, targetValue: T, duration: number, now?: number, currentValue?: T): void;
        getInterpolation(now?: number): T;
        abstract computeCurrentValue(t: number): T;
    }
    class ValueSmootherNumber extends ValueSmoother<number> {
        computeCurrentValue(t: number): number;
    }
    class ValueSmootherVector3 extends ValueSmoother<THREE.Vector3> {
        constructor();
        computeCurrentValue(t: number): THREE.Vector3;
    }
    class ValueSmootherQuat extends ValueSmoother<THREE.Quaternion> {
        constructor();
        computeCurrentValue(t: number): THREE.Quaternion;
    }
}
declare module plume {
    class Collections {
        static shuffle(array: Array<any>, useSeed?: boolean): void;
        static rotate(array: Array<any>, step: number): void;
        static concat(...arrays: Array<any>[]): Array<any>;
        static addAll<T>(source: Array<T>, elements: Array<T>): void;
        static copy<T>(array: Array<T>): Array<T>;
        static one<T>(array: Array<T>, useSeed?: boolean): T;
        static some<T>(array: Array<T>, count: number, useSeed?: boolean): Array<T>;
        static remove<T>(array: Array<T>, obj: T): boolean;
        static contains<T>(array: Array<T>, obj: T): boolean;
    }
}
declare module plume.http {
    function loadJs(url: string, onload: () => void): void;
    function loadArrayBuffer(url: string, onload: (data: ArrayBuffer) => void, onerror?: () => void): void;
}
declare module plume {
    class StartupParams {
        private static _INSTANCE;
        private _params;
        static getInstance(): StartupParams;
        static add(key: string, value: string): void;
        static remove(key: string): string;
        static get(key: string, valueIfNull?: string): string;
        static has(key: string): boolean;
        static urlParametersToHashMap(url: string): HashMap<string>;
    }
}
declare module plume {
    interface Listener<T> {
        (event: T): any;
    }
    class Handler<T> {
        private _handlers;
        private _onces;
        readonly length: number;
        add(listener: Listener<T>, context?: any): void;
        once(listener: Listener<T>, context?: any): void;
        remove(listener: Listener<T>): void;
        clear(): void;
        fire(event: T): void;
    }
    class ContextualCallback {
        callback: Function;
        context: any;
        constructor(callback: Function, context?: any);
        execute(...args: any[]): void;
        static removeFromArray(fn: Function, array: Array<ContextualCallback>): boolean;
    }
}
declare module plume {
    class WaitQueue {
        private size;
        private _data;
        private _resolved;
        onComplete: (data: Array<any>) => any;
        constructor(size: number);
        resolve(index: number, data: any): void;
        private _allDone;
    }
}
declare namespace plume {
    class LocalStorage {
        private _id;
        cacheData: Object;
        available: boolean;
        constructor(_id: string);
        private _isAvailable;
        put(key: string, value: any): void;
        get<T>(key: string, defValue?: T): T;
        remove(key: string): void;
    }
}
