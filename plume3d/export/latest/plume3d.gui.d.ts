/// <reference path="plume3d.core.d.ts" />
declare module plume {
    type CanvasGuiOptions = {
        canvasId?: string;
    };
    class CanvasGui implements plume.Gui {
        options: CanvasGuiOptions;
        private static _inst;
        canvas: HTMLCanvasElement;
        scene: GuiScene;
        layers: GameContainerGroup;
        scaling: GuiScaling;
        expandedBounds: Rectangle;
        private _currentOrPendingScene;
        private _context2D;
        private _mouseInterpretor;
        constructor(options: CanvasGuiOptions);
        static get(): CanvasGui;
        update(dt: number): void;
        render(dt: number, interpolation: number): void;
        switchScene(newScene: GuiScene, animateOutCurrentScene?: boolean, animateInNewScene?: boolean): void;
        private _pushScene;
        getCurrentOrPendingScene(): GuiScene;
        private _initCanvas;
        private _beginDraw;
        private _endDraw;
        readonly context: CanvasRenderingContext2D;
    }
}
declare module plume {
    class Colors {
        static getRgb(color: number): Array<number>;
        static fromRgb(r: number, g: number, b: number): number;
        static fromHSV(h: number, s: number, v: number): number;
        static toString(color: number, alpha?: number): string;
        static toHexString(color: number): string;
        static fromString(color: string): number;
    }
}
declare module plume {
    class GuiMouseEventInterpretor implements MouseEventInterpretor {
        private _inputManager;
        private _mouseHandler;
        private _desktop;
        private _clickHandlerCount;
        rootContainer: Container;
        constructor();
        onUpdate(): void;
        onImmediate(): void;
        private _triggerMouseHit;
        private _invalidateInteractive;
        private _triggerMouseHit0;
        private _triggerImmediateClick0;
        static _collectInteractiveHit(position: Point, container: Container, collector: Array<DisplayObject>): void;
    }
}
declare module plume {
    class GuiScaling {
        private _scaleManager;
        constructor();
        init(): void;
        private _onResize;
    }
}
declare module plume {
    class GuiScene {
        game: Game;
        gui: CanvasGui;
        container: Container;
        private _hideHandler;
        constructor();
        addChild(child: DisplayObject): void;
        removeChild(child: DisplayObject): void;
        update(dt?: number): void;
        render(dt?: number, interpolation?: number): void;
        show(animateIn?: boolean): void;
        protected animateIn(callback: Function): void;
        protected onShow(): void;
        hide(callback: Function, animateOut?: boolean): void;
        protected animateOut(callback: Function): void;
        protected onHide(): void;
        protected dispose(): void;
        readonly hideHandler: Handler<{}>;
    }
}
declare module plume {
    interface HasWidth {
        width: number;
    }
    interface HasHeight {
        height: number;
    }
    interface HasSize extends HasWidth, HasHeight {
    }
    interface HasX {
        x: number;
    }
    interface HasY {
        y: number;
    }
    interface HasCoordinate extends HasX, HasY {
    }
    interface HasBounds extends HasCoordinate, HasSize {
    }
}
declare module plume {
    class DisplayObject {
        _parent: Container | null;
        _root: boolean;
        showCursor: boolean;
        debugName: string;
        protected _debug: boolean;
        protected _visible: boolean;
        protected _hitArea: Rectangle;
        protected _interactive: boolean;
        protected _hasInteractiveChild: boolean;
        static _interactiveDirty: boolean;
        protected _downHandler: Handler<boolean>;
        protected _upHandler: Handler<boolean>;
        protected _overHandler: Handler<boolean>;
        protected _outHandler: Handler<boolean>;
        protected _clickHandler: Handler<any>;
        protected _detachListener: Handler<boolean>;
        protected _id: number;
        protected _debugId: string;
        protected static _idCount: number;
        protected _x: number;
        protected _y: number;
        protected _width: number;
        protected _height: number;
        protected _rotation: number;
        protected _alpha: number;
        protected _scaleX: number;
        protected _scaleY: number;
        protected _pivotX: number;
        protected _pivotY: number;
        protected _flipX: boolean;
        protected _flipY: boolean;
        protected _tint: number;
        protected _mask: Mask;
        protected _worldHitArea: Array<number> | null;
        protected _transformDirty: boolean;
        protected _transform: Matrix;
        protected _hitTransform: Matrix;
        protected _worldTransform: Matrix;
        protected _worldAlpha: number;
        protected _cachedAsBitmap: boolean;
        protected _cachedTexture: Texture;
        constructor();
        addDebugBorder(color?: string): void;
        tint: number;
        draw(context: CanvasRenderingContext2D): void;
        protected computeWorldTransform(): void;
        protected getParentWorldTransform(): Matrix;
        protected getParentAlpha(): number;
        protected getParentHitTransform(): Matrix;
        protected updateWorldHitArea(worldTransform: Matrix): void;
        protected draw0(context: CanvasRenderingContext2D): void;
        protected createCachedTexture(): void;
        protected drawCachedTexture(context: CanvasRenderingContext2D): void;
        onDetach(): void;
        removeSelf(): boolean;
        getDebugId(): string;
        setDebugId(id: string, appendDynId?: boolean): void;
        hitPoint(point: Point): boolean;
        containsPoint(point: Point): boolean;
        interactive: boolean;
        hitArea: Rectangle;
        hasInteractiveChild: boolean;
        mask: Mask;
        static interactiveDirty: boolean;
        visible: boolean;
        onClick(fn: () => void, context?: any, once?: boolean): void;
        hasDownHandler(): boolean;
        hasUpHandler(): boolean;
        hasOverHandler(): boolean;
        hasOutHandler(): boolean;
        hasClickHandler(): boolean;
        readonly downHandler: Handler<boolean>;
        readonly upHandler: Handler<boolean>;
        readonly overHandler: Handler<boolean>;
        readonly outHandler: Handler<boolean>;
        readonly clickHandler: Handler<any>;
        readonly detachListener: Handler<boolean>;
        setPivot(x: number, y: number): void;
        setPivotToCenter(): void;
        x: number;
        y: number;
        alpha: number;
        scaleX: number;
        scaleY: number;
        scaleXY: number;
        debug: boolean;
        pivotX: number;
        pivotY: number;
        flipX: boolean;
        flipY: boolean;
        angle: number;
        width: number;
        height: number;
        parent: Container | null;
        rotation: number;
        cachedAsBitmap: boolean;
        readonly top: number;
        readonly bottom: number;
        readonly left: number;
        readonly right: number;
    }
}
declare module plume {
    class Sprite extends DisplayObject {
        protected _texture: Texture;
        constructor(texture: Texture);
        setSizeFromTexture(): void;
        static fromFrame(frameId: string): Sprite;
        static fromImage(imageId: string): Sprite;
        static fromTexture(texture: Texture): Sprite;
        setTexture(texture: Texture): void;
        setFrame(frameId: string): void;
        addLoadedHandler(cb: () => any): void;
        readonly texture: Texture;
        protected draw0(context: CanvasRenderingContext2D): void;
    }
}
declare module plume {
    class Animation extends Sprite implements Updatable {
        static fromFrames(frameIds: Array<string>, id?: string): Animation;
        static fromTextures(textures: Array<Texture>, id?: string): Animation;
        fps: number;
        private _curIndex;
        private _textures;
        private _playing;
        private _startTime;
        private _startFrame;
        private _lastIndex;
        private _from;
        private _to;
        onEnd: Function;
        private _repeats;
        private _totalFrames;
        forward: boolean;
        animationSpeed: number;
        paused: boolean;
        pausedDuration: number;
        constructor(textures: Array<Texture>, id?: string);
        destroy(): void;
        setTextures(textureIdList: Array<string>): void;
        setTextures2(textures: Array<Texture>): void;
        setFrameIndex(index: number): void;
        protected setActiveFrame(index: number): void;
        private _onAnimationEnded;
        gotoAndStop(frameName: number): void;
        gotoAndPlay(frameName: number): void;
        update(simulationTimeStep: number): void;
        getTotalFrames(): number;
        getCurrentFrame(): number;
        private _play0;
        play(from?: number, to?: number): void;
        repeat(repeats?: number): void;
        isPlaying(): boolean;
        stop(): void;
        pause(): void;
        resume(): void;
    }
}
declare module plume {
    class Container extends DisplayObject {
        private _children;
        constructor(width?: number, height?: number);
        addChild(child: DisplayObject): DisplayObject;
        addChildAt(child: DisplayObject, index: number): DisplayObject;
        removeChild(child: DisplayObject): DisplayObject | null;
        removeChildAt(index: number): DisplayObject;
        removeChildren(beginIndex?: number, endIndex?: number): Array<DisplayObject>;
        getChildIndex(child: DisplayObject): number;
        getChildById(id: string): DisplayObject;
        setChildIndex(child: DisplayObject, index: number): void;
        onDetach(): void;
        getChildAt(index: number): DisplayObject;
        getChildrenCount(): number;
        _outputToConsole(level: number, properties?: Array<string>): void;
        _countElements(visible?: boolean): number;
        private _getDebugPropertyString;
        protected draw0(context: CanvasRenderingContext2D): void;
        setSize(w: number, h: number): void;
        setSizeFromChildren(recursive?: boolean): void;
    }
}
declare module plume {
    class Button extends Container {
        protected _down: boolean;
        protected _over: boolean;
        protected _enabled: boolean;
        disableAlpha: number;
        constructor();
        enable(): void;
        disable(): void;
        onStateDown(): void;
        onStateUp(): void;
        onStateOver(): void;
        onStateOut(): void;
    }
}
declare module plume {
    class ImageButtonStyle {
        _stateUp: DisplayObject;
        _stateDown: DisplayObject;
        _stateOver: DisplayObject;
        constructor(stateUp: DisplayObject, stateDown?: DisplayObject, stateOver?: DisplayObject);
        static fromFrames(stateUp: string, stateDown?: string, stateOver?: string): ImageButtonStyle;
        static fromTextures(stateUp: Texture, stateDown?: Texture, stateOver?: Texture): ImageButtonStyle;
    }
    class ImageButton extends Button {
        private _style;
        private _text;
        private _icon;
        constructor(style: ImageButtonStyle);
        static fromFramesArray(states: string[]): ImageButton;
        static fromFrames(stateUp: string, stateDown?: string, stateOver?: string): ImageButton;
        static fromTextures(stateUp: Texture, stateDown?: Texture, stateOver?: Texture): ImageButton;
        setFrames(stateUp: string, stateDown?: string, stateOver?: string): void;
        setFramesArray(states: string[]): void;
        setContent(content: Container): void;
        setText(text: BitmapText): void;
        setIcon(frame: string, scale?: number): void;
        protected setStateTexture(): void;
        onStateDown(): void;
        onStateUp(): void;
        onStateOver(): void;
        onStateOut(): void;
        readonly style: ImageButtonStyle;
    }
}
declare module plume {
    class ContainerGroup implements Updatable {
        private _root;
        private _childs;
        private _dirty;
        constructor();
        destroy(): void;
        update(): void;
        getChild(name: string): Container;
        addChild(name: string, index?: number): Container;
        removeChild(name: string): Container;
        setChildIndex(name: string, zindex: number): void;
        private _sortLayers;
        private _ensureLayer;
        root: Container;
    }
}
declare module plume {
    class GameContainerGroup extends ContainerGroup {
        private _background;
        private _scene;
        private _gameplayHUD;
        private _popup;
        private _top;
        private _technical;
        background: Container;
        scene: Container;
        gameplayHUD: Container;
        sceneHUD: Container;
        popup: Container;
        top: Container;
        technical: Container;
    }
}
declare module plume.layout {
    function centerOnX(source: HasWidth & HasX, container: HasWidth & HasX, absolute?: boolean): void;
    function centerOnY(source: HasHeight & HasY, container: HasHeight & HasY, absolute?: boolean): void;
    function centerOnXInParent(obj: DisplayObject): void;
    function centerOnYInParent(obj: DisplayObject): void;
    function centerOnXYInParent(obj: DisplayObject): void;
    function computeBounds(container: Container, recursive?: boolean): HasBounds;
    function setContainerSize(container: Container, recursive?: boolean): void;
}
declare module plume {
    class Mask {
        private _command;
        moveTo(x: number, y: number): void;
        lineTo(x: number, y: number): void;
        rectangle(x: number, y: number, width: number, height: number): void;
        arc(cx: number, cy: number, radius: number, startAngle?: number, endAngle?: number): void;
        _applyMask(context: CanvasRenderingContext2D): void;
    }
}
declare module plume {
    class AbstractProgressBar {
        protected _background: DisplayObject;
        protected _progress: number;
        onProgressExpired: () => void;
        protected _lastProgress: number;
        private _startTime;
        private _duration;
        private _expired;
        private _started;
        constructor(_background: DisplayObject, _progress?: number);
        startTimeLimited(duration: number, ellapsed?: number): void;
        setProgress(progress: number): void;
        update(dt: number): void;
        render(interpolation: number): void;
        getProgress(): number;
        private _onExpired;
        private _autoUpdate;
    }
}
declare module plume {
    const enum ProgressBarWay {
        LeftToRight = 0,
        RightToLeft = 1,
        TopToBottom = 2,
        BottomToTop = 3
    }
    class ProgressBar extends AbstractProgressBar {
        private _way;
        private _bgwidth;
        private _bgheight;
        private _inverted;
        constructor(_background: DisplayObject, _way?: ProgressBarWay, _progress?: number);
        updateProgress(progress: number): void;
        update(dt: number): void;
        render(interpolation: number): void;
        setInverted(inverted: boolean): void;
    }
}
declare module plume {
    class RoundProgressBar extends AbstractProgressBar {
        start: number;
        radius: number;
        centerX: number;
        centerY: number;
        private _inverted;
        constructor(_background: DisplayObject, _progress?: number);
        setInverted(inverted: boolean): void;
        updateProgress(progress: number): void;
        update(dt: number): void;
        render(interpolation: number): void;
    }
}
declare module plume {
    class Tinter {
        static rounding: number;
        private static _multiplyDetected;
        private static _isMultiplySupported;
        static tint(texture: Texture, color: number, useCache?: boolean): Texture;
    }
}
declare module plume {
    class Collisions {
        constructor();
        static overlapRectangles(r1: Rectangle, r2: Rectangle): boolean;
        static pointInPolygon2(points: Array<number>, x: number, y: number): boolean;
    }
}
declare module plume {
    class Matrix {
        a: number;
        b: number;
        c: number;
        d: number;
        e: number;
        f: number;
        context: CanvasRenderingContext2D;
        /**
         * 2D transformation matrix object initialized with identity matrix.
         *
         * The matrix can synchronize a canvas context by supplying the context
         * as an argument, or later apply current absolute transform to an
         * existing context.
         *
         * All values are handled as floating point values.
         *
         * @param {CanvasRenderingContext2D} [context] - Optional context to sync with Matrix
         * @prop {number} a - scale x
         * @prop {number} b - skew y
         * @prop {number} c - skew x
         * @prop {number} d - scale y
         * @prop {number} e - translate x
         * @prop {number} f - translate y
         * @prop {CanvasRenderingContext2D} [context] - set or get current canvas context
         * @constructor
         */
        constructor(context?: CanvasRenderingContext2D);
        toString(): string;
        /**
         * Flips the horizontal values.
         */
        flipX(): Matrix;
        /**
         * Flips the vertical values.
         */
        flipY(): Matrix;
        /**
         * Short-hand to reset current matrix to an identity matrix.
         */
        reset(): Matrix;
        /**
         * Rotates current matrix accumulative by angle.
         * @param {number} angle - angle in radians
         */
        rotate(angle: number): Matrix;
        /**
         * Helper method to make a rotation based on an angle in degrees.
         * @param {number} angle - angle in degrees
         */
        rotateDeg(angle: number): Matrix;
        /**
         * Scales current matrix accumulative.
         * @param {number} sx - scale factor x (1 does nothing)
         * @param {number} sy - scale factor y (1 does nothing)
         */
        scale(sx: number, sy: number): Matrix;
        /**
         * Scales current matrix on x axis accumulative.
         * @param {number} sx - scale factor x (1 does nothing)
         */
        scaleX(sx: number): Matrix;
        /**
         * Scales current matrix on y axis accumulative.
         * @param {number} sy - scale factor y (1 does nothing)
         */
        scaleY(sy: number): Matrix;
        /**
         * Apply skew to the current matrix accumulative.
         * @param {number} sx - amount of skew for x
         * @param {number} sy - amount of skew for y
         */
        skew(sx: number, sy: number): Matrix;
        /**
         * Apply skew for x to the current matrix accumulative.
         * @param {number} sx - amount of skew for x
         */
        skewX(sx: number): Matrix;
        /**
         * Apply skew for y to the current matrix accumulative.
         * @param {number} sy - amount of skew for y
         */
        skewY(sy: number): Matrix;
        /**
         * Set current matrix to new absolute matrix.
         * @param {number} a - scale x
         * @param {number} b - skew y
         * @param {number} c - skew x
         * @param {number} d - scale y
         * @param {number} e - translate x
         * @param {number} f - translate y
         */
        setTransform(a: number, b: number, c: number, d: number, e: number, f: number): Matrix;
        /**
         * Translate current matrix accumulative.
         * @param {number} tx - translation for x
         * @param {number} ty - translation for y
         */
        translate(tx: number, ty: number): Matrix;
        /**
         * Translate current matrix on x axis accumulative.
         * @param {number} tx - translation for x
         */
        translateX(tx: number): Matrix;
        /**
         * Translate current matrix on y axis accumulative.
         * @param {number} ty - translation for y
         */
        translateY(ty: number): Matrix;
        /**
         * Multiplies current matrix with new matrix values.
         * @param {number} a2 - scale x
         * @param {number} b2 - skew y
         * @param {number} c2 - skew x
         * @param {number} d2 - scale y
         * @param {number} e2 - translate x
         * @param {number} f2 - translate y
         */
        transform(a2: number, b2: number, c2: number, d2: number, e2: number, f2: number): Matrix;
        /**
         * Get an inverse matrix of current matrix. The method returns a new
         * matrix with values you need to use to get to an identity matrix.
         * Context from parent matrix is not applied to the returned matrix.
         * @returns {Matrix}
         */
        getInverse(): Matrix;
        /**
         * Interpolate this matrix with another and produce a new matrix.
         * t is a value in the range [0.0, 1.0] where 0 is this instance and
         * 1 is equal to the second matrix. The t value is not constrained.
         *
         * Context from parent matrix is not applied to the returned matrix.
         *
         * @param {Matrix} m2 - the matrix to interpolate with.
         * @param {number} t - interpolation [0.0, 1.0]
         * @returns {Matrix} - new instance with the interpolated result
         */
        interpolate(m2: Matrix, t: number): Matrix;
        /**
         * Apply current matrix to x and y point.
         * Returns a point object.
         *
         * @param {number} x - value for x
         * @param {number} y - value for y
         * @returns {{x: number, y: number}} A new transformed point object
         */
        applyToPoint(x: number, y: number): {
            x: number;
            y: number;
        };
        /**
         * Apply current matrix to a typed array with point pairs. Although
         * the input array may be an ordinary array, this method is intended
         * for more performant use where typed arrays are used. The returned
         * array is regardless always returned as a Float32Array.
         *
         * @param {*} points - (typed) array with point pairs
         * @returns {Float32Array} A new array with transformed points
         */
        applyToArray(points: Array<number>): Array<number>;
        /**
         * Apply to any canvas 2D context object. This does not affect the
         * context that optionally was referenced in constructor unless it is
         * the same context.
         * @param {CanvasRenderingContext2D} context
         */
        applyToContext(context: CanvasRenderingContext2D): Matrix;
        /**
         * Returns true if matrix is an identity matrix (no transforms applied).
         * @returns {boolean} True if identity (not transformed)
         */
        isIdentity(): boolean;
        /**
         * Compares current matrix with another matrix. Returns true if equal
         * (within epsilon tolerance).
         * @param {Matrix} m - matrix to compare this matrix with
         * @returns {boolean}
         */
        isEqual(m: Matrix): boolean;
        /**
         * Compares floating point values with some tolerance (epsilon)
         * @param {number} f1 - float 1
         * @param {number} f2 - float 2
         * @returns {boolean}
         * @private
         */
        _isEqual(f1: number, f2: number): boolean;
        /**
         * Apply current absolute matrix to context if defined, to sync it.
         * @private
         */
        _setCtx(): void;
    }
}
declare module plume {
    class BitmapText extends Container {
        private _missingChar;
        private _text;
        private _font;
        private _fontName;
        private _size;
        private _ratio;
        private _align;
        private _maxWidth;
        private _dirty;
        constructor(text: string, fontName: string, size?: number);
        text: string;
        align: string;
        maxWidth: number;
        readonly width: number;
        readonly height: number;
        tint: number;
        hasMissingChar(): boolean;
        draw0(context: CanvasRenderingContext2D): void;
        private _create;
        private _normalizeSize;
        private _getLetterWidthToDraw;
    }
}
declare module plume {
    class Graphics extends DisplayObject {
        private _ops;
        constructor(width?: number, height?: number);
        customDraw(cb: (context: CanvasRenderingContext2D) => any): Graphics;
        beginFill(color: number, alpha?: number): Graphics;
        beginStroke(lineWidth: number, color: number, alpha?: number): Graphics;
        endStroke(): Graphics;
        endFill(): Graphics;
        moveTo(x: number, y: number): Graphics;
        lineTo(x: number, y: number): Graphics;
        quadraticCurveTo(cpX: number, cpY: number, toX: number, toY: number): Graphics;
        bezierCurveTo(cpX: number, cpY: number, cpX2: number, cpY2: number, toX: number, toY: number): Graphics;
        arcTo(x1: number, y1: number, x2: number, y2: number, radius: number): Graphics;
        arc(cx: number, cy: number, radius: number, startAngle?: number, endAngle?: number, anticlockwise?: boolean): Graphics;
        drawRect(x: number, y: number, width: number, height: number): Graphics;
        drawRoundedRect(x: number, y: number, width: number, height: number, radius: number): Graphics;
        drawCircle(x: number, y: number, radius: number): Graphics;
        clear(): Graphics;
        protected createCachedTexture(): void;
        protected draw0(context: CanvasRenderingContext2D): void;
        setSize(w: number, h: number): void;
    }
}
declare module plume {
    class RenderTexture {
        render(display: DisplayObject): string;
    }
}
declare module plume {
    class Text extends DisplayObject {
        private _text;
        private _fontName;
        private _fontSize;
        private _color;
        private _canvas;
        private _font;
        private _align;
        private _colorString;
        private _dirty;
        _heightRatio: number;
        constructor(_text: string, _fontName: string, _fontSize: number, _color?: number);
        text: string;
        readonly width: number;
        readonly height: number;
        align: string;
        color: number;
        draw0(context: CanvasRenderingContext2D): void;
        private _setFontStyle;
        private _create;
        private _createMultiline;
    }
}
declare module plume {
    interface FontSpec {
        face: string;
        size: number;
        lineHeight: number;
        chars: Array<Letter>;
    }
    class FontLoader {
        static loadAll(fonts: Array<FontSpec>): void;
        static load(font: FontSpec): void;
    }
}
declare module plume {
    interface AssetDef {
        type: "img" | "spritesheet" | "font" | "soundsheet" | "file";
        name: string;
        file: string;
        atlas?: string;
    }
    const enum PreloadingProgressAlgo {
        Assets = 1,
        AssetsSmooth = 2,
        FakeDuration = 3
    }
    class H5PreLoadingScene extends GuiScene {
        static retryMax: number;
        protected loader: Loader2d;
        protected assetList: Array<AssetDef>;
        protected callback: Function;
        private _loadingStatus;
        progressListener: Handler<number>;
        private _timeCounted;
        protected startLoadTime: number;
        optDelayBeforeCompleteFire: number;
        optProgressAlgo: number;
        optEstimateLoadingTime: number;
        optOnProgress: (progress: number) => void;
        estimatedProgress: number;
        lastEstimatedProgress: number;
        constructor(assetList: Array<AssetDef>, callback: Function);
        protected onLoadComplete(): void;
        addProgressListener(fn: (progress: number) => void, context?: any): void;
        onShow(): void;
        update(delta: number): void;
        render(interpolation: number): void;
        private _updateProgress;
        private _updateProgress_AssetsSmooth;
        private _updateProgress_FakeDuration;
        private _updateProgress_Assets;
    }
}
declare module plume {
    class Fonts {
        static clearYOffset(fontName: string): void;
    }
}
