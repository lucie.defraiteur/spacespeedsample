"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var plume;
(function (plume) {
    var CanvasGui = /** @class */ (function () {
        function CanvasGui(options) {
            this.options = options;
            CanvasGui._inst = this;
            this.expandedBounds = new plume.Rectangle(0, 0, plume.Game.get().width, plume.Game.get().height);
            this.layers = new plume.GameContainerGroup();
            this._initCanvas();
            this.scaling = new plume.GuiScaling();
            this.scaling.init();
            this._mouseInterpretor = new plume.GuiMouseEventInterpretor();
            plume.Game.get().inputManager.mouseEventInterpretors.push(this._mouseInterpretor);
        }
        CanvasGui.get = function () {
            return this._inst;
        };
        CanvasGui.prototype.update = function (dt) {
            this.layers.update();
            if (this.scene) {
                this.scene.update(dt);
            }
        };
        CanvasGui.prototype.render = function (dt, interpolation) {
            if (this.scene) {
                this.scene.render(dt, interpolation);
            }
            this._beginDraw();
            this.layers.root.draw(this._context2D);
            this._endDraw();
        };
        CanvasGui.prototype.switchScene = function (newScene, animateOutCurrentScene, animateInNewScene) {
            var _this = this;
            if (animateOutCurrentScene === void 0) { animateOutCurrentScene = true; }
            if (animateInNewScene === void 0) { animateInNewScene = true; }
            if (this.scene) {
                this._currentOrPendingScene = newScene;
                this.scene.hide(function () {
                    _this.layers.scene.removeChild(_this.scene.container);
                    _this._pushScene(newScene, animateInNewScene);
                }, animateOutCurrentScene);
            }
            else {
                this._currentOrPendingScene = newScene;
                this._pushScene(newScene, animateInNewScene);
            }
        };
        CanvasGui.prototype._pushScene = function (newScene, animateIn) {
            plume.logger.debug("Change scene");
            this.scene = newScene;
            if (this.scene) {
                this.layers.scene.addChild(this.scene.container);
                this.scene.show(animateIn);
            }
        };
        // get current scene (or scene we are switching to)
        CanvasGui.prototype.getCurrentOrPendingScene = function () {
            return this._currentOrPendingScene;
        };
        //
        // Canvas draw
        //
        CanvasGui.prototype._initCanvas = function () {
            if (this.options.canvasId != null) {
                var canvasId = this.options.canvasId;
                this.canvas = document.getElementById(canvasId);
                if (this.canvas == null) {
                    throw new Error("Unable to find canvas: " + canvasId);
                }
            }
            else {
                this.canvas = document.createElement("canvas");
                this.canvas.style.position = "absolute";
                document.body.appendChild(this.canvas);
            }
            var bounds = plume.Game.get().bounds;
            this.canvas.width = bounds.width;
            this.canvas.height = bounds.height;
            this._context2D = this.canvas.getContext("2d");
            plume.Renderer._applyStyle(this.canvas);
            this.canvas.style.pointerEvents = "none"; // gui canvas do not catch mouse event, forward to gl canvas
        };
        CanvasGui.prototype._beginDraw = function () {
            this._context2D.clearRect(0, 0, this.canvas.width, this.canvas.height);
        };
        CanvasGui.prototype._endDraw = function () {
        };
        Object.defineProperty(CanvasGui.prototype, "context", {
            get: function () {
                return this._context2D;
            },
            enumerable: true,
            configurable: true
        });
        return CanvasGui;
    }());
    plume.CanvasGui = CanvasGui;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Colors = /** @class */ (function () {
        function Colors() {
        }
        Colors.getRgb = function (color) {
            var r = (color >> 16) & 0x0000FF;
            var g = (color >> 8) & 0x0000FF;
            var b = color & 0x0000FF;
            return [r, g, b];
        };
        Colors.fromRgb = function (r, g, b) {
            return r << 16 | g << 8 | b;
        };
        Colors.fromHSV = function (h, s, v) {
            var r, g, b;
            var i = Math.floor(h * 6);
            var f = h * 6 - i;
            var p = v * (1 - s);
            var q = v * (1 - f * s);
            var t = v * (1 - (1 - f) * s);
            switch (i % 6) {
                case 0:
                    r = v, g = t, b = p;
                    break;
                case 1:
                    r = q, g = v, b = p;
                    break;
                case 2:
                    r = p, g = v, b = t;
                    break;
                case 3:
                    r = p, g = q, b = v;
                    break;
                case 4:
                    r = t, g = p, b = v;
                    break;
                case 5:
                    r = v, g = p, b = q;
                    break;
            }
            r = Math.round(r * 255);
            g = Math.round(g * 255);
            b = Math.round(b * 255);
            return this.fromRgb(r, g, b);
        };
        Colors.toString = function (color, alpha) {
            if (alpha === void 0) { alpha = 1; }
            var red = ((color & 0xFF0000) >>> 16);
            var green = ((color & 0xFF00) >>> 8);
            var blue = (color & 0xFF);
            return 'rgba(' + red + ',' + green + ',' + blue + ',' + alpha + ')';
        };
        Colors.toHexString = function (color) {
            return '#' + plume.Strings.pads(color.toString(16), 6);
        };
        Colors.fromString = function (color) {
            if (color.length != 7)
                return 0;
            var ci = parseInt(color.replace("#", ""), 16);
            return ci;
        };
        return Colors;
    }());
    plume.Colors = Colors;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var GuiMouseEventInterpretor = /** @class */ (function () {
        function GuiMouseEventInterpretor() {
            this._clickHandlerCount = 0;
            this._inputManager = plume.Game.get().inputManager;
            this._mouseHandler = plume.Game.get().inputManager.mouse;
            this._desktop = plume.Game.get().deviceManager.os.desktop;
            this.rootContainer = plume.CanvasGui.get().layers.root;
        }
        GuiMouseEventInterpretor.prototype.onUpdate = function () {
            this._triggerMouseHit();
        };
        GuiMouseEventInterpretor.prototype.onImmediate = function () {
            if (this._clickHandlerCount <= 0) {
                return;
            }
            for (var i = 0; i < this._mouseHandler.pointers.length; i++) {
                var pointer = this._mouseHandler.pointers[i];
                if (pointer.isActive()) {
                    this._triggerImmediateClick0(pointer);
                }
            }
        };
        //
        // In update loop
        //
        GuiMouseEventInterpretor.prototype._triggerMouseHit = function () {
            var checkInteractiveElement = this._inputManager._pointerDirty;
            this._inputManager._pointerDirty = false;
            if (plume.DisplayObject.interactiveDirty) {
                //console.log("Invalidate interactive (root)");
                this._clickHandlerCount = 0;
                this._invalidateInteractive(this.rootContainer);
                plume.DisplayObject.interactiveDirty = false;
                checkInteractiveElement = true;
                //this.rootContainer._outputToConsole(0, ["hasInteractiveChild", "interactive", "x", "y"]);
                //this.rootContainer._outputToConsole(0, []);
            }
            if (!checkInteractiveElement) {
                //console.log("checkInteractiveElement");
                return;
            }
            for (var i = 0; i < this._mouseHandler.pointers.length; i++) {
                var pointer = this._mouseHandler.pointers[i];
                this._triggerMouseHit0(pointer);
            }
        };
        // Tag scene graph element without interactive children
        // Count immadiate click hanlders
        GuiMouseEventInterpretor.prototype._invalidateInteractive = function (container) {
            // mark container without children interactive
            container.hasInteractiveChild = false;
            // go through all children
            var children = container.getChildrenCount();
            for (var c = 0; c < children; c++) {
                var child = container.getChildAt(c);
                if (child instanceof plume.Container) {
                    // go down
                    this._invalidateInteractive(child);
                    if (child.hasInteractiveChild || child.interactive) {
                        container.hasInteractiveChild = true;
                    }
                }
                else {
                    if (child.interactive) {
                        // mark with children interactive
                        container.hasInteractiveChild = true;
                    }
                }
                if (child.hasClickHandler()) {
                    this._clickHandlerCount++;
                }
            }
        };
        GuiMouseEventInterpretor.prototype._triggerMouseHit0 = function (pointer) {
            if (!pointer.mouseChanged())
                return;
            var collector = new Array();
            GuiMouseEventInterpretor._collectInteractiveHit(pointer.position, this.rootContainer, collector);
            if (collector.length > 0) {
                var target = collector[collector.length - 1];
                pointer.interactiveElement = target;
                if (pointer.isJustDown) {
                    pointer._lastDownTarget = target;
                    if (target.hasDownHandler())
                        target.downHandler.fire(true);
                }
                else if (pointer.isJustUp) {
                    if (target.hasUpHandler())
                        target.upHandler.fire(true);
                }
                if (target != pointer._previousHit) {
                    // over new target
                    if (target.hasOverHandler())
                        target.overHandler.fire(true);
                    // leave previous
                    if (pointer._previousHit != null) {
                        if (pointer._previousHit.hasOutHandler())
                            pointer._previousHit.outHandler.fire();
                    }
                }
                pointer._previousHit = target;
            }
            else {
                pointer.interactiveElement = null;
                if (pointer._previousHit != null) {
                    if (pointer._previousHit.hasOutHandler())
                        pointer._previousHit.outHandler.fire();
                }
                pointer._previousHit = null;
            }
            if (this._desktop && !this._inputManager.noCursor) {
                var canvas = plume.Game.get().engine.canvas;
                if (pointer._previousHit == null || !pointer._previousHit.showCursor) {
                    canvas.style.cursor = "default";
                }
                else {
                    canvas.style.cursor = "pointer";
                }
            }
        };
        GuiMouseEventInterpretor.prototype._triggerImmediateClick0 = function (pointer) {
            var collector = new Array();
            GuiMouseEventInterpretor._collectInteractiveHit(pointer.position, this.rootContainer, collector);
            if (collector.length > 0) {
                var target = collector[collector.length - 1];
                if (target.hasClickHandler()) {
                    if (!pointer.wasSwipe()) {
                        target.clickHandler.fire(true);
                    }
                }
            }
        };
        GuiMouseEventInterpretor._collectInteractiveHit = function (position, container, collector) {
            //if(1==1) return;
            // container not visible, skip
            if (!container.visible)
                return;
            //console.log("position : " + position);
            // add container if hit and interactive
            if (container.interactive) {
                var containerContainsPoint = container.hitPoint(position);
                if (containerContainsPoint)
                    collector.push(container);
            }
            if (!container.hasInteractiveChild)
                return;
            // loop through children
            var children = container.getChildrenCount();
            for (var c = 0; c < children; c++) {
                var child = container.getChildAt(c);
                if (child instanceof plume.Container) {
                    this._collectInteractiveHit(position, child, collector);
                }
                else {
                    if (child.visible && child.interactive) {
                        if (child.hitPoint(position)) {
                            collector.push(child);
                        }
                    }
                }
            }
        };
        return GuiMouseEventInterpretor;
    }());
    plume.GuiMouseEventInterpretor = GuiMouseEventInterpretor;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var GuiScaling = /** @class */ (function () {
        function GuiScaling() {
            this._scaleManager = plume.Game.get().scaleManager;
        }
        GuiScaling.prototype.init = function () {
            this._scaleManager.resizeHandler.add(this._onResize, this);
            this._onResize();
        };
        GuiScaling.prototype._onResize = function () {
            var current = this._scaleManager.getCurrentScaling();
            var devicePixelRatio = window.devicePixelRatio || 1;
            var width = current.windowWidth;
            var height = current.windowHeight;
            var canvasGui = plume.CanvasGui.get();
            var canvas = canvasGui.canvas;
            var root = canvasGui.layers.root;
            canvas.width = width * devicePixelRatio;
            canvas.height = height * devicePixelRatio;
            canvas.style.width = width + 'px';
            canvas.style.height = height + 'px';
            root.scaleXY = current.scale * devicePixelRatio;
            root.x = current.x * devicePixelRatio;
            root.y = current.y * devicePixelRatio;
            var game = plume.Game.get();
            var marginX = current.x / current.scale;
            var marginY = current.y / current.scale;
            plume.CanvasGui.get().expandedBounds = new plume.Rectangle(-marginX, -marginY, game.width + 2 * marginX, game.height + 2 * marginY);
        };
        return GuiScaling;
    }());
    plume.GuiScaling = GuiScaling;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var GuiScene = /** @class */ (function () {
        function GuiScene() {
            this.game = plume.Game.get();
            this.gui = plume.CanvasGui.get();
            this.container = new plume.Container(this.game.bounds.width, this.game.bounds.height);
            this.container.debugName = "Scene";
        }
        GuiScene.prototype.addChild = function (child) {
            this.container.addChild(child);
        };
        GuiScene.prototype.removeChild = function (child) {
            this.container.removeChild(child);
        };
        GuiScene.prototype.update = function (dt) {
        };
        GuiScene.prototype.render = function (dt, interpolation) {
        };
        //SHOULD NEVER BE OVERRIDED  
        GuiScene.prototype.show = function (animateIn) {
            var _this = this;
            if (animateIn === void 0) { animateIn = true; }
            var self = this;
            if (animateIn == true) {
                this.animateIn(function () {
                    _this.game.inputManager.enable();
                    plume.DisplayObject.interactiveDirty = true;
                    self.onShow();
                });
            }
            else {
                this.game.inputManager.enable();
                plume.DisplayObject.interactiveDirty = true;
                this.onShow();
            }
        };
        //TO be overrided for scene transition IN
        GuiScene.prototype.animateIn = function (callback) {
            callback();
        };
        //called when scene is ready (after animation)
        GuiScene.prototype.onShow = function () {
            //TO BE OVERRIDED 
        };
        //SHOULD NEVER BE OVERRIDED   
        GuiScene.prototype.hide = function (callback, animateOut) {
            if (animateOut === void 0) { animateOut = true; }
            plume.Game.get().inputManager.disable();
            if (this._hideHandler != null) {
                this._hideHandler.fire({});
                this._hideHandler.clear();
            }
            var self = this;
            if (animateOut == true) {
                this.animateOut(function () {
                    self.onHide();
                    self.dispose();
                    callback();
                });
            }
            else {
                this.onHide();
                this.dispose();
                callback();
            }
        };
        //TO be overrided for scene transition OUT
        GuiScene.prototype.animateOut = function (callback) {
            callback();
        };
        //called when scene is hided (after animation)
        GuiScene.prototype.onHide = function () {
            //TO BE OVERRIDED
        };
        GuiScene.prototype.dispose = function () {
            //TO BE OVERRIDED
        };
        Object.defineProperty(GuiScene.prototype, "hideHandler", {
            get: function () {
                if (this._hideHandler == null) {
                    this._hideHandler = new plume.Handler();
                }
                return this._hideHandler;
            },
            enumerable: true,
            configurable: true
        });
        return GuiScene;
    }());
    plume.GuiScene = GuiScene;
})(plume || (plume = {}));
/// <reference path="../../../build/latest/plume3d.core.d.ts" />
var plume;
(function (plume) {
    var DisplayObject = /** @class */ (function () {
        function DisplayObject() {
            this._root = false;
            this.showCursor = true;
            this._visible = true;
            this._interactive = false;
            this._hasInteractiveChild = false;
            this._x = 0;
            this._y = 0;
            this._width = 0;
            this._height = 0;
            this._rotation = 0;
            this._alpha = 1;
            this._scaleX = 1;
            this._scaleY = 1;
            this._pivotX = 0;
            this._pivotY = 0;
            this._flipX = false;
            this._flipY = false;
            this._tint = 0xFFFFFF;
            this._transformDirty = false;
            this._worldAlpha = 1;
            this._cachedAsBitmap = false;
            this._x = 0;
            this._y = 0;
            this._scaleX = 1;
            this._scaleY = 1;
            this._id = (DisplayObject._idCount++);
            this._debugId = "DisplayObject-" + this._id;
            this._transform = new plume.Matrix();
            this._worldTransform = new plume.Matrix();
            this._hitTransform = new plume.Matrix();
        }
        DisplayObject.prototype.addDebugBorder = function (color) {
            if (color === void 0) { color = "#00FFBB"; }
            this.debug = true;
        };
        Object.defineProperty(DisplayObject.prototype, "tint", {
            get: function () {
                return this._tint;
            },
            set: function (tint) {
                if (tint == null)
                    this._tint = 0xFFFFFF;
                else
                    this._tint = tint;
            },
            enumerable: true,
            configurable: true
        });
        DisplayObject.prototype.draw = function (context) {
            if (!this.visible) {
                return;
            }
            this.computeWorldTransform();
            // Update world alpha
            var palpha = this.getParentAlpha();
            this._worldAlpha = this._alpha * palpha;
            context.globalAlpha = this._worldAlpha;
            context.save();
            this._worldTransform.applyToContext(context);
            // this._transform.transformContext(context);
            if (this._mask != null) {
                this._mask._applyMask(context);
            }
            if (this.cachedAsBitmap) {
                if (this._cachedTexture == null) {
                    this.createCachedTexture();
                }
                this.drawCachedTexture(context);
            }
            else {
                this.draw0(context);
            }
            context.restore();
        };
        DisplayObject.prototype.computeWorldTransform = function () {
            // let dirty = false;
            // Reset and update local transform if dirty
            if (this._transformDirty) {
                // dirty = true;
                this._transformDirty = false;
                this._transform.reset();
                // Compute transform (transformation will be done in the reverse order (read bottom to top))
                // We do:
                // Flip x/y
                // Apply pivot
                // Rotate
                // Scale
                // Undo pivot
                // Translate
                if (this._x != 0 || this._y != 0)
                    this._transform.translate(this._x, this._y);
                if (this._rotation != 0 || this._scaleX != 1 || this._scaleY != 1 || this._flipX || this._flipY) {
                    if (this._pivotX != 0 || this._pivotY != 0)
                        this._transform.translate(this._pivotX, this._pivotY);
                    if (this._rotation != 0)
                        this._transform.rotate(this._rotation);
                    if (this._scaleX != 1 || this._scaleY != 1)
                        this._transform.scale(this._scaleX, this._scaleY);
                    if (this._pivotX != 0 || this._pivotY != 0)
                        this._transform.translate(-this._pivotX, -this._pivotY);
                    if (this._flipX || this._flipY) {
                        this._transform.translate(this._width * (this._flipX ? 1 : 0), this._height * (this._flipY ? 1 : 0));
                        this._transform.scale(this._flipX ? -1 : 1, this._flipY ? -1 : 1);
                    }
                }
            }
            // Update world transform
            var pwt = this.getParentWorldTransform();
            var newWt = new plume.Matrix();
            newWt.setTransform(pwt.a, pwt.b, pwt.c, pwt.d, pwt.e, pwt.f);
            newWt.transform(this._transform.a, this._transform.b, this._transform.c, this._transform.d, this._transform.e, this._transform.f);
            var pht = this.getParentHitTransform();
            var newHt = new plume.Matrix();
            if (pht)
                newHt.setTransform(pht.a, pht.b, pht.c, pht.d, pht.e, pht.f);
            if (!this._root) {
                newHt.transform(this._transform.a, this._transform.b, this._transform.c, this._transform.d, this._transform.e, this._transform.f);
            }
            // Compute interaction limit
            if (this._interactive) {
                var worldChanged = !newHt.isEqual(this._hitTransform);
                if (worldChanged || this._worldHitArea == null) {
                    this.updateWorldHitArea(newHt);
                }
            }
            this._worldTransform = newWt;
            this._hitTransform = newHt;
            // if (dirty && this._debugId == "root") {
            //     console.log("ROOT dirty => ");
            //     console.log("Transform => " + this._transform);
            //     console.log("World Transform => " + this._worldTransform);
            //     console.log("Hit Transform => " + this._hitTransform);
            // }
            // if (dirty && this._debugId == "blue-square") {
            //     console.log("blue dirty => ");
            //     console.log("Transform => " + this._transform);
            //     console.log("World Transform => " + this._worldTransform);
            //     console.log("Hit Transform => " + this._hitTransform);
            //     console.log("Hit area: " + this._worldHitArea);
            // }
        };
        DisplayObject.prototype.getParentWorldTransform = function () {
            if (this._parent == null)
                return new plume.Matrix();
            return this._parent._worldTransform;
        };
        DisplayObject.prototype.getParentAlpha = function () {
            if (this._parent == null)
                return 1;
            return this._parent._worldAlpha;
        };
        DisplayObject.prototype.getParentHitTransform = function () {
            if (this._root) {
                return this._hitTransform;
            }
            if (this._parent)
                return this._parent._hitTransform;
            else
                return null;
        };
        // compute absolute coordiantes
        DisplayObject.prototype.updateWorldHitArea = function (worldTransform) {
            var transformedPoints;
            if (this._hitArea != null) {
                transformedPoints = worldTransform.applyToArray([this._hitArea.x, this._hitArea.y, this._hitArea.x + this._hitArea.width, this._hitArea.y, this._hitArea.x + this._hitArea.width, this._hitArea.y + this._hitArea.height, this._hitArea.x, this._hitArea.y + this._hitArea.height]);
            }
            else {
                if (this._width <= 0 || this.height <= 0) {
                    plume.logger.warn("Cannot compute hit area on no-size object. Check bounds of #" + this.getDebugId() + ". Size:" + this._width + "x" + this._height);
                    return;
                }
                transformedPoints = worldTransform.applyToArray([0, 0, this._width, 0, this._width, this._height, 0, this._height]);
            }
            // logger.debug("New hit area: " + transformedPoints);
            transformedPoints.push(transformedPoints[0]);
            transformedPoints.push(transformedPoints[1]);
            this._worldHitArea = transformedPoints;
        };
        DisplayObject.prototype.draw0 = function (context) {
        };
        DisplayObject.prototype.createCachedTexture = function () {
        };
        DisplayObject.prototype.drawCachedTexture = function (context) {
            context.drawImage(this._cachedTexture.getImpl(), this._cachedTexture.x, this._cachedTexture.y, this._cachedTexture.width, this._cachedTexture.height, 0, 0, this._cachedTexture.width, this._cachedTexture.height);
        };
        DisplayObject.prototype.onDetach = function () {
            if (this._detachListener != null) {
                this._detachListener.fire(true);
            }
        };
        DisplayObject.prototype.removeSelf = function () {
            if (this._parent == null)
                return false;
            var removed = this._parent.removeChild(this);
            return removed != null;
        };
        DisplayObject.prototype.getDebugId = function () {
            return this._debugId;
        };
        DisplayObject.prototype.setDebugId = function (id, appendDynId) {
            if (appendDynId === void 0) { appendDynId = false; }
            this._debugId = id;
            if (appendDynId) {
                this._debugId += this._id;
            }
        };
        DisplayObject.prototype.hitPoint = function (point) {
            if (this._worldHitArea != null && this._worldHitArea.length > 0) {
                return plume.Collisions.pointInPolygon2(this._worldHitArea, point.x, point.y);
            }
            else {
                if (this._transformDirty) {
                    // Not draw yet
                    return false;
                }
                else {
                    plume.logger.error("Using containsPoint without worldHitArea. Check object bounds of #" + this.getDebugId() + ". Size:" + this._width + "x" + this._height + ", Hit area: " + this._hitArea);
                    return this.containsPoint(point);
                }
            }
        };
        DisplayObject.prototype.containsPoint = function (point) {
            return false;
            //return point.x > x && point.x < x + this.width && point.y > y && point.y < y + this.height
        };
        Object.defineProperty(DisplayObject.prototype, "interactive", {
            get: function () {
                return this._interactive;
            },
            set: function (interactive) {
                DisplayObject.interactiveDirty = true;
                this._worldHitArea = null;
                this._interactive = interactive;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "hitArea", {
            get: function () {
                return this._hitArea;
            },
            set: function (rectangle) {
                this._hitArea = rectangle;
                this._worldHitArea = null; // clear world hit area to update it
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "hasInteractiveChild", {
            get: function () {
                return this._hasInteractiveChild;
            },
            set: function (b) {
                this._hasInteractiveChild = b;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "mask", {
            get: function () {
                return this._mask;
            },
            set: function (mask) {
                this._mask = mask;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject, "interactiveDirty", {
            get: function () {
                return this._interactiveDirty;
            },
            set: function (b) {
                this._interactiveDirty = b;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "visible", {
            get: function () {
                return this._visible;
            },
            set: function (visible) {
                this._visible = visible;
            },
            enumerable: true,
            configurable: true
        });
        // Interaction
        DisplayObject.prototype.onClick = function (fn, context, once) {
            this.interactive = true;
            if (once != null && once == true)
                this.clickHandler.once(fn, context);
            else
                this.clickHandler.add(fn, context);
        };
        DisplayObject.prototype.hasDownHandler = function () {
            return this._downHandler != null && this._downHandler.length > 0;
        };
        DisplayObject.prototype.hasUpHandler = function () {
            return this._upHandler != null && this._upHandler.length > 0;
        };
        DisplayObject.prototype.hasOverHandler = function () {
            return this._overHandler != null && this._overHandler.length > 0;
        };
        DisplayObject.prototype.hasOutHandler = function () {
            return this._outHandler != null && this._outHandler.length > 0;
        };
        DisplayObject.prototype.hasClickHandler = function () {
            return this._clickHandler != null && this._clickHandler.length > 0;
        };
        Object.defineProperty(DisplayObject.prototype, "downHandler", {
            get: function () {
                if (this._downHandler == null)
                    this._downHandler = new plume.Handler();
                return this._downHandler;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "upHandler", {
            get: function () {
                if (this._upHandler == null)
                    this._upHandler = new plume.Handler();
                return this._upHandler;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "overHandler", {
            get: function () {
                if (this._overHandler == null)
                    this._overHandler = new plume.Handler();
                return this._overHandler;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "outHandler", {
            get: function () {
                if (this._outHandler == null)
                    this._outHandler = new plume.Handler();
                return this._outHandler;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "clickHandler", {
            get: function () {
                if (this._clickHandler == null)
                    this._clickHandler = new plume.Handler();
                return this._clickHandler;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "detachListener", {
            get: function () {
                if (this._detachListener == null)
                    this._detachListener = new plume.Handler();
                return this._detachListener;
            },
            enumerable: true,
            configurable: true
        });
        // Accessors
        DisplayObject.prototype.setPivot = function (x, y) {
            this._pivotX = x;
            this._pivotY = y;
            this._transformDirty = true;
        };
        DisplayObject.prototype.setPivotToCenter = function () {
            if (this._width <= 0 || this._height <= 0)
                throw new Error("Cannot set pivot to center on display object without size");
            this._pivotX = this._width / 2;
            this._pivotY = this._height / 2;
            this._transformDirty = true;
        };
        Object.defineProperty(DisplayObject.prototype, "x", {
            get: function () {
                return this._x;
            },
            set: function (x) {
                this._x = x;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "y", {
            get: function () {
                return this._y;
            },
            set: function (y) {
                this._y = y;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "alpha", {
            get: function () {
                return this._alpha;
            },
            set: function (alpha) {
                this._alpha = alpha;
                if (this._alpha > 1)
                    this._alpha = 1;
                else if (this._alpha < 0)
                    this.alpha = 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "scaleX", {
            get: function () {
                return this._scaleX;
            },
            set: function (x) {
                this._scaleX = x;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "scaleY", {
            get: function () {
                return this._scaleY;
            },
            set: function (y) {
                this._scaleY = y;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "scaleXY", {
            get: function () {
                return this._scaleX;
            },
            set: function (scale) {
                this._scaleX = scale;
                this._scaleY = scale;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "debug", {
            get: function () {
                return this._debug;
            },
            set: function (debug) {
                this._debug = debug;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "pivotX", {
            get: function () {
                return this._pivotX;
            },
            set: function (x) {
                this._pivotX = x;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "pivotY", {
            get: function () {
                return this._pivotY;
            },
            set: function (y) {
                this._pivotY = y;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "flipX", {
            get: function () {
                return this._flipX;
            },
            set: function (flipX) {
                this._flipX = flipX;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "flipY", {
            get: function () {
                return this._flipY;
            },
            set: function (flipY) {
                this._flipY = flipY;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "angle", {
            get: function () {
                return this._rotation * plume.Mathf.RAD2DEG;
            },
            set: function (deg) {
                this.rotation = deg * plume.Mathf.DEG2RAD;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "width", {
            get: function () {
                return this._width * this._scaleX;
            },
            set: function (width) {
                if (this._width == 0)
                    return;
                this.scaleX = width / this._width;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "height", {
            get: function () {
                return this._height * this._scaleY;
            },
            set: function (height) {
                if (this._height == 0)
                    return;
                this.scaleY = height / this._height;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "parent", {
            get: function () {
                return this._parent;
            },
            set: function (container) {
                throw new Error("parent is readonly");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "rotation", {
            get: function () {
                return this._rotation;
            },
            set: function (rotation) {
                this._rotation = rotation;
                this._transformDirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "cachedAsBitmap", {
            get: function () {
                return this._cachedAsBitmap;
            },
            set: function (cachedAsBitmap) {
                this._cachedAsBitmap = cachedAsBitmap;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "top", {
            get: function () {
                return this.y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "bottom", {
            get: function () {
                return this.y + this.height;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "left", {
            get: function () {
                return this.x;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DisplayObject.prototype, "right", {
            get: function () {
                return this.x + this.width;
            },
            enumerable: true,
            configurable: true
        });
        DisplayObject._interactiveDirty = false;
        DisplayObject._idCount = 0;
        return DisplayObject;
    }());
    plume.DisplayObject = DisplayObject;
})(plume || (plume = {}));
/// <reference path="GUIDisplayObject.ts" />
var plume;
(function (plume) {
    var Sprite = /** @class */ (function (_super) {
        __extends(Sprite, _super);
        function Sprite(texture) {
            var _this = _super.call(this) || this;
            _this._texture = texture;
            _this._debugId = "Sprite-" + _this._id;
            _this.setSizeFromTexture();
            if (!_this.texture.isLoaded()) {
                // Texture not yet loaded
                var self_1 = _this;
                _this.texture.addLoadedHandler(function () {
                    self_1.setSizeFromTexture();
                });
            }
            return _this;
        }
        Sprite.prototype.setSizeFromTexture = function () {
            if (this.texture.trimmed) {
                // Get original sprite size
                var t = this.texture.getTrim();
                this._width = t.width;
                this._height = t.height;
            }
            else {
                this._width = this.texture.width;
                this._height = this.texture.height;
            }
        };
        Sprite.fromFrame = function (frameId) {
            var sprite = new Sprite(plume.Texture.getTexture(frameId));
            return sprite;
        };
        Sprite.fromImage = function (imageId) {
            var texture = plume.Texture.fromImage(imageId);
            var sprite = new Sprite(texture);
            return sprite;
        };
        Sprite.fromTexture = function (texture) {
            return new Sprite(texture);
        };
        Sprite.prototype.setTexture = function (texture) {
            this._texture = texture;
        };
        Sprite.prototype.setFrame = function (frameId) {
            this._texture = plume.Texture.getTexture(frameId);
        };
        Sprite.prototype.addLoadedHandler = function (cb) {
            this.texture.addLoadedHandler(cb);
        };
        Object.defineProperty(Sprite.prototype, "texture", {
            // Accessor
            get: function () {
                return this._texture;
            },
            enumerable: true,
            configurable: true
        });
        // 40 fps with 1500 bunnies
        // 38 fps with 100 bunnies (nexus4)
        Sprite.prototype.draw0 = function (context) {
            var texture = (this._tint == 0xFFFFFF ? this._texture : plume.Tinter.tint(this._texture, this._tint));
            var dx = 0;
            var dy = 0;
            if (this.texture.trimmed) {
                dx = this.texture.getTrim().x;
                dy = this.texture.getTrim().y;
            }
            context.drawImage(texture.getImpl(), texture.x, texture.y, texture.width, texture.height, dx, dy, texture.width, texture.height);
        };
        return Sprite;
    }(plume.DisplayObject));
    plume.Sprite = Sprite;
})(plume || (plume = {}));
/// <reference path="../../primitives/GUISprite.ts" />
var plume;
(function (plume) {
    var Animation = /** @class */ (function (_super) {
        __extends(Animation, _super);
        function Animation(textures, id) {
            var _this = _super.call(this, textures[0]) || this;
            _this.fps = 1000 / 60;
            _this._curIndex = 0;
            _this._textures = [];
            _this._playing = false;
            _this._startTime = 0;
            _this._startFrame = 0;
            _this._lastIndex = -1;
            _this._from = 0;
            _this._to = 0;
            _this._repeats = 1;
            _this.forward = true;
            _this.animationSpeed = 1;
            _this.paused = false;
            _this.pausedDuration = 0;
            var self = _this;
            textures.forEach(function (t) {
                self._textures.push(t);
            });
            plume.Game.get().addUpdatable(_this);
            _this.detachListener.once(_this.destroy, _this);
            if (id != null)
                _this.setDebugId(id);
            return _this;
        }
        Animation.fromFrames = function (frameIds, id) {
            var animation = new Animation(plume.Texture.fromFrames(frameIds), id);
            return animation;
        };
        Animation.fromTextures = function (textures, id) {
            var animation = new Animation(textures, id);
            return animation;
        };
        Animation.prototype.destroy = function () {
            this.stop();
            plume.Game.get().removeUpdatable(this);
        };
        Animation.prototype.setTextures = function (textureIdList) {
            this.setTextures2(plume.Texture.fromFrames(textureIdList));
        };
        Animation.prototype.setTextures2 = function (textures) {
            var wasPlaying = this.isPlaying();
            if (wasPlaying == true) {
                this.stop();
            }
            this._textures = textures;
            this.gotoAndStop(0);
            if (wasPlaying == true) {
                this.play();
            }
        };
        Animation.prototype.setFrameIndex = function (index) {
            var t = this._textures[index];
            this.setTexture(t);
            if (this._playing == false) {
                this._playing = true;
            }
        };
        Animation.prototype.setActiveFrame = function (index) {
            if (index == null)
                return;
            var newIndex;
            if (index < 0)
                newIndex = (index % (this._totalFrames) + this._totalFrames) % this._totalFrames;
            else
                newIndex = index % (this._totalFrames);
            if (newIndex == this._curIndex)
                return;
            this._curIndex = newIndex;
            var frameIndex = this._from + (this._curIndex % this._totalFrames);
            this.setFrameIndex(frameIndex);
            if (this._lastIndex > 0) {
                index = Math.abs(index);
                if (index >= this._lastIndex) {
                    // time to stop
                    this.setFrameIndex(this._to);
                    this._onAnimationEnded();
                }
            }
        };
        Animation.prototype._onAnimationEnded = function () {
            this._playing = false;
            if (this.onEnd)
                this.onEnd();
        };
        Animation.prototype.gotoAndStop = function (frameName) {
            this.setFrameIndex(frameName);
            this._playing = false;
        };
        ;
        Animation.prototype.gotoAndPlay = function (frameName) {
            this.setFrameIndex(frameName);
        };
        ;
        Animation.prototype.update = function (simulationTimeStep) {
            if (this.paused) {
                this.pausedDuration += simulationTimeStep;
            }
            else if (this._playing) {
                var frameEllapsed = Math.floor((plume.Time.now() - this._startTime) / this.fps);
                frameEllapsed = Math.floor(frameEllapsed * this.animationSpeed);
                var frame = this._startFrame;
                if (this.forward) {
                    frame += frameEllapsed;
                }
                else {
                    frame -= frameEllapsed;
                }
                this.setActiveFrame(frame);
            }
        };
        Animation.prototype.getTotalFrames = function () {
            return this._totalFrames;
        };
        Animation.prototype.getCurrentFrame = function () {
            return this._curIndex;
        };
        Animation.prototype._play0 = function () {
            this._playing = true;
            this._startTime = plume.Time.now();
            this._startFrame = this._curIndex;
        };
        Animation.prototype.play = function (from, to) {
            if (from === void 0) { from = 0; }
            if (to === void 0) { to = 0; }
            this._from = from;
            if (to > 0)
                this._to = to;
            else
                this._to = this._textures.length - 1;
            this._totalFrames = (this._to - this._from) + 1;
            this._curIndex = from;
            this.setFrameIndex(from);
            if (this._repeats < 0) {
                this._lastIndex = -1;
            }
            else {
                this._lastIndex = this._curIndex + this._totalFrames * this._repeats;
            }
            this._play0();
        };
        Animation.prototype.repeat = function (repeats) {
            if (repeats === void 0) { repeats = 0; }
            this._repeats = repeats;
        };
        Animation.prototype.isPlaying = function () {
            return this._playing;
        };
        Animation.prototype.stop = function () {
            this._playing = false;
        };
        Animation.prototype.pause = function () {
            this.paused = true;
        };
        Animation.prototype.resume = function () {
            this._startTime += this.pausedDuration;
            this.pausedDuration = 0;
            this.paused = false;
        };
        return Animation;
    }(plume.Sprite));
    plume.Animation = Animation;
})(plume || (plume = {}));
/// <reference path="GUIDisplayObject.ts" />
var plume;
(function (plume) {
    var Container = /** @class */ (function (_super) {
        __extends(Container, _super);
        function Container(width, height) {
            if (width === void 0) { width = 0; }
            if (height === void 0) { height = 0; }
            var _this = _super.call(this) || this;
            _this._children = [];
            _this._debugId = "Container-" + _this._id;
            _this._width = width;
            _this._height = height;
            return _this;
        }
        Container.prototype.addChild = function (child) {
            var object = this.addChildAt(child, this._children.length);
            return object;
        };
        Container.prototype.addChildAt = function (child, index) {
            // virtual update
            if (child._parent) {
                if (child._parent == this) {
                    console.error("Child already in H5Container. Use setChildIndex.");
                    return child;
                }
                child._parent.removeChild(child);
            }
            this._children.splice(index, 0, child);
            child._parent = this;
            return child;
        };
        Container.prototype.removeChild = function (child) {
            var index = this._children.indexOf(child);
            if (index === -1)
                return null;
            return this.removeChildAt(index);
        };
        Container.prototype.removeChildAt = function (index) {
            // virtual update
            var child = this.getChildAt(index);
            child._parent = null;
            child.onDetach();
            this._children.splice(index, 1);
            return child;
        };
        Container.prototype.removeChildren = function (beginIndex, endIndex) {
            // virtual update
            var start = (beginIndex == null ? 0 : beginIndex);
            var end = (endIndex == null ? this._children.length : endIndex);
            var range = end - start;
            if (range <= 0) {
                return [];
            }
            var removed = this._children.splice(start, range);
            for (var i = 0; i < removed.length; i++) {
                var child = removed[i];
                child._parent = null;
                child.onDetach();
            }
            return removed;
        };
        Container.prototype.getChildIndex = function (child) {
            var index = this._children.indexOf(child);
            return index;
        };
        Container.prototype.getChildById = function (id) {
            for (var i = 0; i < this._children.length; i++) {
                var c = this._children[i];
                if (c.getDebugId() == id) {
                    return c;
                }
            }
            return null;
        };
        Container.prototype.setChildIndex = function (child, index) {
            var currentIndex = this.getChildIndex(child);
            if (currentIndex == index) {
                console.error("Child " + child.getDebugId() + "already @ that index : " + index);
                return;
            }
            this._children.splice(currentIndex, 1); // Remove from old position
            this._children.splice(index, 0, child); // Add at new position
        };
        // onDetach called also on all children
        Container.prototype.onDetach = function () {
            _super.prototype.onDetach.call(this);
            var count = this._children.length;
            for (var i = 0; i < count; i++) {
                this._children[i].onDetach();
            }
        };
        Container.prototype.getChildAt = function (index) {
            var child = this._children[index];
            return child;
        };
        Container.prototype.getChildrenCount = function () {
            return this._children.length;
        };
        Container.prototype._outputToConsole = function (level, properties) {
            if (properties === void 0) { properties = []; }
            var spaces = '';
            for (var i = 0; i < level; i++)
                spaces += '  ';
            for (var i = 0; i < this._children.length; i++) {
                var child = this._children[i];
                console.log(spaces + this._getDebugPropertyString(child, properties));
                if (child instanceof Container) {
                    child._outputToConsole(level++, properties);
                }
            }
        };
        Container.prototype._countElements = function (visible) {
            if (visible === void 0) { visible = false; }
            if (visible && !this.visible)
                return 0;
            var sum = 1;
            for (var i = 0; i < this._children.length; i++) {
                var child = this._children[i];
                if (child instanceof Container) {
                    sum = sum + child._countElements();
                }
                else {
                    sum = sum + 1;
                }
            }
            return sum;
        };
        Container.prototype._getDebugPropertyString = function (o, properties) {
            var str = o.getDebugId() + " ";
            properties.forEach(function (p) {
                str += p + "=" + o[p] + " ";
            });
            return str;
        };
        Container.prototype.draw0 = function (context) {
            _super.prototype.draw0.call(this, context);
            for (var i = 0; i < this._children.length; i++) {
                var child = this._children[i];
                child.draw(context);
            }
        };
        Container.prototype.setSize = function (w, h) {
            this._width = w;
            this._height = h;
        };
        Container.prototype.setSizeFromChildren = function (recursive) {
            if (recursive === void 0) { recursive = false; }
            var bounds = plume.layout.computeBounds(this, recursive);
            this.setSize(bounds.width, bounds.height);
        };
        return Container;
    }(plume.DisplayObject));
    plume.Container = Container;
})(plume || (plume = {}));
/// <reference path="../../primitives/GUIContainer.ts" />
var plume;
(function (plume) {
    var Button = /** @class */ (function (_super) {
        __extends(Button, _super);
        function Button() {
            var _this = _super.call(this) || this;
            _this._enabled = true;
            _this.disableAlpha = 0.5;
            _this.interactive = true;
            _this.downHandler.add(_this.onStateDown, _this);
            _this.upHandler.add(_this.onStateUp, _this);
            _this.overHandler.add(_this.onStateOver, _this);
            _this.outHandler.add(_this.onStateOut, _this);
            _this.enable();
            return _this;
        }
        Button.prototype.enable = function () {
            this._enabled = true;
            this.interactive = true;
            this.alpha = 1;
        };
        Button.prototype.disable = function () {
            this._enabled = false;
            this.interactive = false;
            this.alpha = this.disableAlpha;
        };
        Button.prototype.onStateDown = function () {
            this._down = true;
        };
        Button.prototype.onStateUp = function () {
            this._over = false; // for mobile, do not maintain "over" state when releasing button
            this._down = false;
        };
        Button.prototype.onStateOver = function () {
            this._over = true;
        };
        Button.prototype.onStateOut = function () {
            this._over = false;
            this._down = false;
        };
        return Button;
    }(plume.Container));
    plume.Button = Button;
})(plume || (plume = {}));
/// <reference path="GUIButton.ts" />
var plume;
(function (plume) {
    var ImageButtonStyle = /** @class */ (function () {
        function ImageButtonStyle(stateUp, stateDown, stateOver) {
            this._stateUp = stateUp;
            this._stateDown = stateDown;
            this._stateOver = stateOver;
        }
        ImageButtonStyle.fromFrames = function (stateUp, stateDown, stateOver) {
            var up = plume.Sprite.fromFrame(stateUp);
            var down = null;
            var over = null;
            if (stateDown != null)
                down = plume.Sprite.fromFrame(stateDown);
            if (stateOver != null)
                over = plume.Sprite.fromFrame(stateOver);
            var style = new ImageButtonStyle(up, down, over);
            return style;
        };
        ImageButtonStyle.fromTextures = function (stateUp, stateDown, stateOver) {
            var up = plume.Sprite.fromTexture(stateUp);
            var down = null;
            var over = null;
            if (stateDown != null)
                down = plume.Sprite.fromTexture(stateDown);
            if (stateOver != null)
                over = plume.Sprite.fromTexture(stateOver);
            var style = new ImageButtonStyle(up, down, over);
            return style;
        };
        return ImageButtonStyle;
    }());
    plume.ImageButtonStyle = ImageButtonStyle;
    var ImageButton = /** @class */ (function (_super) {
        __extends(ImageButton, _super);
        function ImageButton(style) {
            var _this = _super.call(this) || this;
            _this._text = null;
            _this._icon = null;
            _this._style = style;
            _this.addChild(_this._style._stateDown);
            _this.addChild(_this._style._stateOver);
            _this.addChild(_this._style._stateUp);
            _this.setSize(_this._style._stateUp.width, _this._style._stateUp.height);
            return _this;
        }
        ImageButton.fromFramesArray = function (states) {
            var length = states.length;
            if (length < 1 || length > 3) {
                throw new Error("Invalid states array size");
            }
            var up = states[0];
            var down = null;
            var over = null;
            if (length > 1)
                down = states[1];
            if (length > 2)
                over = states[2];
            return ImageButton.fromFrames(up, down, over);
        };
        ImageButton.fromFrames = function (stateUp, stateDown, stateOver) {
            var style = ImageButtonStyle.fromFrames(stateUp, stateDown, stateOver);
            return new ImageButton(style);
        };
        ImageButton.fromTextures = function (stateUp, stateDown, stateOver) {
            var style = ImageButtonStyle.fromTextures(stateUp, stateDown, stateOver);
            return new ImageButton(style);
        };
        ImageButton.prototype.setFrames = function (stateUp, stateDown, stateOver) {
            if (this._style._stateDown)
                this._style._stateDown.removeSelf();
            if (this._style._stateOver)
                this._style._stateOver.removeSelf();
            if (this._style._stateUp)
                this._style._stateUp.removeSelf();
            this._style = ImageButtonStyle.fromFrames(stateUp, stateDown, stateOver);
            this.addChildAt(this._style._stateUp, 0);
            if (this._style._stateDown)
                this.addChildAt(this._style._stateDown, 0);
            if (this._style._stateOver)
                this.addChildAt(this._style._stateOver, 0);
        };
        ImageButton.prototype.setFramesArray = function (states) {
            var stateUp = states[0];
            var stateDown = null;
            var stateOver = null;
            if (states.length > 1) {
                stateDown = states[1];
            }
            if (states.length > 2) {
                stateOver = states[2];
            }
            this.setFrames(stateUp, stateDown, stateOver);
        };
        ImageButton.prototype.setContent = function (content) {
            this.addChild(content);
            plume.layout.centerOnX(content, this, false);
            plume.layout.centerOnY(content, this, false);
        };
        ImageButton.prototype.setText = function (text) {
            if (this._text != null)
                this._text.removeSelf();
            this._text = text;
            // add text to group
            this.addChild(text);
            // center text
            plume.layout.centerOnX(text, this, false);
            plume.layout.centerOnY(text, this, false);
        };
        ImageButton.prototype.setIcon = function (frame, scale) {
            if (this._icon != null)
                this._icon.removeSelf();
            this._icon = plume.Sprite.fromFrame(frame);
            if (scale != null)
                this._icon.scaleXY = scale;
            plume.layout.centerOnX(this._icon, this, false);
            plume.layout.centerOnY(this._icon, this, false);
            this.addChild(this._icon);
        };
        ImageButton.prototype.setStateTexture = function () {
            if (this._style._stateDown)
                this._style._stateDown.visible = false;
            if (this._style._stateOver)
                this._style._stateDown.visible = false;
            this._style._stateUp.visible = false;
            if (this._down) {
                if (this._style._stateDown) {
                    this._style._stateDown.visible = true;
                }
                return;
            }
            if (this._over) {
                if (this._style._stateOver) {
                    this._style._stateOver.visible = true;
                }
                return;
            }
            if (this._style._stateUp) {
                this._style._stateUp.visible = true;
            }
        };
        ImageButton.prototype.onStateDown = function () {
            _super.prototype.onStateDown.call(this);
            this.setStateTexture();
        };
        ImageButton.prototype.onStateUp = function () {
            _super.prototype.onStateUp.call(this);
            this.setStateTexture();
        };
        ImageButton.prototype.onStateOver = function () {
            _super.prototype.onStateOver.call(this);
            this.setStateTexture();
        };
        ImageButton.prototype.onStateOut = function () {
            _super.prototype.onStateOut.call(this);
            this.setStateTexture();
        };
        Object.defineProperty(ImageButton.prototype, "style", {
            get: function () {
                return this._style;
            },
            enumerable: true,
            configurable: true
        });
        return ImageButton;
    }(plume.Button));
    plume.ImageButton = ImageButton;
})(plume || (plume = {}));
/// <reference path="../../primitives/GuiContainer.ts" />
var plume;
(function (plume) {
    var ContainerGroup = /** @class */ (function () {
        function ContainerGroup() {
            this._childs = new plume.HashMap();
            this._dirty = false;
            this._root = new plume.Container();
            this._root._root = true;
            this._root.setDebugId("root");
        }
        ContainerGroup.prototype.destroy = function () {
            this._childs.clear();
        };
        ContainerGroup.prototype.update = function () {
            if (this._dirty) {
                this._sortLayers();
                this._dirty = false;
            }
        };
        // create child if not there
        ContainerGroup.prototype.getChild = function (name) {
            var layer = this._ensureLayer(name);
            return layer.container;
        };
        ContainerGroup.prototype.addChild = function (name, index) {
            if (index === void 0) { index = 1; }
            var layer = this._ensureLayer(name);
            layer.zindex = index;
            return layer.container;
        };
        ContainerGroup.prototype.removeChild = function (name) {
            var layer = this._childs.remove(name);
            if (layer != null) {
                this._root.removeChild(layer.container);
            }
            return layer.container;
        };
        ContainerGroup.prototype.setChildIndex = function (name, zindex) {
            var layer = this._ensureLayer(name);
            layer.zindex = zindex;
            this._dirty = true;
        };
        ContainerGroup.prototype._sortLayers = function () {
            var layers = this._childs.values();
            var sorted = layers.sort(function (l1, l2) {
                return l1.zindex - l2.zindex;
            });
            for (var i = 0; i < sorted.length; i++) {
                var layer = sorted[i];
                var current = this._root.getChildIndex(layer.container);
                if (current != i) {
                    this._root.setChildIndex(layer.container, i);
                }
            }
        };
        ContainerGroup.prototype._ensureLayer = function (name) {
            var layer = this._childs.get(name);
            if (layer == null) {
                layer = new ContainerGroupData(name);
                this._root.addChild(layer.container);
                this._childs.put(name, layer);
                this._dirty = true;
            }
            return layer;
        };
        Object.defineProperty(ContainerGroup.prototype, "root", {
            get: function () {
                return this._root;
            },
            set: function (root) {
                throw new Error("root is read-only");
            },
            enumerable: true,
            configurable: true
        });
        return ContainerGroup;
    }());
    plume.ContainerGroup = ContainerGroup;
    var ContainerGroupData = /** @class */ (function () {
        function ContainerGroupData(name) {
            this.container = new plume.Container();
            this.container.setDebugId(name);
            this.zindex = 1;
            this.name = name;
        }
        return ContainerGroupData;
    }());
})(plume || (plume = {}));
/// <reference path="GUIContainerGroup.ts" />
var plume;
(function (plume) {
    // Basic layer for a game
    var GameContainerGroup = /** @class */ (function (_super) {
        __extends(GameContainerGroup, _super);
        function GameContainerGroup() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Object.defineProperty(GameContainerGroup.prototype, "background", {
            get: function () {
                if (this._background == null) {
                    this._background = this.addChild("background", 1);
                }
                return this._background;
            },
            set: function (container) {
                throw new Error("background is read-only");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameContainerGroup.prototype, "scene", {
            get: function () {
                if (this._scene == null) {
                    this._scene = this.addChild("scene", 100);
                }
                return this._scene;
            },
            set: function (container) {
                throw new Error("scene is read-only");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameContainerGroup.prototype, "gameplayHUD", {
            get: function () {
                if (this._gameplayHUD == null) {
                    this._gameplayHUD = this.addChild("gameplayHUD", 150);
                }
                return this._gameplayHUD;
            },
            set: function (container) {
                throw new Error("gameplayHUD is read-only");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameContainerGroup.prototype, "sceneHUD", {
            get: function () {
                if (this._gameplayHUD == null) {
                    this._gameplayHUD = this.addChild("sceneHUD", 180);
                }
                return this._gameplayHUD;
            },
            set: function (container) {
                throw new Error("sceneHUD is read-only");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameContainerGroup.prototype, "popup", {
            get: function () {
                if (this._popup == null) {
                    this._popup = this.addChild("popup", 200);
                }
                return this._popup;
            },
            set: function (container) {
                throw new Error("popup is read-only");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameContainerGroup.prototype, "top", {
            get: function () {
                if (this._top == null) {
                    this._top = this.addChild("top", 300);
                }
                return this._top;
            },
            set: function (container) {
                throw new Error("top is read-only");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GameContainerGroup.prototype, "technical", {
            get: function () {
                if (this._technical == null) {
                    this._technical = this.addChild("technical", 400);
                }
                return this._technical;
            },
            set: function (container) {
                throw new Error("technical is read-only");
            },
            enumerable: true,
            configurable: true
        });
        return GameContainerGroup;
    }(plume.ContainerGroup));
    plume.GameContainerGroup = GameContainerGroup;
})(plume || (plume = {}));
// module plume {
//     export class ExpandedBounds {
//         private static INSTANCE: ExpandedBounds;
//         static getInstance(): ExpandedBounds {
//             return this.INSTANCE;
//         }
//         static get(): Rectangle {
//             return this.getInstance().bounds;
//         }
//         private game: Game;
//         private sm: ScaleManager;
//         public bounds = new h5ge.Rectangle(0, 0, 0, 0);
//         constructor(public maxWidth: number = null, public maxHeight: number = null) {
//             ExpandedBounds.INSTANCE = this;
//             this.game = Game.get();
//             this.sm = this.game.scaleManager;
//             this.sm.resizeHandler.add(this.onResize, this);
//             this.onResize();
//         }
//         private onResize() {
//             let scaling = this.sm.getCurrentScaling();
//             let marginX = scaling.x / scaling.scale;
//             let marginY = scaling.y / scaling.scale;
//             if (this.maxWidth != null) {
//                 let totalWidth = this.game.width + 2 * marginX;
//                 if (totalWidth > this.maxWidth) {
//                     marginX = marginX - (totalWidth - this.maxWidth) / 2;
//                 }
//             }
//             if (this.maxHeight != null) {
//                 let totalHeight = this.game.height + 2 * marginY;
//                 if (totalHeight > this.maxHeight) {
//                     marginY = marginY - (totalHeight - this.maxHeight) / 2;
//                 }
//             }
//             this.bounds = new h5ge.Rectangle(-marginX, -marginY, this.game.width + 2 * marginX, this.game.height + 2 * marginY);
//         }
//     }
// }
var plume;
(function (plume) {
    var layout;
    (function (layout) {
        function centerOnX(source, container, absolute) {
            if (absolute === void 0) { absolute = true; }
            var dx = Math.round((container.width - source.width) / 2);
            source.x = absolute ? container.x + dx : dx;
        }
        layout.centerOnX = centerOnX;
        function centerOnY(source, container, absolute) {
            if (absolute === void 0) { absolute = true; }
            var dy = Math.round((container.height - source.height) / 2);
            source.y = absolute ? container.y + dy : dy;
        }
        layout.centerOnY = centerOnY;
        function centerOnXInParent(obj) {
            centerOnX(obj, obj.parent, false);
        }
        layout.centerOnXInParent = centerOnXInParent;
        function centerOnYInParent(obj) {
            centerOnY(obj, obj.parent, false);
        }
        layout.centerOnYInParent = centerOnYInParent;
        function centerOnXYInParent(obj) {
            centerOnX(obj, obj.parent, false);
            centerOnY(obj, obj.parent, false);
        }
        layout.centerOnXYInParent = centerOnXYInParent;
        function computeBounds(container, recursive) {
            if (recursive === void 0) { recursive = false; }
            var left = Number.POSITIVE_INFINITY;
            var top = Number.POSITIVE_INFINITY;
            var bottom = Number.NEGATIVE_INFINITY;
            var right = Number.NEGATIVE_INFINITY;
            for (var i = 0; i < container.getChildrenCount(); i++) {
                var child = container.getChildAt(i);
                var bounds = void 0;
                if (recursive && child instanceof plume.Container) {
                    bounds = computeBounds(child, true);
                }
                else {
                    bounds = child;
                }
                left = Math.min(left, bounds.x);
                top = Math.min(top, bounds.y);
                if (bounds.width > 0)
                    right = Math.max(right, bounds.x + bounds.width);
                if (bounds.height > 0)
                    bottom = Math.max(bottom, bounds.y + bounds.height);
            }
            return {
                x: left,
                y: top,
                width: (right - left),
                height: (bottom - top)
            };
        }
        layout.computeBounds = computeBounds;
        function setContainerSize(container, recursive) {
            if (recursive === void 0) { recursive = false; }
            var bounds = computeBounds(container, recursive);
            container.setSize(bounds.width, bounds.height);
        }
        layout.setContainerSize = setContainerSize;
    })(layout = plume.layout || (plume.layout = {}));
})(plume || (plume = {}));
// module plume {
//     export class ScrollableList extends Container {
//         private _upOrLeft: Button;
//         private _downOrRight: Button;
//         private _itemSize: number;
//         private _items: Array<DisplayObject> = [];
//         private _scrollableContainer: ScrollableContainer;
//         private _contentBounds: Rectangle;
//         private _topItemOverlay: Graphics;
//         private _bottomItemOverlay: Graphics;
//         //
//         optShowItemOverlay = true;
//         constructor(private _itemWidth: number, private _itemHeight: number, private _visibleItems: number, private _vertical: boolean) {
//             super();
//             var self = this;
//             this._scrollableContainer = new ScrollableContainer();
//             this.addChild(this._scrollableContainer);
//             var visibleWidth = _itemWidth;
//             var visibleHeight = _itemHeight;
//             if (_vertical) {
//                 this._itemSize = _itemHeight;
//                 visibleHeight = _itemHeight * _visibleItems;
//                 this._scrollableContainer.scrollEnableOnX = false;
//             } else {
//                 this._itemSize = _itemWidth;
//                 visibleWidth = _itemWidth * _visibleItems;
//                 this._scrollableContainer.scrollEnableOnY = false;
//             }
//             this._scrollableContainer.setContainerSize(visibleWidth, visibleHeight);
//             this._scrollableContainer.addScrollHandler(this.onScroll, this);
//             this._scrollableContainer.wheelOnY = this._vertical;
//             this._contentBounds = new Rectangle(0, 0, visibleWidth, visibleHeight);
//             this.setSize(visibleWidth, visibleHeight);
//             this.updateVisibleItems(0);
//         }
//         getContentBounds(): Rectangle {
//             return this._contentBounds;
//         }
//         addItem(item: DisplayObject) {
//             var position = this._items.length;
//             // update x/y before adding to scrollable container to have accurate bounds
//             if (this._vertical) {
//                 item.x = 0;
//                 item.y = position * this._itemHeight;
//             } else {
//                 item.x = position * this._itemWidth;
//                 item.y = 0;
//             }
//             this._scrollableContainer.addDisplayObject(item);
//             this._items.push(item);
//             this.updateVisibleItems();
//         }
//         clear() {
//             this._scrollableContainer.clear();
//             this._items = [];
//             this.updateVisibleItems(0);
//         }
//         // keeping button right now, but better to remove them and do cleaner UI with scrolling indicator (ie: scrollbar or just cutting next item)
//         setControlButton(upOrLeft: Button, downOrRight: Button) {
//             var self = this;
//             this._upOrLeft = upOrLeft;
//             this._downOrRight = downOrRight;
//             this._upOrLeft.onClick(function () {
//                 self.moveUpOrLeft();
//             });
//             this._downOrRight.onClick(function () {
//                 self.moveDownOrRight();
//             });
//         }
//         moveUpOrLeft() {
//             if (this._vertical) this.moveUp();
//             else this.moveLeft();
//         }
//         moveDownOrRight() {
//             if (this._vertical) this.moveDown();
//             else this.moveRight();
//         }
//         moveUp() {
//             if (!this._vertical) return;
//             this.moveUp0();
//         }
//         moveDown() {
//             if (!this._vertical) return;
//             this.moveDown0();
//         }
//         moveLeft() {
//             if (this._vertical) return;
//             this.moveUp0();
//         }
//         moveRight() {
//             if (this._vertical) return;
//             this.moveDown0();
//         }
//         scrollToItem(topIndex: number) {
//             if (topIndex > (this._items.length - this._visibleItems)) {
//                 topIndex = this._items.length - this._visibleItems;
//             }
//             if (topIndex >= 0) {
//                 this.moveTo(topIndex);
//             }
//         }
//         getItems(): Array<DisplayObject> {
//             return this._items;
//         }
//         private onScroll(offsetX: number, offsetY: number, x: number, y: number) {
//             this.updateVisibleItems();
//         }
//         private moveUp0() {
//             var move = this.getMoveByCount();
//             this.moveBy(-move);
//         }
//         private moveDown0() {
//             var move = this.getMoveByCount();
//             this.moveBy(move);
//         }
//         private getMoveByCount() {
//             if (this._visibleItems > 3) return this._visibleItems - 2;
//             return 1;
//         }
//         private moveBy(count: number) {
//             var offset = count * this._itemSize;
//             if (this._vertical) this._scrollableContainer.moveByAbout(null, offset);
//             else this._scrollableContainer.moveByAbout(offset, null);
//             this.updateVisibleItems();
//         }
//         private moveTo(index: number) {
//             var position = index * this._itemSize;
//             if (this._vertical) this._scrollableContainer.moveTo(null, position);
//             else this._scrollableContainer.moveTo(position, null);
//             this.updateVisibleItems();
//         }
//         private updateVisibleItems(top: number = null) {
//             if (top == null) {
//                 var cb = this._scrollableContainer.getContentBound();
//                 top = (this._vertical ? cb.y : cb.x);
//             }
//             if (this._topItemOverlay != null) this._topItemOverlay.visible = false;
//             if (this._bottomItemOverlay != null) this._bottomItemOverlay.visible = false;
//             var bottom = top + this._visibleItems * this._itemSize;
//             for (var i = 0; i < this._items.length; i++) {
//                 var item = this._items[i];
//                 var l1 = this._vertical ? item.y : item.x;
//                 var l2 = this._vertical ? (item.y + this._itemSize) : (item.x + this._itemSize);
//                 if (l1 >= top && l2 <= bottom) {
//                     // fully visible
//                     item.visible = true;
//                 } else {
//                     if (l1 >= bottom || l2 <= top) {
//                         item.visible = false;
//                     } else {
//                         // cut
//                         item.visible = true;
//                         if ((l1 + this._itemSize / 2) < top) {
//                             this.addTopItemOverlay(item);
//                         } else if ((l2 - this._itemSize / 2) > bottom) {
//                             this.addBottomItemOverlay(item);
//                         }
//                     }
//                 }
//             }
//         }
//         private addTopItemOverlay(item: DisplayObject) {
//             if (!this.optShowItemOverlay) return;
//             if (this._topItemOverlay == null) {
//                 this._topItemOverlay = this.buildItemOverlay(item);
//                 this._scrollableContainer.addDisplayObject(this._topItemOverlay);
//             }
//             this._topItemOverlay.visible = true;
//             this._topItemOverlay.x = item.x;
//             this._topItemOverlay.y = item.y;
//         }
//         private addBottomItemOverlay(item: DisplayObject) {
//             if (!this.optShowItemOverlay) return;
//             if (this._bottomItemOverlay == null) {
//                 this._bottomItemOverlay = this.buildItemOverlay(item);
//                 this._scrollableContainer.addDisplayObject(this._bottomItemOverlay);
//             }
//             this._bottomItemOverlay.visible = true;
//             this._bottomItemOverlay.x = item.x;
//             this._bottomItemOverlay.y = item.y;
//         }
//         private buildItemOverlay(item: DisplayObject): Graphics {
//             var w = this._itemWidth;
//             var h = this._itemWidth;
//             if (item instanceof Container) {
//                 w = Math.min(this._itemWidth, item.width);
//                 h = Math.min(this._itemHeight, item.height);
//             }
//             var overlay = new Graphics();
//             overlay.beginFill(0x000000, 0.2);
//             overlay.drawRect(0, 0, w, h);
//             overlay.endFill();
//             overlay.showCursor = false;
//             return overlay;
//         }
//     }
// }   
var plume;
(function (plume) {
    var Mask = /** @class */ (function () {
        function Mask() {
            this._command = [];
        }
        Mask.prototype.moveTo = function (x, y) {
            this._command.push(-1 /* MoveTo */);
            this._command.push(x);
            this._command.push(y);
        };
        Mask.prototype.lineTo = function (x, y) {
            this._command.push(-2 /* LineTo */);
            this._command.push(x);
            this._command.push(y);
        };
        Mask.prototype.rectangle = function (x, y, width, height) {
            this._command.push(-3 /* Rectangle */);
            this._command.push(x);
            this._command.push(y);
            this._command.push(width);
            this._command.push(height);
        };
        Mask.prototype.arc = function (cx, cy, radius, startAngle, endAngle) {
            if (startAngle === void 0) { startAngle = 0; }
            if (endAngle === void 0) { endAngle = 2 * Math.PI; }
            this._command.push(-4 /* Arc */);
            this._command.push(cx);
            this._command.push(cy);
            this._command.push(radius);
            this._command.push(startAngle);
            this._command.push(endAngle);
        };
        Mask.prototype._applyMask = function (context) {
            context.beginPath();
            for (var i = 0; i < this._command.length; i++) {
                var action = this._command[i];
                if (action == -1 /* MoveTo */) {
                    context.moveTo(this._command[i + 1], this._command[i + 2]);
                    i += 2;
                }
                else if (action == -2 /* LineTo */) {
                    context.lineTo(this._command[i + 1], this._command[i + 2]);
                    i += 2;
                }
                else if (action == -3 /* Rectangle */) {
                    context.rect(this._command[i + 1], this._command[i + 2], this._command[i + 3], this._command[i + 4]);
                    i += 4;
                }
                else if (action == -4 /* Arc */) {
                    context.arc(this._command[i + 1], this._command[i + 2], this._command[i + 3], this._command[i + 4], this._command[i + 5]);
                    i += 5;
                }
            }
            context.clip();
        };
        return Mask;
    }());
    plume.Mask = Mask;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var AbstractProgressBar = /** @class */ (function () {
        function AbstractProgressBar(_background, _progress) {
            if (_progress === void 0) { _progress = 0; }
            this._background = _background;
            this._progress = _progress;
            this._lastProgress = -1;
            // time limited
            this._startTime = 0;
            this._duration = 0;
            this._expired = false;
            this._started = false;
            this._autoUpdate();
        }
        AbstractProgressBar.prototype.startTimeLimited = function (duration, ellapsed) {
            if (ellapsed === void 0) { ellapsed = 0; }
            this._progress = ellapsed / duration;
            this._duration = duration;
            this._startTime = performance.now() - ellapsed;
            this._started = true;
        };
        // manual progress update (between 0 and 1)
        AbstractProgressBar.prototype.setProgress = function (progress) {
            if (progress < 0)
                progress = 0;
            else if (progress > 1)
                progress = 1;
            this._progress = progress;
        };
        // automatically called
        AbstractProgressBar.prototype.update = function (dt) {
            if (this._expired)
                return;
            if (!this._started)
                return;
            if (this._startTime == 0)
                return;
            var now = performance.now();
            this._progress = (now - this._startTime) / this._duration;
            this._progress = Math.min(this._progress, 1);
            if (this._progress == 1) {
                this._onExpired();
            }
        };
        // automatically called
        AbstractProgressBar.prototype.render = function (interpolation) {
        };
        AbstractProgressBar.prototype.getProgress = function () {
            return this._progress;
        };
        AbstractProgressBar.prototype._onExpired = function () {
            this._expired = true;
            this._started = false;
            if (this.onProgressExpired != null) {
                this.onProgressExpired();
            }
        };
        AbstractProgressBar.prototype._autoUpdate = function () {
            var self = this;
            var game = plume.Game.get();
            game.addRenderable(this);
            game.addUpdatable(this);
            this._background.detachListener.once(function () {
                game.removeRenderable(self);
                game.removeUpdatable(self);
            });
        };
        return AbstractProgressBar;
    }());
    plume.AbstractProgressBar = AbstractProgressBar;
})(plume || (plume = {}));
/// <reference path="GuiAbstractProgressBar.ts" />
var plume;
(function (plume) {
    var ProgressBar = /** @class */ (function (_super) {
        __extends(ProgressBar, _super);
        function ProgressBar(_background, _way, _progress) {
            if (_way === void 0) { _way = 0 /* LeftToRight */; }
            if (_progress === void 0) { _progress = 0; }
            var _this = _super.call(this, _background, _progress) || this;
            _this._way = _way;
            _this._bgwidth = 0;
            _this._bgheight = 0;
            _this._inverted = false;
            _this._bgwidth = _background.width;
            _this._bgheight = _background.height;
            return _this;
        }
        // progress between 0 and 1
        ProgressBar.prototype.updateProgress = function (progress) {
            if (progress < 0)
                progress = 0;
            else if (progress > 1)
                progress = 1;
            this._progress = progress;
        };
        ProgressBar.prototype.update = function (dt) {
            _super.prototype.update.call(this, dt);
        };
        ProgressBar.prototype.render = function (interpolation) {
            _super.prototype.render.call(this, interpolation);
            if (this._progress == this._lastProgress)
                return;
            this._background.mask = new plume.Mask();
            var mX, mY, mW, mH;
            if (this._way == 0 /* LeftToRight */) {
                mX = 0;
                mY = 0;
                mW = this._bgwidth * this._progress;
                mH = this._bgheight;
                if (this._inverted) {
                    mX = mW;
                    mW = this._bgwidth - mW;
                }
            }
            else if (this._way == 1 /* RightToLeft */) {
                var w = this._background.width * this._progress;
                mX = this._bgwidth - w;
                mY = 0;
                mW = w;
                mH = this._bgheight;
                if (this._inverted) {
                    mW = mX;
                    mX = 0;
                }
            }
            else if (this._way == 2 /* TopToBottom */) {
                mX = 0;
                mY = 0;
                mW = this._bgwidth;
                mH = this._bgheight * this._progress;
                if (this._inverted) {
                    mY = mH;
                    mH = this._bgheight - mH;
                }
            }
            else if (this._way == 3 /* BottomToTop */) {
                var h = this._bgheight * this._progress;
                mX = 0;
                mY = this._bgheight - h;
                mW = this._bgwidth;
                mH = h;
                if (this._inverted) {
                    mH = mY;
                    mY = 0;
                }
            }
            this._background.mask.rectangle(mX, mY, mW, mH);
            this._lastProgress = this._progress;
        };
        ProgressBar.prototype.setInverted = function (inverted) {
            this._inverted = true;
        };
        return ProgressBar;
    }(plume.AbstractProgressBar));
    plume.ProgressBar = ProgressBar;
})(plume || (plume = {}));
/// <reference path="GuiAbstractProgressBar.ts" />
var plume;
(function (plume) {
    var RoundProgressBar = /** @class */ (function (_super) {
        __extends(RoundProgressBar, _super);
        function RoundProgressBar(_background, _progress) {
            if (_progress === void 0) { _progress = 0; }
            var _this = _super.call(this, _background, _progress) || this;
            _this.start = -Math.PI / 2;
            _this._inverted = false;
            _this.radius = _background.width / 2;
            _this.centerX = _this.radius;
            _this.centerY = _this.radius;
            return _this;
        }
        RoundProgressBar.prototype.setInverted = function (inverted) {
            this._inverted = inverted;
        };
        // progress between 0 and 1
        RoundProgressBar.prototype.updateProgress = function (progress) {
            if (progress < 0)
                progress = 0;
            else if (progress > 1)
                progress = 1;
            this._progress = progress;
        };
        RoundProgressBar.prototype.update = function (dt) {
            _super.prototype.update.call(this, dt);
        };
        RoundProgressBar.prototype.render = function (interpolation) {
            _super.prototype.render.call(this, interpolation);
            if (this._progress == this._lastProgress)
                return;
            this._background.mask = new plume.Mask();
            if (this._progress > 0) {
                this._background.visible = true;
                if (!this._inverted) {
                    var progressToAngle = this._progress * 2 * Math.PI;
                    this._background.mask.moveTo(this.centerX, this.centerY);
                    this._background.mask.lineTo(this.radius, 0);
                    this._background.mask.arc(this.centerX, this.centerY, this.radius, this.start, this.start + progressToAngle);
                    this._background.mask.lineTo(this.centerX, this.centerY);
                }
                else {
                    var progressToAngle = this._progress * 2 * Math.PI;
                    this._background.mask.moveTo(this.centerX, this.centerY);
                    this._background.mask.lineTo(this.radius + this.radius * Math.cos(this.start + progressToAngle), this.radius + this.radius * Math.sin(this.start + progressToAngle));
                    this._background.mask.arc(this.centerX, this.centerY, this.radius, this.start + progressToAngle, 3 * Math.PI / 2);
                    this._background.mask.lineTo(this.centerX, this.centerY);
                }
            }
            else {
                this._background.visible = false;
            }
            this._lastProgress = this._progress;
        };
        return RoundProgressBar;
    }(plume.AbstractProgressBar));
    plume.RoundProgressBar = RoundProgressBar;
})(plume || (plume = {}));
// module plume {
//     export class ScrollableContainer extends Container implements DragListener, Updatable {
//         applyVelocity = true;
//         velocityTheshold = 0.2;
//         dragDurationVelocityTheshold = 400;
//         wheelOnY = true;
//         scrollEnableOnX = true;
//         scrollEnableOnY = true;
//         private _dragManager: DragManager;
//         private _innerVelocity: Vector = new Vector();
//         private _dragStartPosition: Point = new Point();
//         private _dragStartTime: number = 0;
//         private _lastInnerPosition: Point = new Point();
//         private _inner: Container;
//         private _innerMin: Point;
//         private _innerMax: Point;
//         private _contentMask: Mask;
//         private _contentBounds: Rectangle;
//         private _game: Game;
//         private _scrollHandler: HandlerCollection = new HandlerCollection();
//         constructor() {
//             super();
//             var self = this;
//             var gb = Game.get().bounds;
//             this.interactive = true;
//             this.showCursor = false;
//             this._game = Game.get();
//             this._game.addUpdatable(this);
//             this._inner = new Container();
//             this.addChild(this._inner);
//             this._dragManager = new DragManager(this);
//             this._dragManager.setStartDragController(function (position: Point) {
//                 return self.canStartDrag(position);
//             });
//             this._game.inputManager.mouse.mouseWheelHandlers.add(this.onMouseWheel, this);
//             this.detachListener.once(function () {
//                 self.destroy();
//             });
//             // default, set size to game size
//             this.setContainerSize(gb.width, gb.height);
//         }
//         setContainerSize(width: number, height: number) {
//             this.setSize(width, height);
//             this._contentBounds = new Rectangle(0, 0, width, height);
//             this._contentMask = new Mask();
//             this._contentMask.rectangle(0, 0, width, height);
//             this.mask = this._contentMask;
//         }
//         onDragBegin(position: Point) {
//             this._dragStartPosition = position.copy();
//             this._dragStartTime = Date.now();
//             this._innerVelocity.x = 0;
//             this._innerVelocity.y = 0;
//         }
//         onDragMove(xOffset: number, yOffset: number, position: Point) {
//             // offset is negative
//             if (this.scrollEnableOnX) this._inner.x += xOffset;
//             if (this.scrollEnableOnY) this._inner.y += yOffset;
//             this.checkBoundLimit();
//         }
//         onDragEnd(position: Point) {
//             var moveX = position.x - this._dragStartPosition.x;
//             var moveY = position.y - this._dragStartPosition.y;
//             var duration = Date.now() - this._dragStartTime;
//             if (!this.scrollEnableOnX) moveX = 0;
//             if (!this.scrollEnableOnY) moveY = 0;
//             if (this.applyVelocity) {
//                 if (duration < this.dragDurationVelocityTheshold) {
//                     this._innerVelocity.x = moveX / duration;
//                     this._innerVelocity.y = moveY / duration;
//                 }
//             }
//         }
//         onMouseWheel(delta: number) {
//             var mousePosition = Game.get().inputManager.mouse.position;
//             if (!this.canStartDrag(mousePosition)) {
//                 return;
//             }
//             // simulate drag
//             var offsetX = 0;
//             var offsetY = 0;
//             var offset = -delta * 50;
//             if (this.wheelOnY) {
//                 offsetY = offset;
//             } else {
//                 offsetX = offset;
//             }
//             this.doFakeDrag(offsetX, offsetY, 1000);
//         }
//         addDisplayObject(child: DisplayObject, at?: number): DisplayObject {
//             var added;
//             if (at != null) {
//                 added = this._inner.addChildAt(child, at);
//             } else {
//                 added = this._inner.addChild(child);
//             }
//             this.computeInnerMinMax();
//             return added;
//         }
//         clear() {
//             this._inner.removeChildren();
//             this.computeInnerMinMax();
//         }
//         moveTo(x: number = null, y: number = null) {
//             if (x != null) this._inner.x = -x;
//             if (y != null) this._inner.y = -y;
//             this.checkBoundLimit();
//         }
//         moveBy(offsetx: number = null, offsety: number = null) {
//             if (offsetx != null) this._inner.x -= offsetx;
//             if (offsety != null) this._inner.y -= offsety;
//             this.checkBoundLimit();
//         }
//         // move for about x/y simulate a drag from "inner - offset" to "inner"
//         // movement is only the resulting animation based of displacement / acceleration (not real offset)
//         moveByAbout(offsetx: number = null, offsety: number = null) {
//             if (!this.canMove()) return;
//             if (offsetx == null) offsetx = 0;
//             if (offsety == null) offsety = 0;
//             this._inner.x += offsetx;
//             this._inner.y += offsety;
//             this.doFakeDrag(-offsetx, -offsety, 300);
//         }
//         update(ts: number) {
//             if (this._innerVelocity.x != 0 || this._innerVelocity.y != 0) {
//                 this._innerVelocity.x *= 0.95;
//                 this._innerVelocity.y *= 0.95;
//                 if (Math.abs(this._innerVelocity.x) < this.velocityTheshold) this._innerVelocity.x = 0;
//                 if (Math.abs(this._innerVelocity.y) < this.velocityTheshold) this._innerVelocity.y = 0;
//                 this._inner.x = this._inner.x + ts * this._innerVelocity.x;
//                 this._inner.y = this._inner.y + ts * this._innerVelocity.y;
//                 this.checkBoundLimit();
//             }
//         }
//         getContentBound(): Rectangle {
//             this._contentBounds.x = -this._inner.x;
//             this._contentBounds.y = -this._inner.y;
//             return this._contentBounds;
//         }
//         private doFakeDrag(offsetX: number, offsetY: number, duration: number) {
//             var start = new Point(this._inner.x, this._inner.y);
//             var end = start.copy();
//             end.x = end.x + offsetX;
//             end.y = end.y + offsetY;
//             this.onDragBegin(start);
//             this._dragStartTime = Date.now() - duration;
//             this.onDragMove(offsetX, offsetY, end);
//             this.onDragEnd(end);
//         }
//         private computeInnerMinMax() {
//             // compute inner width/height dynamically based on children added
//             var innerBounds = layout.computeBounds(this._inner, false);
//             this._inner.setSize(innerBounds.width, innerBounds.height);
//             var w = this._inner.width;
//             var h = this._inner.height;
//             this._innerMin = new Point(-w + this._contentBounds.width, -h + this._contentBounds.height);
//             this._innerMax = new Point(0, 0);
//         }
//         private canMove(): boolean {
//             if (this._inner.width <= this._contentBounds.width && this._inner.height <= this._contentBounds.height) {
//                 return false;
//             }
//             return true;
//         }
//         private canStartDrag(position: Point): boolean {
//             if (!this.canMove()) return false;
//             var inside = this.hitPoint(position);
//             return inside;
//         }
//         // called after each inner x/y change
//         private checkBoundLimit() {
//             if (this.scrollEnableOnX) {
//                 if (this._inner.x < this._innerMin.x) this._inner.x = this._innerMin.x;
//                 else if (this._inner.x > this._innerMax.x) this._inner.x = this._innerMax.x;
//             }
//             if (this.scrollEnableOnY) {
//                 if (this._inner.y < this._innerMin.y) this._inner.y = this._innerMin.y;
//                 else if (this._inner.y > this._innerMax.y) this._inner.y = this._innerMax.y;
//             }
//             var offsetX = this._lastInnerPosition.x - this._inner.x;
//             var offsetY = this._lastInnerPosition.y - this._inner.y;
//             this._lastInnerPosition.x = this._inner.x;
//             this._lastInnerPosition.y = this._inner.y;
//             if (offsetX != 0 || offsetY != 0) {
//                 this._scrollHandler.fire(offsetX, offsetY, -this._lastInnerPosition.x, -this._lastInnerPosition.y);
//             }
//         }
//         private destroy() {
//             this._dragManager.destroy();
//             this._game.inputManager.mouse.mouseWheelHandlers.remove(this.onMouseWheel);
//             this._game.removeUpdatable(this);
//         }
//         addScrollHandler(fn: (offsetX: number, offsetY: number, x: number, y: number) => any, context?: any) {
//             this._scrollHandler.add(fn, context);
//         }
//         removeScrollHandler(fn: Function) {
//             this._scrollHandler.remove(fn);
//         }
//     }
// }
// module plume {
//     export class TilingSprite extends Container {
//         private _tilePositionX = 0;
//         private _tilePositionY = 0;
//         private _dirty = true;
//         private _elementW = 0;
//         private _elementH = 0;
//         private _elementCountW = 0;
//         private _elementCountH = 0;
//         constructor(width: number, height: number, private objectFactory: () => DisplayObject) {
//             super(width, height);
//             this.mask = new Mask();
//             this.mask.rectangle(0, 0, width, height);
//         }
//         static fromFrame(width: number, height: number, name: string): TilingSprite {
//             let ts = new TilingSprite(width, height, function () {
//                 return Sprite.fromFrame(name);
//             });
//             return ts;
//         }
//         draw0(context: CanvasRenderingContext2D) {
//             if (this._dirty) {
//                 this.create();
//                 this._dirty = false;
//             }
//             super.draw0(context);
//         }
//         private create() {
//             if (this._elementCountW == 0) {
//                 // first init, fill container width all childs
//                 let elem = this.objectFactory();
//                 let w = elem.width;
//                 let h = elem.height;
//                 this._elementCountW = Math.ceil((this.width / w) + 1);
//                 this._elementCountH = Math.ceil((this.height / h) + 1);
//                 this.addChild(elem);
//                 let count = (this._elementCountW * this._elementCountH);
//                 for (let i = 1; i < count; i++) {
//                     this.addChild(this.objectFactory());
//                 }
//                 this._elementW = w;
//                 this._elementH = h;
//             }
//             // Move all elements
//             let startx = Maths.mod(this._tilePositionX, this._elementW) - this._elementW;
//             let starty = Maths.mod(this._tilePositionY, this._elementH) - this._elementH;
//             for (let x = 0; x < this._elementCountW; x++) {
//                 for (let y = 0; y < this._elementCountH; y++) {
//                     let idx = (x * this._elementCountH) + y;
//                     let child = this.getChildAt(idx);
//                     child.x = startx + (x * this._elementW);
//                     child.y = starty + (y * this._elementH);
//                 }
//             }
//         }
//         get tilePositionX(): number {
//             return this._tilePositionX;
//         }
//         set tilePositionX(v: number) {
//             this._tilePositionX = v;
//             this._dirty = true;
//         }
//         get tilePositionY(): number {
//             return this._tilePositionY;
//         }
//         set tilePositionY(v: number) {
//             this._tilePositionY = v;
//             this._dirty = true;
//         }
//     }
// }
var plume;
(function (plume) {
    var Tinter = /** @class */ (function () {
        function Tinter() {
        }
        Tinter._isMultiplySupported = function () {
            if (this._multiplyDetected === null) { //we haven't checked yet
                var canvas = document.createElement("canvas");
                var context = canvas.getContext("2d");
                canvas.width = 1;
                canvas.height = 1;
                context.clearRect(0, 0, 1, 1);
                context.globalCompositeOperation = 'source-over';
                context.fillStyle = "#000"; //black
                context.fillRect(0, 0, 1, 1);
                context.globalCompositeOperation = 'multiply';
                context.fillStyle = "#fff"; //white
                context.fillRect(0, 0, 1, 1);
                this._multiplyDetected = context.getImageData(0, 0, 1, 1).data[0] === 0;
                if (!this._multiplyDetected) {
                    plume.logger.warn("Tinter : globalCompositeOperation multiply not supported, fallback to manual multiply");
                }
            }
            return this._multiplyDetected;
        };
        Tinter.tint = function (texture, color, useCache) {
            if (useCache === void 0) { useCache = true; }
            var r = (color >> 16) & 0x0000FF;
            var g = (color >> 8) & 0x0000FF;
            var b = color & 0x0000FF;
            r = Math.min(255, Math.round(r / Tinter.rounding) * Tinter.rounding);
            g = Math.min(255, Math.round(g / Tinter.rounding) * Tinter.rounding);
            b = Math.min(255, Math.round(b / Tinter.rounding) * Tinter.rounding);
            var tintedTextureName = texture.name + "-tint-" + r + "," + g + "," + b;
            if (useCache) {
                var found = plume.Texture.containsTexture(tintedTextureName);
                if (found != null) {
                    return found;
                }
            }
            var canvas = document.createElement("canvas");
            var context = canvas.getContext("2d");
            canvas.width = texture.width;
            canvas.height = texture.height;
            context.fillStyle = 'rgba(' + r + ',' + g + ',' + b + ',' + 1 + ')';
            context.fillRect(0, 0, texture.width, texture.height);
            //TB check multiply supported (ie does not support it)
            //https://caniuse.com/#feat=canvas-blending
            if (this._isMultiplySupported()) {
                context.globalCompositeOperation = 'multiply';
                context.drawImage(texture.getImpl(), texture.x, texture.y, texture.width, texture.height, 0, 0, texture.width, texture.height);
            }
            else {
                //multiply not supported (internet explorer)
                //fallback to manual impl
                //1 - draw image
                context.drawImage(texture.getImpl(), texture.x, texture.y, texture.width, texture.height, 0, 0, texture.width, texture.height);
                //2  - manual multiply
                var imgData = context.getImageData(0, 0, canvas.width, canvas.height);
                var data = imgData.data;
                for (var i = 0; i < data.length; i += 4) {
                    data[i] = r * data[i] / 255;
                    data[i + 1] = g * data[i + 1] / 255;
                    data[i + 2] = b * data[i + 2] / 255;
                }
                context.putImageData(imgData, 0, 0);
            }
            context.globalCompositeOperation = 'destination-atop';
            context.drawImage(texture.getImpl(), texture.x, texture.y, texture.width, texture.height, 0, 0, texture.width, texture.height);
            var tintedTexture = new plume.Texture(tintedTextureName, 0, 0, texture.width, texture.height);
            tintedTexture.setImpl(canvas);
            if (useCache) {
                plume.Texture.addTexture(tintedTexture);
            }
            return tintedTexture;
        };
        Tinter.rounding = 8;
        Tinter._multiplyDetected = null;
        return Tinter;
    }());
    plume.Tinter = Tinter;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Collisions = /** @class */ (function () {
        function Collisions() {
        }
        Collisions.overlapRectangles = function (r1, r2) {
            return r1.left < r2.right && r1.right > r2.left && r1.top < r2.bottom && r1.bottom > r2.top;
        };
        Collisions.pointInPolygon2 = function (points, x, y) {
            var polyCornersCount = points.length;
            var i = 0;
            var j = points.length - 2;
            var oddNodes = false;
            for (i = 0; i < polyCornersCount; i = i + 2) {
                if (points[i + 1] < y && points[j + 1] >= y
                    || points[j + 1] < y && points[i + 1] >= y) {
                    if (points[i] + (y - points[i + 1]) / (points[j + 1] - points[i + 1]) * (points[j] - points[i]) < x) {
                        oddNodes = !oddNodes;
                    }
                }
                j = i;
            }
            return oddNodes;
        };
        return Collisions;
    }());
    plume.Collisions = Collisions;
})(plume || (plume = {}));
//
// TODO use the one from threejs
//
var plume;
(function (plume) {
    var Matrix = /** @class */ (function () {
        /**
         * 2D transformation matrix object initialized with identity matrix.
         *
         * The matrix can synchronize a canvas context by supplying the context
         * as an argument, or later apply current absolute transform to an
         * existing context.
         *
         * All values are handled as floating point values.
         *
         * @param {CanvasRenderingContext2D} [context] - Optional context to sync with Matrix
         * @prop {number} a - scale x
         * @prop {number} b - skew y
         * @prop {number} c - skew x
         * @prop {number} d - scale y
         * @prop {number} e - translate x
         * @prop {number} f - translate y
         * @prop {CanvasRenderingContext2D} [context] - set or get current canvas context
         * @constructor
         */
        function Matrix(context) {
            this.a = 1;
            this.b = 0;
            this.c = 0;
            this.d = 1;
            this.e = 0;
            this.f = 0;
            this.context = context || null;
            // reset canvas transformations (if any) to enable 100% sync.
            if (context)
                context.setTransform(1, 0, 0, 1, 0, 0);
        }
        Matrix.prototype.toString = function () {
            return "[" + this.a + "," + this.b + "," + this.c + "," + this.d + "," + this.e + "," + this.f + "]";
        };
        /**
         * Flips the horizontal values.
         */
        Matrix.prototype.flipX = function () {
            this.transform(-1, 0, 0, 1, 0, 0);
            return this;
        };
        /**
         * Flips the vertical values.
         */
        Matrix.prototype.flipY = function () {
            this.transform(1, 0, 0, -1, 0, 0);
            return this;
        };
        /**
         * Short-hand to reset current matrix to an identity matrix.
         */
        Matrix.prototype.reset = function () {
            this.a = this.d = 1;
            this.b = this.c = this.e = this.f = 0;
            this._setCtx();
            return this;
        };
        /**
         * Rotates current matrix accumulative by angle.
         * @param {number} angle - angle in radians
         */
        Matrix.prototype.rotate = function (angle) {
            var cos = Math.cos(angle), sin = Math.sin(angle);
            this.transform(cos, sin, -sin, cos, 0, 0);
            return this;
        };
        /**
         * Helper method to make a rotation based on an angle in degrees.
         * @param {number} angle - angle in degrees
         */
        Matrix.prototype.rotateDeg = function (angle) {
            this.rotate(angle * 0.017453292519943295);
            return this;
        };
        /**
         * Scales current matrix accumulative.
         * @param {number} sx - scale factor x (1 does nothing)
         * @param {number} sy - scale factor y (1 does nothing)
         */
        Matrix.prototype.scale = function (sx, sy) {
            this.transform(sx, 0, 0, sy, 0, 0);
            return this;
        };
        /**
         * Scales current matrix on x axis accumulative.
         * @param {number} sx - scale factor x (1 does nothing)
         */
        Matrix.prototype.scaleX = function (sx) {
            this.transform(sx, 0, 0, 1, 0, 0);
            return this;
        };
        /**
         * Scales current matrix on y axis accumulative.
         * @param {number} sy - scale factor y (1 does nothing)
         */
        Matrix.prototype.scaleY = function (sy) {
            this.transform(1, 0, 0, sy, 0, 0);
            return this;
        };
        /**
         * Apply skew to the current matrix accumulative.
         * @param {number} sx - amount of skew for x
         * @param {number} sy - amount of skew for y
         */
        Matrix.prototype.skew = function (sx, sy) {
            this.transform(1, sy, sx, 1, 0, 0);
            return this;
        };
        /**
         * Apply skew for x to the current matrix accumulative.
         * @param {number} sx - amount of skew for x
         */
        Matrix.prototype.skewX = function (sx) {
            this.transform(1, 0, sx, 1, 0, 0);
            return this;
        };
        /**
         * Apply skew for y to the current matrix accumulative.
         * @param {number} sy - amount of skew for y
         */
        Matrix.prototype.skewY = function (sy) {
            this.transform(1, sy, 0, 1, 0, 0);
            return this;
        };
        /**
         * Set current matrix to new absolute matrix.
         * @param {number} a - scale x
         * @param {number} b - skew y
         * @param {number} c - skew x
         * @param {number} d - scale y
         * @param {number} e - translate x
         * @param {number} f - translate y
         */
        Matrix.prototype.setTransform = function (a, b, c, d, e, f) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
            this.f = f;
            this._setCtx();
            return this;
        };
        /**
         * Translate current matrix accumulative.
         * @param {number} tx - translation for x
         * @param {number} ty - translation for y
         */
        Matrix.prototype.translate = function (tx, ty) {
            this.transform(1, 0, 0, 1, tx, ty);
            return this;
        };
        /**
         * Translate current matrix on x axis accumulative.
         * @param {number} tx - translation for x
         */
        Matrix.prototype.translateX = function (tx) {
            this.transform(1, 0, 0, 1, tx, 0);
            return this;
        };
        /**
         * Translate current matrix on y axis accumulative.
         * @param {number} ty - translation for y
         */
        Matrix.prototype.translateY = function (ty) {
            this.transform(1, 0, 0, 1, 0, ty);
            return this;
        };
        /**
         * Multiplies current matrix with new matrix values.
         * @param {number} a2 - scale x
         * @param {number} b2 - skew y
         * @param {number} c2 - skew x
         * @param {number} d2 - scale y
         * @param {number} e2 - translate x
         * @param {number} f2 - translate y
         */
        Matrix.prototype.transform = function (a2, b2, c2, d2, e2, f2) {
            var a1 = this.a, b1 = this.b, c1 = this.c, d1 = this.d, e1 = this.e, f1 = this.f;
            /* matrix order (canvas compatible):
            * ace
            * bdf
            * 001
            */
            this.a = a1 * a2 + c1 * b2;
            this.b = b1 * a2 + d1 * b2;
            this.c = a1 * c2 + c1 * d2;
            this.d = b1 * c2 + d1 * d2;
            this.e = a1 * e2 + c1 * f2 + e1;
            this.f = b1 * e2 + d1 * f2 + f1;
            this._setCtx();
            return this;
        };
        /**
         * Get an inverse matrix of current matrix. The method returns a new
         * matrix with values you need to use to get to an identity matrix.
         * Context from parent matrix is not applied to the returned matrix.
         * @returns {Matrix}
         */
        Matrix.prototype.getInverse = function () {
            var a = this.a, b = this.b, c = this.c, d = this.d, e = this.e, f = this.f, m = new Matrix(), dt = (a * d - b * c);
            m.a = d / dt;
            m.b = -b / dt;
            m.c = -c / dt;
            m.d = a / dt;
            m.e = (c * f - d * e) / dt;
            m.f = -(a * f - b * e) / dt;
            return m;
        };
        /**
         * Interpolate this matrix with another and produce a new matrix.
         * t is a value in the range [0.0, 1.0] where 0 is this instance and
         * 1 is equal to the second matrix. The t value is not constrained.
         *
         * Context from parent matrix is not applied to the returned matrix.
         *
         * @param {Matrix} m2 - the matrix to interpolate with.
         * @param {number} t - interpolation [0.0, 1.0]
         * @returns {Matrix} - new instance with the interpolated result
         */
        Matrix.prototype.interpolate = function (m2, t) {
            var m = new Matrix();
            m.a = this.a + (m2.a - this.a) * t;
            m.b = this.b + (m2.b - this.b) * t;
            m.c = this.c + (m2.c - this.c) * t;
            m.d = this.d + (m2.d - this.d) * t;
            m.e = this.e + (m2.e - this.e) * t;
            m.f = this.f + (m2.f - this.f) * t;
            return m;
        };
        /**
         * Apply current matrix to x and y point.
         * Returns a point object.
         *
         * @param {number} x - value for x
         * @param {number} y - value for y
         * @returns {{x: number, y: number}} A new transformed point object
         */
        Matrix.prototype.applyToPoint = function (x, y) {
            return {
                x: x * this.a + y * this.c + this.e,
                y: x * this.b + y * this.d + this.f
            };
        };
        /**
         * Apply current matrix to a typed array with point pairs. Although
         * the input array may be an ordinary array, this method is intended
         * for more performant use where typed arrays are used. The returned
         * array is regardless always returned as a Float32Array.
         *
         * @param {*} points - (typed) array with point pairs
         * @returns {Float32Array} A new array with transformed points
         */
        Matrix.prototype.applyToArray = function (points) {
            var i = 0;
            var p;
            var l = points.length;
            var mxPoints = new Array(l);
            while (i < l) {
                p = this.applyToPoint(points[i], points[i + 1]);
                mxPoints[i++] = p.x;
                mxPoints[i++] = p.y;
            }
            return mxPoints;
        };
        /**
         * Apply to any canvas 2D context object. This does not affect the
         * context that optionally was referenced in constructor unless it is
         * the same context.
         * @param {CanvasRenderingContext2D} context
         */
        Matrix.prototype.applyToContext = function (context) {
            context.setTransform(this.a, this.b, this.c, this.d, this.e, this.f);
            return this;
        };
        /**
         * Returns true if matrix is an identity matrix (no transforms applied).
         * @returns {boolean} True if identity (not transformed)
         */
        Matrix.prototype.isIdentity = function () {
            return (this._isEqual(this.a, 1) &&
                this._isEqual(this.b, 0) &&
                this._isEqual(this.c, 0) &&
                this._isEqual(this.d, 1) &&
                this._isEqual(this.e, 0) &&
                this._isEqual(this.f, 0));
        };
        /**
         * Compares current matrix with another matrix. Returns true if equal
         * (within epsilon tolerance).
         * @param {Matrix} m - matrix to compare this matrix with
         * @returns {boolean}
         */
        Matrix.prototype.isEqual = function (m) {
            return (this._isEqual(this.a, m.a) &&
                this._isEqual(this.b, m.b) &&
                this._isEqual(this.c, m.c) &&
                this._isEqual(this.d, m.d) &&
                this._isEqual(this.e, m.e) &&
                this._isEqual(this.f, m.f));
        };
        /**
         * Compares floating point values with some tolerance (epsilon)
         * @param {number} f1 - float 1
         * @param {number} f2 - float 2
         * @returns {boolean}
         * @private
         */
        Matrix.prototype._isEqual = function (f1, f2) {
            return Math.abs(f1 - f2) < 1e-14;
        };
        /**
         * Apply current absolute matrix to context if defined, to sync it.
         * @private
         */
        Matrix.prototype._setCtx = function () {
            if (this.context)
                this.context.setTransform(this.a, this.b, this.c, this.d, this.e, this.f);
        };
        return Matrix;
    }());
    plume.Matrix = Matrix;
})(plume || (plume = {}));
/// <reference path="GUISprite.ts" />
var plume;
(function (plume) {
    var BitmapText = /** @class */ (function (_super) {
        __extends(BitmapText, _super);
        function BitmapText(text, fontName, size) {
            var _this = _super.call(this) || this;
            _this._missingChar = false;
            _this._ratio = 1;
            _this._align = "left";
            _this._maxWidth = 10000;
            _this._dirty = true;
            var font = plume.Resources.fonts[fontName];
            if (font == null) {
                throw new Error("Font not loaded: " + fontName);
            }
            _this._fontName = fontName;
            _this._font = font;
            _this._text = text;
            _this._size = size == null ? font.size : size;
            _this._ratio = _this._size / font.size;
            _this._debugId = "Text-" + _this._id;
            return _this;
        }
        Object.defineProperty(BitmapText.prototype, "text", {
            get: function () {
                return this._text;
            },
            // Accessor
            set: function (text) {
                if (text == this._text)
                    return;
                this._text = text;
                this._dirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BitmapText.prototype, "align", {
            get: function () {
                return this._align;
            },
            set: function (align) {
                this._align = align;
                this._dirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BitmapText.prototype, "maxWidth", {
            get: function () {
                return this._maxWidth;
            },
            set: function (maxWidth) {
                this._maxWidth = maxWidth;
                this._dirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BitmapText.prototype, "width", {
            get: function () {
                if (this._dirty)
                    this._create();
                return this._width * this._scaleX;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BitmapText.prototype, "height", {
            get: function () {
                if (this._dirty)
                    this._create();
                return this._height * this._scaleY;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BitmapText.prototype, "tint", {
            set: function (tint) {
                if (tint == null)
                    this._tint = 0xFFFFFF;
                else
                    this._tint = tint;
                this._dirty = true;
            },
            enumerable: true,
            configurable: true
        });
        BitmapText.prototype.hasMissingChar = function () {
            if (this._dirty)
                this._create();
            return this._missingChar;
        };
        BitmapText.prototype.draw0 = function (context) {
            if (this._dirty) {
                this._create();
                this._dirty = false;
            }
            _super.prototype.draw0.call(this, context);
        };
        // Take in account: maxWidth, line break, alignment
        BitmapText.prototype._create = function () {
            this.removeChildren();
            if (this.text == null || this.text == "") {
                this.setSize(0, 0);
            }
            this._missingChar = false;
            var lines = []; // Letters by line
            var xPos = 0;
            var currentLine = { width: 0, letters: [], xposition: [] };
            lines.push(currentLine);
            var lastSpaceLetterAdded = -1;
            var lastSpaceLineWidth = -1;
            var maxHeight = 0;
            for (var i = 0; i < this._text.length; i++) {
                var c = this._text.charCodeAt(i);
                if (c == 10) {
                    // line break
                    currentLine = { width: 0, letters: [], xposition: [] };
                    lines.push(currentLine);
                    xPos = 0;
                    lastSpaceLetterAdded = -1;
                    lastSpaceLineWidth = -1;
                    continue;
                }
                var letter = this._font.lettersByCode[c];
                if (letter == null) {
                    // No mapping for char, ignore
                    this._missingChar = true;
                    continue;
                }
                if (c == 32) {
                    // space, reset flags
                    lastSpaceLetterAdded = 0;
                    lastSpaceLineWidth = xPos;
                }
                if (lastSpaceLetterAdded > -1) {
                    lastSpaceLetterAdded++;
                }
                currentLine.letters.push(letter);
                currentLine.xposition.push(xPos);
                xPos += this._getLetterWidthToDraw(letter);
                currentLine.width = xPos;
                var gliphHeight = letter.yoffset + letter.height;
                if (gliphHeight > maxHeight) {
                    maxHeight = gliphHeight;
                }
                if (this._maxWidth > 0 && xPos > this._maxWidth) {
                    // We go over maxWidth, need to go back from x letter, break line and continue
                    if (lastSpaceLetterAdded > 0) {
                        // Remove letter to add to new line
                        var from = currentLine.letters.length - lastSpaceLetterAdded;
                        currentLine.letters.splice(from, lastSpaceLetterAdded);
                        currentLine.xposition.splice(from, lastSpaceLetterAdded);
                        // Compute new line width
                        var newLineWidth = currentLine.xposition[from - 1] + this._getLetterWidthToDraw(currentLine.letters[from - 1]);
                        currentLine.width = newLineWidth;
                        // Reset index
                        i = i - lastSpaceLetterAdded + 1; // (+1 to ignore space replace by line break)
                        // Add new line
                        currentLine = { width: 0, letters: [], xposition: [] };
                        lines.push(currentLine);
                        xPos = 0;
                        lastSpaceLetterAdded = -1;
                        lastSpaceLineWidth = -1;
                    }
                }
            }
            var maxWidth = 0;
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                maxWidth = Math.max(line.width, maxWidth);
            }
            var width = Math.ceil(maxWidth);
            var lineHeight = (lines.length <= 1 ? this._normalizeSize(maxHeight) : this._normalizeSize(this._font.lineHeight)); // need to investigate good way to find line height
            var height = Math.ceil(lines.length * lineHeight);
            this.setSize(width, height);
            var yPos = 0;
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                var lineOffset = 0;
                if (this._align == "center") {
                    lineOffset = (maxWidth - line.width) / 2;
                }
                else if (this._align == "right") {
                    lineOffset = (maxWidth - line.width);
                }
                for (var c = 0; c < line.letters.length; c++) {
                    var letter = line.letters[c];
                    var x = line.xposition[c];
                    x += lineOffset;
                    if (letter.width > 0 && letter.height > 0) {
                        var texture = this._font.textureByCode[letter.id];
                        if (this._tint != 0xFFFFFF) {
                            var tinted = plume.Tinter.tint(texture, this._tint, true);
                            texture = tinted;
                        }
                        var sprite = new plume.Sprite(texture);
                        sprite.scaleXY = this._ratio;
                        sprite.x = x + letter.xoffset * this._ratio;
                        sprite.y = yPos + letter.yoffset * this._ratio;
                        this.addChild(sprite);
                    }
                }
                yPos += lineHeight;
            }
        };
        BitmapText.prototype._normalizeSize = function (s) {
            return s * this._ratio;
        };
        BitmapText.prototype._getLetterWidthToDraw = function (letter) {
            if (letter.id == 32) {
                return letter.xadvance * this._ratio;
            }
            return (letter.xoffset + letter.width) * this._ratio;
        };
        return BitmapText;
    }(plume.Container));
    plume.BitmapText = BitmapText;
})(plume || (plume = {}));
/// <reference path="GUIContainer.ts" />
var plume;
(function (plume) {
    var Graphics = /** @class */ (function (_super) {
        __extends(Graphics, _super);
        function Graphics(width, height) {
            if (width === void 0) { width = 0; }
            if (height === void 0) { height = 0; }
            var _this = _super.call(this) || this;
            _this._ops = new Array();
            _this._debugId = "Graphics-" + _this._id;
            _this._width = width;
            _this._height = height;
            return _this;
        }
        Graphics.prototype.customDraw = function (cb) {
            this._ops.push(cb);
            return this;
        };
        Graphics.prototype.beginFill = function (color, alpha) {
            if (alpha === void 0) { alpha = 1; }
            this._ops.push(function (context) {
                context.fillStyle = plume.Colors.toString(color, alpha);
                context.beginPath();
            });
            return this;
        };
        Graphics.prototype.beginStroke = function (lineWidth, color, alpha) {
            if (alpha === void 0) { alpha = 1; }
            var self = this;
            this._ops.push(function (context) {
                context.lineWidth = lineWidth;
                context.strokeStyle = plume.Colors.toString(color, alpha);
                context.beginPath();
            });
            return this;
        };
        Graphics.prototype.endStroke = function () {
            this._ops.push(function (context) {
                context.stroke();
            });
            return this;
        };
        Graphics.prototype.endFill = function () {
            this._ops.push(function (context) {
                context.fill();
            });
            return this;
        };
        Graphics.prototype.moveTo = function (x, y) {
            this._ops.push(function (context) {
                context.moveTo(x, y);
            });
            return this;
        };
        Graphics.prototype.lineTo = function (x, y) {
            this._ops.push(function (context) {
                context.lineTo(x, y);
            });
            return this;
        };
        Graphics.prototype.quadraticCurveTo = function (cpX, cpY, toX, toY) {
            this._ops.push(function (context) {
                context.quadraticCurveTo(cpX, cpY, toX, toY);
            });
            return this;
        };
        Graphics.prototype.bezierCurveTo = function (cpX, cpY, cpX2, cpY2, toX, toY) {
            this._ops.push(function (context) {
                context.bezierCurveTo(cpX, cpY, cpX2, cpY2, toX, toY);
            });
            return this;
        };
        Graphics.prototype.arcTo = function (x1, y1, x2, y2, radius) {
            this._ops.push(function (context) {
                context.arcTo(x1, y1, x2, y2, radius);
            });
            return this;
        };
        Graphics.prototype.arc = function (cx, cy, radius, startAngle, endAngle, anticlockwise) {
            if (startAngle === void 0) { startAngle = 0; }
            if (endAngle === void 0) { endAngle = 2 * Math.PI; }
            this._ops.push(function (context) {
                context.arc(cx, cy, radius, startAngle, endAngle, anticlockwise);
            });
            return this;
        };
        Graphics.prototype.drawRect = function (x, y, width, height) {
            var self = this;
            this._ops.push(function (context) {
                context.rect(x, y, width, height);
            });
            return this;
        };
        Graphics.prototype.drawRoundedRect = function (x, y, width, height, radius) {
            this._ops.push(function (context) {
                context.moveTo(x + radius, y);
                context.lineTo(x + width - radius, y);
                context.quadraticCurveTo(x + width, y, x + width, y + radius);
                context.lineTo(x + width, y + height - radius);
                context.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
                context.lineTo(x + radius, y + height);
                context.quadraticCurveTo(x, y + height, x, y + height - radius);
                context.lineTo(x, y + radius);
                context.quadraticCurveTo(x, y, x + radius, y);
            });
            return this;
        };
        Graphics.prototype.drawCircle = function (x, y, radius) {
            var self = this;
            this._ops.push(function (context) {
                context.arc(x, y, radius, 0, 2 * Math.PI);
            });
            return this;
        };
        Graphics.prototype.clear = function () {
            this._ops.length = 0;
            return this;
        };
        Graphics.prototype.createCachedTexture = function () {
            var canvas = document.createElement("canvas");
            canvas.width = this._width;
            canvas.height = this._height;
            if (this._width == 0 || this._height == 0) {
                plume.logger.warn("Caching graphics without size.");
            }
            var context = canvas.getContext("2d");
            this.draw0(context);
            this._cachedTexture = new plume.Texture("graphics", 0, 0, this._width, this._height);
            this._cachedTexture.setImpl(canvas);
        };
        Graphics.prototype.draw0 = function (context) {
            this._ops.forEach(function (f) {
                f(context);
            });
        };
        Graphics.prototype.setSize = function (w, h) {
            this._width = w;
            this._height = h;
        };
        return Graphics;
    }(plume.DisplayObject));
    plume.Graphics = Graphics;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var RenderTexture = /** @class */ (function () {
        function RenderTexture() {
        }
        RenderTexture.prototype.render = function (display) {
            var canvas = document.createElement("canvas");
            var context = canvas.getContext("2d");
            canvas.width = display.x + display.width;
            canvas.height = display.y + display.height;
            var parent = display._parent;
            display._parent = null;
            display.draw(context);
            display._parent = parent;
            var base4 = canvas.toDataURL("image/png");
            return base4;
        };
        return RenderTexture;
    }());
    plume.RenderTexture = RenderTexture;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Text = /** @class */ (function (_super) {
        __extends(Text, _super);
        function Text(_text, _fontName, _fontSize, _color) {
            if (_color === void 0) { _color = 0xFFFFFF; }
            var _this = _super.call(this) || this;
            _this._text = _text;
            _this._fontName = _fontName;
            _this._fontSize = _fontSize;
            _this._color = _color;
            _this._align = "left";
            _this._dirty = true;
            _this._heightRatio = 1.4;
            _this._font = _fontSize + "px " + _fontName;
            _this._debugId = "Text-" + _this._id;
            _this._colorString = plume.Colors.toHexString(_color);
            return _this;
        }
        Object.defineProperty(Text.prototype, "text", {
            get: function () {
                return this._text;
            },
            // Accessor
            set: function (text) {
                if (text == this._text)
                    return;
                this._text = text;
                this._dirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Text.prototype, "width", {
            get: function () {
                if (this._dirty)
                    this._create();
                return this._width * this._scaleX;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Text.prototype, "height", {
            get: function () {
                if (this._dirty)
                    this._create();
                return this._height * this._scaleY;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Text.prototype, "align", {
            get: function () {
                return this._align;
            },
            set: function (align) {
                this._align = align;
                this._dirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Text.prototype, "color", {
            set: function (color) {
                if (color == null)
                    this.color = 0xFFFFFF;
                else
                    this.color = color;
                this._colorString = plume.Colors.toHexString(color);
                this._dirty = true;
            },
            enumerable: true,
            configurable: true
        });
        Text.prototype.draw0 = function (context) {
            if (this._dirty) {
                this._create();
                this._dirty = false;
            }
            if (this._canvas == null)
                return;
            if (this._height == 0)
                return;
            if (this._width == 0)
                return;
            _super.prototype.draw0.call(this, context);
            context.drawImage(this._canvas, 0, 0);
        };
        Text.prototype._setFontStyle = function (context) {
            context.textBaseline = "alphabetic";
            context.font = this._font;
            context.fillStyle = this._colorString;
        };
        // Take in account: maxWidth, line break, alignment
        Text.prototype._create = function () {
            if (this.text == null || this.text == "") {
                this._width = 0;
                this._height = 0;
                this._canvas = null;
                return;
            }
            if (this._canvas != null) {
                this._canvas = null;
            }
            this._canvas = document.createElement("canvas");
            if (this._text.indexOf("\n") > 0) {
                this._createMultiline();
                return;
            }
            var context = this._canvas.getContext("2d");
            this._setFontStyle(context);
            var measure = context.measureText(this._text);
            var width = measure.width;
            var lineHeight = estimateTextHeight(this._fontName, this._fontSize);
            this._canvas.width = width;
            this._canvas.height = lineHeight * this._heightRatio;
            this._width = this._canvas.width;
            this._height = this._canvas.height;
            this._setFontStyle(context);
            context.fillText(this._text, 0, lineHeight);
        };
        Text.prototype._createMultiline = function () {
            var context = this._canvas.getContext("2d");
            var split = this._text.split("\n");
            var lineHeight = estimateTextHeight(this._fontName, this._fontSize);
            var lineHeightBig = lineHeight * this._heightRatio;
            var maxw = 0;
            var lines = [];
            this._setFontStyle(context);
            for (var i = 0; i < split.length; i++) {
                var line = split[i];
                var w = context.measureText(line).width;
                lines.push({
                    x: 0,
                    y: (i * lineHeightBig),
                    width: w
                });
                maxw = Math.max(maxw, w);
            }
            this._canvas.width = maxw;
            this._canvas.height = lineHeightBig * lines.length;
            this._width = this._canvas.width;
            this._height = this._canvas.height;
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                var text = split[i];
                var y = (i * lineHeightBig);
                var w = line.width;
                var x = 0;
                if (this._align == "center") {
                    x = (maxw - w) / 2;
                }
                else if (this.align == "right") {
                    x = maxw - w;
                }
                this._setFontStyle(context);
                // console.log("write " + text + " at " + x + "," + y);
                context.fillText(text, x, y + lineHeight);
            }
        };
        return Text;
    }(plume.DisplayObject));
    plume.Text = Text;
    var heightcache;
    function estimateTextHeight(fontName, fontSize) {
        if (heightcache == null) {
            heightcache = new plume.HashMap();
        }
        var key = fontName + fontSize;
        var cached = heightcache.get(key);
        if (cached != null)
            return cached;
        var div = document.createElement("div");
        div.innerHTML = "(|MÃ‰qgy";
        div.style.position = 'absolute';
        div.style.top = '-9999px';
        div.style.left = '-9999px';
        div.style.fontFamily = fontName;
        div.style.fontWeight = 'normal';
        div.style.fontSize = fontSize + 'px';
        document.body.appendChild(div);
        var height = div.offsetHeight;
        heightcache.put(key, height);
        document.body.removeChild(div);
        return height;
    }
})(plume || (plume = {}));
var plume;
(function (plume) {
    var FontLoader = /** @class */ (function () {
        function FontLoader() {
        }
        FontLoader.loadAll = function (fonts) {
            for (var i = 0; i < fonts.length; i++) {
                FontLoader.load(fonts[i]);
            }
        };
        FontLoader.load = function (font) {
            var sprite = plume.Sprite.fromFrame(font.face + ".png");
            var chars = new Array();
            var letters = new Array(65000);
            var textureByCode = new Array(65000);
            var f = {
                size: font.size,
                lettersByCode: letters,
                lineHeight: font.lineHeight,
                textureByCode: textureByCode,
                chars: chars,
                resource: sprite.texture.getImpl(),
            };
            var frameX = sprite.texture.x;
            var frameY = sprite.texture.y;
            if (sprite.texture.trimmed) {
                frameX -= sprite.texture.getTrim().x;
                frameY -= sprite.texture.getTrim().y;
            }
            for (var i = 0; i < font.chars.length; i++) {
                var char = font.chars[i];
                var letter = {
                    id: char.id,
                    x: char.x + frameX,
                    y: char.y + frameY,
                    xoffset: char.xoffset,
                    yoffset: char.yoffset,
                    width: char.width,
                    height: char.height,
                    xadvance: char.xadvance
                };
                var texture = new plume.Texture(font.face + "/" + letter.id, letter.x, letter.y, letter.width, letter.height);
                texture.setImpl(sprite.texture.getImpl());
                //texture.setTrim();
                letters[char.id] = letter;
                textureByCode[char.id] = texture;
                chars.push(char.id);
                // if ("a".charCodeAt(0) == char.id || "p".charCodeAt(0) == char.id) {
                //     let aa = letters[letters.length - 1];
                //     console.log(char.id + ", It is 'a' => " + JSON.stringify(aa));
                // }
            }
            plume.Resources.addLoadedFont(font.face, f);
        };
        return FontLoader;
    }());
    plume.FontLoader = FontLoader;
})(plume || (plume = {}));
/// <reference path="../GUIScene.ts" />
var plume;
(function (plume) {
    var H5PreLoadingScene = /** @class */ (function (_super) {
        __extends(H5PreLoadingScene, _super);
        function H5PreLoadingScene(assetList, callback) {
            var _this = _super.call(this) || this;
            _this.progressListener = new plume.Handler();
            // Used to compute progress
            _this._timeCounted = 0;
            _this.startLoadTime = 0;
            // options
            _this.optDelayBeforeCompleteFire = 0;
            _this.optProgressAlgo = 1 /* Assets */;
            _this.optEstimateLoadingTime = 2000;
            _this.estimatedProgress = -1;
            _this.lastEstimatedProgress = -1;
            _this.assetList = assetList;
            _this.callback = callback;
            return _this;
        }
        H5PreLoadingScene.prototype.onLoadComplete = function () {
            var self = this;
            var now = performance.now();
            var ellapsed = now - this.startLoadTime;
            plume.logger.debug("Loading time: " + ellapsed);
            //this.game.renderer.backgroundColor = this.gameBackgroundColor;
            //Analytics.sendTiming(AnalyticsTiming.ASSETS, ellapsed);
            for (var i = 0; i < this.assetList.length; i++) {
                var def = this.assetList[i];
                if (def.type != "spritesheet")
                    continue;
                var split = def.atlas.split("/");
                var filename = split[split.length - 1];
                var jsIndex = filename.indexOf('.json');
                if (jsIndex != -1) {
                    var fakeFrame = "fake-" + def.name + ".json";
                    // if (Texture.hasInCache(fakeFrame)) {
                    //     let sprite = Sprite.fromFrame(fakeFrame);
                    //     self.addChild(sprite);
                    // }
                }
            }
            if (this.optDelayBeforeCompleteFire > 0) {
                plume.TimeManager.get().createAndStartTimer(this.optDelayBeforeCompleteFire, function () {
                    self.progressListener.fire(100);
                    self.callback();
                });
            }
            else {
                self.progressListener.fire(100);
                self.callback();
            }
        };
        H5PreLoadingScene.prototype.addProgressListener = function (fn, context) {
            this.progressListener.add(fn, context);
        };
        H5PreLoadingScene.prototype.onShow = function () {
            var self = this;
            this.startLoadTime = performance.now();
            this.loader = new plume.Loader2d(this.assetList, H5PreLoadingScene.retryMax);
            this.loader.load(function (success) {
                if (success) {
                    self.onLoadComplete();
                }
                else {
                    plume.logger.error("Failed to load all assets :/");
                }
            });
            this._loadingStatus = this.loader.getStatus();
        };
        H5PreLoadingScene.prototype.update = function (delta) {
            _super.prototype.update.call(this, delta);
            if (this.startLoadTime <= 0)
                return;
            this.lastEstimatedProgress = this.estimatedProgress;
            this._updateProgress(delta);
            if (this.optOnProgress != null) {
                this.optOnProgress(this.estimatedProgress);
            }
            if (this._loadingStatus != null) {
                var newstatus = this.loader.getStatus();
                if (newstatus.loaded != this._loadingStatus.loaded) {
                    this._loadingStatus = newstatus;
                }
            }
            if (this.lastEstimatedProgress != this.estimatedProgress) {
                this.progressListener.fire(this.estimatedProgress * 100);
            }
        };
        H5PreLoadingScene.prototype.render = function (interpolation) {
        };
        H5PreLoadingScene.prototype._updateProgress = function (delta) {
            if (this.optProgressAlgo == 2 /* AssetsSmooth */) {
                this._updateProgress_AssetsSmooth(delta);
            }
            else if (this.optProgressAlgo == 3 /* FakeDuration */) {
                this._updateProgress_FakeDuration(delta);
            }
            else {
                this._updateProgress_Assets(delta);
            }
        };
        H5PreLoadingScene.prototype._updateProgress_AssetsSmooth = function (delta) {
            var totalAssets = (this._loadingStatus.loading + this._loadingStatus.loaded + this._loadingStatus.failed);
            var progress = (this._loadingStatus.loaded + 1) / totalAssets;
            progress = Math.min(progress, 1);
            var ellapsed = this._timeCounted / this.optEstimateLoadingTime;
            if (ellapsed < progress) {
                this._timeCounted += delta;
            }
            this.estimatedProgress = Math.min(ellapsed, 1);
        };
        H5PreLoadingScene.prototype._updateProgress_FakeDuration = function (delta) {
            var ellapsed = performance.now() - this.startLoadTime;
            var progress = (ellapsed) / this.optEstimateLoadingTime;
            this.estimatedProgress = Math.min(progress, 1);
        };
        H5PreLoadingScene.prototype._updateProgress_Assets = function (delta) {
            var totalAssets = (this._loadingStatus.loading + this._loadingStatus.loaded + this._loadingStatus.failed);
            var progress = (this._loadingStatus.loaded) / totalAssets;
            this.estimatedProgress = Math.min(progress, 1);
        };
        H5PreLoadingScene.retryMax = 5;
        return H5PreLoadingScene;
    }(plume.GuiScene));
    plume.H5PreLoadingScene = H5PreLoadingScene;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Fonts = /** @class */ (function () {
        function Fonts() {
        }
        Fonts.clearYOffset = function (fontName) {
            var font = plume.Resources.fonts[fontName];
            if (font == null) {
                throw new Error("Font not loaded: " + fontName);
            }
            var offsetmin = Number.POSITIVE_INFINITY;
            for (var c = 0; c < font.chars.length; c++) {
                var letter = font.lettersByCode[font.chars[c]];
                if (letter.height <= 0)
                    continue;
                if (letter.yoffset < offsetmin)
                    offsetmin = letter.yoffset;
            }
            for (var c = 0; c < font.chars.length; c++) {
                var letter = font.lettersByCode[font.chars[c]];
                if (letter.height <= 0)
                    continue;
                letter.yoffset = letter.yoffset - offsetmin;
            }
        };
        return Fonts;
    }());
    plume.Fonts = Fonts;
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.gui.js.map