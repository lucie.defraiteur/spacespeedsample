"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var plume;
(function (plume) {
    var AbstractSound = /** @class */ (function () {
        function AbstractSound(_howl) {
            this._howl = _howl;
            this.muted = false;
        }
        AbstractSound.prototype.play0 = function (sprite, onEndCallback) {
            if (this.muted) {
                if (onEndCallback)
                    onEndCallback();
                return 0;
            }
            var soundId = this._howl.play(sprite);
            if (onEndCallback != null) {
                this._howl.once("end", function () {
                    onEndCallback();
                }, soundId);
            }
            return soundId;
        };
        AbstractSound.prototype.pause = function (id) {
            this._howl.pause(id);
        };
        AbstractSound.prototype.stop = function (id) {
            this._howl.stop(id);
        };
        AbstractSound.prototype.mute = function (id) {
            this.muted = true;
            this._howl.mute(true, id);
        };
        AbstractSound.prototype.unmute = function (id) {
            this.muted = false;
            this._howl.mute(false, id);
        };
        AbstractSound.prototype.fade = function (from, to, duration, id, callback) {
            if (this.muted) {
                if (callback)
                    callback();
                return;
            }
            this._howl.fade(from, to, duration, id);
            if (callback) {
                this._howl.once("fade", function (sid) {
                    callback();
                }, id);
            }
        };
        AbstractSound.prototype.loop = function (loop, id) {
            this._howl.loop(loop, id);
        };
        AbstractSound.prototype.seek = function (seek, id) {
            return this._howl.seek(seek, id);
        };
        AbstractSound.prototype.rate = function (rate, id) {
            return this._howl.rate(rate, id);
        };
        AbstractSound.prototype.setVolume = function (volume, id) {
            this._howl.volume(volume, id);
        };
        AbstractSound.prototype.getVolume = function () {
            return this._howl.volume();
        };
        return AbstractSound;
    }());
    plume.AbstractSound = AbstractSound;
})(plume || (plume = {}));
///<reference path="../../../vendor/howler.d.ts"/>
var plume;
(function (plume) {
    var AudioManager = /** @class */ (function () {
        function AudioManager(_visibilityManager) {
            this._visibilityManager = _visibilityManager;
            this._soundSprites = {};
            this._musics = {};
            this._enableMusics = true;
            this._enableSoundsSprites = true;
            this._musicWasEnable = true;
            this._soundWasEnable = true;
            this.musicStateChangeHandlers = new plume.Handler();
            AudioManager._INSTANCE = this;
            this._visibilityManager.addVisibilityChangeHandler(this.onVisibilityChange.bind(this));
        }
        // Allow to load sound after preloading
        AudioManager.prototype.loadSounds = function (sounds, onComplete) {
            var loader = new plume.Loader2d(sounds);
            loader.load(function (complete) {
                onComplete(complete);
            });
        };
        AudioManager.prototype.registerSoundsheet = function (name, data) {
            var sprites = plume.JSONS.safeJson(data);
            if (sprites == null)
                return;
            // Copy urls property from json def to "src" property for howler
            sprites.src = sprites.urls;
            var howl = new Howl(sprites);
            if (this.getSpriteCount(sprites.sprite) <= 1) {
                // Single sprite: assume it is music
                plume.logger.debug("Registering music: " + name);
                this.addMusic(name, new plume.Music(howl, sprites.sprite));
            }
            else {
                plume.logger.debug("Registering sound: " + name);
                this.addSoundSprite(name, new plume.Sound(howl));
            }
        };
        AudioManager.prototype.getSpriteCount = function (spriteObj) {
            if (spriteObj == null)
                return 0;
            var count = 0;
            for (var k in spriteObj) {
                count++;
            }
            return count;
        };
        AudioManager.prototype.mute = function () {
            Howler.mute(true);
        };
        AudioManager.prototype.unmute = function () {
            Howler.mute(false);
        };
        AudioManager.prototype.volume = function (volume) {
            Howler.volume(volume);
        };
        AudioManager.prototype.addMusic = function (name, music) {
            if (!this._enableMusics) {
                music.mute();
            }
            this._musics[name] = music;
        };
        AudioManager.prototype.getMusic = function (fileName) {
            var music = this._musics[fileName];
            if (!music) {
                plume.logger.error("Can't find music with name: " + fileName);
                return null;
            }
            return music;
        };
        AudioManager.prototype.muteMusics = function () {
            plume.logger.debug("muting musics ");
            for (var key in this._musics) {
                this._musics[key].mute();
            }
            this._enableMusics = false;
            this.musicStateChangeHandlers.fire(false);
        };
        AudioManager.prototype.unmuteMusics = function () {
            plume.logger.debug("unmuting musics ");
            for (var key in this._musics) {
                this._musics[key].unmute();
            }
            this._enableMusics = true;
            this.musicStateChangeHandlers.fire(true);
        };
        AudioManager.prototype.volumeMusics = function (volume) {
            for (var key in this._musics) {
                this._musics[key].setVolume(volume);
            }
        };
        Object.defineProperty(AudioManager.prototype, "enableMusics", {
            get: function () {
                return this._enableMusics;
            },
            set: function (e) {
                if (e) {
                    this.unmuteMusics();
                }
                else {
                    this.muteMusics();
                }
            },
            enumerable: true,
            configurable: true
        });
        AudioManager.prototype.addSoundSprite = function (name, sound) {
            this._soundSprites[name] = sound;
            if (!this._enableSoundsSprites) {
                sound.mute();
            }
        };
        AudioManager.prototype.getSoundSprite = function (fileName) {
            var sound = this._soundSprites[fileName];
            if (!sound) {
                plume.logger.error("Can't find sound sprite with name: " + fileName + ". Sprite may not be loaded yet or load failed.");
                return null;
            }
            return sound;
        };
        AudioManager.prototype.muteSoundSprites = function (key, mute) {
            var s = this._soundSprites[key];
            if (s != null) {
                if (mute)
                    s.mute();
                else
                    s.unmute();
            }
        };
        AudioManager.prototype.muteSoundsSprites = function () {
            for (var key in this._soundSprites) {
                this._soundSprites[key].mute();
            }
            this._enableSoundsSprites = false;
        };
        AudioManager.prototype.unmuteSoundsSprites = function () {
            for (var key in this._soundSprites) {
                this._soundSprites[key].unmute();
            }
            this._enableSoundsSprites = true;
        };
        AudioManager.prototype.volumeSoundsSprites = function (volume) {
            for (var key in this._soundSprites) {
                this._soundSprites[key].setVolume(volume);
            }
        };
        Object.defineProperty(AudioManager.prototype, "enableSoundsSprites", {
            get: function () {
                return this._enableSoundsSprites;
            },
            set: function (e) {
                if (e) {
                    this.unmuteSoundsSprites();
                }
                else {
                    this.muteSoundsSprites();
                }
            },
            enumerable: true,
            configurable: true
        });
        AudioManager.prototype.onVisibilityChange = function (visible) {
            if (visible) {
                // Restore audio parameters
                this.enableMusics = this._musicWasEnable;
                this.enableSoundsSprites = this._soundWasEnable;
            }
            else {
                // Save audio parameters
                this._musicWasEnable = this._enableMusics;
                this._soundWasEnable = this._enableSoundsSprites;
                this.muteMusics();
                this.muteSoundsSprites();
            }
        };
        AudioManager.get = function () {
            return AudioManager._INSTANCE;
        };
        return AudioManager;
    }());
    plume.AudioManager = AudioManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Music = /** @class */ (function (_super) {
        __extends(Music, _super);
        function Music(howl, sprite) {
            var _this = _super.call(this, howl) || this;
            _this._spriteName = null;
            if (sprite != null) {
                // assumption: we have only one sprite with the music
                for (var k in sprite) {
                    _this._spriteName = k;
                    break;
                }
            }
            return _this;
        }
        Music.prototype.play = function (onEndCallback) {
            return _super.prototype.play0.call(this, this._spriteName, onEndCallback);
        };
        return Music;
    }(plume.AbstractSound));
    plume.Music = Music;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Sound = /** @class */ (function (_super) {
        __extends(Sound, _super);
        function Sound(howl) {
            return _super.call(this, howl) || this;
        }
        Sound.prototype.play = function (sprite, onEndCallback) {
            return _super.prototype.play0.call(this, sprite, onEndCallback);
        };
        return Sound;
    }(plume.AbstractSound));
    plume.Sound = Sound;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Game = /** @class */ (function () {
        function Game(width, height, options) {
            if (options === void 0) { options = {}; }
            this._lastTick = 0;
            // Add generic object aware of the gameloop
            this._updatables = [];
            this._renderables = [];
            this._doingUpdate = false;
            this._afterUpdate = [];
            // TODO move from game to stat collector
            this._trackStats = false;
            Game.instance = this;
            // sanitize options
            options.gameLoop = options.gameLoop || {};
            options.gameLoop.desiredFps = options.gameLoop.desiredFps || 60;
            options.gameLoop.forceSetTimeout = options.gameLoop.forceSetTimeout || false;
            options.gameLoop.smoothDelta = options.gameLoop.smoothDelta || false;
            options.input = options.input || {};
            options.input.maxTouchPointer = options.input.maxTouchPointer || 1;
            this._lastTick = performance.now();
            this._bounds = new plume.Rectangle(0, 0, width, height);
            this.options = options;
            this.timeManager = plume.TimeManager.get();
            this.deviceManager = new plume.DeviceManager();
            this.audioManager = this.newAudioManager();
        }
        Game.prototype.start = function () {
            var _this = this;
            this.deviceManager.whenReady(function () {
                try {
                    _this.start0();
                }
                catch (e) {
                    plume.logger.error("Error occured while starting game", e);
                }
            });
        };
        Game.prototype.newAudioManager = function () {
            return new plume.AudioManager(this.deviceManager.visibilityManager);
        };
        Game.prototype.newScaleManager = function () {
            return new plume.ScaleManager(this);
        };
        Game.get = function () {
            return Game.instance;
        };
        Game.options = function () {
            return Game.instance.options;
        };
        Game.prototype.start0 = function () {
            var self = this;
            this.scaleManager = this.newScaleManager();
            this.mainLoop = new plume.MainLoop(this.update.bind(this), this.render.bind(this));
            this.mainLoop.start();
            this.inputManager = new plume.InputManager(this);
            this.onCreate();
            window.addEventListener('load', function () {
                self.scaleManager.resize();
            });
            // In iframe, take focus on click
            if (window.parent) {
                document.addEventListener('click', function () {
                    if (!document.hasFocus()) {
                        window.focus();
                    }
                }, false);
            }
        };
        Game.prototype.initializeEngine = function (engine) {
            this._engine = engine;
            this.inputManager.mouse.enableOnElement(engine.canvas);
        };
        Game.prototype.initializeGui = function (gui) {
            this._gui = gui;
        };
        Game.prototype.initializeSimulation = function (simulation) {
            this._simulation = simulation;
        };
        Game.prototype.onCreate = function () {
        };
        Game.prototype.update = function (time, timeEllapsed) {
            var lastTick = this._lastTick;
            this._lastTick = performance.now();
            if (this._trackStats) {
                var delta = this._lastTick - lastTick;
                this._deltaStat.sum += delta;
                this._deltaStat.count += 1;
            }
            plume.Time._refresh(time);
            var t0 = performance.now();
            this._doingUpdate = true;
            try {
                this.inputManager.update(timeEllapsed);
                this.timeManager.update(timeEllapsed);
                for (var i = 0; i < this._updatables.length; i++) {
                    this._updatables[i].update(timeEllapsed);
                }
                if (this._simulation != null && this._simulation.running)
                    this._simulation.update(timeEllapsed);
                if (this._gui)
                    this._gui.update(timeEllapsed);
                this._doingUpdate = false;
                if (this._afterUpdate.length > 0) {
                    for (var i = 0; i < this._afterUpdate.length; i++) {
                        this._afterUpdate[i]();
                    }
                    this._afterUpdate = [];
                }
            }
            catch (e) {
                plume.logger.error("Error occured in game update:" + e.message, e);
                this._doingUpdate = false;
            }
            var t1 = performance.now();
            if (this._trackStats) {
                var ellapsed = t1 - t0;
                this._updateStat.sum += ellapsed;
                this._updateStat.count += 1;
            }
        };
        Game.prototype.render = function (time, timeEllapsed, interpolation) {
            var t1 = performance.now();
            try {
                for (var i = 0; i < this._renderables.length; i++) {
                    this._renderables[i].render(interpolation);
                }
                if (this._engine)
                    this._engine.render(timeEllapsed, interpolation);
                if (this._gui)
                    this._gui.render(timeEllapsed, interpolation);
            }
            catch (e) {
                plume.logger.error("Error occured in game render: ", e);
            }
            var t2 = performance.now();
            if (this._trackStats) {
                var ellapsed = t2 - t1;
                this._renderStat.sum += ellapsed;
                this._renderStat.count += 1;
            }
            if (plume.ephemeralPool != null) {
                plume.ephemeralPool.clean();
            }
        };
        Game.prototype.addRenderable = function (renderable) {
            this._renderables.push(renderable);
        };
        Game.prototype.removeRenderable = function (renderable) {
            var i = this._renderables.indexOf(renderable);
            if (i >= 0) {
                this._renderables.splice(i, 1);
            }
            //else {
            // logger.warn("Cannot remove renderable " + i);
            //}
        };
        Game.prototype.addUpdatable = function (updatable) {
            if (this._doingUpdate) {
                var self_1 = this;
                this._afterUpdate.push(function () {
                    self_1._updatables.push(updatable);
                });
            }
            else {
                this._updatables.push(updatable);
            }
        };
        Game.prototype.removeUpdatable = function (updatable) {
            if (this._doingUpdate) {
                var self_2 = this;
                this._afterUpdate.push(function () {
                    self_2.removeUpdatable(updatable);
                });
            }
            else {
                var i = this._updatables.indexOf(updatable);
                if (i >= 0) {
                    this._updatables.splice(i, 1);
                }
                //else {
                // logger.warn("Cannot remove updatable " + i);
                //}
            }
        };
        Game.prototype.getGui = function () {
            return this._gui;
        };
        Object.defineProperty(Game.prototype, "engine", {
            get: function () {
                return this._engine;
            },
            set: function (v) {
                throw new Error("use initializeEngine()");
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Game.prototype, "simulation", {
            get: function () {
                return this._simulation;
            },
            set: function (v) {
                this._simulation = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Game.prototype, "trackStats", {
            get: function () {
                return this._trackStats;
            },
            set: function (v) {
                this._trackStats = true;
                this._updateStat = { count: 0, start: performance.now(), sum: 0 };
                this._renderStat = { count: 0, start: performance.now(), sum: 0 };
                this._deltaStat = { count: 0, start: performance.now(), sum: 0 };
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Game.prototype, "width", {
            get: function () {
                return this._bounds.width;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Game.prototype, "height", {
            get: function () {
                return this._bounds.height;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Game.prototype, "bounds", {
            get: function () {
                return this._bounds;
            },
            enumerable: true,
            configurable: true
        });
        return Game;
    }());
    plume.Game = Game;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var RequestAnimationFrame = /** @class */ (function () {
        function RequestAnimationFrame(callback, forceSetTimeOut, desiredFps) {
            if (desiredFps === void 0) { desiredFps = 60; }
            this.callback = callback;
            this.forceSetTimeOut = forceSetTimeOut;
            this._running = false;
            this._setTimeoutSleep = 1000 / desiredFps;
            this._onSetTimeoutCbBound = this._onSetTimeoutCb.bind(this);
            this._onRafCbBound = this._onRafCb.bind(this);
        }
        RequestAnimationFrame.prototype.start = function () {
            if (this._running)
                return;
            this._running = true;
            this._tick = performance.now();
            this._timeoutId = (this.forceSetTimeOut) ? window.setTimeout(this._onSetTimeoutCbBound, 0) : window.requestAnimationFrame(this._onRafCbBound);
        };
        RequestAnimationFrame.prototype.stop = function () {
            if (!this._running)
                return;
            this._running = false;
            if (this.forceSetTimeOut) {
                clearTimeout(this._timeoutId);
            }
            else {
                window.cancelAnimationFrame(this._timeoutId);
            }
        };
        RequestAnimationFrame.prototype._onSetTimeoutCb = function () {
            this._tick = performance.now();
            this.callback(this._tick);
            var nextCall = Math.min(this._setTimeoutSleep, performance.now() - this._tick);
            this._timeoutId = window.setTimeout(this._onSetTimeoutCbBound, nextCall);
        };
        RequestAnimationFrame.prototype._onRafCb = function (timestamp) {
            this._tick = timestamp;
            this.callback(timestamp);
            this._timeoutId = window.requestAnimationFrame(this._onRafCbBound);
        };
        return RequestAnimationFrame;
    }());
    plume.RequestAnimationFrame = RequestAnimationFrame;
    var MainLoop = /** @class */ (function () {
        function MainLoop(updateCb, renderCb) {
            this.updateCb = updateCb;
            this.renderCb = renderCb;
            this._started = false;
            this._lastTime = 0;
            this._deltaHistoryIndex = 0;
            this._deltaHistoryCount = 10;
            this._deltaHistory = [];
            this._frameDelta = 0;
        }
        MainLoop.prototype.start = function () {
            if (this._started)
                return;
            var loopOptions = plume.Game.get().options.gameLoop;
            var desiredFps = (loopOptions != null && loopOptions.desiredFps != null) ? loopOptions.desiredFps : 60;
            var useSetimeout = (loopOptions != null && loopOptions.forceSetTimeout != null) ? loopOptions.forceSetTimeout : false;
            var self = this;
            this._started = true;
            this._targetDeltaTime = 1000 / desiredFps; // Estimated time between 2 loop
            this._timestep = 1000 / desiredFps;
            this._lastTime = performance.now();
            if (loopOptions.smoothDelta) {
                this._raf = new RequestAnimationFrame(this._ticksmooth.bind(this), useSetimeout, desiredFps);
                for (var i = 0; i < this._deltaHistoryCount; i++) {
                    this._deltaHistory[i] = this._targetDeltaTime;
                }
            }
            else {
                this._raf = new RequestAnimationFrame(this._tick.bind(this), useSetimeout, desiredFps);
            }
            this._raf.start();
        };
        MainLoop.prototype._tick = function (time) {
            time = performance.now();
            var delta = time - this._lastTime;
            delta = Math.min(delta, 100);
            var interpolation = plume.Mathf.clamp(this._targetDeltaTime / delta, 0, 1);
            this._lastTime = time;
            this.updateCb(time, delta);
            this.renderCb(time, delta, interpolation);
        };
        // test, smooth delta time
        MainLoop.prototype._ticksmooth = function (time) {
            time = performance.now();
            var delta = time - this._lastTime;
            if (delta > 100) {
                delta = this._deltaHistory[this._deltaHistoryIndex];
            }
            this._deltaHistory[this._deltaHistoryIndex] = delta;
            this._deltaHistoryIndex++;
            if (this._deltaHistoryIndex >= this._deltaHistoryCount)
                this._deltaHistoryIndex = 0;
            var avg = 0;
            for (var i = 0; i < this._deltaHistoryCount; i++) {
                avg += this._deltaHistory[i];
            }
            avg /= this._deltaHistoryCount;
            var interpolation = avg / delta;
            this._lastTime = time;
            this.updateCb(time, avg);
            this.renderCb(time, delta, interpolation);
        };
        MainLoop.prototype.stop = function () {
            if (!this._started)
                return;
            this._started = false;
            this._raf.stop();
        };
        return MainLoop;
    }());
    plume.MainLoop = MainLoop;
})(plume || (plume = {}));
var plume;
(function (plume) {
    (function (win) {
        // Date.now
        if (!(Date.now && Date.prototype.getTime)) {
            Date.now = function now() {
                return new Date().getTime();
            };
        }
        // performance.now
        if (!(win.performance && win.performance.now)) {
            var startTime_1 = Date.now();
            if (!win.performance) {
                win.performance = {};
            }
            win.performance.now = function () {
                return Date.now() - startTime_1;
            };
        }
        // requestAnimationFrame
        var lastTime = Date.now();
        var prefix = ['ms', 'moz', 'webkit', 'o'];
        for (var x = 0; x < prefix.length && !win.requestAnimationFrame; ++x) {
            win.requestAnimationFrame = win[prefix[x] + 'RequestAnimationFrame'];
            win.cancelAnimationFrame = win[prefix[x] + 'CancelAnimationFrame'] || win[prefix[x] + 'CancelRequestAnimationFrame'];
        }
        if (!win.requestAnimationFrame) {
            win.requestAnimationFrame = function (callback) {
                if (typeof callback !== 'function') {
                    throw new TypeError(callback + 'is not a function');
                }
                var currentTime = Date.now();
                var delay = 16 + lastTime - currentTime;
                if (delay < 0) {
                    delay = 0;
                }
                lastTime = currentTime;
                return setTimeout(function () {
                    lastTime = Date.now();
                    callback(performance.now());
                }, delay);
            };
        }
        if (!win.cancelAnimationFrame) {
            win.cancelAnimationFrame = function (id) {
                clearTimeout(id);
            };
        }
    })(window);
})(plume || (plume = {}));
var plume;
(function (plume) {
    var ScaleManager = /** @class */ (function () {
        function ScaleManager(game) {
            this._pendingResize = false;
            this.enable = true;
            this.canvasToResize = [];
            this._game = game;
            this.init();
        }
        ScaleManager.prototype.init = function () {
            // fit screen by default
            this._scalingStategy = new FitScalingStrategy();
            var self = this;
            window.addEventListener('resize', function () {
                if (self._pendingResize)
                    return;
                self._pendingResize = true;
                window.setTimeout(function () {
                    self.resize();
                }, 50);
            });
            window.addEventListener('deviceOrientation', function () {
                if (self._pendingResize)
                    return;
                self._pendingResize = true;
                window.setTimeout(function () {
                    self.resize();
                }, 50);
            });
            this.resizeHandler = new plume.Handler();
            this.resize();
        };
        ScaleManager.prototype.resize = function () {
            this._pendingResize = false;
            if (!this.enable)
                return;
            var width = document.documentElement.clientWidth;
            var height = document.documentElement.clientHeight;
            if (this._windowSizeProvider != null) {
                var size = this._windowSizeProvider();
                width = size.width;
                height = size.height;
            }
            var gameBounds = this._game.bounds;
            this._currentScaling = this._scalingStategy.getScaling(gameBounds.width, gameBounds.height, width, height);
            if (window.scrollTo) {
                window.scrollTo(0, 0);
            }
            // Fire resize event
            this.resizeHandler.fire({});
            // logger.debug("Scaling is => " + this.scale);
            // logger.debug("Game bounds is => " + gameBounds);
            // logger.debug("Absolute game size is:" + JSON.stringify(this.getAbsoluteGameSize()));
        };
        ScaleManager.prototype.setWindowSizeProvider = function (provider) {
            this._windowSizeProvider = provider;
        };
        ScaleManager.prototype.setScalingStategy = function (scalingStategy) {
            this._scalingStategy = scalingStategy;
        };
        ScaleManager.prototype.getAbsoluteGameSize = function () {
            var game = plume.Game.get();
            var w = game.width * this._currentScaling.scale;
            var h = game.height * this._currentScaling.scale;
            return {
                width: w,
                height: h,
                marginX: this._currentScaling.x,
                marginY: this._currentScaling.y
            };
        };
        ScaleManager.prototype.getWindowSize = function () {
            return {
                width: this._currentScaling.windowWidth,
                height: this._currentScaling.windowHeight
            };
        };
        ScaleManager.prototype.getCurrentScaling = function () {
            return this._currentScaling;
        };
        Object.defineProperty(ScaleManager.prototype, "offsetX", {
            get: function () {
                return this._currentScaling.x;
            },
            set: function (s) {
                this._currentScaling.x = s;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScaleManager.prototype, "offsetY", {
            get: function () {
                return this._currentScaling.y;
            },
            set: function (s) {
                this._currentScaling.y = s;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScaleManager.prototype, "scale", {
            get: function () {
                return this._currentScaling.scale;
            },
            set: function (s) {
                this._currentScaling.scale = s;
            },
            enumerable: true,
            configurable: true
        });
        return ScaleManager;
    }());
    plume.ScaleManager = ScaleManager;
    var FitScalingStrategy = /** @class */ (function () {
        function FitScalingStrategy() {
        }
        FitScalingStrategy.prototype.getScaling = function (sourceWidth, sourceHeight, targetWidth, targetHeight) {
            var targetRatio = targetHeight / targetWidth;
            var sourceRatio = sourceHeight / sourceWidth;
            var scale = targetRatio > sourceRatio ? targetWidth / sourceWidth : targetHeight / sourceHeight;
            return {
                x: (targetWidth - sourceWidth * scale) / 2,
                y: (targetHeight - sourceHeight * scale) / 2,
                scale: scale,
                windowWidth: targetWidth,
                windowHeight: targetHeight,
            };
        };
        return FitScalingStrategy;
    }());
    plume.FitScalingStrategy = FitScalingStrategy;
    var FillScalingStrategy = /** @class */ (function () {
        function FillScalingStrategy() {
        }
        FillScalingStrategy.prototype.getScaling = function (sourceWidth, sourceHeight, targetWidth, targetHeight) {
            var targetRatio = targetHeight / targetWidth;
            var sourceRatio = sourceHeight / sourceWidth;
            var scale = targetRatio < sourceRatio ? targetWidth / sourceWidth : targetHeight / sourceHeight;
            return {
                x: (targetWidth - sourceWidth * scale) / 2,
                y: (targetHeight - sourceHeight * scale) / 2,
                scale: scale,
                windowWidth: targetWidth,
                windowHeight: targetHeight,
            };
        };
        return FillScalingStrategy;
    }());
    plume.FillScalingStrategy = FillScalingStrategy;
})(plume || (plume = {}));
// this file is auto-generated.
var plume;
(function (plume) {
    var core;
    (function (core) {
        var shaders;
        (function (shaders) {
            shaders.test1_fragment = 'void main() {\ngl_FragColor = vec4(1, 0, 0.5, 1);\n}';
            shaders.test1_vertex = 'attribute vec2 a_position;\nvoid main() {\ngl_Position = vec4(a_position, 0, 1);\n}';
        })(shaders = core.shaders || (core.shaders = {}));
    })(core = plume.core || (plume.core = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var JSONS = /** @class */ (function () {
        function JSONS() {
        }
        // deep merge source in dest
        JSONS.merge = function (source, dest) {
            for (var key in source) {
                var sValue = source[key];
                var typ = (typeof sValue);
                if (typ === "constructor" || typ == "function") {
                    continue;
                }
                if ((key in dest) && (typeof sValue === "object")) {
                    JSONS.merge(sValue, dest[key]);
                }
                else {
                    dest[key] = sValue;
                }
            }
        };
        JSONS.safeJson = function (data, defaultValue) {
            if (defaultValue === void 0) { defaultValue = null; }
            if (data == null || data.length == 0)
                return defaultValue;
            try {
                var jso = JSON.parse(data);
                return jso;
            }
            catch (e) {
                plume.logger.error("Failed to parse json data=" + data, e);
            }
            return defaultValue;
        };
        return JSONS;
    }());
    plume.JSONS = JSONS;
})(plume || (plume = {}));
/// <reference path="../utils/Jsons.ts" />
var plume;
(function (plume) {
    var I18N = /** @class */ (function () {
        function I18N() {
        }
        I18N.setAvailableLocales = function (locales) {
            I18N.availableLocales = locales;
        };
        I18N.registerTranslation = function (locale, translation) {
            if (I18N.translations[locale] == null) {
                I18N.translations[locale] = {};
            }
            plume.JSONS.merge(translation, I18N.translations[locale]);
        };
        I18N.setDefaultLocale = function (locale) {
            I18N.defaultLocale = locale;
        };
        I18N.setLocale = function (locale) {
            if (I18N.availableLocales.indexOf(locale) >= 0) {
                I18N.locale = locale;
            }
            else {
                I18N.locale = I18N.defaultLocale;
            }
        };
        I18N.isAvailableLocale = function (locale) {
            return I18N.availableLocales.indexOf(locale) >= 0;
        };
        I18N.get = function (key, params) {
            var value = I18N.translations[I18N.locale][key];
            if (value) {
                if (params != null)
                    return plume.Strings.interpolate(value, params);
                return value;
            }
            else {
                var en = I18N.translations[I18N.defaultLocale][key];
                if (en) {
                    if (params != null)
                        return plume.Strings.interpolate(en, params);
                    return en;
                }
                return key;
            }
        };
        I18N.getPluralForm = function (n, local) {
            if (local == null)
                local = I18N.locale;
            // n : actual number
            var plural = 2;
            if (local == "fr") {
                plural = (n > 1 ? 1 : 0);
            }
            else if (local == "lv") {
                plural = (n == 0 ? 0 : (n % 10 == 1 && n % 100 != 11 ? 1 : 2));
            }
            else if (local == "pl") {
                plural = (n == 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
            }
            else if (local == "ru") {
                plural = (n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
            }
            else {
                // english rule
                plural = (n == 1 ? 0 : 1);
            }
            return plural;
        };
        I18N.getPlural = function (n, keyBase, params) {
            if (params == null) {
                // default, put "n" as parameters
                params = { "n": n };
            }
            // n : actual number
            // keyBase: ref key to get "exetnded form". ie if key is "N_POINT" we will look in dictionnaries N_POINT_F0, N_POINT_F1 or N_POINT_F2
            var plural = I18N.getPluralForm(n);
            var key = keyBase + "_F" + plural;
            var str = I18N.get(key, params);
            return str;
        };
        I18N.availableLocales = [];
        I18N.defaultLocale = "en";
        I18N.locale = "en";
        I18N.translations = [];
        return I18N;
    }());
    plume.I18N = I18N;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var AbstractInput = /** @class */ (function () {
        function AbstractInput() {
            this.isDown = false;
            this.isUp = true;
            this.isJustDown = false;
            this.isJustUp = false;
            this.timeDown = 0;
            this.timeUp = 0;
            this.downDuration = 0;
            this.justUpdated = false;
            this._duration = 0;
        }
        AbstractInput.prototype.update = function () {
            if (this.isDown) {
                this.downDuration = plume.Time.now() - this.timeDown;
            }
            if (this.justUpdated) {
                this.isJustDown = this.isDown;
                this.isJustUp = this.isUp;
            }
            else {
                this.isJustDown = false;
                this.isJustUp = false;
            }
            this.justUpdated = false;
        };
        AbstractInput.prototype.onDown = function () {
            if (this.isDown)
                return;
            this.justUpdated = true;
            this.isDown = true;
            this.isUp = false;
            this.timeDown = plume.Time.now();
            this.downDuration = 0;
        };
        AbstractInput.prototype.onUp = function () {
            if (this.isUp)
                return;
            this.justUpdated = true;
            this.isDown = false;
            this.isUp = true;
            this.timeUp = plume.Time.now();
        };
        return AbstractInput;
    }());
    plume.AbstractInput = AbstractInput;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var InputManager = /** @class */ (function () {
        function InputManager(game) {
            this._pointerDirty = false;
            this._maxTouchPointer = 1;
            this._disabled = false;
            this.mouseEventInterpretors = [];
            this.noCursor = false;
            this._maxTouchPointer = (game.options.input == null ? 1 : (game.options.input.maxTouchPointer == null ? 1 : game.options.input.maxTouchPointer));
            this.desktop = game.deviceManager.os.desktop;
            this.keyboard = new plume.KeyboardHandler(game, this);
            this.mouse = new plume.MouseHandler(game, this);
        }
        InputManager.prototype.update = function (dt) {
            if (!this._disabled) {
                this.keyboard.update();
                this.mouse.update();
                // delegate mouse cursor check to Gui or others
                for (var i = 0; i < this.mouseEventInterpretors.length; i++) {
                    this.mouseEventInterpretors[i].onUpdate();
                    //cf this._triggerMouseHit();
                }
            }
        };
        InputManager.prototype.triggerImmediateClick = function () {
            // delegate mouse cursor check to Gui or others
            for (var i = 0; i < this.mouseEventInterpretors.length; i++) {
                this.mouseEventInterpretors[i].onImmediate();
            }
            // for (let i = 0; i < this.mouse.pointers.length; i++) {
            //     let pointer = this.mouse.pointers[i];
            //     if (pointer.isActive()) {
            //         pointer._triggerImmediateClick();
            //     }
            // }
        };
        // 
        // Go through the scene and dispatch down, up, enter, out event
        // 
        InputManager.prototype._triggerMouseHit = function () {
            // TODO hover, mouse-in, ect in GUI
        };
        Object.defineProperty(InputManager.prototype, "maxTouchPointer", {
            get: function () {
                return this._maxTouchPointer;
            },
            enumerable: true,
            configurable: true
        });
        InputManager.prototype.enable = function () {
            this._disabled = false;
        };
        InputManager.prototype.disable = function () {
            this._disabled = true;
            // we need to update pointer (else isJustUp will be true for many gameloop)
            // better way ?
            this.mouse.update();
        };
        InputManager.prototype.hideCursor = function () {
            this.noCursor = true;
            // Game.get().renderer.canvas.style.cursor = "none";
        };
        return InputManager;
    }());
    plume.InputManager = InputManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var KeyCode = /** @class */ (function () {
        function KeyCode() {
        }
        KeyCode.fromChar = function (c) {
            return c.charCodeAt(0) - 32;
        };
        KeyCode.SHIFT = 17;
        KeyCode.CTRL = 17;
        KeyCode.ALT = 18;
        KeyCode.SPACEBAR = 32;
        KeyCode.LEFT = 37;
        KeyCode.PAGEUP = 33;
        KeyCode.PAGEDOWN = 34;
        KeyCode.END = 35;
        KeyCode.HOME = 36;
        KeyCode.UP = 38;
        KeyCode.RIGHT = 39;
        KeyCode.DOWN = 40;
        KeyCode.INSERT = 45;
        KeyCode.DELETE = 46;
        KeyCode.F1 = 112;
        KeyCode.F2 = 113;
        KeyCode.F3 = 114;
        KeyCode.F4 = 115;
        KeyCode.F5 = 116;
        KeyCode.F6 = 117;
        KeyCode.F7 = 118;
        KeyCode.F8 = 119;
        KeyCode.F9 = 120;
        KeyCode.F10 = 121;
        KeyCode.F11 = 122;
        KeyCode.F12 = 123;
        return KeyCode;
    }());
    plume.KeyCode = KeyCode;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var KeyInput = /** @class */ (function (_super) {
        __extends(KeyInput, _super);
        function KeyInput(keycode) {
            var _this = _super.call(this) || this;
            _this.keycode = keycode;
            return _this;
        }
        KeyInput.prototype.keyDown = function (e) {
            if (this.isDown)
                return;
            this.event = e;
            _super.prototype.onDown.call(this);
        };
        KeyInput.prototype.keyUp = function (e) {
            if (this.isUp)
                return;
            this.event = e;
            _super.prototype.onUp.call(this);
        };
        return KeyInput;
    }(plume.AbstractInput));
    plume.KeyInput = KeyInput;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var KeyboardHandler = /** @class */ (function () {
        function KeyboardHandler(game, inputManager) {
            var _this = this;
            this._enable = false;
            this.keys = [];
            this.keysByCode = new plume.HashMap();
            this.propagatedKeys = [];
            this.propagateAllKeys = false;
            this._keydown = function (e) {
                var key = _this._ensureKey(e.which, e.code);
                key.keyDown(e);
                _this.keyDownHandlers.fire(key);
                _this._stopEvent(e);
            };
            this._keyup = function (e) {
                var key = _this._ensureKey(e.which, e.code);
                key.keyUp(e);
                _this.keyUpHandlers.fire(key);
                _this._stopEvent(e);
            };
            this.enable();
            // dev (refresh/ devtools)
            this.propagatedKeys.push(plume.KeyCode.F5);
            this.propagatedKeys.push(plume.KeyCode.F12);
            this.keyUpHandlers = new plume.Handler();
            this.keyDownHandlers = new plume.Handler();
        }
        KeyboardHandler.prototype.update = function () {
            if (!this.enable)
                return;
            for (var i = 0; i < this.keys.length; i++) {
                var k = this.keys[i];
                if (k != null) {
                    k.update();
                }
            }
        };
        KeyboardHandler.prototype.enable = function () {
            if (this._enable)
                return;
            this._enable = true;
            window.addEventListener("keydown", this._keydown, false);
            window.addEventListener("keyup", this._keyup, false);
        };
        KeyboardHandler.prototype.disable = function () {
            if (!this._enable)
                return;
            this._enable = false;
            window.removeEventListener("keydown", this._keydown);
            window.removeEventListener("keyup", this._keyup);
        };
        KeyboardHandler.prototype.key = function (code) {
            return this._getInput(code);
        };
        KeyboardHandler.prototype.isDown = function (code) {
            var key = this._getInput(code);
            if (key == null)
                return false;
            return key.isDown;
        };
        KeyboardHandler.prototype.isUp = function (code) {
            var key = this._getInput(code);
            if (key == null)
                return true;
            return key.isUp;
        };
        KeyboardHandler.prototype.isJustDown = function (code) {
            var key = this._getInput(code);
            if (key == null)
                return false;
            return key.isJustDown;
        };
        KeyboardHandler.prototype.isJustUp = function (code) {
            var key = this._getInput(code);
            if (key == null)
                return false;
            return key.isJustUp;
        };
        KeyboardHandler.prototype._stopEvent = function (e) {
            if (this.propagateAllKeys)
                return;
            var keycode = e.which;
            if (keycode && plume.Collections.contains(this.propagatedKeys, keycode)) {
                return;
            }
            e.preventDefault();
            e.stopPropagation();
        };
        KeyboardHandler.prototype._ensureKey = function (keycode, code) {
            var key = this.keys[keycode];
            if (key == null) {
                key = new plume.KeyInput(keycode);
                this.keys[keycode] = key; // keyocde : 1, 2, 3, etc
                this.keysByCode.put(code, key); // code: KeyA, KeyB, Space, etc
            }
            return key;
        };
        KeyboardHandler.prototype._getInput = function (k) {
            if (typeof k == "number")
                return this.keys[k];
            return this.keysByCode.get(k);
        };
        return KeyboardHandler;
    }());
    plume.KeyboardHandler = KeyboardHandler;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var MouseHandler = /** @class */ (function () {
        function MouseHandler(game, inputManager) {
            var _this = this;
            this._enable = false;
            this._touchEndFired = false;
            this.propagateAllEvents = false;
            this.wheelDelta = 0;
            this._lastWheel = 0;
            this._mousedown = function (e) {
                _this.pointer.mouseDown(e);
                _this.inputManager._pointerDirty = true;
                _this._stopEvent(e);
            };
            this._mousemove = function (e) {
                _this.pointer.mouseMove(e);
                _this.inputManager._pointerDirty = true;
                _this._stopEvent(e);
            };
            this._mouseup = function (e) {
                _this.pointer.mouseUp(e);
                _this.inputManager._pointerDirty = true;
                _this._stopEvent(e);
            };
            this._mouseout = function (e) {
                // we do not want to trigger a fake mouseup when leaving
                // this.mouseup(e);
            };
            this._contextmenu = function (e) {
                _this._stopEvent(e);
            };
            this._mousewheel = function (e) {
                if (e.deltaY) {
                    var delta = e.deltaY;
                    if (delta < -1)
                        delta = -1;
                    else if (delta > 1)
                        delta = 1;
                    if (delta != 0) {
                        _this._lastWheel = delta;
                        _this.wheelDelta = delta;
                        _this.mouseWheelHandlers.fire({ delta: delta, event: e });
                    }
                }
                _this._stopEvent(e);
            };
            this._touchstart = function (e) {
                //this.debugTouch("touchstart", e);
                for (var i = 0; i < e.changedTouches.length; i++) {
                    var touch = e.changedTouches[i];
                    var pointer = _this._getPointerOrFirstFree(touch);
                    if (pointer != null) {
                        pointer.touchDown(touch);
                    }
                }
                _this.inputManager._pointerDirty = true;
                _this._stopEvent(e);
            };
            this._touchmove = function (e) {
                for (var i = 0; i < e.changedTouches.length; i++) {
                    var touch = e.changedTouches[i];
                    var pointer = _this._getPointerOrFirstFree(touch);
                    if (pointer != null) {
                        pointer.touchMove(touch);
                    }
                }
                _this.inputManager._pointerDirty = true;
                _this._stopEvent(e);
            };
            this._touchend = function (e) {
                //this.debugTouch("touchend", e);
                for (var i = 0; i < e.changedTouches.length; i++) {
                    var touch = e.changedTouches[i];
                    var pointer = _this._getPointerOrFirstFree(touch);
                    if (pointer != null) {
                        pointer.touchUp(touch);
                    }
                }
                _this.inputManager._pointerDirty = true;
                if (!_this._touchEndFired) {
                    // First event is fired up for howler to initiate sound on IOS
                    _this._touchEndFired = true;
                    return;
                }
                _this._stopEvent(e);
            };
            this._touchcancel = function (e) {
                _this._touchend(e);
            };
            this._touchleave = function (e) {
                _this._touchend(e);
            };
            this.game = game;
            this.inputManager = inputManager;
            this.pointer = new plume.PointerInput(game, inputManager);
            this.pointers = new Array(inputManager.maxTouchPointer);
            this.pointers[0] = this.pointer;
            for (var i = 1; i < inputManager.maxTouchPointer; i++) {
                this.pointers[i] = new plume.PointerInput(game, inputManager);
            }
            this.mouseWheelHandlers = new plume.Handler();
        }
        MouseHandler.prototype.update = function () {
            for (var i = 0; i < this.pointers.length; i++) {
                this.pointers[i].update();
            }
            this.wheelDelta = this._lastWheel;
            this._lastWheel = 0;
        };
        MouseHandler.prototype.enableOnElement = function (element) {
            this.disable();
            this._element = element;
            this.enable();
        };
        MouseHandler.prototype.enable = function () {
            if (this._enable)
                return;
            if (this._element == null)
                throw new Error("No element specified for mouse event handling");
            this._enable = true;
            // let element = document.body; => on ios => trigger pull to refresh
            this._element.addEventListener('mousedown', this._mousedown);
            this._element.addEventListener('mousemove', this._mousemove);
            this._element.addEventListener('mouseup', this._mouseup);
            this._element.addEventListener('mouseout', this._mouseout);
            this._element.addEventListener('wheel', this._mousewheel);
            this._element.addEventListener('contextmenu', this._contextmenu);
            this._element.addEventListener('touchstart', this._touchstart);
            this._element.addEventListener('touchmove', this._touchmove);
            this._element.addEventListener('touchend', this._touchend);
            this._element.addEventListener('touchcancel', this._touchcancel);
            this._element.addEventListener('touchleave', this._touchleave);
        };
        MouseHandler.prototype.disable = function () {
            if (!this._enable)
                return;
            if (this._element == null)
                return; // not enable
            this._enable = false;
            // let element = document.body;
            this._element.removeEventListener('mousedown', this._mousedown);
            this._element.removeEventListener('mousemove', this._mousemove);
            this._element.removeEventListener('mouseup', this._mouseup);
            this._element.removeEventListener('mouseout', this._mouseout);
            this._element.removeEventListener('wheel', this._mousewheel);
            this._element.removeEventListener('contextmenu', this._contextmenu);
            this._element.removeEventListener('touchstart', this._touchstart);
            this._element.removeEventListener('touchmove', this._touchmove);
            this._element.removeEventListener('touchend', this._touchend);
            this._element.removeEventListener('touchcancel', this._touchcancel);
            this._element.removeEventListener('touchleave', this._touchleave);
        };
        Object.defineProperty(MouseHandler.prototype, "isDown", {
            get: function () {
                return this.pointer.isDown;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MouseHandler.prototype, "isUp", {
            get: function () {
                return this.pointer.isUp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MouseHandler.prototype, "isJustDown", {
            get: function () {
                return this.pointer.isJustDown;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MouseHandler.prototype, "isJustUp", {
            get: function () {
                return this.pointer.isJustUp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MouseHandler.prototype, "downDuration", {
            get: function () {
                return this.pointer.downDuration;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MouseHandler.prototype, "downPosition", {
            get: function () {
                return this.pointer.downPosition;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MouseHandler.prototype, "position", {
            get: function () {
                return this.pointer.position;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MouseHandler.prototype, "upPosition", {
            get: function () {
                return this.pointer.upPosition;
            },
            enumerable: true,
            configurable: true
        });
        MouseHandler.prototype._getPointerOrFirstFree = function (touch) {
            var identifier = touch.identifier;
            var firstFree = null;
            var idFound = null;
            for (var i = 0; i < this.pointers.length; i++) {
                var p = this.pointers[i];
                if (p.identifier == null && firstFree == null) {
                    firstFree = p;
                }
                else if (p.identifier == identifier) {
                    //console.log("Found active pointer. " + i + "  -- identifier=" + p.identifier);
                    idFound = p;
                    break;
                }
            }
            if (idFound != null) {
                return idFound;
            }
            return firstFree;
        };
        MouseHandler.prototype._stopEvent = function (e) {
            if (this.propagateAllEvents)
                return;
            e.preventDefault();
            e.stopPropagation();
        };
        MouseHandler.prototype._debugTouch = function (event, e) {
            var changes = "";
            for (var i = 0; i < e.changedTouches.length; i++) {
                changes += "" + e.changedTouches[i].identifier;
                if (i < e.changedTouches.length - 1)
                    changes += ",";
            }
            console.log("#### MouseHandler: " + event + ", changes: " + changes);
        };
        return MouseHandler;
    }());
    plume.MouseHandler = MouseHandler;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var PointerInput = /** @class */ (function (_super) {
        __extends(PointerInput, _super);
        function PointerInput(game, inputManager) {
            var _this = _super.call(this) || this;
            _this.downPosition = new plume.Point(0, 0);
            _this.position = new plume.Point(0, 0);
            _this.upPosition = new plume.Point(0, 0);
            _this.diffPosition = new plume.Point(0, 0);
            _this.mouseActive = false;
            _this._previousHit = null;
            _this._lastDownTarget = null;
            _this._touchInProgress = false;
            _this._touchEndTime = 0;
            _this._touchEndDebounce = 4000;
            _this._prevMousePosition = new plume.Point(-1, -1);
            _this._prevMouseBState = -1;
            _this._lastX = 0;
            _this._lastY = 0;
            _this.inputManager = inputManager;
            _this.scaleManager = game.scaleManager;
            return _this;
        }
        PointerInput.prototype.update = function () {
            _super.prototype.update.call(this);
            if (this._touchEndTime != null) {
                if (this._touchEndTime < plume.Time.now()) {
                    this._touchEndTime = null;
                    this._touchInProgress = false;
                }
            }
            this.diffPosition.x = this.position.x - this._lastX;
            this.diffPosition.y = this.position.y - this._lastY;
            this._lastX = this.position.x;
            this._lastY = this.position.y;
            if (this.isUp && this.identifier != null) {
                this._releasePointer();
            }
        };
        PointerInput.prototype.isActive = function () {
            return this.mouseActive || this.identifier != null;
        };
        PointerInput.prototype.mouseDown = function (e) {
            if (this._touchInProgress)
                return;
            if (this.isDown)
                return;
            _super.prototype.onDown.call(this);
            this.downPosition.x = this._getXValue(e.pageX);
            this.downPosition.y = this._getYValue(e.pageY);
            this.position.x = this.downPosition.x;
            this.position.y = this.downPosition.y;
            this.diffPosition.x = 0;
            this.diffPosition.y = 0;
            this._lastX = this.position.x;
            this._lastY = this.position.y;
            this._lastMouseEvent = e;
        };
        PointerInput.prototype.mouseMove = function (e) {
            this.position.x = this._getXValue(e.pageX);
            this.position.y = this._getYValue(e.pageY);
            this._lastMouseEvent = e;
            this.mouseActive = true;
        };
        PointerInput.prototype.mouseUp = function (e) {
            if (this._touchInProgress)
                return;
            if (this.isUp)
                return;
            _super.prototype.onUp.call(this);
            this.upPosition.x = this._getXValue(e.pageX);
            this.upPosition.y = this._getYValue(e.pageY);
            this._lastMouseEvent = e;
            this.inputManager.triggerImmediateClick();
        };
        PointerInput.prototype.touchDown = function (e) {
            if (this.isDown)
                return;
            if (this.identifier != null)
                return; // Processing another touch
            _super.prototype.onDown.call(this);
            this._touchInProgress = true;
            this._touchEndTime = null;
            this.identifier = e.identifier;
            this.downPosition.x = this._getXValue(e.pageX);
            this.downPosition.y = this._getYValue(e.pageY);
            this.position.x = this.downPosition.x;
            this.position.y = this.downPosition.y;
            this._acquirePointer();
        };
        PointerInput.prototype.touchMove = function (e) {
            if (!this.isDown)
                return;
            if (this.identifier != e.identifier)
                return; // Processing another touch
            this._touchInProgress = true;
            this.position.x = this._getXValue(e.pageX);
            this.position.y = this._getYValue(e.pageY);
        };
        PointerInput.prototype.touchUp = function (e) {
            if (this.isUp)
                return;
            if (this.identifier != e.identifier)
                return; // Processing another touch
            _super.prototype.onUp.call(this);
            this._touchInProgress = true;
            this._touchEndTime = plume.Time.now() + this._touchEndDebounce;
            this.upPosition.x = this._getXValue(e.pageX);
            this.upPosition.y = this._getYValue(e.pageY);
            this.inputManager.triggerImmediateClick();
        };
        PointerInput.prototype._getXValue = function (pagex) {
            return Math.round((pagex - this.scaleManager.offsetX) / this.scaleManager.scale);
        };
        PointerInput.prototype._getYValue = function (pagey) {
            return Math.round((pagey - this.scaleManager.offsetY) / this.scaleManager.scale);
        };
        PointerInput.prototype._acquirePointer = function () {
            this._prevMousePosition.x = -1;
            this._prevMousePosition.y = -1;
            this._prevMouseBState = -1;
        };
        PointerInput.prototype._releasePointer = function () {
            this.identifier = null;
        };
        //
        // private 
        //
        // use only by InputManager
        PointerInput.prototype.mouseChanged = function () {
            var changed = false;
            var bstate = this.isDown ? 0 : 1;
            var newx = this.position.x;
            var newy = this.position.y;
            if (bstate != this._prevMouseBState) {
                changed = true;
            }
            if (!changed && (this._prevMousePosition.x != newx || this._prevMousePosition.y != newy)) {
                changed = true;
            }
            this._prevMouseBState = bstate;
            this._prevMousePosition.x = newx;
            this._prevMousePosition.y = newy;
            return changed;
        };
        PointerInput.prototype.wasSwipe = function () {
            var dst = plume.Point.distance(this.upPosition, this.downPosition);
            return dst >= 60;
        };
        PointerInput.prototype.getLastMouseEvent = function () {
            return this._lastMouseEvent;
        };
        return PointerInput;
    }(plume.AbstractInput));
    plume.PointerInput = PointerInput;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var EPS = 0.000001;
    //From threejs example
    var OrbitControls = /** @class */ (function () {
        function OrbitControls(object, domElement, handleMouseEvent) {
            if (handleMouseEvent === void 0) { handleMouseEvent = true; }
            this.object = object;
            this.domElement = domElement;
            this.handleMouseEvent = handleMouseEvent;
            // Set to false to disable this control
            this.enabled = true;
            // "target" sets the location of focus, where the object orbits around
            this.target = new THREE.Vector3();
            // How far you can dolly in and out ( PerspectiveCamera only )
            this.minDistance = 0;
            this.maxDistance = Infinity;
            // How far you can zoom in and out ( OrthographicCamera only )
            this.minZoom = 0;
            this.maxZoom = Infinity;
            // How far you can orbit vertically, upper and lower limits.
            // Range is 0 to Math.PI radians.
            this.minPolarAngle = 0; // radians
            this.maxPolarAngle = Math.PI; // radians
            // How far you can orbit horizontally, upper and lower limits.
            // If set, must be a sub-interval of the interval [ - Math.PI, Math.PI ].
            this.minAzimuthAngle = -Infinity; // radians
            this.maxAzimuthAngle = Infinity; // radians
            // Set to true to enable damping (inertia)
            // If damping is enabled, you must call controls.update() in your animation loop
            this.enableDamping = false;
            this.dampingFactor = 0.25;
            // This option actually enables dollying in and out; left as "zoom" for backwards compatibility.
            // Set to false to disable zooming
            this.enableZoom = true;
            this.zoomSpeed = 1.0;
            // Set to false to disable rotating
            this.enableRotate = true;
            this.rotateSpeed = 1.0;
            // Set to false to disable panning
            this.enablePan = true;
            this.panSpeed = 1.0;
            this.screenSpacePanning = false; // if true, pan in screen-space
            this.keyPanSpeed = 7.0; // pixels moved per arrow key push
            // Set to true to automatically rotate around the target
            // If auto-rotate is enabled, you must call controls.update() in your animation loop
            this.autoRotate = false;
            this.autoRotateSpeed = 2.0; // 30 seconds per round when fps is 60
            // Set to false to disable use of the keys
            this.enableKeys = true;
            // The four arrow keys
            this.keys = { LEFT: 37, UP: 38, RIGHT: 39, BOTTOM: 40 };
            // Mouse buttons
            this.mouseButtons = { ORBIT: THREE.MOUSE.LEFT, ZOOM: THREE.MOUSE.MIDDLE, PAN: THREE.MOUSE.RIGHT };
            // for reset
            this.target0 = this.target.clone();
            this.position0 = this.object.position.clone();
            this.zoom0 = this.object.zoom;
            this._changeEvent = { type: 'change' };
            this._startEvent = { type: 'start' };
            this._endEvent = { type: 'end' };
            this._state = -1 /* NONE */;
            this._EPS = 0.000001;
            // current position in spherical coordinates
            this._spherical = new THREE.Spherical();
            this._sphericalDelta = new THREE.Spherical();
            this._scale = 1;
            this._panOffset = new THREE.Vector3();
            this._zoomChanged = false;
            this._rotateStart = new THREE.Vector2();
            this._rotateEnd = new THREE.Vector2();
            this._rotateDelta = new THREE.Vector2();
            this._panStart = new THREE.Vector2();
            this._panEnd = new THREE.Vector2();
            this._panDelta = new THREE.Vector2();
            this._dollyStart = new THREE.Vector2();
            this._dollyEnd = new THREE.Vector2();
            this._dollyDelta = new THREE.Vector2();
            if (domElement == null) {
                this.domElement = window.document.body;
            }
            if (this.handleMouseEvent) {
                this._onContextMenuBound = this._onContextMenu.bind(this);
                this._onMouseDownBound = this._onMouseDown.bind(this);
                this._onMouseWheelBound = this._onMouseWheel.bind(this);
                this._onMouseMoveBound = this._onMouseMove.bind(this);
                this._onMouseUpBound = this._onMouseUp.bind(this);
                this._onTouchStartBound = this._onTouchStart.bind(this);
                this._onTouchMoveBound = this._onTouchMove.bind(this);
                this._onTouchEndBound = this._onTouchEnd.bind(this);
                this.domElement.addEventListener('contextmenu', this._onContextMenuBound, false);
                this.domElement.addEventListener('mousedown', this._onMouseDownBound, false);
                this.domElement.addEventListener('wheel', this._onMouseWheelBound, false);
                this.domElement.addEventListener('touchstart', this._onTouchStartBound, false);
                this.domElement.addEventListener('touchend', this._onTouchEndBound, false);
                this.domElement.addEventListener('touchmove', this._onTouchMoveBound, false);
            }
            // force an update at start
            this.update();
        }
        //
        // public methods
        //
        OrbitControls.prototype.getPolarAngle = function () {
            return this._spherical.phi;
        };
        OrbitControls.prototype.getAzimuthalAngle = function () {
            return this._spherical.theta;
        };
        OrbitControls.prototype.saveState = function () {
            this.target0.copy(this.target);
            this.position0.copy(this.object.position);
            this.zoom0 = this.object.zoom;
        };
        OrbitControls.prototype.reset = function () {
            this.target.copy(this.target0);
            this.object.position.copy(this.position0);
            this.object.zoom = this.zoom0;
            this.object.updateProjectionMatrix();
            this._DispatchEvent(this._changeEvent);
            this.update();
            this._state = -1 /* NONE */;
        };
        OrbitControls.prototype._DispatchEvent = function (e) {
        };
        // this method is exposed, but perhaps it would be better if we can make it private...
        OrbitControls.prototype.update = function () {
            var offset = new THREE.Vector3();
            // so camera.up is the orbit axis
            var quat = new THREE.Quaternion().setFromUnitVectors(this.object.up, new THREE.Vector3(0, 1, 0));
            var quatInverse = quat.clone().inverse();
            var lastPosition = new THREE.Vector3();
            var lastQuaternion = new THREE.Quaternion();
            var position = this.object.position;
            offset.copy(position).sub(this.target);
            // rotate offset to "y-axis-is-up" space
            offset.applyQuaternion(quat);
            // angle from z-axis around y-axis
            this._spherical.setFromVector3(offset);
            if (this.autoRotate && this._state === -1 /* NONE */) {
                this._rotateLeft(this._getAutoRotationAngle());
            }
            this._spherical.theta += this._sphericalDelta.theta;
            this._spherical.phi += this._sphericalDelta.phi;
            // restrict theta to be between desired limits
            this._spherical.theta = Math.max(this.minAzimuthAngle, Math.min(this.maxAzimuthAngle, this._spherical.theta));
            // restrict phi to be between desired limits
            this._spherical.phi = Math.max(this.minPolarAngle, Math.min(this.maxPolarAngle, this._spherical.phi));
            this._spherical.makeSafe();
            this._spherical.radius *= this._scale;
            // restrict radius to be between desired limits
            this._spherical.radius = Math.max(this.minDistance, Math.min(this.maxDistance, this._spherical.radius));
            // move target to panned location
            this.target.add(this._panOffset);
            offset.setFromSpherical(this._spherical);
            // rotate offset back to "camera-up-vector-is-up" space
            offset.applyQuaternion(quatInverse);
            position.copy(this.target).add(offset);
            this.object.lookAt(this.target);
            if (this.enableDamping === true) {
                this._sphericalDelta.theta *= (1 - this.dampingFactor);
                this._sphericalDelta.phi *= (1 - this.dampingFactor);
                this._panOffset.multiplyScalar(1 - this.dampingFactor);
            }
            else {
                this._sphericalDelta.set(0, 0, 0);
                this._panOffset.set(0, 0, 0);
            }
            this._scale = 1;
            // update condition is:
            // min(camera displacement, camera rotation in radians)^2 > EPS
            // using small-angle approximation cos(x/2) = 1 - x^2 / 8
            if (this._zoomChanged ||
                lastPosition.distanceToSquared(this.object.position) > EPS ||
                8 * (1 - lastQuaternion.dot(this.object.quaternion)) > EPS) {
                this._DispatchEvent(this._changeEvent);
                lastPosition.copy(this.object.position);
                lastQuaternion.copy(this.object.quaternion);
                this._zoomChanged = false;
                return true;
            }
            return false;
        };
        OrbitControls.prototype.dispose = function () {
            if (this.handleMouseEvent) {
                this.domElement.removeEventListener('contextmenu', this._onContextMenuBound, false);
                this.domElement.removeEventListener('mousedown', this._onMouseDownBound, false);
                this.domElement.removeEventListener('wheel', this._onMouseWheelBound, false);
                this.domElement.removeEventListener('touchstart', this._onTouchStartBound, false);
                this.domElement.removeEventListener('touchend', this._onTouchEndBound, false);
                this.domElement.removeEventListener('touchmove', this._onTouchMoveBound, false);
                document.removeEventListener('mousemove', this._onMouseMoveBound, false);
                document.removeEventListener('mouseup', this._onMouseUpBound, false);
            }
        };
        OrbitControls.prototype._getAutoRotationAngle = function () {
            return 2 * Math.PI / 60 / 60 * this.autoRotateSpeed;
        };
        OrbitControls.prototype._getZoomScale = function () {
            return Math.pow(0.95, this.zoomSpeed);
        };
        OrbitControls.prototype._rotateLeft = function (angle) {
            this._sphericalDelta.theta -= angle;
        };
        OrbitControls.prototype._rotateUp = function (angle) {
            this._sphericalDelta.phi -= angle;
        };
        OrbitControls.prototype._panLeft = function (distance, objectMatrix) {
            var v = new THREE.Vector3();
            v.setFromMatrixColumn(objectMatrix, 0); // get X column of objectMatrix
            v.multiplyScalar(-distance);
            this._panOffset.add(v);
        };
        OrbitControls.prototype._panUp = function (distance, objectMatrix) {
            var v = new THREE.Vector3();
            if (this.screenSpacePanning === true) {
                v.setFromMatrixColumn(objectMatrix, 1);
            }
            else {
                v.setFromMatrixColumn(objectMatrix, 0);
                v.crossVectors(this.object.up, v);
            }
            v.multiplyScalar(distance);
            this._panOffset.add(v);
        };
        // deltaX and deltaY are in pixels; right and down are positive
        OrbitControls.prototype._pan0 = function (deltaX, deltaY) {
            var offset = new THREE.Vector3();
            // perspective
            var position = this.object.position;
            offset.copy(position).sub(this.target);
            var targetDistance = offset.length();
            // half of the fov is center to top of screen
            targetDistance *= Math.tan((this.object.fov / 2) * Math.PI / 180.0);
            // we use only clientHeight here so aspect ratio does not distort speed
            this._panLeft(2 * deltaX * targetDistance / this.domElement.clientHeight, this.object.matrix);
            this._panUp(2 * deltaY * targetDistance / this.domElement.clientHeight, this.object.matrix);
        };
        OrbitControls.prototype.dollyIn = function (dollyScale, update) {
            if (update === void 0) { update = true; }
            this._scale /= dollyScale;
            if (update) {
                this.update();
            }
        };
        OrbitControls.prototype.dollyOut = function (dollyScale, update) {
            if (update === void 0) { update = true; }
            this._scale *= dollyScale;
            if (update) {
                this.update();
            }
        };
        //
        // event callbacks - update the object state
        //
        OrbitControls.prototype.startRotate = function (x, y) {
            this._rotateStart.set(x, y);
        };
        OrbitControls.prototype.startDolly = function (x, y) {
            this._dollyStart.set(x, y);
        };
        OrbitControls.prototype.startPan = function (x, y) {
            this._panStart.set(x, y);
        };
        OrbitControls.prototype.rotate = function (x, y) {
            this._rotateEnd.set(x, y);
            this._rotateDelta.subVectors(this._rotateEnd, this._rotateStart).multiplyScalar(this.rotateSpeed);
            var element = this.domElement;
            this._rotateLeft(2 * Math.PI * this._rotateDelta.x / element.clientHeight); // yes, height
            this._rotateUp(2 * Math.PI * this._rotateDelta.y / element.clientHeight);
            this._rotateStart.copy(this._rotateEnd);
            this.update();
        };
        OrbitControls.prototype.dolly = function (x, y) {
            this._dollyEnd.set(x, y);
            this._dollyDelta.subVectors(this._dollyEnd, this._dollyStart);
            if (this._dollyDelta.y > 0) {
                this.dollyIn(this._getZoomScale(), false);
            }
            else if (this._dollyDelta.y < 0) {
                this.dollyOut(this._getZoomScale(), false);
            }
            this._dollyStart.copy(this._dollyEnd);
            this.update();
        };
        OrbitControls.prototype.pan = function (x, y) {
            this._panEnd.set(x, y);
            this._panDelta.subVectors(this._panEnd, this._panStart).multiplyScalar(this.panSpeed);
            this._pan0(this._panDelta.x, this._panDelta.y);
            this._panStart.copy(this._panEnd);
            this.update();
        };
        //
        // event handlers - FSM: listen for events and reset state
        //
        OrbitControls.prototype._handleMouseDownRotate = function (event) {
            this.startRotate(event.clientX, event.clientY);
        };
        OrbitControls.prototype._handleMouseDownDolly = function (event) {
            this.startDolly(event.clientX, event.clientY);
        };
        OrbitControls.prototype._handleMouseDownPan = function (event) {
            this.startPan(event.clientX, event.clientY);
        };
        OrbitControls.prototype._handleMouseMoveRotate = function (event) {
            this.rotate(event.clientX, event.clientY);
        };
        OrbitControls.prototype._handleMouseMoveDolly = function (event) {
            this.dolly(event.clientX, event.clientY);
        };
        OrbitControls.prototype._handleMouseMovePan = function (event) {
            this.pan(event.clientX, event.clientY);
        };
        OrbitControls.prototype._handleMouseUp = function (event) {
        };
        OrbitControls.prototype._handleMouseWheel = function (event) {
            if (event.deltaY < 0) {
                this.dollyOut(this._getZoomScale(), false);
            }
            else if (event.deltaY > 0) {
                this.dollyIn(this._getZoomScale(), false);
            }
            this.update();
        };
        OrbitControls.prototype._onMouseDown = function (event) {
            if (this.enabled === false)
                return;
            // event.preventDefault();
            switch (event.button) {
                case this.mouseButtons.ORBIT:
                    if (this.enableRotate === false)
                        return;
                    this._handleMouseDownRotate(event);
                    this._state = 0 /* ROTATE */;
                    break;
                case this.mouseButtons.ZOOM:
                    if (this.enableZoom === false)
                        return;
                    this._handleMouseDownDolly(event);
                    this._state = 1 /* DOLLY */;
                    break;
                case this.mouseButtons.PAN:
                    if (this.enablePan === false)
                        return;
                    this._handleMouseDownPan(event);
                    this._state = 2 /* PAN */;
                    break;
            }
            if (this._state !== -1 /* NONE */) {
                document.addEventListener('mousemove', this._onMouseMoveBound, false);
                document.addEventListener('mouseup', this._onMouseUpBound, false);
                this._DispatchEvent(this._startEvent);
            }
        };
        OrbitControls.prototype._onMouseMove = function (event) {
            if (this.enabled === false)
                return;
            // event.preventDefault();
            switch (this._state) {
                case 0 /* ROTATE */:
                    if (this.enableRotate === false)
                        return;
                    this._handleMouseMoveRotate(event);
                    break;
                case 1 /* DOLLY */:
                    if (this.enableZoom === false)
                        return;
                    this._handleMouseMoveDolly(event);
                    break;
                case 2 /* PAN */:
                    if (this.enablePan === false)
                        return;
                    this._handleMouseMovePan(event);
                    break;
            }
        };
        OrbitControls.prototype._onMouseUp = function (event) {
            if (this.enabled === false)
                return;
            this._handleMouseUp(event);
            document.removeEventListener('mousemove', this._onMouseMoveBound, false);
            document.removeEventListener('mouseup', this._onMouseUpBound, false);
            this._DispatchEvent(this._endEvent);
            this._state = -1 /* NONE */;
        };
        OrbitControls.prototype._onMouseWheel = function (event) {
            if (this.enabled === false || this.enableZoom === false || (this._state !== -1 /* NONE */ && this._state !== 0 /* ROTATE */))
                return;
            // event.preventDefault();
            // event.stopPropagation();
            this._DispatchEvent(this._startEvent);
            this._handleMouseWheel(event);
            this._DispatchEvent(this._endEvent);
        };
        OrbitControls.prototype._onContextMenu = function (event) {
            if (this.enabled === false)
                return;
            event.preventDefault();
        };
        OrbitControls.prototype._onTouchStart = function (event) {
            if (this.enabled === false)
                return;
            event.preventDefault();
            switch (event.touches.length) {
                case 1: // one-fingered touch: rotate
                    if (this.enableRotate === false)
                        return;
                    this._handleTouchStartRotate(event);
                    this._state = 3 /* TOUCH_ROTATE */;
                    break;
                case 2: // two-fingered touch: dolly-pan
                    if (this.enableZoom === false && this.enablePan === false)
                        return;
                    this._handleTouchStartDollyPan(event);
                    this._state = 4 /* TOUCH_DOLLY_PAN */;
                    break;
                default:
                    this._state = -1 /* NONE */;
            }
            if (this._state !== -1 /* NONE */) {
                this._DispatchEvent(this._startEvent);
            }
        };
        OrbitControls.prototype._onTouchMove = function (event) {
            if (this.enabled === false)
                return;
            event.preventDefault();
            event.stopPropagation();
            switch (event.touches.length) {
                case 1: // one-fingered touch: rotate
                    if (this.enableRotate === false)
                        return;
                    if (this._state !== 3 /* TOUCH_ROTATE */)
                        return; // is this needed?
                    this._handleTouchMoveRotate(event);
                    break;
                case 2: // two-fingered touch: dolly-pan
                    if (this.enableZoom === false && this.enablePan === false)
                        return;
                    if (this._state !== 4 /* TOUCH_DOLLY_PAN */)
                        return; // is this needed?
                    this._handleTouchMoveDollyPan(event);
                    break;
                default:
                    this._state = -1 /* NONE */;
            }
        };
        OrbitControls.prototype._onTouchEnd = function (event) {
            if (this.enabled === false)
                return;
            this._handleTouchEnd(event);
            this._DispatchEvent(this._endEvent);
            this._state = -1 /* NONE */;
        };
        OrbitControls.prototype._handleTouchStartRotate = function (event) {
            this._rotateStart.set(event.touches[0].pageX, event.touches[0].pageY);
        };
        OrbitControls.prototype._handleTouchStartDollyPan = function (event) {
            if (this.enableZoom) {
                var dx = event.touches[0].pageX - event.touches[1].pageX;
                var dy = event.touches[0].pageY - event.touches[1].pageY;
                var distance = Math.sqrt(dx * dx + dy * dy);
                this._dollyStart.set(0, distance);
            }
            if (this.enablePan) {
                var x = 0.5 * (event.touches[0].pageX + event.touches[1].pageX);
                var y = 0.5 * (event.touches[0].pageY + event.touches[1].pageY);
                this._panStart.set(x, y);
            }
        };
        OrbitControls.prototype._handleTouchMoveRotate = function (event) {
            this._rotateEnd.set(event.touches[0].pageX, event.touches[0].pageY);
            this._rotateDelta.subVectors(this._rotateEnd, this._rotateStart).multiplyScalar(this.rotateSpeed);
            var element = this.domElement;
            //let element = this.domElement === document ? this.domElement.body : this.domElement;
            this._rotateLeft(2 * Math.PI * this._rotateDelta.x / element.clientHeight); // yes, height
            this._rotateUp(2 * Math.PI * this._rotateDelta.y / element.clientHeight);
            this._rotateStart.copy(this._rotateEnd);
            this.update();
        };
        OrbitControls.prototype._handleTouchMoveDollyPan = function (event) {
            if (this.enableZoom) {
                var dx = event.touches[0].pageX - event.touches[1].pageX;
                var dy = event.touches[0].pageY - event.touches[1].pageY;
                var distance = Math.sqrt(dx * dx + dy * dy);
                this._dollyEnd.set(0, distance);
                this._dollyDelta.set(0, Math.pow(this._dollyEnd.y / this._dollyStart.y, this.zoomSpeed));
                this.dollyIn(this._dollyDelta.y);
                this._dollyStart.copy(this._dollyEnd);
            }
            if (this.enablePan) {
                var x = 0.5 * (event.touches[0].pageX + event.touches[1].pageX);
                var y = 0.5 * (event.touches[0].pageY + event.touches[1].pageY);
                // this._panEnd.set(x, y);
                // this._panDelta.subVectors(this._panEnd, this._panStart).multiplyScalar(this.panSpeed);
                // this.pan(this._panDelta.x, this._panDelta.y);
                // this._panStart.copy(this._panEnd);
                this.pan(x, y);
            }
            this.update();
        };
        OrbitControls.prototype._handleTouchEnd = function (event) {
        };
        return OrbitControls;
    }());
    plume.OrbitControls = OrbitControls;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Mathf = /** @class */ (function () {
        function Mathf() {
        }
        Mathf.clamp = function (value, min, max) {
            if (value < min)
                value = min;
            else if (value > max)
                value = max;
            return value;
        };
        Mathf.clamp01 = function (value) {
            if (value < 0)
                return 0;
            else if (value > 1)
                return 1;
            else
                return value;
        };
        // positive mod
        Mathf.mod = function (a, n) {
            return (a % n + n) % n;
        };
        // t between 0 and 1
        Mathf.lerp = function (v0, v1, t) {
            if (t <= 0)
                return v0;
            if (t >= 1)
                return v1;
            return (1 - t) * v0 + t * v1;
        };
        Mathf.inverseLerp = function (a, b, value) {
            if (a == b)
                return 0;
            return (value - a) / (b - a);
        };
        Mathf.sign = function (v) {
            if (v >= 0)
                return 1;
            return -1;
        };
        Mathf.repeat = function (t, length) {
            return this.mod(t, length);
        };
        Mathf.pingPong = function (t, length) {
            if (t < 0)
                t = -t;
            var mod = t % length;
            // if mod is even
            if (Math.ceil(t / length) % 2 === 0) {
                return (mod === 0) ? 0 : length - (mod);
            }
            return (mod === 0) ? length : mod;
        };
        Mathf.map = function (num, inMin, inMax, outMin, outMax) {
            if (num < inMin)
                num = inMin;
            else if (num > inMax)
                num = inMax;
            return (num - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
        };
        // Gradually changes a value towards a desired goal over time.
        Mathf.smoothDamp = function (current, target, currentVelocity, smoothTime, maxSpeed, deltaTime) {
            smoothTime = Math.max(0.0001, smoothTime);
            deltaTime = deltaTime / 1000; // convert ms to s. tomatch unity algo
            var omega = 2 / smoothTime;
            var x = omega * deltaTime;
            var exp = 1 / (1 + x + 0.48 * x * x + 0.235 * x * x * x);
            var change = current - target;
            var originalTo = target;
            if (maxSpeed != null) {
                var maxChange = maxSpeed * smoothTime;
                change = Mathf.clamp(change, -maxChange, maxChange);
            }
            target = current - change;
            var temp = (currentVelocity + omega * change) * deltaTime;
            currentVelocity = (currentVelocity - omega * temp) * exp;
            var output = target + (change + temp) * exp;
            if (originalTo - current > 0 == output > originalTo) {
                output = originalTo;
                currentVelocity = (output - originalTo) / deltaTime;
            }
            return {
                smoothValue: output,
                newVelocity: currentVelocity,
            };
        };
        // Gradually changes an angle given in degrees towards a desired goal angle over time.
        Mathf.smoothDampAngle = function (current, target, currentVelocity, smoothTime, maxSpeed, deltaTime) {
            target = current + this.deltaAngle(current, target);
            return this.smoothDamp(current, target, currentVelocity, smoothTime, maxSpeed, deltaTime);
        };
        // Calculates the shortest difference between two given angles.
        Mathf.deltaAngle = function (current, target) {
            var delta = Mathf.repeat((target - current), this.PI2);
            if (delta > Math.PI)
                delta -= this.PI2;
            return delta;
        };
        Mathf.PI2 = Math.PI;
        Mathf.DEG2RAD = Math.PI / 180;
        Mathf.RAD2DEG = 180 / Math.PI;
        return Mathf;
    }());
    plume.Mathf = Mathf;
})(plume || (plume = {}));
var plume;
(function (plume) {
    //
    // TODO merge with 3js ?
    //
    var Point = /** @class */ (function () {
        function Point(x, y) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            this._x = x;
            this._y = y;
        }
        Point.prototype.copy = function () {
            return new Point(this._x, this._y);
        };
        Point.prototype.equals = function (point) {
            if (point == null)
                return false;
            return (this.x == point.x && this.y == point.y);
        };
        Object.defineProperty(Point.prototype, "x", {
            // Accessor
            get: function () {
                return this._x;
            },
            set: function (x) {
                this._x = x;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Point.prototype, "y", {
            get: function () {
                return this._y;
            },
            set: function (y) {
                this._y = y;
            },
            enumerable: true,
            configurable: true
        });
        Point.prototype.toString = function () {
            return this._x + "," + this._y;
        };
        Point.distance = function (p1, p2) {
            var xs = 0;
            var ys = 0;
            xs = p2.x - p1.x;
            xs = xs * xs;
            ys = p2.y - p1.y;
            ys = ys * ys;
            return Math.sqrt(xs + ys);
        };
        return Point;
    }());
    plume.Point = Point;
})(plume || (plume = {}));
var plume;
(function (plume) {
    //
    // TODO merge with 3js ?
    //
    var Rectangle = /** @class */ (function () {
        function Rectangle(left, top, width, height) {
            this._left = left;
            this._top = top;
            this._width = width;
            this._height = height;
        }
        Rectangle.prototype.getCenter = function () {
            return new plume.Point(this._left + Math.round(this._width / 2), this._top + Math.round(this._height / 2));
        };
        Rectangle.prototype.containsPoint = function (point) {
            return this.containsPoint2(point.x, point.y);
        };
        Rectangle.prototype.containsPoint2 = function (px, py) {
            if ((px >= this.left) && (px <= this.right)) {
                if ((py >= this.top) && (py <= this.bottom)) {
                    return true;
                }
                return false;
            }
            return false;
        };
        Rectangle.prototype.overlap = function (rectangle) {
            return this.left < rectangle.right && this.right > rectangle.left && this.top < rectangle.bottom && this.bottom > rectangle.top;
        };
        Rectangle.prototype.toString = function () {
            return this.x + "," + this.y + " | " + this._width + "," + this._height;
        };
        Rectangle.prototype.copy = function () {
            return new Rectangle(this._left, this._top, this._width, this._height);
        };
        Object.defineProperty(Rectangle.prototype, "x", {
            // Accessor
            get: function () {
                return this._left;
            },
            set: function (x) {
                this._left = x;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "left", {
            get: function () {
                return this._left;
            },
            set: function (x) {
                this._left = x;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "right", {
            get: function () {
                return this._left + this._width;
            },
            set: function (x) {
                this._left = x - this._width;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "y", {
            get: function () {
                return this._top;
            },
            set: function (y) {
                this._top = y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "top", {
            get: function () {
                return this._top;
            },
            set: function (y) {
                this._top = y;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "bottom", {
            get: function () {
                return this._top + this._height;
            },
            set: function (y) {
                this._top = y - this._height;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "width", {
            get: function () {
                return this._width;
            },
            set: function (width) {
                this._width = width;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Rectangle.prototype, "height", {
            get: function () {
                return this._height;
            },
            set: function (height) {
                this._height = height;
            },
            enumerable: true,
            configurable: true
        });
        return Rectangle;
    }());
    plume.Rectangle = Rectangle;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var HttpRequest = /** @class */ (function () {
        function HttpRequest() {
        }
        HttpRequest.prototype.get = function (url, connectionReceivedHandler, asyncSendFailResult) {
            var self = this;
            this._initHTTPRequest(asyncSendFailResult);
            if (this._xhrObject) {
                self._xhrObject.open("GET", url, true);
                self._xhrObject.onreadystatechange = function () {
                    if (self._xhrObject.readyState == 4) {
                        if (self._xhrObject.status == 200) {
                            connectionReceivedHandler(self._xhrObject.responseText);
                            self._xhrObject.onreadystatechange = null;
                        }
                        else {
                            asyncSendFailResult("invalid status: " + self._xhrObject.status + " text=" + self._xhrObject.responseText + " statusText: " + self._xhrObject.statusText);
                            self._xhrObject.onreadystatechange = null;
                        }
                    }
                    if (self._xhrObject.readyState <= 0) {
                        asyncSendFailResult("invalid ready state: " + self._xhrObject.readyState);
                        self._xhrObject.onreadystatechange = null;
                    }
                };
                this._xhrObject.onerror = function (e) {
                    asyncSendFailResult("XHR onerror statusText: " + self._xhrObject.statusText);
                };
                this._xhrObject.send(null);
            }
        };
        HttpRequest.prototype._initHTTPRequest = function (asyncSendFailResult) {
            if (window["XMLHttpRequest"]) // Firefox
                this._xhrObject = new XMLHttpRequest();
            else {
                asyncSendFailResult("Not Supported");
                return;
            }
        };
        HttpRequest.prototype.post = function (url, data, connectionReceivedHandler, asyncSendFailResult) {
            var self = this;
            this._initHTTPRequest(asyncSendFailResult);
            if (this._xhrObject) {
                self._xhrObject.open("POST", url, true);
                self._xhrObject.onreadystatechange = function () {
                    if (self._xhrObject.readyState == 4) {
                        if (self._xhrObject.status == 200) {
                            connectionReceivedHandler(self._xhrObject.responseText);
                            self._xhrObject.onreadystatechange = null;
                        }
                        else {
                            asyncSendFailResult("invalid status: " + self._xhrObject.status);
                        }
                    }
                    if (self._xhrObject.readyState <= 0) {
                        asyncSendFailResult("invalid ready state: " + self._xhrObject.readyState);
                        self._xhrObject.onreadystatechange = null;
                    }
                };
                this._xhrObject.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                this._xhrObject.send(data);
            }
        };
        return HttpRequest;
    }());
    plume.HttpRequest = HttpRequest;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var SimulationSupport = /** @class */ (function () {
        function SimulationSupport() {
            this._running = false;
        }
        SimulationSupport.prototype.update = function (dt) {
            if (!this._running)
                return;
        };
        SimulationSupport.prototype.setGravity = function (x, y, z) {
        };
        SimulationSupport.prototype.onStart = function () {
        };
        SimulationSupport.prototype.onStop = function () {
        };
        Object.defineProperty(SimulationSupport.prototype, "running", {
            get: function () {
                return this._running;
            },
            set: function (v) {
                if (this._running == v)
                    return;
                this._running = v;
                if (this._running)
                    this.onStart();
                else
                    this.onStop();
            },
            enumerable: true,
            configurable: true
        });
        return SimulationSupport;
    }());
    plume.SimulationSupport = SimulationSupport;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var RecyclePool = /** @class */ (function () {
        function RecyclePool(factory, capacity) {
            this._factory = factory;
            this._count = 0;
            this._data = new Array(capacity);
            for (var i = 0; i < capacity; ++i) {
                this._data[i] = factory();
            }
        }
        Object.defineProperty(RecyclePool.prototype, "length", {
            get: function () {
                return this._count;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(RecyclePool.prototype, "data", {
            get: function () {
                return this._data;
            },
            enumerable: true,
            configurable: true
        });
        RecyclePool.prototype.reset = function () {
            this._count = 0;
        };
        RecyclePool.prototype.resize = function (size) {
            if (size > this._data.length) {
                for (var i = this._data.length; i < size; ++i) {
                    this._data[i] = this._factory();
                }
            }
        };
        // Virtualy add a new element in the array
        // Resize the array if not enough capacity
        RecyclePool.prototype.add = function () {
            if (this._count >= this._data.length) {
                this.resize(this._data.length * 2);
            }
            return this._data[this._count++];
        };
        RecyclePool.prototype.remove = function (idxOrElem) {
            var idx = (typeof idxOrElem == "number" ? idxOrElem : this._data.indexOf(idxOrElem));
            if (idx >= this._count) {
                return null;
            }
            var last = this._count - 1;
            var tmp = this._data[idx];
            this._data[idx] = this._data[last];
            this._data[last] = tmp;
            this._count -= 1;
            return tmp;
        };
        return RecyclePool;
    }());
    plume.RecyclePool = RecyclePool;
})(plume || (plume = {}));
/// <reference path="../utils/memory/RecyclePool.ts" />
var plume;
(function (plume) {
    // Keep only one instance for the game
    // Acces via plume.ephemeralPool
    // Data are automatically released each tick!
    var EphemeralPool = /** @class */ (function () {
        function EphemeralPool() {
            this.vectors3 = new plume.RecyclePool(function () {
                return new THREE.Vector3();
            }, 10);
            this.quaternions = new plume.RecyclePool(function () {
                return new THREE.Quaternion();
            }, 5);
        }
        EphemeralPool.prototype.clean = function () {
            this.vectors3.reset();
            this.quaternions.reset();
        };
        EphemeralPool.prototype.v3 = function (x, y, z) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (z === void 0) { z = 0; }
            var v = this.vectors3.add();
            v.set(x, y, z);
            return v;
        };
        EphemeralPool.prototype.q = function (x, y, z, w) {
            if (x === void 0) { x = 0; }
            if (y === void 0) { y = 0; }
            if (z === void 0) { z = 0; }
            if (w === void 0) { w = 1; }
            var v = this.quaternions.add();
            v.set(x, y, z, w);
            return v;
        };
        return EphemeralPool;
    }());
    plume.EphemeralPool = EphemeralPool;
    plume.ephemeralPool = new EphemeralPool();
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Renderer = /** @class */ (function () {
        function Renderer() {
        }
        Renderer._applyStyle = function (canvas) {
            var self = this;
            var vendors = [
                '-webkit-',
                '-khtml-',
                '-moz-',
                '-ms-',
                ''
            ];
            vendors.forEach(function (vendor) {
                canvas.style[vendor + 'user-select'] = "none";
            });
            canvas.style['-webkit-touch-callout'] = "none";
            canvas.style['-webkit-tap-highlight-color'] = 'rgba(0, 0, 0, 0)';
        };
        return Renderer;
    }());
    plume.Renderer = Renderer;
})(plume || (plume = {}));
/// <reference path="../../../../vendor/three.d.ts" />
// TODO how to complete three.d.ts ??
window["THREEEXT"] = THREE;
var plume;
(function (plume) {
    var Engine = /** @class */ (function () {
        function Engine(options) {
            this._prerender = [];
            this._postrender = [];
            var self = this;
            var now = performance.now();
            this.game = plume.Game.get();
            this._options = this._initOption(options);
            this._initCanvas();
            Engine._INSTANCE = this;
            var width = window.innerWidth;
            var height = window.innerHeight;
            this._scene = new THREE.Scene();
            this._camera = new THREE.PerspectiveCamera(50, width / height);
            this._camera.position.z = 30;
            this._renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias: this._options.antialias, alpha: this._options.alpha });
            this._renderer.setPixelRatio(window.devicePixelRatio);
            this._renderer.setSize(width, height);
            this._renderer.setClearColor(0x000000, 1);
            this._textureCache = new plume.TextureCache();
            this._loader = new plume.Loader3d();
            this.game.scaleManager.resizeHandler.add(function () {
                var size = self.game.scaleManager.getWindowSize();
                self.resize(size.width, size.height);
            });
        }
        Engine.get = function () {
            return Engine._INSTANCE;
        };
        Engine.prototype._initOption = function (o) {
            if (o == null)
                o = {};
            if (o.antialias == null)
                o.antialias = true;
            if (o.alpha == null)
                o.alpha = true;
            return o;
        };
        Engine.prototype._initCanvas = function () {
            if (this._options.canvasId != null) {
                this.canvas = document.getElementById(this._options.canvasId);
            }
            else {
                this.canvas = document.createElement("canvas");
                this.canvas.style.position = "absolute";
                this.canvas.id = "canvas3D";
                document.body.appendChild(this.canvas);
            }
        };
        Engine.prototype.render = function (dt, interpolation) {
            if (this._effectComposer == null) {
                for (var i = 0; i < this._prerender.length; i++) {
                    this._prerender[i].execute(dt);
                }
                this._renderer.render(this._scene, this._camera);
                for (var i = 0; i < this._postrender.length; i++) {
                    this._postrender[i].execute(dt);
                }
            }
            else {
                this._effectComposer.render(dt);
            }
        };
        Engine.prototype.resize = function (width, height) {
            this._camera.aspect = width / height;
            this._camera.updateProjectionMatrix();
            this._renderer.setSize(width, height);
        };
        Engine.prototype.addRenderable = function (renderable, prerender, ctx) {
            var cb = new plume.ContextualCallback(renderable, ctx);
            if (prerender) {
                this._prerender.push(cb);
            }
            else {
                this._postrender.push(cb);
            }
            return cb;
        };
        Engine.prototype.removeRenderable = function (renderable) {
            var removed = plume.ContextualCallback.removeFromArray(renderable, this._postrender);
            if (removed) {
                return true;
            }
            removed = plume.ContextualCallback.removeFromArray(renderable, this._prerender);
            return removed;
        };
        Object.defineProperty(Engine.prototype, "renderer", {
            get: function () {
                return this._renderer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Engine.prototype, "scene", {
            get: function () {
                return this._scene;
            },
            set: function (s) {
                this._scene = s;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Engine.prototype, "camera", {
            get: function () {
                return this._camera;
            },
            set: function (camera) {
                this._camera = camera;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Engine.prototype, "effectComposer", {
            get: function () {
                return this._effectComposer;
            },
            set: function (v) {
                this._effectComposer = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Engine.prototype, "loader", {
            get: function () {
                return this._loader;
            },
            set: function (v) {
                this._loader = v;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Engine.prototype, "textureCache", {
            get: function () {
                return this._textureCache;
            },
            enumerable: true,
            configurable: true
        });
        return Engine;
    }());
    plume.Engine = Engine;
})(plume || (plume = {}));
var plume;
(function (plume) {
    function parallelTraverse(a, b, callback) {
        callback(a, b);
        for (var i = 0; i < a.children.length; i++) {
            parallelTraverse(a.children[i], b.children[i], callback);
        }
    }
    // Waiting: https://github.com/mrdoob/three.js/pull/14494
    var AnimUtils = /** @class */ (function () {
        function AnimUtils() {
        }
        AnimUtils.clone = function (source) {
            var cloneLookup = new Map();
            var clone = source.clone();
            parallelTraverse(source, clone, function (sourceNode, clonedNode) {
                cloneLookup.set(sourceNode, clonedNode);
            });
            source.traverse(function (sourceMesh) {
                if (!sourceMesh.isSkinnedMesh)
                    return;
                var sourceBones = sourceMesh.skeleton.bones;
                var clonedMesh = cloneLookup.get(sourceMesh);
                clonedMesh.skeleton = sourceMesh.skeleton.clone();
                clonedMesh.skeleton.bones = sourceBones.map(function (sourceBone) {
                    if (!cloneLookup.has(sourceBone)) {
                        throw new Error('THREE.AnimationUtils: Required bones are not descendants of the given object.');
                    }
                    return cloneLookup.get(sourceBone);
                });
                clonedMesh.bind(clonedMesh.skeleton, sourceMesh.bindMatrix);
                // console.log("Skeleton cloned! " + clonedMesh.name);
            });
            return clone;
        };
        return AnimUtils;
    }());
    plume.AnimUtils = AnimUtils;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var SpriteAnimation = /** @class */ (function () {
        function SpriteAnimation(_texture, _xframes, _yframes) {
            this._texture = _texture;
            this._xframes = _xframes;
            this._yframes = _yframes;
            this._curIndex = 0;
            this._playing = false;
            this._startTime = 0;
            this._lastIndex = -1;
            this._repeats = 1;
            this.fps = 1000 / 60;
            this.animationSpeed = 1;
            this.destroyOnEnd = true;
            // ! texture updated by ref !
            this._tilesCount = _xframes * _yframes;
            this._texture.wrapS = _texture.wrapT = THREE.RepeatWrapping;
            this._texture.repeat.set(1 / this._xframes, 1 / this._yframes);
            plume.Game.get().addUpdatable(this);
        }
        SpriteAnimation.prototype.start = function () {
            this._playing = true;
            this._totalFrames = this._tilesCount;
            this._curIndex = 0;
            this._startTime = performance.now();
            this._SetFrameIndex(0);
            // compute animation end
            if (this._repeats < 0) {
                this._lastIndex = -1;
            }
            else {
                this._lastIndex = this._curIndex + this._totalFrames * this._repeats;
            }
        };
        SpriteAnimation.prototype.stop = function () {
            this._playing = false;
        };
        SpriteAnimation.prototype.destroy = function () {
            this.stop();
            plume.Game.get().removeUpdatable(this);
        };
        SpriteAnimation.prototype.update = function (dt) {
            if (!this._playing)
                return;
            var frameEllapsed = Math.floor((performance.now() - this._startTime) / this.fps);
            frameEllapsed = Math.floor(frameEllapsed * this.animationSpeed);
            this._SetActiveFrame(frameEllapsed);
        };
        SpriteAnimation.prototype.onEnd = function (cb) {
            if (this._onEnd == null) {
                this._onEnd = new plume.Handler();
            }
            this._onEnd.add(cb);
        };
        SpriteAnimation.prototype._SetActiveFrame = function (index) {
            if (index == null)
                return;
            var newIndex = index % (this._totalFrames);
            if (newIndex == this._curIndex)
                return;
            this._curIndex = newIndex;
            this._SetFrameIndex(this._curIndex);
            if (this._lastIndex > 0) {
                index = Math.abs(index);
                if (index >= this._lastIndex) {
                    // time to stop
                    this._SetFrameIndex(this._tilesCount - 1);
                    this._OnAnimationEnded();
                }
            }
        };
        SpriteAnimation.prototype._SetFrameIndex = function (index) {
            var currentColumn = index % this._xframes;
            var currentRow = Math.floor(index / this._xframes);
            this._texture.offset.x = currentColumn / this._xframes;
            this._texture.offset.y = currentRow / this._yframes;
        };
        SpriteAnimation.prototype._OnAnimationEnded = function () {
            this._playing = false;
            if (this._onEnd != null)
                this._onEnd.fire({});
            if (this.destroyOnEnd) {
                this.destroy();
            }
        };
        SpriteAnimation.prototype.repeat = function (repeats) {
            if (repeats === void 0) { repeats = 0; }
            this._repeats = repeats;
        };
        SpriteAnimation.prototype.isPlaying = function () {
            return this._playing;
        };
        ////////////// static helper
        SpriteAnimation.onPlane = function (texture, xframes, yframes) {
            var tex = texture.clone();
            tex.needsUpdate = true;
            var mesh = new THREE.Mesh(new THREE.PlaneBufferGeometry(1, 1), new THREE.MeshBasicMaterial({ map: tex, transparent: true, side: THREE.DoubleSide }));
            var spriteAnimator = new plume.SpriteAnimation(tex, xframes, yframes);
            spriteAnimator.onEnd(function () {
                if (mesh.parent)
                    mesh.parent.remove(mesh);
            });
            return {
                mesh: mesh,
                animator: spriteAnimator,
                texture: tex,
            };
        };
        // Use sprite : always facing the camera
        SpriteAnimation.onSprite = function (texture, xframes, yframes) {
            var tex = texture.clone();
            tex.needsUpdate = true;
            var mesh = new THREE.Sprite(new THREE.SpriteMaterial({ map: tex, transparent: true, side: THREE.DoubleSide }));
            var spriteAnimator = new plume.SpriteAnimation(tex, xframes, yframes);
            spriteAnimator.onEnd(function () {
                if (mesh.parent)
                    mesh.parent.remove(mesh);
            });
            return {
                mesh: mesh,
                animator: spriteAnimator,
                texture: tex,
            };
        };
        return SpriteAnimation;
    }());
    plume.SpriteAnimation = SpriteAnimation;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var HeightMap = /** @class */ (function () {
        function HeightMap(_width, _depth, _options) {
            this._width = _width;
            this._depth = _depth;
            this._options = _options;
            this.optFilterR = 0.3;
            this.optFilterG = 0.59;
            this.optFilterB = 0.11;
            this._loaded = false;
        }
        HeightMap.prototype.fromImage = function (url) {
            var self = this;
            var resource = new Image();
            resource.src = url;
            resource.onload = function () {
                self._readImg(resource);
            };
            resource.onerror = function () {
                plume.logger.error("Failed to load " + url);
            };
            if (resource.complete) {
                // already loaded
                self._readImg(resource);
            }
        };
        HeightMap.prototype.fromPoints = function (w, z, heights) {
            this._createGeometry(w, z, heights);
        };
        HeightMap.prototype._readImg = function (img) {
            var canvas = document.createElement("canvas");
            var context = canvas.getContext("2d");
            var width = img.width;
            var height = img.height;
            canvas.width = width;
            canvas.height = height;
            context.drawImage(img, 0, 0);
            var values = [];
            var data = context.getImageData(0, 0, width, height).data;
            for (var k = 0; k < data.length; k = k + 4) {
                var r = data[k + 0] / 255;
                var g = data[k + 1] / 255;
                var b = data[k + 2] / 255;
                var value = r * this.optFilterR + g * this.optFilterG + b * this.optFilterB;
                // position.y = options.minHeight + (options.maxHeight - options.minHeight) * gradient;
                values.push(value);
            }
            this._createGeometry(width, height, values);
        };
        HeightMap.prototype._createGeometry = function (swidth, sheight, heights) {
            if (swidth * sheight != heights.length) {
                plume.logger.warn("Invalid parameters (not enough or too much points)");
            }
            var minHeight = this._options.minHeight || 0;
            var maxHeight = this._options.maxHeight || 1;
            //swidth-1?
            this._geometry = new THREE.PlaneBufferGeometry(this._width, this._depth, swidth - 1, sheight - 1);
            this._geometry.rotateX(-Math.PI / 2);
            var vertices = this._geometry.getAttribute("position").array;
            for (var i = 0, j = 0, l = vertices.length; i < l; i++, j += 3) {
                var v = minHeight + (maxHeight - minHeight) * heights[i];
                vertices[j + 1] = v;
            }
            this._notifyLodead();
        };
        HeightMap.prototype._notifyLodead = function () {
            this._loaded = true;
            if (this._loadedCb != null) {
                this._loadedCb();
            }
        };
        HeightMap.prototype.createMesh = function (material) {
            var mesh = new THREE.Mesh(this._geometry, material);
            return mesh;
        };
        HeightMap.prototype.getGeometry = function () {
            return this._geometry;
        };
        HeightMap.prototype.isLoaded = function () {
            return this._loaded;
        };
        HeightMap.prototype.onLoaded = function (cb) {
            if (this._loaded) {
                cb();
                return;
            }
            this._loadedCb = cb;
        };
        return HeightMap;
    }());
    plume.HeightMap = HeightMap;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var InlineLoadingManager = /** @class */ (function () {
        function InlineLoadingManager() {
            InlineLoadingManager._INST = this;
        }
        InlineLoadingManager.get = function () {
            return this._INST;
        };
        InlineLoadingManager.prototype.initialize = function (loader) {
            loader.manager.setURLModifier(this._getLocalUrl.bind(this));
            loader.onLoaded(this._onLoaded.bind(this));
            this.assetByName = new plume.HashMap();
            var assets = window["__gb_assets"];
            if (assets != null) {
                for (var i = 0; i < assets.length; i++) {
                    var a = assets[i];
                    if (this.assetByName.get(a.name) != null) {
                    }
                    this.assetByName.put(a.name, a);
                }
            }
            // ! Ugly hook for ads to overload assets (see VAdsHandler) !
            var __gb_adshandler = window["__gb_adshandler"];
            if (__gb_adshandler != null) {
                var override = __gb_adshandler.assets;
                for (var k in override) {
                    var asset = this.assetByName.get(k);
                    if (asset == null) {
                        plume.logger.warn("No asset def found for " + k);
                    }
                    else {
                        plume.logger.debug("Replacing asset " + asset.name);
                        asset.data = override[k];
                    }
                }
            }
        };
        InlineLoadingManager.prototype._getLocalUrl = function (url) {
            var asset = this.assetByName.get(url);
            if (asset == null) {
                //TB avoid logging big stuff
                var urlLenght = url.length;
                var maxLenght = Math.min(urlLenght, 100);
                if (url && url.substring(0, 4) != "data") {
                    plume.logger.warn("No inline ressource found for " + url.slice(0, maxLenght) + "...(truncated)");
                }
                return url;
            }
            if (asset.t == "png") {
                //TB test zlib decompression 
                //TODO try to do less data conversion
                // maybe this should be done elsewhere
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        var b64Str = asset.data;
                        var strData = atob(b64Str);
                        //uncompress
                        var unzipData = pako.inflate(strData, { raw: false });
                        //convert back array to base64 str
                        asset.data = this._arrayBufferToBase64(unzipData);
                    }
                    catch (err) {
                        plume.logger.error("failed to decompress image", err);
                    }
                }
                return asset.data;
            }
            else if (asset.t == "bin") {
                //TB test zlib decompression 
                //TODO try to do less data conversion
                // maybe this should be done elsewhere
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        var b64Str = asset.data;
                        var strData = atob(b64Str);
                        //uncompress
                        var unzipData = pako.inflate(strData, { raw: false });
                        //convert back array to base64 str
                        asset.data = this._arrayBufferToBase64(unzipData);
                    }
                    catch (err) {
                        plume.logger.error("failed to decompress bin", err);
                    }
                }
                return "data:arraybuffer;base64," + asset.data;
            }
            else if (asset.t == "json") {
                //TB test zlib decompression 
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        var b64Str = asset.data;
                        var strData = atob(b64Str);
                        //directly decompress to utf8 string
                        var unzipData = pako.inflate(strData, { raw: false, to: 'string' });
                        asset.data = unzipData;
                    }
                    catch (err) {
                        plume.logger.error("failed to decompress json", err);
                    }
                }
                return "data:json," + asset.data;
            }
            else {
                plume.logger.warn("unsupported type: " + asset.t + " for " + asset.name);
            }
            return url;
        };
        InlineLoadingManager.prototype._onLoaded = function () {
            // console.log("LOADING FINISHED!");
            // KO
        };
        InlineLoadingManager.prototype._arrayBufferToBase64 = function (buffer) {
            var binary = '';
            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;
            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        };
        return InlineLoadingManager;
    }());
    plume.InlineLoadingManager = InlineLoadingManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Loader3d = /** @class */ (function () {
        function Loader3d(onloaded) {
            this._total = 0;
            this._loaded = 0;
            this._ressourceByName = new plume.HashMap();
            this._textureByName = new plume.HashMap();
            this._loadedHandler = new plume.Handler();
            var self = this;
            this._manager = new THREE.LoadingManager();
            this._textureLoaderManager = new plume.TextureLoaderManager(this._manager);
            if (onloaded != null) {
                self._loadedHandler.once(onloaded);
            }
        }
        Loader3d.prototype._fbxloader = function () {
            var fbxloader0 = new THREE.FBXLoader(this._manager);
            return fbxloader0;
        };
        Loader3d.prototype._objloader = function () {
            var objloader0 = new THREE.OBJLoader2(this._manager);
            objloader0.setLogging(false, false);
            return objloader0;
        };
        Loader3d.prototype._gltfloader = function () {
            if (THREE["GLTFLoader"] != null) {
                // TODO, need to be removed
                var gltfloader0 = new THREE["GLTFLoader"](this._manager);
                var dracoloader = THREE["DRACOLoader"];
                if (dracoloader) {
                    dracoloader.setDecoderPath(Loader3d.dracoLib);
                    gltfloader0.setDRACOLoader(new dracoloader());
                }
                return gltfloader0;
            }
            else {
                // Good one
                var gltfloader0 = new plume["gltf"]["GLTFLoader"](this._manager, this._textureLoaderManager);
                return gltfloader0;
            }
        };
        Loader3d.prototype.setAvailableCompressionAndQuanlity = function (textures) {
            // TODO we might want to do 2d loading pass in custom order
            // (ie: loading texture near camera)
            if (textures == null || textures.length == 0)
                return null;
            // check there is at least 1 low res or 1 compressed texture
            var hasCompressionOrResolution = false;
            for (var i = 0; i < textures.length; i++) {
                var t = textures[i];
                if (t.quality != 1 /* NORMAL */ || t.format != 1 /* NORMAL */) {
                    hasCompressionOrResolution = true;
                    break;
                }
            }
            if (!hasCompressionOrResolution) {
                plume.logger.warn("No compressed or low res textures registered.");
                return null;
            }
            this._total++;
            var tl = new plume.ProgressiveTextureLoaderManager(this._manager);
            tl.initialize(textures, this);
            this._textureLoaderManager = tl;
            this._total--; // bof bof
            return tl;
        };
        Loader3d.prototype.reset = function () {
            this._total = 0;
            this._loaded = 0;
        };
        Loader3d.prototype.onLoaded = function (cb) {
            if (this.isLoaded()) {
                cb();
                return;
            }
            this._loadedHandler.once(cb);
        };
        Loader3d.prototype.isLoaded = function () {
            return (this._loaded == this._total);
        };
        Loader3d.prototype.getModel = function (name) {
            return this._ressourceByName.get(name);
        };
        Loader3d.prototype.getTexture = function (name) {
            return this._textureByName.get(name);
        };
        Loader3d.prototype.getCubeTexture = function (name) {
            return this._textureByName.get(name);
        };
        Loader3d.prototype.loadTexture = function (url, name) {
            var self = this;
            this._total++;
            var texture = this._textureLoaderManager.load(url, function (texture) {
                self._textureByName.put(name, texture);
                self._loaded++;
                self._checkLoadingComplete();
            });
            return texture;
        };
        Loader3d.prototype.loadCubeTexture = function (url, name) {
            var self = this;
            this._total++;
            // TODO / test: use this._textureLoaderManager to have progressive loading
            var loader = new THREE.CubeTextureLoader(this._manager);
            var texture = loader.load(url, function (texture) {
                self._textureByName.put(name, texture);
                self._loaded++;
                self._checkLoadingComplete();
            });
            return texture;
        };
        Loader3d.prototype.loadObj = function (objUrl, mtlUrl, name) {
            var self = this;
            this._total++;
            var objLoader = this._objloader();
            var callbackOnLoad = function (event) {
                // event.detail.loaderRootNode
                // event.detail.modelName
                self._ressourceByName.put(name, event.detail.loaderRootNode);
                self._loaded++;
                self._checkLoadingComplete();
            };
            var onLoadMtl = function (materials) {
                objLoader.setModelName(name); // important ??
                objLoader.setMaterials(materials);
                objLoader.load(objUrl, callbackOnLoad, null, null, null, false);
            };
            objLoader.loadMtl(mtlUrl, null, onLoadMtl);
        };
        Loader3d.prototype.loadFbx = function (objUrl, name) {
            var self = this;
            this._total++;
            this._fbxloader().load(objUrl, function (object) {
                self._ressourceByName.put(name, object);
                self._loaded++;
                self._checkLoadingComplete();
            });
        };
        Loader3d.prototype.loadGltf = function (objUrl, name, configHandler) {
            var self = this;
            this._total++;
            var loader = this._gltfloader();
            if (configHandler != null) {
                configHandler(loader);
            }
            loader.load(objUrl, function (gltf) {
                self._ressourceByName.put(name, gltf);
                self._loaded++;
                self._checkLoadingComplete();
            }, function (xhr) {
                //logger.debug((xhr.loaded / xhr.total * 100) + '% loaded');
            }, function (error) {
                plume.logger.error('An error happened', error);
            });
        };
        Loader3d.prototype.addModel = function (mesh, name) {
            this._ressourceByName.put(name, mesh);
        };
        Object.defineProperty(Loader3d.prototype, "manager", {
            get: function () {
                return this._manager;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Loader3d.prototype, "progressiveLoader", {
            get: function () {
                return this._textureLoaderManager;
            },
            enumerable: true,
            configurable: true
        });
        Loader3d.prototype._checkLoadingComplete = function () {
            if (this.isLoaded()) {
                this._loadedHandler.fire(true);
            }
        };
        Loader3d.dracoLib = 'build/lib/draco/';
        return Loader3d;
    }());
    plume.Loader3d = Loader3d;
})(plume || (plume = {}));
var plume;
(function (plume) {
    //
    // Same as THREE.KTXLoader but can update an existing texture
    //
    var KTXLoader = /** @class */ (function () {
        function KTXLoader(manager, cache) {
            this.manager = manager;
            this.cache = cache;
            this.crossOrigin = "anonymous";
            if (manager == null) {
                this.manager = THREE.DefaultLoadingManager;
            }
            if (cache == null) {
                this.cache = plume.Engine.get().textureCache;
            }
        }
        KTXLoader.prototype.load = function (url, onLoad, onError) {
            var texture = new THREE.CompressedTexture();
            this._load0(url, texture, onLoad, onError);
            return texture;
        };
        KTXLoader.prototype.swap = function (url, texture, onLoad, onError) {
            this._load0(url, texture, onLoad, onError);
            return texture;
        };
        // TODO array of url ?
        KTXLoader.prototype._load0 = function (url, texture, onLoad, onError) {
            var self = this;
            var loader = new THREE.FileLoader(this.manager);
            loader.setResponseType('arraybuffer');
            // loader.setPath(this.path);
            loader.load(url, function (buffer) {
                var texDatas = self._parse(buffer, true);
                if (texDatas.isCubemap) { // TODO need to test
                    var images = [];
                    texture.image = images;
                    var faces = texDatas.mipmaps.length / texDatas.mipmapCount;
                    for (var f = 0; f < faces; f++) {
                        images[f] = { mipmaps: [] };
                        for (var i = 0; i < texDatas.mipmapCount; i++) {
                            images[f].mipmaps.push(texDatas.mipmaps[f * texDatas.mipmapCount + i]);
                            images[f].format = texDatas.format;
                            images[f].width = texDatas.width;
                            images[f].height = texDatas.height;
                        }
                    }
                }
                else {
                    texture.image.width = texDatas.width;
                    texture.image.height = texDatas.height;
                    texture.mipmaps = texDatas.mipmaps;
                }
                if (texDatas.mipmapCount === 1) {
                    texture.minFilter = THREE.LinearFilter;
                }
                texture.format = texDatas.format;
                texture.needsUpdate = true;
                if (onLoad)
                    onLoad(texture);
            }, null, onError);
            return texture;
        };
        KTXLoader.prototype._parse = function (buffer, loadMipmaps) {
            var ktx = new KhronosTextureContainer(buffer, 1);
            return {
                mipmaps: ktx.mipmaps(loadMipmaps),
                width: ktx.pixelWidth,
                height: ktx.pixelHeight,
                format: ktx.glInternalFormat,
                isCubemap: ktx.numberOfFaces === 6,
                mipmapCount: ktx.numberOfMipmapLevels
            };
        };
        return KTXLoader;
    }());
    plume.KTXLoader = KTXLoader;
    var KhronosTextureContainer = /** @class */ (function () {
        /**
         * @param {ArrayBuffer} arrayBuffer- contents of the KTX container file
         * @param {number} facesExpected- should be either 1 or 6, based whether a cube texture or or
         * @param {boolean} threeDExpected- provision for indicating that data should be a 3D texture, not implemented
         * @param {boolean} textureArrayExpected- provision for indicating that data should be a texture array, not implemented
         */
        function KhronosTextureContainer(arrayBuffer, facesExpected) {
            this.arrayBuffer = arrayBuffer;
            // Test that it is a ktx formatted file, based on the first 12 bytes, character representation is:
            // '´', 'K', 'T', 'X', ' ', '1', '1', 'ª', '\r', '\n', '\x1A', '\n'
            // 0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31, 0x31, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A
            var identifier = new Uint8Array(this.arrayBuffer, 0, 12);
            if (identifier[0] !== 0xAB ||
                identifier[1] !== 0x4B ||
                identifier[2] !== 0x54 ||
                identifier[3] !== 0x58 ||
                identifier[4] !== 0x20 ||
                identifier[5] !== 0x31 ||
                identifier[6] !== 0x31 ||
                identifier[7] !== 0xBB ||
                identifier[8] !== 0x0D ||
                identifier[9] !== 0x0A ||
                identifier[10] !== 0x1A ||
                identifier[11] !== 0x0A) {
                plume.logger.error('texture missing KTX identifier');
                return;
            }
            // load the reset of the header in native 32 bit int
            var header = new Int32Array(this.arrayBuffer, 12, 13);
            // determine of the remaining header values are recorded in the opposite endianness & require conversion
            var oppositeEndianess = header[0] === 0x01020304;
            // read all the header elements in order they exist in the file, without modification (sans endainness)
            this.glType = oppositeEndianess ? this.switchEndainness(header[1]) : header[1]; // must be 0 for compressed textures
            this.glTypeSize = oppositeEndianess ? this.switchEndainness(header[2]) : header[2]; // must be 1 for compressed textures
            this.glFormat = oppositeEndianess ? this.switchEndainness(header[3]) : header[3]; // must be 0 for compressed textures
            this.glInternalFormat = oppositeEndianess ? this.switchEndainness(header[4]) : header[4]; // the value of arg passed to gl.compressedTexImage2D(,,x,,,,)
            this.glBaseInternalFormat = oppositeEndianess ? this.switchEndainness(header[5]) : header[5]; // specify GL_RGB, GL_RGBA, GL_ALPHA, etc (un-compressed only)
            this.pixelWidth = oppositeEndianess ? this.switchEndainness(header[6]) : header[6]; // level 0 value of arg passed to gl.compressedTexImage2D(,,,x,,,)
            this.pixelHeight = oppositeEndianess ? this.switchEndainness(header[7]) : header[7]; // level 0 value of arg passed to gl.compressedTexImage2D(,,,,x,,)
            this.pixelDepth = oppositeEndianess ? this.switchEndainness(header[8]) : header[8]; // level 0 value of arg passed to gl.compressedTexImage3D(,,,,,x,,)
            this.numberOfArrayElements = oppositeEndianess ? this.switchEndainness(header[9]) : header[9]; // used for texture arrays
            this.numberOfFaces = oppositeEndianess ? this.switchEndainness(header[10]) : header[10]; // used for cubemap textures, should either be 1 or 6
            this.numberOfMipmapLevels = oppositeEndianess ? this.switchEndainness(header[11]) : header[11]; // number of levels; disregard possibility of 0 for compressed textures
            this.bytesOfKeyValueData = oppositeEndianess ? this.switchEndainness(header[12]) : header[12]; // the amount of space after the header for meta-data
            // Make sure we have a compressed type.  Not only reduces work, but probably better to let dev know they are not compressing.
            if (this.glType !== 0) {
                plume.logger.warn('only compressed formats currently supported');
                return;
            }
            else {
                // value of zero is an indication to generate mipmaps @ runtime.  Not usually allowed for compressed, so disregard.
                this.numberOfMipmapLevels = Math.max(1, this.numberOfMipmapLevels);
            }
            if (this.pixelHeight === 0 || this.pixelDepth !== 0) {
                plume.logger.warn('only 2D textures currently supported');
                return;
            }
            if (this.numberOfArrayElements !== 0) {
                plume.logger.warn('texture arrays not currently supported');
                return;
            }
            if (this.numberOfFaces !== facesExpected) {
                plume.logger.warn('number of faces expected' + facesExpected + ', but found ' + this.numberOfFaces);
                return;
            }
            // we now have a completely validated file, so could use existence of loadType as success
            // would need to make this more elaborate & adjust checks above to support more than one load type
            this.loadType = KhronosTextureContainer.COMPRESSED_2D;
        }
        // not as fast hardware based, but will probably never need to use
        KhronosTextureContainer.prototype.switchEndainness = function (val) {
            return ((val & 0xFF) << 24) | ((val & 0xFF00) << 8) | ((val >> 8) & 0xFF00) | ((val >> 24) & 0xFF);
        };
        // return mipmaps for THREE.js
        KhronosTextureContainer.prototype.mipmaps = function (loadMipmaps) {
            var mipmaps = [];
            // initialize width & height for level 1
            var dataOffset = KhronosTextureContainer.HEADER_LEN + this.bytesOfKeyValueData;
            var width = this.pixelWidth;
            var height = this.pixelHeight;
            var mipmapCount = loadMipmaps ? this.numberOfMipmapLevels : 1;
            for (var level = 0; level < mipmapCount; level++) {
                var imageSize = new Int32Array(this.arrayBuffer, dataOffset, 1)[0]; // size per face, since not supporting array cubemaps
                for (var face = 0; face < this.numberOfFaces; face++) {
                    // let byteArray = new Uint8Array(this.arrayBuffer, dataOffset + 4, imageSize);
                    var byteArray = new Uint8ClampedArray(this.arrayBuffer, dataOffset + 4, imageSize);
                    mipmaps.push({ "data": byteArray, "width": width, "height": height });
                    dataOffset += imageSize + 4; // size of the image + 4 for the imageSize field
                    dataOffset += 3 - ((imageSize + 3) % 4); // add padding for odd sized image
                }
                width = Math.max(1.0, width * 0.5);
                height = Math.max(1.0, height * 0.5);
            }
            return mipmaps;
        };
        KhronosTextureContainer.HEADER_LEN = 12 + (13 * 4); // identifier + header elements (not including key value meta-data pairs)
        // load types
        KhronosTextureContainer.COMPRESSED_2D = 0; // uses a gl.compressedTexImage2D()
        KhronosTextureContainer.COMPRESSED_3D = 1; // uses a gl.compressedTexImage3D()
        KhronosTextureContainer.TEX_2D = 2; // uses a gl.texImage2D()
        KhronosTextureContainer.TEX_3D = 3; // uses a gl.texImage3D()
        return KhronosTextureContainer;
    }());
})(plume || (plume = {}));
var plume;
(function (plume) {
    // Entry point of texture loading
    var TextureLoaderManager = /** @class */ (function () {
        function TextureLoaderManager(manager, cache) {
            this.manager = manager;
            this.cache = cache;
            if (manager == null) {
                this.manager = THREE.DefaultLoadingManager;
            }
            if (cache == null) {
                this.cache = plume.Engine.get().textureCache;
            }
        }
        TextureLoaderManager.prototype.load = function (url, onLoad, onError) {
            // Default, load regular image
            var loader = new plume.TextureLoader(this.manager);
            var texture = loader.load(url, onLoad, onError);
            this.cache.add(url, texture);
            return texture;
        };
        return TextureLoaderManager;
    }());
    plume.TextureLoaderManager = TextureLoaderManager;
    var TextureCache = /** @class */ (function () {
        function TextureCache() {
            this._textureByUrl = new plume.HashMap();
        }
        TextureCache.prototype.add = function (url, texture) {
            this._textureByUrl.put(url, texture);
        };
        TextureCache.prototype.get = function (url) {
            return this._textureByUrl.get(url);
        };
        return TextureCache;
    }());
    plume.TextureCache = TextureCache;
})(plume || (plume = {}));
/// <reference path="TextureLoaderManager.ts" />
var plume;
(function (plume) {
    // Entry point of texture loading
    // It can delegate to TextureLoader or CompressedTextureLoader and handle low/high res resolution loading
    var ProgressiveTextureLoaderManager = /** @class */ (function (_super) {
        __extends(ProgressiveTextureLoaderManager, _super);
        function ProgressiveTextureLoaderManager() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this._phase = 1; // only 1 loading phase for now (ie: low res then normal res)
            _this._infoByPath = new plume.HashMap();
            _this._imageDataByPath = new plume.HashMap();
            _this._nextPhaseUrls = new Array();
            _this._compressionFormat = null;
            _this.progressiveLoadingEnabled = true;
            return _this;
        }
        ProgressiveTextureLoaderManager.prototype.initialize = function (texturesInfos, loader) {
            var self = this;
            var compressionFormat = 1;
            var renderer = plume.Engine.get().renderer;
            for (var i = 0; i < texturesInfos.length; i++) {
                var info = texturesInfos[i];
                this._infoByPath.put(info.path, info);
                compressionFormat = (compressionFormat | info.format);
                var dotIdx = info.path.lastIndexOf(".");
                var slashIdx = info.path.lastIndexOf("/");
                var imageData = {
                    path: info.path.substring(0, slashIdx),
                    name: info.path.substring(slashIdx + 1, dotIdx),
                    ext: info.path.substring(dotIdx + 1),
                };
                this._imageDataByPath.put(info.path, imageData);
            }
            loader.onLoaded(function () {
                self._onFirstLoadComplete();
            });
            // We suppose that for all compressed image we have the same compressed version available
            // (ie: we have ASTC & S3TC for all compressed image, not ASTC & S3TC for 1 image, ASTC & PVR for another image, etc.)
            var allFormats = [2 /* ASTC */, 8 /* S3TC */, 4 /* PVR */, 16 /* ETC */];
            for (var i = 0; i < allFormats.length; i++) {
                var f = allFormats[i];
                var supported = this._isSupported(f, compressionFormat);
                if (supported) {
                    // logger.debug("Using compression format " + f);
                    this._compressionFormat = f;
                    break;
                }
            }
        };
        ProgressiveTextureLoaderManager.prototype._isSupported = function (format, compressionFormat) {
            // if ((compressionFormat & TextureCompression.ASTC) == TextureCompression.ASTC) {
            if ((compressionFormat & format) == format) {
                // Format used by texture
                // Does renderer support it ?
                var extension = ProgressiveTextureLoaderManager.GLEXTENSION_BY_TEXTURECOMPRESSION[format];
                return (plume.Engine.get().renderer.extensions.get(extension) != null);
            }
            else {
                // Format not used
                return false;
            }
        };
        // Override TextureLoaderManager to handle:
        // - low res loading
        // - compressed texture loading
        ProgressiveTextureLoaderManager.prototype.load = function (url, onLoad, onError) {
            var realUrl = url;
            var compressed = false;
            var info = this._infoByPath.get(url);
            if (info != null) {
                var lowRes = this._getLowRes(info);
                var extension = this._getCompressionExt(info);
                var imagedata = this._imageDataByPath.get(url);
                realUrl = imagedata.path + "/" + imagedata.name;
                if (lowRes) {
                    realUrl += "_LOW";
                }
                if (extension != null) {
                    realUrl += "_" + extension + ".ktx";
                    compressed = true;
                }
                else {
                    realUrl += "." + imagedata.ext;
                }
                if (lowRes) {
                    var nextPhaseUrl = imagedata.path + "/" + imagedata.name;
                    if (extension != null) {
                        nextPhaseUrl += "_" + extension + ".ktx";
                    }
                    else {
                        nextPhaseUrl += "." + imagedata.ext;
                    }
                    this._nextPhaseUrls.push({ original: url, next: nextPhaseUrl }); // Track for 2d load
                }
            }
            else {
                // No compression/resolution data, ignore
            }
            var texture = this._load(realUrl, compressed, onLoad, onError);
            this.cache.add(url, texture);
            return texture;
        };
        ProgressiveTextureLoaderManager.prototype.getCompressionFormat = function () {
            return this._compressionFormat;
        };
        ProgressiveTextureLoaderManager.prototype.getCompressionFormatAsStr = function () {
            if (this._compressionFormat == null)
                return "";
            return ProgressiveTextureLoaderManager.IMAGEFORMAT_BY_TEXTURECOMPRESSION[this._compressionFormat];
        };
        ProgressiveTextureLoaderManager.prototype._getLowRes = function (textureInfo) {
            if (this._phase > 1)
                return false; // phase 2 => HR texture
            return ((textureInfo.quality & 2 /* LOW */) == 2 /* LOW */); // phase 1 => do we have low-res texture ?
        };
        ProgressiveTextureLoaderManager.prototype._getCompressionExt = function (textureInfo) {
            if (this._compressionFormat == null)
                return null; // No browser support
            var supported = ((textureInfo.format & this._compressionFormat) == this._compressionFormat);
            if (!supported)
                return null; // No texture for the supported compression format
            var extension = ProgressiveTextureLoaderManager.IMAGEEXT_BY_TEXTURECOMPRESSION[this._compressionFormat];
            return extension;
        };
        ProgressiveTextureLoaderManager.prototype._load = function (url, isCompressed, onLoad, onError) {
            if (isCompressed) {
                // KTX file
                var loader = new plume.KTXLoader(this.manager);
                var texture = loader.load(url, onLoad, onError);
                return texture;
            }
            else {
                // Regular image
                var loader = new plume.TextureLoader(this.manager);
                var texture = loader.load(url, onLoad, onError);
                return texture;
            }
        };
        ProgressiveTextureLoaderManager.prototype._onFirstLoadComplete = function () {
            if (!this.progressiveLoadingEnabled)
                return; // HD texture loading explicitely disabled by user
            // console.log("Upgrading textures..");
            // First load complete, upgrade to higher resolution
            this._phase++;
            for (var i = 0; i < this._nextPhaseUrls.length; i++) {
                this._upgradeTexture(this._nextPhaseUrls[i]);
            }
            this._nextPhaseUrls = [];
        };
        ProgressiveTextureLoaderManager.prototype._upgradeTexture = function (data) {
            var texture = this.cache.get(data.original);
            if (texture == null) {
                plume.logger.warn("No mapping found for texture " + data.original);
                return;
            }
            var extension = data.next.substring(data.next.length - 3);
            if (extension == "ktx") {
                // Compressed texture
                var loader = new plume.KTXLoader(this.manager);
                loader.swap(data.next, texture, function () {
                    // logger.debug("HR version of " + data.original + " loaded (" + data.next + ")");
                });
            }
            else {
                // Normal texture
                var loader = new plume.TextureLoader(this.manager);
                loader.swap(data.next, texture, function () {
                    // logger.debug("HR version of " + data.original + " loaded (" + data.next + ")");
                });
            }
        };
        ProgressiveTextureLoaderManager.GLEXTENSION_BY_TEXTURECOMPRESSION = {
            2: "WEBGL_compressed_texture_astc",
            4: "WEBGL_compressed_texture_pvrtc",
            8: "WEBGL_compressed_texture_s3tc",
            16: "WEBGL_compressed_texture_etc1",
        };
        // should be in sync with grunt asset & texture script
        ProgressiveTextureLoaderManager.IMAGEEXT_BY_TEXTURECOMPRESSION = {
            2: "ASTC",
            4: "PVRX",
            8: "S3TC",
            16: "ETCX",
        };
        // should be in sync with grunt asset & texture script
        ProgressiveTextureLoaderManager.IMAGEFORMAT_BY_TEXTURECOMPRESSION = {
            2: "ASTC",
            4: "PVRTC",
            8: "S3TC",
            16: "ETC1",
        };
        return ProgressiveTextureLoaderManager;
    }(plume.TextureLoaderManager));
    plume.ProgressiveTextureLoaderManager = ProgressiveTextureLoaderManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    //
    // Same as THREE.TextureLoader but can update an existing texture
    //
    var TextureLoader = /** @class */ (function () {
        function TextureLoader(manager, cache) {
            this.manager = manager;
            this.cache = cache;
            this.crossOrigin = "anonymous";
            if (manager == null) {
                this.manager = THREE.DefaultLoadingManager;
            }
            if (cache == null) {
                this.cache = plume.Engine.get().textureCache;
            }
        }
        TextureLoader.prototype.load = function (url, onLoad, onError) {
            var texture = new THREE.Texture();
            this._load0(url, texture, onLoad, onError);
            return texture;
        };
        TextureLoader.prototype.swap = function (url, texture, onLoad, onError) {
            this._load0(url, texture, onLoad, onError);
            return texture;
        };
        TextureLoader.prototype._load0 = function (url, texture, onLoad, onError) {
            var loader = new THREE.ImageLoader(this.manager);
            loader.setCrossOrigin(this.crossOrigin);
            // loader.setPath(this.path);
            loader.load(url, function (image) {
                texture.image = image;
                // JPEGs can't have an alpha channel, so memory can be saved by storing them as RGB.
                var isJPEG = url.search(/\.jpe?g$/i) > 0 || url.search(/^data\:image\/jpeg/) === 0;
                texture.format = isJPEG ? THREE.RGBFormat : THREE.RGBAFormat;
                texture.needsUpdate = true;
                if (onLoad !== undefined) {
                    onLoad(texture);
                }
            }, null, onError);
            return texture;
        };
        return TextureLoader;
    }());
    plume.TextureLoader = TextureLoader;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var DynamicTexture = /** @class */ (function () {
        function DynamicTexture(canvas) {
            this.canvas = canvas;
            this._width = 0;
            this._height = 0;
            this.heightRatio = 3.65;
            this._context2d = canvas.getContext("2d");
            this._width = this.canvas.width;
            this._height = this.canvas.height;
            this._texture = new THREE.Texture(canvas);
        }
        DynamicTexture.fromSize = function (width, height) {
            var canvas = document.createElement("canvas");
            canvas.width = width;
            canvas.height = height;
            return new DynamicTexture(canvas);
        };
        DynamicTexture.prototype._debug = function () {
            this.canvas.style.zIndex = "15";
            this.canvas.style.position = "absolute";
            document.body.appendChild(this.canvas);
        };
        DynamicTexture.prototype.update = function () {
            this._texture.needsUpdate = true;
        };
        Object.defineProperty(DynamicTexture.prototype, "texture", {
            get: function () {
                return this._texture;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DynamicTexture.prototype, "context2d", {
            get: function () {
                return this._context2d;
            },
            enumerable: true,
            configurable: true
        });
        DynamicTexture.prototype.clear = function () {
            this.context2d.clearRect(0, 0, this._width, this._height);
        };
        // shortcut to draw text
        DynamicTexture.prototype.drawText = function (text, fontName, fontWeight, fontSize, color, bgColor, x, y, update) {
            if (bgColor === void 0) { bgColor = null; }
            if (x === void 0) { x = null; }
            if (y === void 0) { y = null; }
            if (update === void 0) { update = true; }
            var font = fontWeight + " " + fontSize + "px" + " " + fontName;
            if (bgColor != null) {
                this._context2d.fillStyle = bgColor;
                this._context2d.fillRect(0, 0, this._width, this._height);
            }
            this._context2d.font = font;
            this._context2d.textBaseline = "alphabetic";
            if (x == null) {
                // center
                var textSize = this._context2d.measureText(text);
                x = (this._width - textSize.width) / 2;
            }
            if (y == null) {
                // center
                var textHeight = fontSize / this.heightRatio;
                y = (this._width - textHeight) / 2;
            }
            this._context2d.font = font;
            this._context2d.textBaseline = "alphabetic";
            this._context2d.fillStyle = color;
            this._context2d.fillText(text, x, y);
            if (update) {
                this.update();
            }
        };
        return DynamicTexture;
    }());
    plume.DynamicTexture = DynamicTexture;
})(plume || (plume = {}));
var plume;
(function (plume) {
    // Make all faces use unique vertices so that each face can be separated from others
    var ExplodeModifier = /** @class */ (function () {
        function ExplodeModifier() {
        }
        ExplodeModifier.prototype.modify = function (geometry) {
            var vertices = [];
            for (var i = 0, il = geometry.faces.length; i < il; i++) {
                var n = vertices.length;
                var face = geometry.faces[i];
                var a = face.a;
                var b = face.b;
                var c = face.c;
                var va = geometry.vertices[a];
                var vb = geometry.vertices[b];
                var vc = geometry.vertices[c];
                vertices.push(va.clone());
                vertices.push(vb.clone());
                vertices.push(vc.clone());
                face.a = n;
                face.b = n + 1;
                face.c = n + 2;
            }
            geometry.vertices = vertices;
        };
        return ExplodeModifier;
    }());
    plume.ExplodeModifier = ExplodeModifier;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var FpsHelper = /** @class */ (function () {
        function FpsHelper(showDiv) {
            if (showDiv === void 0) { showDiv = true; }
            this.optShowFps = true;
            this.optShowUpdateTime = false;
            this.optShowRenderTime = false;
            // rendering
            this.optShowCalls = false;
            this.optShowTriangles = false;
            // memory
            this.optShowGeometries = false;
            this.optShowTextures = false;
            // physics
            this.optShowPhysics = false;
            // debug
            this.optPrintRenderableOnClick = true;
            this._customStats = new plume.HashMap();
            this._statsFlush = performance.now();
            if (showDiv) {
                var div = document.createElement("div");
                div.id = "plume-fps";
                div.style.position = "absolute";
                div.style.color = "white";
                div.style.fontFamily = "Consolas, monaco, monospace";
                div.style.fontSize = "12px";
                div.style.backgroundColor = "#242526";
                div.style.zIndex = "11";
                div.onclick = this._OnClick.bind(this);
                document.body.appendChild(div);
                this._statsDiv = div;
            }
            this._engine = plume.Engine.get();
            this._game = plume.Game.get();
            this._game.trackStats = true;
            this._game.addUpdatable(this);
        }
        FpsHelper.prototype.showAll = function () {
            this.optShowFps = true;
            this.optShowUpdateTime = true;
            this.optShowRenderTime = true;
            this.optShowCalls = true;
            this.optShowTriangles = true;
            this.optShowGeometries = true;
            this.optShowTextures = true;
            this.optShowPhysics = true;
        };
        FpsHelper.prototype.update = function (dt) {
            var now = performance.now();
            var ellapsed = now - this._statsFlush;
            if (ellapsed >= 1000) {
                var info = this._engine.renderer.info;
                var text_1 = "";
                // fps
                var fps = (this._game._renderStat.count / (ellapsed / 1000)).toFixed();
                this._statsFlush = now;
                text_1 += fps + " fps\n";
                if (this.optShowUpdateTime) {
                    text_1 += "Update: " + (this._game._updateStat.sum / this._game._updateStat.count).toFixed(2) + " ms\n";
                }
                if (this.optShowRenderTime) {
                    text_1 += "Render: " + (this._game._renderStat.sum / this._game._updateStat.count).toFixed(2) + " ms\n";
                    text_1 += "Render delta: " + (this._game._deltaStat.sum / this._game._deltaStat.count).toFixed(2) + " ms\n";
                }
                if (this._game.simulation && this._game.simulation.stats) {
                    if (this.optShowPhysics) {
                        text_1 += "Physics: " + this._game.simulation.stats.avg + " ms\n";
                    }
                }
                this._game._updateStat.count = 0;
                this._game._updateStat.sum = 0;
                this._game._renderStat.count = 0;
                this._game._renderStat.sum = 0;
                this._game._deltaStat.count = 0;
                this._game._deltaStat.sum = 0;
                //
                // threejs stats 
                if (this.optShowCalls) {
                    text_1 += "Calls: " + info.render.calls + "\n";
                }
                if (this.optShowTriangles) {
                    text_1 += "Triangles: " + info.render.triangles + "\n";
                }
                if (this.optShowGeometries) {
                    text_1 += "Geometries: " + info.memory.geometries + "\n";
                }
                if (this.optShowTextures) {
                    text_1 += "Textures: " + info.memory.textures + "\n";
                }
                if (this._customStats.size() > 0) {
                    this._customStats.forEach(function (key, entry) {
                        var avg = entry.sum / entry.count;
                        text_1 += key + ": " + avg.toFixed(2) + "\n";
                        entry.count = 0;
                        entry.sum = 0;
                    }, this);
                }
                var optData = this.onFlushStats();
                if (optData != null) {
                    text_1 += optData;
                }
                if (this._statsDiv != null) {
                    this._statsDiv.innerText = text_1;
                }
            }
        };
        FpsHelper.prototype.destroy = function () {
            this._game.removeUpdatable(this);
            if (this._statsDiv != null) {
                this._statsDiv.parentElement.removeChild(this._statsDiv);
            }
        };
        // Add any text for fps logger
        FpsHelper.prototype.onFlushStats = function () {
            return null;
        };
        FpsHelper.prototype.addCustom = function (name) {
            var entry = {
                count: 0,
                start: 0,
                sum: 0
            };
            this._customStats.put(name, entry);
            return entry;
        };
        FpsHelper.prototype.getCustom = function (name) {
            return this._customStats.get(name);
        };
        FpsHelper.prototype._OnClick = function () {
            if (!this.optPrintRenderableOnClick)
                return;
            var renderable = this._engine.renderer.renderLists.get(this._engine.scene, this._engine.camera);
            plume.logger.debug("Opaque: " + renderable.opaque.length + ", Transparent: " + renderable.transparent.length);
            var datas = this._CollectMeshData(renderable.opaque, true);
            datas = this._CollectMeshData(renderable.transparent, true, datas);
            datas.sort(function (m0, m1) {
                return (m1.faces - m0.faces);
            });
            var total = 0;
            plume.logger.debug("Triangles:");
            for (var i = 0; i < datas.length; i++) {
                var d = datas[i];
                plume.logger.debug(d.name + ": " + d.faces + " triangles (with " + d.count + " instances)");
                total += d.faces;
            }
        };
        Object.defineProperty(FpsHelper.prototype, "div", {
            get: function () {
                return this._statsDiv;
            },
            enumerable: true,
            configurable: true
        });
        // Call on demand only!
        FpsHelper.prototype._CollectMeshData = function (input, sum, output) {
            if (sum === void 0) { sum = true; }
            if (output == null) {
                output = [];
            }
            var findByName = function (name) {
                for (var i = 0; i < output.length; i++) {
                    if (output[i].name == name)
                        return output[i];
                }
                return null;
            };
            for (var i = 0; i < input.length; i++) {
                var renderitem = input[i];
                var object = renderitem.object;
                var name_1 = (object.name == null ? "na" : object.name);
                var faces = -1;
                if (object instanceof THREE.Mesh) {
                    var geometry = object.geometry;
                    if (geometry instanceof THREE.Geometry) {
                        faces = geometry.faces.length;
                    }
                    else if (geometry instanceof THREE.BufferGeometry) {
                        var g = geometry.toNonIndexed();
                        var vertices = g.getAttribute("position").array;
                        faces = vertices.length / 9;
                    }
                }
                if (sum) {
                    var found = findByName(name_1);
                    if (found == null) {
                        found = { name: name_1, faces: faces, count: 1 };
                        output.push(found);
                    }
                    else {
                        found.count += 1;
                        found.faces += faces;
                    }
                }
                else {
                    output.push({ name: name_1, faces: faces, count: 1 });
                }
            }
            return output;
        };
        return FpsHelper;
    }());
    plume.FpsHelper = FpsHelper;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var SceneUtils = /** @class */ (function () {
        function SceneUtils() {
        }
        SceneUtils.detach = function (child, parent, scene) {
            if (parent == null)
                parent = child.parent;
            if (scene == null)
                scene = plume.Engine.get().scene;
            child.applyMatrix(parent.matrixWorld);
            parent.remove(child);
            scene.add(child);
        };
        SceneUtils.attach = function (child, parent, scene) {
            if (scene == null)
                scene = plume.Engine.get().scene;
            child.applyMatrix(this._tmp_mat4.getInverse(parent.matrixWorld));
            scene.remove(child);
            parent.add(child);
        };
        SceneUtils.move = function (object, newParent) {
            this.detach(object);
            this.attach(object, newParent);
        };
        SceneUtils._tmp_mat4 = new THREE.Matrix4();
        return SceneUtils;
    }());
    plume.SceneUtils = SceneUtils;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var AbstractLoadingResource = /** @class */ (function () {
        function AbstractLoadingResource() {
            this._success = false;
            this._failed = false;
        }
        AbstractLoadingResource.prototype.onSuccess0 = function () {
            this._success = true;
            if (this._onSuccess != null)
                this._onSuccess();
        };
        AbstractLoadingResource.prototype.onFailed0 = function () {
            this._failed = true;
            if (this._onFailed != null)
                this._onFailed();
        };
        AbstractLoadingResource.prototype.onSuccess = function (cb) {
            this._onSuccess = cb;
            if (this._success) {
                cb();
            }
        };
        AbstractLoadingResource.prototype.onFailed = function (cb) {
            this._onFailed = cb;
            if (this._failed) {
                cb();
            }
        };
        AbstractLoadingResource.prototype.isSuccess = function () {
            return this._success;
        };
        AbstractLoadingResource.prototype.isFailed = function () {
            return this._failed;
        };
        AbstractLoadingResource.prototype.isTerminated = function () {
            return (this._success == true || this._failed == true);
        };
        return AbstractLoadingResource;
    }());
    plume.AbstractLoadingResource = AbstractLoadingResource;
})(plume || (plume = {}));
/// <reference path="AbstractLoadingResource.ts" />
var plume;
(function (plume) {
    var FileResource = /** @class */ (function (_super) {
        __extends(FileResource, _super);
        function FileResource(file) {
            var _this = _super.call(this) || this;
            _this.file = file;
            var self = _this;
            var onData = function (data) {
                self.data = data;
                self.onSuccess0();
            };
            var fail = function (reason) {
                plume.logger.error("Can't load " + self.file + ", reason: " + reason);
                self.onFailed0();
            };
            var proxy = plume.LoaderProxy.get();
            proxy.loadXHR(_this.file, onData, fail);
            return _this;
        }
        FileResource.prototype.getAssetDef = function () {
            return {
                type: "file",
                name: this.file,
                file: this.file
            };
        };
        return FileResource;
    }(plume.AbstractLoadingResource));
    plume.FileResource = FileResource;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var HashMap = /** @class */ (function () {
        function HashMap() {
            this._data = {};
            this._size = 0;
        }
        HashMap.prototype.isEmpty = function () {
            return this._size == 0;
        };
        HashMap.prototype.size = function () {
            return this._size;
        };
        HashMap.prototype.clear = function () {
            this._data = {};
            this._size = 0;
        };
        HashMap.prototype.containsKey = function (key) {
            return (this._data[key] != null);
        };
        HashMap.prototype.get = function (key) {
            var v = this._data[key];
            return v;
        };
        HashMap.prototype.put = function (key, value) {
            var previous = this._data[key];
            this._data[key] = value;
            if (previous == null) {
                this._size++;
            }
            return previous;
        };
        HashMap.prototype.remove = function (key) {
            var previous = this._data[key];
            if (previous != null) {
                delete this._data[key];
                this._size--;
            }
            return previous;
        };
        HashMap.prototype.removeValues = function (value) {
            var keys = [];
            for (var key in this._data) {
                var v = this._data[key];
                if (v != null && v == value)
                    keys.push(key);
            }
            for (var i = 0; i < keys.length; i++) {
                this.remove(keys[i]);
            }
        };
        HashMap.prototype.keys = function () {
            var keys = [];
            for (var key in this._data) {
                keys.push(key);
            }
            return keys;
        };
        HashMap.prototype.values = function () {
            var values = [];
            for (var key in this._data) {
                var v = this._data[key];
                if (v != null)
                    values.push(v);
            }
            return values;
        };
        HashMap.prototype.forEach = function (callback, thisArg) {
            for (var key in this._data) {
                var v = this._data[key];
                if (v != null) {
                    var result = callback.call(thisArg, key, v);
                    if (result === false) {
                        break;
                    }
                }
            }
        };
        HashMap.prototype.computeIfAbsent = function (key, mappingFunction) {
            var found = this.get(key);
            if (found != null) {
                return found;
            }
            var v = mappingFunction(key);
            this.put(key, v);
            return v;
        };
        return HashMap;
    }());
    plume.HashMap = HashMap;
})(plume || (plume = {}));
/// <reference path="../utils/collection/HashMap.ts" />
var plume;
(function (plume) {
    var TextureType;
    (function (TextureType) {
        TextureType[TextureType["IMG"] = 0] = "IMG";
        TextureType[TextureType["CANVAS"] = 1] = "CANVAS";
    })(TextureType = plume.TextureType || (plume.TextureType = {}));
    var Texture = /** @class */ (function () {
        function Texture(name, x, y, width, height) {
            this.name = name;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this._type = TextureType.IMG;
            this._loaded = false;
        }
        Texture.prototype.setTrim = function (dx, dy, sourceW, sourceH) {
            this._trim = { x: dx, y: dy, width: sourceW, height: sourceH };
        };
        Texture.prototype.getTrim = function () {
            return this._trim;
        };
        Texture.prototype.getFrameTexture = function (x, y, width, height, nameId) {
            if (nameId && Texture.containsTexture(nameId))
                return Texture.getTexture(nameId);
            var trim = this._trim;
            if (trim) {
                x += trim.x;
                y += trim.y;
            }
            var texture = new Texture(nameId ? nameId : 'useless', x, y, width, height);
            texture.setImpl(this._impl);
            texture.setTrim(0, 0, width, height);
            if (nameId)
                Texture.addTexture(texture);
            return texture;
        };
        Object.defineProperty(Texture.prototype, "trimmed", {
            get: function () {
                return this._trim != null;
            },
            set: function (t) {
                throw new Error("trimmed is read-only");
            },
            enumerable: true,
            configurable: true
        });
        Texture.addTexture = function (texture) {
            Texture.textures.put(texture.name, texture);
        };
        Texture.getTexture = function (name) {
            var texture = Texture.textures.get(name);
            if (texture == null) {
                throw new Error("Texture " + name + " not loaded.");
            }
            return texture;
        };
        // Like getTexture, but do not throw exception
        Texture.containsTexture = function (name) {
            var texture = Texture.textures.get(name);
            return texture;
        };
        Texture.fromFrame = function (frameId) {
            var texture = Texture.getTexture(frameId);
            return texture;
        };
        Texture.fromImage = function (url) {
            var texture = Texture.containsTexture(url);
            if (texture != null) {
                return texture;
            }
            var img = new plume.PImage(url, url);
            texture = img.getTexture();
            return texture;
        };
        Texture.fromFrames = function (frameIds) {
            var textureArray = [];
            frameIds.forEach(function (frameId) {
                textureArray.push(Texture.fromFrame(frameId));
            });
            return textureArray;
        };
        Texture.prototype.setLoaded = function (loaded) {
            this._loaded = loaded;
            if (loaded && this._loadedHadler != null) {
                this._loadedHadler.fire(true);
            }
        };
        Texture.prototype.isLoaded = function () {
            return this._loaded;
        };
        Texture.prototype.setImpl = function (impl) {
            this._impl = impl;
        };
        Texture.prototype.getImpl = function () {
            return this._impl;
        };
        Texture.prototype.addLoadedHandler = function (cb) {
            if (this._loaded) {
                cb();
                return;
            }
            if (this._loadedHadler == null)
                this._loadedHadler = new plume.Handler();
            this._loadedHadler.once(cb);
        };
        Texture.textures = new plume.HashMap();
        return Texture;
    }());
    plume.Texture = Texture;
})(plume || (plume = {}));
/// <reference path="AbstractLoadingResource.ts" />
/// <reference path="Texture.ts" />
var plume;
(function (plume) {
    var HFont = /** @class */ (function (_super) {
        __extends(HFont, _super);
        function HFont(_name, _img, _def) {
            var _this = _super.call(this) || this;
            _this._name = _name;
            _this._img = _img;
            _this._def = _def;
            // Wait for 2 the requests to be completed before marking resource as laded/failed
            var self = _this;
            var q = new plume.WaitQueue(2);
            q.onComplete = function () {
                self._onAllLoaded();
            };
            _this.chars = [];
            _this.lettersByCode = new Array(65000);
            _this.textureByCode = new Array(65000);
            _this._atlas = new plume.FileResource(_def);
            _this._image = plume.Resources.addImage(_name, _img);
            _this.resource = _this._image.resource;
            _this._atlas.onSuccess(function () {
                q.resolve(0, null);
            });
            _this._atlas.onFailed(function () {
                q.resolve(0, null);
            });
            _this._image.onSuccess(function () {
                q.resolve(1, null);
            });
            _this._image.onFailed(function () {
                q.resolve(1, null);
            });
            return _this;
        }
        HFont.prototype._onAllLoaded = function () {
            if (!this._image.isTerminated() || !this._atlas.isTerminated()) {
                plume.logger.error("Bad font loading. Image and atlas should be success/failed here");
                return;
            }
            if (this._image.isSuccess() && this._atlas.isSuccess()) {
                this._createFont();
                this.onSuccess0();
            }
            else {
                this.onFailed0();
            }
        };
        HFont.prototype._createFont = function () {
            var xmlDoc;
            if (window['DOMParser']) {
                var parser = new DOMParser();
                xmlDoc = parser.parseFromString(this._atlas.data, "text/xml");
            }
            else {
                xmlDoc = new window["ActiveXObject"]("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(this._atlas.data);
            }
            // <?xml version='1.0'?>
            // <font>
            //     <info aa='1' size='54' smooth='1' stretchH='100' bold='0' padding='0,0,0,0' spacing='0,0' charset='' italic='0' unicode='0' face='cantarell_white'/>
            //     <common scaleW='384' packed='0' pages='1' lineHeight='75' scaleH='384' base='61'/>
            //     <pages>
            //          <page id='0' file='cantarell_white.png'/>
            //     </pages>
            //      <chars count='95'>
            //          <char xadvance='15' x='48' chnl='0' yoffset='57' y='60' xoffset='0' id='32' page='0' height='0' width='0'/>
            var font = xmlDoc.getElementsByTagName('font')[0];
            var info = font.getElementsByTagName('info')[0];
            var common = font.getElementsByTagName('common')[0];
            this.size = parseInt(info.getAttribute("size"));
            this.lineHeight = parseInt(common.getAttribute("lineHeight"));
            var chars = font.getElementsByTagName('char');
            for (var i = 0; i < chars.length; i++) {
                var char = chars[i];
                var letter = {
                    id: parseInt(char.getAttribute('id')),
                    x: parseInt(char.getAttribute('x')),
                    y: parseInt(char.getAttribute('y')),
                    xoffset: parseInt(char.getAttribute('xoffset')),
                    yoffset: parseInt(char.getAttribute('yoffset')),
                    width: parseInt(char.getAttribute('width')),
                    height: parseInt(char.getAttribute('height')),
                    xadvance: parseInt(char.getAttribute('xadvance')),
                };
                this.lettersByCode[letter.id] = letter;
                var texture = new plume.Texture(this._name + "/" + letter.id, letter.x, letter.y, letter.width, letter.height);
                texture.setImpl(this.resource);
                this.textureByCode[letter.id] = texture;
                this.chars.push(letter.id);
            }
        };
        HFont.prototype.getAssetDef = function () {
            return {
                type: "font",
                name: this._name,
                file: this._img,
                atlas: this._def
            };
        };
        return HFont;
    }(plume.AbstractLoadingResource));
    plume.HFont = HFont;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var PImage = /** @class */ (function (_super) {
        __extends(PImage, _super);
        function PImage(_name, _src) {
            var _this = _super.call(this) || this;
            _this._name = _name;
            _this._src = _src;
            _this.created = false;
            var self = _this;
            var proxy = plume.LoaderProxy.get();
            _this.resource = new Image();
            // Create 0x0 texture until loaded
            _this._texture = new plume.Texture(_this._name, 0, 0, 0, 0);
            _this._texture.setImpl(_this.resource);
            plume.Texture.addTexture(_this._texture);
            proxy.loadImage(_this.resource, _src);
            _this.resource.onload = function () {
                self.onSuccess0();
            };
            _this.resource.onerror = function () {
                self.onFailed0();
            };
            if (_this.resource.complete) {
                // already loaded
                self.onSuccess0();
            }
            return _this;
        }
        PImage.prototype.onSuccess0 = function () {
            // Image loaded, create the texture
            this._createTexture();
            // And fire complete
            _super.prototype.onSuccess0.call(this);
        };
        PImage.prototype._createTexture = function () {
            if (this.created)
                return;
            this._texture.x = 0;
            this._texture.y = 0;
            this._texture.width = this.resource.width;
            this._texture.height = this.resource.height;
            this._texture.setLoaded(true);
            this.created = true;
        };
        PImage.prototype.getTexture = function () {
            return this._texture;
        };
        PImage.prototype.getAssetDef = function () {
            return {
                type: "img",
                name: this._name,
                file: this._src
            };
        };
        return PImage;
    }(plume.AbstractLoadingResource));
    plume.PImage = PImage;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Loader2d = /** @class */ (function () {
        function Loader2d(_ressources, _retryAttempt) {
            if (_retryAttempt === void 0) { _retryAttempt = 3; }
            this._ressources = _ressources;
            this._retryAttempt = _retryAttempt;
            this._state = 0 /* PENDING */;
            this._failedResources = [];
            this._successResources = [];
            this._terminated = false;
            this._attempt = 0;
            this._progressHandler = new plume.Handler();
        }
        Loader2d.prototype.load = function (completeCb) {
            this._completeCb = completeCb;
            this._timer = plume.TimeManager.get().createTimer(100, this._checkLoaded, this);
            this._timer.setRepeat(-1);
            this._timer.start();
            this._load0();
        };
        Loader2d.prototype.getResourcesLoaded = function () {
            return this._successResources;
        };
        Loader2d.prototype._checkLoaded = function () {
            if (this._state != 1 /* LOADING */)
                return;
            var self = this;
            var didProgress = false;
            for (var i = this._loadingResources.length - 1; i >= 0; i--) {
                // let resource = this._loadingResources[i] as (HFont | Spritesheet | HImage | Soundsheet);
                var resource = this._loadingResources[i];
                if (resource.isTerminated()) {
                    this._loadingResources.splice(i, 1);
                    if (resource.isSuccess()) {
                        didProgress = true;
                        this._successResources.push(resource);
                    }
                    else {
                        plume.logger.error("Failed to load " + resource.getAssetDef().file);
                        this._failedResources.push(resource.getAssetDef());
                    }
                }
            }
            if (didProgress) {
                var total = this._loadingResources.length + this._successResources.length + this._failedResources.length;
                var progress = this._successResources.length / total;
                this._progressHandler.fire(progress);
            }
            if (this._loadingResources.length <= 0) {
                // All terminated
                if (this._failedResources.length > 0) {
                    this._state = 0 /* PENDING */;
                    // We got some failure
                    setTimeout(function () {
                        self._retry();
                    }, 250);
                }
                else {
                    this._state = 2 /* TERMINATED */;
                    this._terminate();
                }
            }
        };
        Loader2d.prototype._load0 = function () {
            var self = this;
            this._attempt++;
            this._loadingResources = [];
            this._failedResources = [];
            this._ressources.forEach(function (asset) {
                if (asset.type == "img") {
                    var img = plume.Resources.addImage(asset.name, asset.file);
                    self._loadingResources.push(img);
                }
                else if (asset.type == "font") {
                    var font = plume.Resources.addFont(asset.name, asset.file, asset.atlas);
                    self._loadingResources.push(font);
                }
                else if (asset.type == "spritesheet") {
                    var spritesheet = plume.Resources.addSpriteSheet(asset.name, asset.file, asset.atlas);
                    self._loadingResources.push(spritesheet);
                }
                else if (asset.type == "soundsheet") {
                    var soundsheet = plume.Resources.addSoundsheet(asset.name, asset.file);
                    self._loadingResources.push(soundsheet);
                }
                else if (asset.type == "file") {
                    var soundsheet = plume.Resources.addFile(asset.name, asset.file);
                    self._loadingResources.push(soundsheet);
                }
            });
            this._state = 1 /* LOADING */;
        };
        Loader2d.prototype._retry = function () {
            var self = this;
            if (this._attempt >= this._retryAttempt) {
                this._terminate();
            }
            else {
                this._ressources = [];
                this._failedResources.forEach(function (asset) {
                    self._ressources.push(asset);
                });
                this._load0();
            }
        };
        Loader2d.prototype._terminate = function () {
            this._timer.cancel();
            this._terminated = true;
            if (this._failedResources.length > 0) {
                this._completeCb(false);
            }
            else {
                this._completeCb(true);
            }
        };
        Loader2d.prototype.getStatus = function () {
            return {
                loading: this._loadingResources.length,
                loaded: this._successResources.length,
                failed: this._failedResources.length,
            };
        };
        Loader2d.prototype.getProgress = function () {
            var total = this._loadingResources.length + this._successResources.length + this._failedResources.length;
            var progress = this._successResources.length / total;
            return progress;
        };
        Object.defineProperty(Loader2d.prototype, "terminated", {
            get: function () {
                return this._terminated;
            },
            enumerable: true,
            configurable: true
        });
        // helper to load single file data
        Loader2d.loadFile = function (file, onload) {
            var loader = new Loader2d([{ type: "file", name: file, file: file }], 5);
            loader.load(function (success) {
                if (!success) {
                    onload(null);
                    return;
                }
                var resources = loader.getResourcesLoaded();
                var fileResources = resources[0];
                onload(fileResources.data);
            });
        };
        return Loader2d;
    }());
    plume.Loader2d = Loader2d;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var LoaderProxy = /** @class */ (function () {
        function LoaderProxy() {
        }
        LoaderProxy.get = function () {
            if (this._INST == null) {
                this._INST = new LoaderProxy();
            }
            return this._INST;
        };
        LoaderProxy.set = function (factory) {
            this._INST = factory;
        };
        // XHR
        LoaderProxy.prototype.loadXHR = function (url, onsucess, onerror) {
            // console.log("<> will do xhr <> " + url);
            var request = new plume.HttpRequest();
            request.get(url, onsucess, onerror);
        };
        // Image
        LoaderProxy.prototype.loadImage = function (image, src) {
            // console.log("<> will load image <> " + src);
            image.src = src;
        };
        return LoaderProxy;
    }());
    plume.LoaderProxy = LoaderProxy;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Resources = /** @class */ (function () {
        function Resources() {
        }
        Resources.destroy = function () {
            delete Resources.images;
            delete Resources.spritesheets;
            delete Resources.fonts;
            return this;
        };
        Resources.addImage = function (name, file) {
            var image = new plume.PImage(name, file);
            this.images[name] = image;
            return image;
        };
        Resources.addFont = function (name, file, atlas) {
            var font = new plume.HFont(name, file, atlas);
            this.fonts[name] = font;
            return font;
        };
        Resources.addLoadedFont = function (name, font) {
            this.fonts[name] = font;
            return font;
        };
        Resources.addSpriteSheet = function (name, file, atlas) {
            var spritesheet = new plume.Spritesheet(name, file, atlas);
            this.spritesheets[name] = spritesheet;
            return spritesheet;
        };
        Resources.addSoundsheet = function (name, file) {
            var spritesheet = new plume.Soundsheet(name, file);
            return spritesheet;
        };
        Resources.addFile = function (name, file) {
            var spritesheet = new plume.FileResource(file);
            return spritesheet;
        };
        Resources.images = {};
        Resources.spritesheets = {};
        Resources.fonts = {};
        return Resources;
    }());
    plume.Resources = Resources;
})(plume || (plume = {}));
/// <reference path="FileResource.ts" />
var plume;
(function (plume) {
    var Soundsheet = /** @class */ (function (_super) {
        __extends(Soundsheet, _super);
        function Soundsheet(_name, _file) {
            var _this = _super.call(this, _file) || this;
            _this._name = _name;
            return _this;
        }
        Soundsheet.prototype.onSuccess0 = function () {
            _super.prototype.onSuccess0.call(this);
            plume.AudioManager.get().registerSoundsheet(this._name, this.data);
        };
        Soundsheet.prototype.getAssetDef = function () {
            return {
                type: "soundsheet",
                name: this._name,
                file: this.file
            };
        };
        return Soundsheet;
    }(plume.FileResource));
    plume.Soundsheet = Soundsheet;
})(plume || (plume = {}));
/// <reference path="AbstractLoadingResource.ts" />
var plume;
(function (plume) {
    var Spritesheet = /** @class */ (function (_super) {
        __extends(Spritesheet, _super);
        function Spritesheet(_name, _img, _def) {
            var _this = _super.call(this) || this;
            _this._name = _name;
            _this._img = _img;
            _this._def = _def;
            // Wait for 2 the requests to be completed before marking resource as laded/failed
            var self = _this;
            var q = new plume.WaitQueue(2);
            q.onComplete = function () {
                self._onAllLoaded();
            };
            _this._atlas = new plume.FileResource(_def);
            _this._image = plume.Resources.addImage(_name, _img);
            _this._atlas.onSuccess(function () {
                q.resolve(0, null);
            });
            _this._atlas.onFailed(function () {
                q.resolve(0, null);
            });
            _this._image.onSuccess(function () {
                q.resolve(1, null);
            });
            _this._image.onFailed(function () {
                q.resolve(1, null);
            });
            return _this;
        }
        Spritesheet.prototype._onAllLoaded = function () {
            if (!this._image.isTerminated() || !this._atlas.isTerminated()) {
                plume.logger.error("Bad spritesheet loading. Image and atlas should be success/failed here");
                return;
            }
            if (this._image.isSuccess() && this._atlas.isSuccess()) {
                this._createSpritesheet();
                this.onSuccess0();
            }
            else {
                this.onFailed0();
            }
        };
        Spritesheet.prototype._createSpritesheet = function () {
            var json = JSON.parse(this._atlas.data);
            for (var frameName in json.frames) {
                var frame = json.frames[frameName].frame;
                var texture = new plume.Texture(frameName, frame.x, frame.y, frame.w, frame.h);
                if (json.frames[frameName].trimmed) {
                    var sourceSize = json.frames[frameName].sourceSize;
                    var spriteSourceSize = json.frames[frameName].spriteSourceSize;
                    texture.setTrim(spriteSourceSize.x, spriteSourceSize.y, sourceSize.w, sourceSize.h);
                }
                texture.setImpl(this._image.resource);
                texture.setLoaded(true);
                plume.Texture.addTexture(texture);
            }
        };
        Spritesheet.prototype.getAssetDef = function () {
            return {
                type: "spritesheet",
                name: this._name,
                file: this._img,
                atlas: this._def
            };
        };
        return Spritesheet;
    }(plume.AbstractLoadingResource));
    plume.Spritesheet = Spritesheet;
})(plume || (plume = {}));
var plume;
(function (plume) {
    //
    // GB impl (here to simplify build for now)
    // Assets needs to be in html file in in
    // window["__gb_assets"] = [ {t: "png", name: "game.png", data: "data:image/png;base64,iVBORw0KGgoAAAANS"}, {t: "json", name: "game.json", data: "..", etc ]
    var InlineLoader = /** @class */ (function (_super) {
        __extends(InlineLoader, _super);
        function InlineLoader() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.assetExportName = "__gb_assets";
            return _this;
        }
        // XHR
        InlineLoader.prototype.loadXHR = function (url, onsucess, onerror) {
            var asset = this._getAsset(url);
            if (asset == null) {
                plume.logger.error("Asset not found: " + url);
                onerror("Not found " + url);
                return;
            }
            var response = "";
            if (asset.t == "json") {
                //TB test zlib decompression 
                if (asset['zlib'] != null) {
                    try {
                        // Decode base64 (convert ascii to binary)
                        var b64Str = asset.data;
                        var strData = atob(b64Str);
                        //directly decompress to utf8 string
                        var unzipData = pako.inflate(strData, { raw: false, to: 'string' });
                        asset.data = unzipData;
                    }
                    catch (err) {
                        plume.logger.error("failed to decompress json", err);
                    }
                }
                response = asset.data;
            }
            else {
                response = asset.data;
            }
            onsucess(response);
        };
        // Image
        InlineLoader.prototype.loadImage = function (image, src) {
            var asset = this._getAsset(src);
            if (asset == null) {
                var start = src.substr(0, 5);
                if (start != "data:") {
                    plume.logger.error("Image not found: " + src);
                    return;
                }
                else {
                    // request a base64 image, we can use it
                    image.src = src;
                    return;
                }
            }
            image.src = asset.data;
        };
        InlineLoader.prototype._getAsset = function (name) {
            this._ensureInititalized();
            return this._assetByName.get(name);
        };
        InlineLoader.prototype._ensureInititalized = function () {
            if (this._assetByName != null)
                return;
            this._assetByName = new plume.HashMap();
            var assets = window[this.assetExportName];
            for (var i = 0; i < assets.length; i++) {
                var a = assets[i];
                this._assetByName.put(a.name, a);
            }
        };
        return InlineLoader;
    }(plume.LoaderProxy));
    plume.InlineLoader = InlineLoader;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var DeviceManager = /** @class */ (function () {
        function DeviceManager() {
            var _this = this;
            this.os = new plume.OS();
            this.fullscreenManager = new plume.FullScreenManager();
            this.visibilityManager = new plume.VisibilityManager();
            this._checkReady = function () {
                if (!document.body) {
                    window.setTimeout(_this._checkReady, 20);
                }
                else {
                    _this.os.init();
                    document.removeEventListener('DOMContentLoaded', _this._checkReady);
                    window.removeEventListener('load', _this._checkReady);
                    _this.readyCallback();
                }
            };
        }
        DeviceManager.prototype.whenReady = function (callback) {
            var ready = document.readyState == 'complete';
            this.readyCallback = callback;
            if (ready) {
                this.os.init();
                this.readyCallback();
            }
            else {
                document.addEventListener('DOMContentLoaded', this._checkReady);
                window.addEventListener('load', this._checkReady);
            }
        };
        return DeviceManager;
    }());
    plume.DeviceManager = DeviceManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var FullScreenManager = /** @class */ (function () {
        function FullScreenManager() {
            var _this = this;
            this._onFullscreenChange = function () {
                _this._isInFullScreen = _this._hasFullscreenElement();
                if (_this._changeHandler != null) {
                    _this._changeHandler.fire({});
                }
            };
            this.fullscreenEnabled = document.fullscreenEnabled || document['msFullscreenEnabled'] || document['mozFullScreenEnabled'] || document['webkitFullscreenEnabled'];
            this._element = document.documentElement;
            this._isInFullScreen = this._hasFullscreenElement();
            document.addEventListener("fullscreenchange", this._onFullscreenChange);
            document.addEventListener("msfullscreenchange", this._onFullscreenChange);
            document.addEventListener("mozfullscreenchange", this._onFullscreenChange);
            document.addEventListener("webkitfullscreenchange", this._onFullscreenChange);
        }
        Object.defineProperty(FullScreenManager.prototype, "isInFullScreen", {
            get: function () {
                return this._isInFullScreen;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FullScreenManager.prototype, "changeHandler", {
            get: function () {
                if (this._changeHandler == null)
                    this._changeHandler = new plume.Handler();
                return this._changeHandler;
            },
            enumerable: true,
            configurable: true
        });
        FullScreenManager.prototype.requestFullScreen = function () {
            if (!this.fullscreenEnabled)
                return;
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            }
            else if (document.documentElement['msRequestFullscreen']) {
                document.documentElement['msRequestFullscreen']();
            }
            else if (document.documentElement['mozRequestFullScreen']) {
                document.documentElement['mozRequestFullScreen']();
            }
            else if (document.documentElement['webkitRequestFullscreen']) {
                var aa = Element.ALLOW_KEYBOARD_INPUT;
                document.documentElement['webkitRequestFullscreen'](); //Element.ALLOW_KEYBOARD_INPUT
            }
        };
        FullScreenManager.prototype.exitFullscreen = function () {
            if (!this.fullscreenEnabled)
                return;
            if (document['exitFullscreen']) {
                document['exitFullscreen']();
            }
            else if (document['msExitFullscreen']) {
                document['msExitFullscreen']();
            }
            else if (document['mozCancelFullScreen']) {
                document['mozCancelFullScreen']();
            }
            else if (document['webkitExitFullscreen']) {
                document['webkitExitFullscreen']();
            }
        };
        FullScreenManager.prototype._hasFullscreenElement = function () {
            var fullscreenElement = document['fullscreenElement'] || document['msFullscreenElement'] || document['mozFullScreenElement'] || document['webkitFullscreenElement'];
            return fullscreenElement != null;
        };
        FullScreenManager.prototype.toggleFullScreen = function () {
            if (!this.fullscreenEnabled) {
                plume.logger.info("FullScreen is not enabled");
            }
            else {
                if (this.isInFullScreen) {
                    this.exitFullscreen();
                }
                else {
                    this.requestFullScreen();
                }
            }
        };
        return FullScreenManager;
    }());
    plume.FullScreenManager = FullScreenManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    // Keep only one instance for the game.
    // Ie: use logger.debug(...)
    var Logger = /** @class */ (function () {
        function Logger() {
            this.level = 0;
        }
        Logger.prototype.setLevel = function (level) {
            this.level = level;
        };
        Logger.prototype.debug = function (message) {
            if (this.level > 0)
                return;
            console.debug(message);
            if (this.listeners != null)
                this.listeners.fire({ level: "debug", message: message });
        };
        Logger.prototype.info = function (message) {
            if (this.level > 1)
                return;
            console.info(message);
            if (this.listeners != null)
                this.listeners.fire({ level: "info", message: message });
        };
        Logger.prototype.warn = function (message) {
            if (this.level > 2)
                return;
            console.warn(message);
            if (this.listeners != null)
                this.listeners.fire({ level: "warn", message: message });
        };
        Logger.prototype.error = function (message, error) {
            if (this.level > 3)
                return;
            console.error(message, error);
            if (this.listeners != null)
                this.listeners.fire({ level: "error", message: message, error: error });
        };
        Logger.prototype.addListener = function (i) {
            if (this.listeners == null)
                this.listeners = new plume.Handler();
            this.listeners.add(i);
        };
        return Logger;
    }());
    plume.Logger = Logger;
    plume.logger = new Logger();
    //
    // Use it when no access to devtools
    //
    var _debugConsole;
    var _debugconsoleChildren = [];
    function enableDomConsole() {
        var div = document.createElement("div");
        div.id = "plume-logger";
        div.style.background = "white";
        div.style.fontSize = "10px";
        div.style.fontFamily = "Consolas, monaco, monospace";
        div.style.position = "absolute";
        div.style.top = "0";
        div.style.zIndex = "11";
        document.body.appendChild(div);
        var top = document.createElement("div");
        div.appendChild(top);
        _debugconsoleChildren.push(top);
        _debugConsole = div;
        plume.logger.addListener(function (e) {
            append(e.level, e.message, e.error);
        });
    }
    plume.enableDomConsole = enableDomConsole;
    function append(level, message, error) {
        var top = _debugconsoleChildren[0];
        var line = document.createElement("div");
        line.innerHTML = "<b>" + level + "</b>: " + message + (error == null ? "" : ", " + error);
        _debugConsole.insertBefore(line, top);
        _debugconsoleChildren.splice(0, 0, line);
        top = line;
        if (_debugconsoleChildren.length > 8) {
            var last = _debugconsoleChildren[_debugconsoleChildren.length - 1];
            _debugconsoleChildren.splice(_debugconsoleChildren.length - 1, 1);
            _debugConsole.removeChild(last);
        }
    }
})(plume || (plume = {}));
var plume;
(function (plume) {
    var OS = /** @class */ (function () {
        function OS() {
            this.vita = false;
            this.kindle = false;
            this.android = false;
            this.chromeOS = false;
            this.iOS = false;
            this.iPhone = false;
            this.iPad = false;
            this.linux = false;
            this.macOS = false;
            this.windows = false;
            this.windowsPhone = false;
            this.desktop = false;
            this.iOSVersion = 0;
            this.pixelRatio = 1;
        }
        // From phaser Device.js
        OS.prototype.init = function () {
            var ua = navigator.userAgent;
            if (/Playstation Vita/.test(ua)) {
                this.vita = true;
            }
            else if (/Kindle/.test(ua) || /\bKF[A-Z][A-Z]+/.test(ua) || /Silk.*Mobile Safari/.test(ua)) {
                this.kindle = true;
                // This will NOT detect early generations of Kindle Fire, I think there is no reliable way...
                // E.g. "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-us; Silk/1.1.0-80) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16 Silk-Accelerated=true"
            }
            else if (/Android/.test(ua)) {
                this.android = true;
            }
            else if (/CrOS/.test(ua)) {
                this.chromeOS = true;
            }
            else if (/iP[ao]d|iPhone/i.test(ua)) {
                this.iOS = true;
                (navigator.appVersion).match(/OS (\d+)/);
                this.iOSVersion = parseInt(RegExp.$1, 10);
            }
            else if (/Linux/.test(ua)) {
                this.linux = true;
            }
            else if (/Mac OS/.test(ua)) {
                this.macOS = true;
            }
            else if (/Windows/.test(ua)) {
                this.windows = true;
            }
            if (/Windows Phone/i.test(ua) || /IEMobile/i.test(ua)) {
                this.android = false;
                this.iOS = false;
                this.macOS = false;
                this.windows = true;
                this.windowsPhone = true;
            }
            var silk = /Silk/.test(ua); // detected in browsers
            if (this.windows || this.macOS || (this.linux && !silk) || this.chromeOS) {
                this.desktop = true;
            }
            //  Windows Phone / Table reset
            if (this.windowsPhone || ((/Windows NT/i.test(ua)) && (/Touch/i.test(ua)))) {
                this.desktop = false;
            }
            this.iPhone = ua.toLowerCase().indexOf('iphone') !== -1;
            this.iPad = ua.toLowerCase().indexOf('ipad') !== -1;
            this.pixelRatio = window['devicePixelRatio'] || 1;
        };
        return OS;
    }());
    plume.OS = OS;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var VisibilityManager = /** @class */ (function () {
        function VisibilityManager() {
            this._isUnloading = false; // true when onbeforeunload event has been fired (game shutdown)
            this._isVisible = true; // internal flag, defaults to true
            this._isExtendedVisible = true;
            this._visibilityChangeHandler = new plume.Handler();
            this._extendedVisibilityChangeHandler = new plume.Handler();
            this._beforeUnloadHandler = new plume.Handler();
            this._browserPrefixes = ['moz', 'ms', 'o', 'webkit'];
            var self = this;
            // bind and handle events
            var browserPrefix = this._getBrowserPrefix();
            this._hiddenPropertyName = this._getHiddenPropertyName(browserPrefix),
                this._visibilityEventName = this._getVisibilityEvent(browserPrefix);
            document.addEventListener(this._visibilityEventName, this._handleVisibilityChange.bind(this), false);
            // extra event listeners for better behaviour
            document.addEventListener('focus', function () {
                self._handleExtendedVisibilityChange(true, 'document-focus');
            }, false);
            document.addEventListener('blur', function () {
                self._handleExtendedVisibilityChange(false, 'document-blur');
            }, false);
            window.addEventListener('focus', function () {
                self._handleExtendedVisibilityChange(true, 'window-focus');
            }, false);
            window.addEventListener('blur', function () {
                self._handleExtendedVisibilityChange(false, 'window-blur');
            }, false);
            //window.onbeforeunload = this.handleOnBeforeUnload.bind(this);;
            window.addEventListener("beforeunload", this._handleOnBeforeUnload.bind(this), false);
        }
        // get the correct attribute name
        VisibilityManager.prototype._getHiddenPropertyName = function (prefix) {
            return (prefix ? prefix + 'Hidden' : 'hidden');
        };
        // get the correct event name
        VisibilityManager.prototype._getVisibilityEvent = function (prefix) {
            return (prefix ? prefix : '') + 'visibilitychange';
        };
        // get current browser vendor prefix
        VisibilityManager.prototype._getBrowserPrefix = function () {
            for (var i = 0; i < this._browserPrefixes.length; i++) {
                if (this._getHiddenPropertyName(this._browserPrefixes[i]) in document) {
                    // return vendor prefix
                    return this._browserPrefixes[i];
                }
            }
            // no vendor prefix needed
            return null;
        };
        VisibilityManager.prototype._handleVisibilityChange = function () {
            if (document[this._hiddenPropertyName]) {
                this._isVisible = false;
                this._visibilityChangeHandler.fire(false);
                this._onExtendedHidden(this._hiddenPropertyName);
                return;
            }
            else {
                this._isVisible = true;
                this._visibilityChangeHandler.fire(true);
                this._onExtendedVisible(this._hiddenPropertyName);
            }
        };
        VisibilityManager.prototype._handleExtendedVisibilityChange = function (forcedFlag, triggerProperty) {
            // forcedFlag is a boolean when this event handler is triggered by a
            // focus or blur eventotherwise it's an Event object
            if (forcedFlag) {
                return this._onExtendedVisible(triggerProperty);
            }
            return this._onExtendedHidden(triggerProperty);
        };
        VisibilityManager.prototype._onExtendedVisible = function (triggerProperty) {
            // prevent double execution
            if (this._isExtendedVisible) {
                return;
            }
            // change flag value
            this._isExtendedVisible = true;
            this._extendedVisibilityChangeHandler.fire({ isExtended: this._isExtendedVisible, triggerProperty: triggerProperty });
        };
        VisibilityManager.prototype._onExtendedHidden = function (triggerProperty) {
            // prevent double execution
            if (!this._isExtendedVisible) {
                return;
            }
            // change flag value
            this._isExtendedVisible = false;
            this._extendedVisibilityChangeHandler.fire({ isExtended: this._isExtendedVisible, triggerProperty: triggerProperty });
        };
        VisibilityManager.prototype._handleOnBeforeUnload = function (evt) {
            this._isUnloading = true;
            this._beforeUnloadHandler.fire(evt);
        };
        VisibilityManager.prototype.addVisibilityChangeHandler = function (handler) {
            this._visibilityChangeHandler.add(handler);
        };
        VisibilityManager.prototype.removeVisibilityChangeHandler = function (handler) {
            this._visibilityChangeHandler.remove(handler);
        };
        VisibilityManager.prototype.addExtendedVisibilityChangeHandler = function (handler) {
            this._extendedVisibilityChangeHandler.add(handler);
        };
        VisibilityManager.prototype.removeExtendedVisibilityChangeHandler = function (handler) {
            this._extendedVisibilityChangeHandler.remove(handler);
        };
        VisibilityManager.prototype.addBeforeUnloadHandler = function (handler) {
            this._beforeUnloadHandler.add(handler);
        };
        VisibilityManager.prototype.removeBeforeUnloadHandler = function (handler) {
            this._beforeUnloadHandler.remove(handler);
        };
        VisibilityManager.prototype.isVisible = function () {
            return this._isVisible;
        };
        VisibilityManager.prototype.isUnloading = function () {
            return this._isUnloading;
        };
        Object.defineProperty(VisibilityManager.prototype, "hidden", {
            get: function () {
                return !this._isVisible;
            },
            set: function (v) {
                throw "Read only";
            },
            enumerable: true,
            configurable: true
        });
        return VisibilityManager;
    }());
    plume.VisibilityManager = VisibilityManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Datef;
    (function (Datef) {
        Datef.oneday = 24 * 60 * 60 * 1000;
        Datef.serverTimeDiff = 0;
        // allow to fix client time by setting server diff time
        function now() {
            var browserDate = Date.now();
            return browserDate - Datef.serverTimeDiff;
        }
        Datef.now = now;
    })(Datef = plume.Datef || (plume.Datef = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Time;
    (function (Time) {
        var _now = performance.now();
        var _ellapsed = performance.now();
        function _refresh(time) {
            _ellapsed = time - _now;
            _now = time;
        }
        Time._refresh = _refresh;
        function now() {
            return _now;
        }
        Time.now = now;
        function ellapsed() {
            return _ellapsed;
        }
        Time.ellapsed = ellapsed;
    })(Time = plume.Time || (plume.Time = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var TimeManager = /** @class */ (function () {
        function TimeManager() {
            this._timers = [];
            this._inUpdateLoop = false;
            this._timersToAdd = [];
            this._timersToRemove = [];
            TimeManager._TIME_MANAGER = this;
        }
        TimeManager.get = function () {
            if (TimeManager._TIME_MANAGER == null) {
                TimeManager._TIME_MANAGER = new TimeManager();
            }
            return TimeManager._TIME_MANAGER;
        };
        TimeManager.prototype.update = function (dt) {
            this._inUpdateLoop = true;
            this._now = plume.Time.now();
            var self = this;
            var lastExpired = -1;
            for (var i = this._timers.length - 1; i >= 0; i--) {
                var t = this._timers[i];
                var notRunning = t.update(this._now);
                if (notRunning && t.hasExpired()) {
                    //this._timers.slice(i, 1);
                    this._timersToRemove.push(t);
                }
            }
            this._inUpdateLoop = false;
            if (this._timersToRemove.length > 0) {
                this._timersToRemove.forEach(function (timer) {
                    self.remove(timer);
                });
                this._timersToRemove.length = 0;
            }
            if (this._timersToAdd.length > 0) {
                this._timersToAdd.forEach(function (timer) {
                    self._pushTimer(timer);
                });
                this._timersToAdd.length = 0;
            }
        };
        TimeManager.prototype._pushTimer = function (timer) {
            this._timers.push(timer);
        };
        TimeManager.prototype.remove = function (timer) {
            if (this._inUpdateLoop) {
                this._timersToRemove.push(timer);
                return;
            }
            var index = this._timers.indexOf(timer);
            if (index >= 0) {
                this._timers.splice(index, 1);
            }
        };
        TimeManager.prototype.removeAll = function () {
            for (var i = 0; i < this._timers.length; i++) {
                this._timers[i]._expired = true;
            }
            this._timers = [];
        };
        TimeManager.prototype.cancelAll = function () {
            for (var i = this._timers.length - 1; i >= 0; i--) {
                this._timers[i].cancel();
            }
        };
        TimeManager.prototype.createTimer = function (delay, callback, thisArg, argArray) {
            var timer = new plume.Timer(this, delay, callback, thisArg, argArray);
            if (this._inUpdateLoop) {
                this._timersToAdd.push(timer);
            }
            else {
                this._pushTimer(timer);
            }
            return timer;
        };
        TimeManager.prototype.createAndStartTimer = function (delay, callback, thisArg, argArray) {
            var timer = this.createTimer(delay, callback, thisArg, argArray);
            timer.start();
            return timer;
        };
        TimeManager.createAndStartTimer = function (delay, callback, thisArg, argArray) {
            return TimeManager.get().createAndStartTimer(delay, callback, thisArg, argArray);
        };
        Object.defineProperty(TimeManager.prototype, "activeTimersCount", {
            get: function () {
                return this._timers.length;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TimeManager.prototype, "activeTimers", {
            get: function () {
                return this._timers.map(function (elem) {
                    return elem.id;
                }).join();
            },
            enumerable: true,
            configurable: true
        });
        return TimeManager;
    }());
    plume.TimeManager = TimeManager;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Timer = /** @class */ (function () {
        function Timer(timeManager, delay, callback, thisArg, argArray) {
            this.id = "DefaultId";
            this._repeatCount = 1;
            this._running = false;
            this._expired = false;
            this._timeManager = timeManager;
            this._delay = delay;
            this._callback = callback;
            this._thisArg = thisArg;
            this._argArray = argArray;
        }
        // -1 == infinite, default == 1
        Timer.prototype.setRepeat = function (repeat) {
            this._repeatCount = repeat;
        };
        Timer.prototype.update = function (time) {
            if (!this._running)
                return true;
            var timeLeft = this._tick - time;
            if (timeLeft <= 0) {
                // Execute callback
                if (this._callback != null) {
                    this._callback.call(this._thisArg, this._argArray);
                }
                this._repeatCount--;
                if (this._repeatCount == 0) {
                    // Terminated
                    this._running = false;
                    this._expired = true;
                    return true;
                }
                else {
                    if (this._repeatCount < -1)
                        this._repeatCount = -1;
                    // Update next tick
                    this._tick = time + this._delay;
                    this._startTime = time;
                    return false;
                }
            }
            return false;
        };
        Timer.prototype.start = function () {
            if (this._running)
                return;
            this._running = true;
            this._tick = plume.Time.now() + this._delay;
            this._startTime = plume.Time.now();
        };
        Timer.prototype.cancel = function () {
            this._running = false;
            this._timeManager.remove(this);
        };
        Timer.prototype.hasExpired = function () {
            return this._expired;
        };
        Timer.prototype.getProgress = function () {
            if (this._expired)
                return 1;
            if (!this._running)
                return 0;
            var now = plume.Time.now();
            var elapsed = now - this._startTime;
            return elapsed / this._delay;
        };
        Timer.prototype.getDuration = function () {
            return this._delay;
        };
        return Timer;
    }());
    plume.Timer = Timer;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var easing;
    (function (easing) {
        function linear(t) { return t; }
        easing.linear = linear;
        ;
        function easeInQuad(t) { return t * t; }
        easing.easeInQuad = easeInQuad;
        ;
        function easeInCubic(t) { return t * t * t; }
        easing.easeInCubic = easeInCubic;
        ;
        function easeInQuint(t) { return t * t * t * t; }
        easing.easeInQuint = easeInQuint;
        ;
        function easeOutQuad(t) { return 1 - easeInQuad(1 - t); }
        easing.easeOutQuad = easeOutQuad;
        ;
        function easeOutCubic(t) { return 1 - easeInCubic(1 - t); }
        easing.easeOutCubic = easeOutCubic;
        ;
        function easeOutQuint(t) { return 1 - easeInQuint(1 - t); }
        easing.easeOutQuint = easeOutQuint;
        ;
        function easeInOutQuad(t) { return (t < 0.5 ? easeInQuad(t * 2) / 2 : 1 - easeInQuad((1 - t) * 2) / 2); }
        easing.easeInOutQuad = easeInOutQuad;
        ;
        function easeInOutCubic(t) { return (t < 0.5 ? easeInCubic(t * 2) / 2 : 1 - easeInCubic((1 - t) * 2) / 2); }
        easing.easeInOutCubic = easeInOutCubic;
        ;
        function easeInOutQuint(t) { return (t < 0.5 ? easeInQuint(t * 2) / 2 : 1 - easeInQuint((1 - t) * 2) / 2); }
        easing.easeInOutQuint = easeInOutQuint;
        ;
        function easeOutElastic(t) { return Math.pow(2, -10 * t) * Math.sin((t - 0.3 / 4) * (2 * Math.PI) / 0.3) + 1; }
        easing.easeOutElastic = easeOutElastic;
        ;
    })(easing = plume.easing || (plume.easing = {}));
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Random = /** @class */ (function () {
        function Random() {
        }
        Random.init = function (seed) {
            this._seed = seed;
            if (this._seed <= 0)
                this._seed += 2147483646;
        };
        Random.currentSeed = function () {
            return this._seed;
        };
        Random.get = function (useSeed) {
            if (useSeed === void 0) { useSeed = false; }
            if (useSeed)
                return this._random0();
            return Math.random();
        };
        Random.inRange = function (min, max, useSeed) {
            if (useSeed === void 0) { useSeed = false; }
            return this.get(useSeed) * (max - min + 1) + min;
        };
        Random.sign = function (useSeed) {
            if (useSeed === void 0) { useSeed = false; }
            return (this.get(useSeed) < 0.5 ? 1 : -1);
        };
        // Random int beetween min,max inclusive both
        Random.intInRange = function (min, max, useSeed) {
            if (useSeed === void 0) { useSeed = false; }
            return Math.floor(this.get(useSeed) * (max - min + 1)) + min;
        };
        // https://gist.github.com/blixt/f17b47c62508be59987b
        Random._getSeededInt = function () {
            return this._seed = this._seed * 16807 % 2147483647;
        };
        Random._random0 = function () {
            return (this._getSeededInt() - 1) / 2147483646;
        };
        Random._seed = Date.now();
        return Random;
    }());
    plume.Random = Random;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Strings = /** @class */ (function () {
        function Strings() {
        }
        Strings.ellipsis = function (text, maxLength, ellipsChar) {
            if (text == null)
                return text;
            if (text.length <= maxLength)
                return text;
            var shorten = text.substring(0, maxLength);
            if (ellipsChar != null) {
                shorten += ellipsChar;
            }
            return shorten;
        };
        Strings.capitalize = function (text) {
            if (text == null)
                return null;
            if (text.length <= 1)
                return text.toUpperCase();
            return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
        };
        Strings.interpolate = function (textIn, params) {
            var out = textIn.replace(/{([^{}]*)}/g, function (found, key) {
                var r = params[key];
                return r;
            });
            return out;
        };
        Strings.pad = function (num, size) {
            var s = num + "";
            while (s.length < size)
                s = "0" + s;
            return s;
        };
        Strings.pads = function (s, size) {
            while (s.length < size)
                s = "0" + s;
            return s;
        };
        Strings.pad2 = function (num) {
            return Strings.pad(num, 2);
        };
        Strings.randomUUID = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };
        return Strings;
    }());
    plume.Strings = Strings;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var ValueSmoother = /** @class */ (function () {
        function ValueSmoother() {
            this.state = 0;
            this.startTime = 0;
            this.duration = 3000;
        }
        ValueSmoother.prototype.reset = function (startValue, targetValue, duration, now, currentValue) {
            this.duration = duration;
            this.startTime = (now == null ? performance.now() : now);
            this.startValue = startValue;
            this.endValue = targetValue;
            if (currentValue != null) {
                this.currentValue = currentValue;
            }
            else {
                if (this.currentValue == null)
                    this.currentValue = this.startValue; // ensure we have a current value
            }
        };
        ValueSmoother.prototype.getInterpolation = function (now) {
            var now0 = (now == null ? performance.now() : now);
            var ellapsed = now0 - this.startTime;
            if (ellapsed >= this.duration) {
                return this.endValue;
            }
            var percent = ellapsed / this.duration;
            var easingvalue = percent; // linear by default
            if (this.easing != null) {
                easingvalue = this.easing(percent);
            }
            easingvalue = THREE.Math.clamp(easingvalue, 0, 1);
            this.currentValue = this.computeCurrentValue(easingvalue);
            return this.currentValue;
        };
        return ValueSmoother;
    }());
    plume.ValueSmoother = ValueSmoother;
    var ValueSmootherNumber = /** @class */ (function (_super) {
        __extends(ValueSmootherNumber, _super);
        function ValueSmootherNumber() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ValueSmootherNumber.prototype.computeCurrentValue = function (t) {
            return THREEEXT.Math.lerp(this.startValue, this.endValue, t);
        };
        return ValueSmootherNumber;
    }(ValueSmoother));
    plume.ValueSmootherNumber = ValueSmootherNumber;
    var ValueSmootherVector3 = /** @class */ (function (_super) {
        __extends(ValueSmootherVector3, _super);
        function ValueSmootherVector3() {
            var _this = _super.call(this) || this;
            _this.currentValue = new THREE.Vector3();
            return _this;
        }
        ValueSmootherVector3.prototype.computeCurrentValue = function (t) {
            return THREEEXT.Vector3.lerpVectors(this.startValue, this.endValue, t);
        };
        return ValueSmootherVector3;
    }(ValueSmoother));
    plume.ValueSmootherVector3 = ValueSmootherVector3;
    var ValueSmootherQuat = /** @class */ (function (_super) {
        __extends(ValueSmootherQuat, _super);
        function ValueSmootherQuat() {
            var _this = _super.call(this) || this;
            _this.currentValue = new THREE.Quaternion();
            return _this;
        }
        ValueSmootherQuat.prototype.computeCurrentValue = function (t) {
            THREE.Quaternion.slerp(this.startValue, this.endValue, this.currentValue, t);
            return this.currentValue;
        };
        return ValueSmootherQuat;
    }(ValueSmoother));
    plume.ValueSmootherQuat = ValueSmootherQuat;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Collections = /** @class */ (function () {
        function Collections() {
        }
        Collections.shuffle = function (array, useSeed) {
            if (useSeed === void 0) { useSeed = false; }
            var i = 0;
            var j = 0;
            var temp = null;
            for (i = array.length - 1; i > 0; i -= 1) {
                j = Math.floor(plume.Random.get(useSeed) * (i + 1));
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        };
        Collections.rotate = function (array, step) {
            while (step--) {
                var temp = array.shift();
                array.push(temp);
            }
        };
        Collections.concat = function () {
            var arrays = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                arrays[_i] = arguments[_i];
            }
            var result = [];
            for (var i = 0; i < arrays.length; i++) {
                var sa = arrays[i];
                for (var j = 0; j < sa.length; j++) {
                    result.push(sa[j]);
                }
            }
            return result;
        };
        Collections.addAll = function (source, elements) {
            if (elements == null)
                return;
            for (var i = 0; i < elements.length; i++) {
                source.push(elements[i]);
            }
        };
        Collections.copy = function (array) {
            if (array == null)
                return null;
            return array.slice();
        };
        // a random element of the array
        Collections.one = function (array, useSeed) {
            if (useSeed === void 0) { useSeed = false; }
            if (array == null || array.length < 1)
                return null;
            if (array.length == 1)
                return array[0];
            var rand = plume.Random.intInRange(0, array.length - 1, useSeed);
            return array[rand];
        };
        // return n random elements from array
        Collections.some = function (array, count, useSeed) {
            if (useSeed === void 0) { useSeed = false; }
            if (array == null || array.length < 1)
                return null;
            var copy = Collections.copy(array);
            Collections.shuffle(copy, useSeed);
            if (copy.length <= count)
                return copy;
            return copy.slice(0, count);
        };
        Collections.remove = function (array, obj) {
            var idx = array.indexOf(obj);
            if (idx >= 0) {
                var removed = array.splice(idx, 1);
                if (removed != null && removed.length > 0) {
                    return true;
                }
            }
            return false;
        };
        Collections.contains = function (array, obj) {
            return (array.indexOf(obj) >= 0);
        };
        return Collections;
    }());
    plume.Collections = Collections;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var http;
    (function (http) {
        function loadJs(url, onload) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            if (script["readyState"]) {
                script["onreadystatechange"] = function () {
                    if (script["readyState"] === "loaded"
                        || script["readyState"] === "complete") {
                        script["onreadystatechange"] = null;
                        onload();
                    }
                };
            }
            else {
                script.onload = function () {
                    onload();
                };
            }
            script.onerror = function (e) {
                plume.logger.error("Failed to load script " + url + "! " + e);
            };
            script.src = url;
            document.getElementsByTagName("head")[0].appendChild(script);
        }
        http.loadJs = loadJs;
        function loadArrayBuffer(url, onload, onerror) {
            var request = new XMLHttpRequest();
            request.open('GET', url, true);
            request.addEventListener('load', function (event) {
                var response = this.response;
                if (this.status === 200 || this.status === 0) {
                    // ok
                    onload(response);
                }
                else {
                    // on error
                    if (onerror)
                        onerror();
                }
            }, false);
            request.addEventListener('error', function (event) {
                // on error
                if (onerror)
                    onerror();
            }, false);
            request.responseType = 'arraybuffer';
            request.send(null);
        }
        http.loadArrayBuffer = loadArrayBuffer;
    })(http = plume.http || (plume.http = {}));
})(plume || (plume = {}));
/// <reference path="../../utils/collection/HashMap.ts" />
var plume;
(function (plume) {
    var StartupParams = /** @class */ (function () {
        function StartupParams() {
            this._params = StartupParams.urlParametersToHashMap(document.location.href);
        }
        StartupParams.getInstance = function () {
            if (StartupParams._INSTANCE == null) {
                StartupParams._INSTANCE = new StartupParams();
            }
            return StartupParams._INSTANCE;
        };
        StartupParams.add = function (key, value) {
            StartupParams.getInstance()._params.put(key, value);
        };
        StartupParams.remove = function (key) {
            var v = StartupParams.getInstance()._params.remove(key);
            return v;
        };
        StartupParams.get = function (key, valueIfNull) {
            var value = StartupParams.getInstance()._params.get(key);
            if (value == null || value.length <= 0)
                return valueIfNull;
            return value;
        };
        StartupParams.has = function (key) {
            return StartupParams.getInstance()._params.containsKey(key);
        };
        StartupParams.urlParametersToHashMap = function (url) {
            var map = new plume.HashMap();
            var pairs = url.substring(url.indexOf('?') + 1).split('&');
            for (var i = 0; i < pairs.length; i++) {
                if (!pairs[i])
                    continue;
                var pair = pairs[i].split('=');
                map.put(decodeURIComponent(pair[0]), decodeURIComponent(pair[1]));
            }
            return map;
        };
        return StartupParams;
    }());
    plume.StartupParams = StartupParams;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var Handler = /** @class */ (function () {
        function Handler() {
            this._handlers = [];
            this._onces = [];
        }
        Object.defineProperty(Handler.prototype, "length", {
            get: function () {
                return (this._handlers.length + this._onces.length);
            },
            enumerable: true,
            configurable: true
        });
        Handler.prototype.add = function (listener, context) {
            this._handlers.push({
                listener: listener,
                context: context
            });
        };
        Handler.prototype.once = function (listener, context) {
            this._onces.push({
                listener: listener,
                context: context
            });
        };
        Handler.prototype.remove = function (listener) {
            var index = -1;
            for (var i = 0; i < this._handlers.length; i++) {
                if (this._handlers[i].listener == listener) {
                    index = i;
                    break;
                }
            }
            if (index > -1) {
                this._handlers.splice(index, 1);
            }
        };
        Handler.prototype.clear = function () {
            this._handlers = [];
            this._onces = [];
        };
        Handler.prototype.fire = function (event) {
            this._handlers.forEach(function (h) {
                h.listener.apply(h.context, [event]);
            });
            this._onces.forEach(function (h) {
                h.listener.apply(h.context, [event]);
            });
            this._onces = [];
        };
        return Handler;
    }());
    plume.Handler = Handler;
    var ContextualCallback = /** @class */ (function () {
        function ContextualCallback(callback, context) {
            this.callback = callback;
            this.context = context;
        }
        ContextualCallback.prototype.execute = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            this.callback.apply(this.context, args);
        };
        // Helper to clean an array of ContextualCallback
        ContextualCallback.removeFromArray = function (fn, array) {
            var index = -1;
            for (var i = 0; i < array.length; i++) {
                if (array[i].callback == fn) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                array.splice(index, 1);
                return true;
            }
            return false;
        };
        return ContextualCallback;
    }());
    plume.ContextualCallback = ContextualCallback;
})(plume || (plume = {}));
var plume;
(function (plume) {
    // really basic wait n query before triggering onComplete
    var WaitQueue = /** @class */ (function () {
        function WaitQueue(size) {
            this.size = size;
            this._data = [];
            this._resolved = [];
            for (var i = 0; i < size; i++) {
                this._resolved[i] = false;
            }
        }
        WaitQueue.prototype.resolve = function (index, data) {
            this._data[index] = data;
            this._resolved[index] = true;
            if (this._allDone()) {
                this.onComplete(this._data);
            }
        };
        WaitQueue.prototype._allDone = function () {
            for (var i = 0; i < this._resolved.length; i++) {
                if (!this._resolved[i]) {
                    return false;
                }
            }
            return true;
        };
        return WaitQueue;
    }());
    plume.WaitQueue = WaitQueue;
})(plume || (plume = {}));
var plume;
(function (plume) {
    var LocalStorage = /** @class */ (function () {
        function LocalStorage(_id) {
            this._id = _id;
            this.available = false;
            this.available = this._isAvailable();
            if (this.available) {
                var data = localStorage.getItem(this._id);
                this.cacheData = plume.JSONS.safeJson(data, {});
            }
            else {
                this.cacheData = {};
            }
        }
        LocalStorage.prototype._isAvailable = function () {
            try {
                localStorage.setItem("test123456789", "test123456789");
                localStorage.removeItem("test123456789");
                return true;
            }
            catch (e) {
                return false;
            }
        };
        LocalStorage.prototype.put = function (key, value) {
            this.cacheData[key] = value;
            if (this.available) {
                localStorage.setItem(this._id, JSON.stringify(this.cacheData));
            }
        };
        LocalStorage.prototype.get = function (key, defValue) {
            if (this.available) {
                var v = this.cacheData[key];
                if (v == null) {
                    return defValue;
                }
                else {
                    return v;
                }
            }
            return defValue;
        };
        LocalStorage.prototype.remove = function (key) {
            if (this.available) {
                delete this.cacheData[key];
                var keys = Object.keys(this.cacheData);
                if (keys.length == 0) {
                    localStorage.setItem(this._id, "");
                    localStorage.removeItem(this._id);
                }
                else {
                    localStorage.setItem(this._id, JSON.stringify(this.cacheData));
                }
            }
        };
        return LocalStorage;
    }());
    plume.LocalStorage = LocalStorage;
})(plume || (plume = {}));
//# sourceMappingURL=plume3d.core.js.map