/// <reference path="plume3d.core.d.ts" />
declare module plume {
    class Tween {
        id: string;
        private _object;
        private _tweenManager;
        private _repeatCount;
        private _currentStepIndex;
        private _currentStep;
        private _steps;
        private _state;
        _savedValues: {
            [key: string]: number;
        };
        onStart: Function;
        onComplete: Function;
        constructor(object: any, tweenManager: TweenManager);
        getObject(): any;
        delay(delay: number): this;
        repeat(repeat: number): void;
        to(properties: {
            [key: string]: number;
        }, duration?: number, ease?: EasingFunction): Tween;
        from(properties: {
            [key: string]: number;
        }, duration?: number, ease?: EasingFunction): Tween;
        start(): void;
        stop(): void;
        update(simulationTimestep: number): void;
        render(interpolation: number): void;
        private _nextStep;
    }
}
declare module plume {
    class TweenManager implements Updatable, Renderable {
        private static _instance;
        private _game;
        private _tweens;
        constructor();
        static get(): TweenManager;
        create(object: any, id?: string): Tween;
        remove(tween: Tween): void;
        removeAll(): void;
        cancelAll(): void;
        update(simulationTimestep: number): void;
        render(interpolation: number): void;
        readonly activeTweensCount: number;
        readonly activeTweens: string;
    }
}
declare module plume {
    class TweenStep {
        protected duration: number;
        protected complete: boolean;
        protected elapsedTime: number;
        protected startTime: number;
        constructor(duration: number);
        start(): void;
        update(simulationTimestep: number): void;
        render(interpolation: number): void;
        isComplete(): boolean;
    }
    class DelayTweenStep extends TweenStep {
        constructor(duration: number);
    }
    class PropertiesTweenStep extends TweenStep {
        private _object;
        private _properties;
        private _easing;
        private _from;
        private _startValues;
        private _endValues;
        private _previousValues;
        private _computedValues;
        constructor(object: any, props: {
            [key: string]: number;
        }, duration: number, easing: EasingFunction, isFrom: boolean);
        start(): void;
        update(simulationTimestep: number): void;
        render(interpolation: number): void;
    }
}
declare module plume {
    class TweenValueListener {
        private roundValue;
        private _value;
        onValueUp: (nv: number) => any;
        onValueDown: (nv: number) => any;
        onValueChange: (nv: number) => any;
        constructor(initialValue: number, roundValue?: boolean);
        private _notify;
        value: number;
    }
}
