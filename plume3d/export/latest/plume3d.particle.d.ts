/// <reference path="plume3d.core.d.ts" />
declare module plume {
    class Emitter extends THREE.Object3D {
        private _isPlaying;
        private _isPaused;
        private _isStopped;
        private _isEmitting;
        private _time;
        private _particles;
        private _tmpVec3_0;
        private _emitRateTimeCounter;
        private _emitRateDistanceCounter;
        renderer: ParticleSystemRenderer;
        main: ParticleMainModule;
        shape: ParticleShapeModule;
        emission: ParticleEmissionModule;
        constructor();
        initialize(): void;
        destroy(): void;
        play(): void;
        pause(): void;
        stop(): void;
        clear(): void;
        emit(count: number): void;
        private _updateParticles;
        private _emit;
        update(dt: number): void;
        getParticles(): RecyclePool<Particle>;
        getParticleCount(): number;
        readonly isPlaying: boolean;
        readonly isPaused: boolean;
        readonly isStopped: boolean;
        readonly isEmitting: boolean;
        readonly time: number;
    }
}
declare module plume {
    class Particle {
        particleSystem: Emitter;
        position: THREE.Vector3;
        velocity: THREE.Vector3;
        remainingLifetime: number;
        animatedVelocity: THREE.Vector3;
        totalVelocity: THREE.Vector3;
        constructor(particleSystem: Emitter);
    }
}
declare module plume {
    const enum ParticleRenderMode {
        Billboard = 0
    }
}
declare module plume {
    const enum MinMaxCurveMode {
        Constant = 0,
        Curve = 1,
        TwoCurves = 2,
        TwoConstants = 3
    }
    class MinMaxCurve {
        mode: MinMaxCurveMode;
        constant: number;
        constantMin: number;
        constantMax: number;
        evaluate(time?: number, rndRatio?: number): number;
        static constant(v: number): MinMaxCurve;
        static randomBetweenTwoConstant(min: number, max: number): MinMaxCurve;
    }
}
declare module plume.particle.shaders {
    let common_fragment: string;
    let common_vertex: string;
}
declare module plume {
    class ParticleModule {
        particleSystem: Emitter;
        initialize(particleSystem: Emitter): void;
    }
}
declare module plume {
    class ParticleEmissionModule extends ParticleModule {
        enabled: boolean;
        rateOverTime: MinMaxCurve;
        constructor();
    }
}
declare module plume {
    class ParticleMainModule extends ParticleModule {
        maxParticles: number;
        simulationSpeed: number;
        duration: number;
        loop: boolean;
        startSpeed: MinMaxCurve;
        startDelay: MinMaxCurve;
        constructor();
    }
}
declare module plume {
    class ParticleShapeModule extends ParticleModule {
    }
}
declare module plume {
    type ParticleContainerVertexDataType = {
        position?: THREE.Vector3;
        uv?: THREE.Vector2;
    };
    type ParticleContainerVertexDataFlag = {
        position: boolean;
        uv: boolean;
    };
    class ParticleContainer extends THREE.Mesh {
        positionAtt: THREE.BufferAttribute;
        uvAtt: THREE.BufferAttribute;
        tintColor: THREE.Color;
        mainTexture: THREE.Texture;
        constructor(maxParticles: number);
        createMaterial(): void;
        setVertexData(idx: number, data: ParticleContainerVertexDataType): void;
        clear(): void;
    }
}
declare module plume {
    class ParticleSystemRenderer {
        material: THREE.Material;
        renderMode: ParticleRenderMode;
        particleSystem: Emitter;
        model: ParticleContainer;
        private _vertexDataTmp;
        private _vertexDataFlag;
        constructor();
        initialize(particleSystem: Emitter): void;
        updateRenderData(): void;
        private _initializeMaterial;
    }
}
