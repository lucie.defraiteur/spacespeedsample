﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityGLTF;

namespace Gamebuilt.Exporter
{

    public class MeshExporter : Exporter
    {

        public MeshExporter(string path) : base(path) { }

        public void export(GameObject gameObject)
        {
            Transform[] transforms = { gameObject.transform };

            GLTFSceneExporter gltfexporter = new GLTFSceneExporter(transforms, Exporter.RetrieveTexturePath);
            gltfexporter.SaveGLTFandBin(path, gameObject.name);
        }
    }
}