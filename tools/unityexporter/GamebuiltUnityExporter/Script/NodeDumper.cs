﻿using System.Collections;
using System.Collections.Generic;
using GLTF.Schema;
using UnityEngine;

namespace Gamebuilt.Exporter
{
    public class NodeDumper : UnityGLTF.CustomGLTFNodeExporter
    {
        public void exportNode(Transform transform, Node node)
        {
            Debug.Log("In node " + transform.name);
        }
    }
}