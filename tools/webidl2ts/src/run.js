/// <reference path="../node_modules/@types/node/index.d.ts"/>
"use strict";

let util = require("util");
let WebIDL2 = require("webidl2");
let fs = require("fs");

let allClasses = [];
let allEnum = [];

function main() {
    if (!process.argv[2]) {
        console.error("No Filename.");
        return;
    }

    let idl = fs.readFileSync(process.argv[2]).toString();
    let obj = WebIDL2.parse(idl);


    //console.log(util.inspect(obj, {depth:null}));

    for (let i = 0; i < obj.length; i++) {
        let element = obj[i];
        processElement(element);
        //if (i > 40) break;
    }

    output();
}

function processElement(element) {
    //console.log(util.inspect(element, {depth:null}));
    if (element.type == "interface") {
        let clas = findReference(element.name);
        if (clas == null) {
            clas = createClassStruct(element.name);
            allClasses.push(clas);
        }

        for (let i = 0; i < element.members.length; i++) {
            processMember(clas, element.members[i]);
        }
    } else if (element.type == "implements") {
        let clas = findReference(element.target);
        clas.extends.push(element.implements);
    } else if (element.type == "enum") {
        processEnum(element);
    } else {
        console.log(util.inspect(element, { depth: null }));
        throw new Error("Unsupported type: " + element.type);
    }
}

function processEnum(element) {
    let e = {
        name: element.name,
        values: []
    }
    for (let i = 0; i < element.values.length; i++) {
        let value = element.values[i];
        e.values.push(value.value);
    }
    allEnum.push(e);
}

function processMember(clas, member) {
    if (member.type == "operation") {
        let args = readArguments(member);
        let name = member.name;

        if (name == clas.name) {
            // constructors
            clas.constructors.push({
                args: args
            });
        } else {
            let method = {
                name: name,
                args: args,
                return: getTypeFromIdlType(member.idlType)
            }
            clas.methods.push(method);
        }
    } else if (member.type == "attribute") {
        clas.members.push({
            name: member.name,
            type: getTypeFromIdlType(member.idlType)
        })
    } else {
        console.log(util.inspect(member, { depth: null }));
        throw new Error("Unsupported type (member): " + member.type);
    }
}

function readArguments(member) {
    let args = [];
    for (let i = 0; i < member.arguments.length; i++) {
        let arg = member.arguments[i];
        args.push({
            name: arg.name,
            optional: (arg.optional == true),
            type: getTypeFromIdlType(arg.idlType)
        });
    }
    return args;
}

function getTypeFromIdlType(typ) {

    if (typ.sequence == true) {
        let listType = getTypeFromIdlType(typ.idlType);
        return "Array<" + listType + ">";
    }

    let name = typ.idlType;
    switch (name) {
        case "boolean":
            return "boolean";
        case "byte":
        case "short":
        case "long":
        case "float":
        case "double":
            return "number";
        case "VoidPtr":
            // need to check..  //https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/WebIDL-Binder.html
            return "any";
        default:
            //console.log("DEFAULT: " + name + " ==> " + JSON.stringify(typ));
            return name;
    }
}

function findReference(name) {
    for (let i = 0; i < allClasses.length; i++) {
        let c = allClasses[i];
        if (c.name == name) return c;
    }
    return null;
}

function createClassStruct(name) {
    return {
        "name": name,
        "constructors": [],
        "methods": [],
        "extends": [],
        "members": []
    };
}

function getArgsToStr(args) {
    let s = "";
    for (let i = 0; i < args.length; i++) {
        let arg = args[i];
        s += arg.name;
        if (arg.optional == true) {
            s += "?";
        }
        s += ":";
        s += arg.type;

        if (i < args.length - 1) s += ",";
    }
    return s;
}

function output() {

    // console.log(JSON.stringify(allClasses));

    let res = "";

    res += "declare module Ammo {\n";

    for (let i = 0; i < allClasses.length; i++) {
        let clas = allClasses[i];

        res += "export class " + clas.name + " ";
        if (clas.extends.length > 0) {
            res += " extends " + clas.extends[0] + " {";
        } else {
            res += "{";
        }
        res += "\n";

        for (let i = 0; i < clas.constructors.length; i++) {
            let cst = clas.constructors[i];
            res += "constructor(";
            res += getArgsToStr(cst.args);
            res += ");";
            res += "\n";
        }


        for (let i = 0; i < clas.members.length; i++) {
            let member = clas.members[i];
            res += member.name + ":" + member.type;
            res += ";\n";
        }

        for (let i = 0; i < clas.methods.length; i++) {
            let method = clas.methods[i];
            res += method.name + "(";
            res += getArgsToStr(method.args);
            res += ") : ";
            res += method.return;
            res += ";\n";
        }

        res += "}\n";
    }


    for (let i = 0; i < allEnum.length; i++) {
        let e = allEnum[i];
        res += "export enum " + e.name + " {";
        e.values.forEach(function (v) {
            res += v + ",";
        });
        res += "}\n";
    }


    res += "}\n";

    //console.log(res);

    fs.writeFileSync("idl/out.d.ts", res);
}


//
//
main();
