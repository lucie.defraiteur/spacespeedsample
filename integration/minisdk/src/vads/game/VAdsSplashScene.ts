
module vads {

    export abstract class VSplashScene extends plume.GuiScene {

        protected loader: plume.Loader3d;

        constructor() {
            super();

            let game = this.game as VAdsGame;
            let spashduration = 0; // get from config

            this.loader = game.engine.loader;

            let self = this;
            setTimeout(function () {
                self.loadDracoWasm();
                self.loadModels(self.loader);
            }, 0);

            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, spashduration);
        }

        private ensureModelLoadedAndSwitch() {
            if (this.areModelsLoaded(this.loader)) {
                this.goToMenu();
                return;
            }

            let self = this;
            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, 100);
        }

        private loadDracoWasm() {

            let inlineLoadingManager = plume.InlineLoadingManager.get();
            if (inlineLoadingManager != null && THREE["DRACOLoader"] != null) {
                // WASM version not working on FB playable
                // let dracoWasmUrl: string = null;
                // let dracoWasmContent: string = null;
                // let assets = inlineLoadingManager.assetByName;
                // assets.forEach(function (k, v) {
                //     if (k.indexOf("draco_decoder.wasm") > 0) {
                //         dracoWasmUrl = k;
                //         dracoWasmContent = v.data;
                //     }
                // }, this);

                // if (dracoWasmUrl != null) {
                //     // DRACOLoader is calling DRACOLoader.getDecoderModule() before decoding draco file.
                //     // We need to initialize it to avoid network request
                //     let decoded = window.atob(dracoWasmContent);
                //     let view = new Uint8Array(decoded.length);
                //     for (var i = 0; i < decoded.length; i++) {
                //         view[i] = decoded.charCodeAt(i);
                //     }
                //     let wasmBinary = view.buffer;
                //     let config = THREE["DRACOLoader"].decoderConfig;
                //     config.wasmBinaryFile = dracoWasmUrl;
                //     config.wasmBinary = wasmBinary;
                // }
            }
        }

        // Can be overload
        protected areModelsLoaded(loader: plume.Loader3d): boolean {
            return loader.isLoaded();
        }

        protected abstract loadModels(loader: plume.Loader3d): void;
        protected abstract goToMenu(): void;

    }


}