/// <reference path="../../../../../plume3d/export/latest/plume3d.core.d.ts" />
/// <reference path="../../../../../plume3d/export/latest/plume3d.gui.d.ts" />


//
// Helper class for voodoo playable ads
//

namespace vads {

    export let logger = plume.logger;

    export type Config = {
        gameId: string,
        gameVersion: string,
    }

    export abstract class VAdsGame extends plume.Game {

        config: Config;
        adsHandler: AdsHandler;

        constructor(width: number, height: number, options?: plume.GameOption) {
            super(width, height, options);

            this.initNetwork();
            this.adsHandler = this.newAdsHandler();
        }

        abstract newAdsHandler(): AdsHandler;

        // To be called by game
        protected initialize(config: Config) {
            this.config = config;

            logger.debug("Starting " + config.gameId + " " + config.gameVersion);

            let gui = new plume.CanvasGui({ canvasId: "2d" });
            let engine = new plume.Engine({ canvasId: "3d" });
            this.initializeEngine(engine);
            this.initializeGui(gui);

            // voodoo request
            this.inputManager.keyboard.propagateAllKeys = true;

            // not a good idea..
            // this.inputManager.mouse.propagateAllEvents = true;

            this.scaleManager.resizeHandler.add(this.onResize, this);
            this.onResize();
        }

        // To be called by game
        protected loadAssets(assets: Array<plume.AssetDef>) {
            let self = this;
            let preloadingScene = new plume.H5PreLoadingScene(assets, () => {
                self.onAssetsLoaded();
                self.goToGame();
            });

            preloadingScene.progressListener.add(function (progress: number) {
                progress = plume.Mathf.clamp01(progress);
                progress *= 100;
                logger.debug("Loading " + progress + "%");
            });
            (this.getGui() as plume.CanvasGui).switchScene(preloadingScene);
        }


        // To be overrided by game (show menu). Called by lib when all assets/initialization steps are finished
        protected goToGame() {
        }

        // Optional
        protected onAssetsLoaded() {
        }

        protected onResize() {
        }

        private initNetwork() {
            // applovin failed to load image created with document.createElementNS
            let createElementNSOrig = document.createElementNS
            window["createElementNSOrig"] = createElementNSOrig;

            let d = document as any;
            d.createElementNS = function (ns: string, tag: string) {
                return document.createElement(tag);
            };
        }

        // Helper
        public switchScene(scene: plume.GuiScene) {
            this.gui.switchScene(scene);
        }

        get gui(): plume.CanvasGui {
            return this._gui as plume.CanvasGui;
        }
    }

}