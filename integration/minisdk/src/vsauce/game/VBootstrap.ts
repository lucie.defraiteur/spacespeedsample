namespace vsauce {

    export interface VBootstrap {
        onReady(assets: Array<plume.AssetDef>, cb: () => void): void;
    }

    export abstract class VDefaultBoostrap implements VBootstrap {

        constructor() {
        }

        abstract onReady(assets: Array<plume.AssetDef>, cb: () => void): void;
    }

    export class VDummyBoostrap extends VDefaultBoostrap {

        constructor() {
            super();
        }

        onReady(assets: Array<plume.AssetDef>, cb: () => void): void {
            let self = this;
            let lc = getGame().loadingConfig;

            if (assets.length <= 0) {
                // no assets
                logger.debug("No assets to load. Preloading complete.");
                cb();
            } else {
                let preloadingScene = new plume.H5PreLoadingScene(assets, () => {
                    cb();
                });
                preloadingScene.optDelayBeforeCompleteFire = lc.optDelayBeforeCompleteFire;
                preloadingScene.optEstimateLoadingTime = lc.optEstimateLoadingTime;
                preloadingScene.optProgressAlgo = lc.optProgressAlgo;
                preloadingScene.progressListener.add(function (progress: number) {
                    progress = plume.Mathf.clamp01(progress);
                    progress *= 100;
                    self.setLoadingProgress(progress);
                });
                getGui().switchScene(preloadingScene);
            }

        }

        private setLoadingProgress(percentage: number): void {
            logger.debug("Loading " + percentage + "%");
        }
    }
}