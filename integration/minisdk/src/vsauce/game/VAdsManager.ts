namespace vsauce {


    export interface VAdsManager {
        canShowInterstitial(): boolean;
        showInterstitial(oncomplete: (success: boolean) => void): void;
        canShowRewardedVideo(): boolean;
        showRewardedVideo(oncomplete: (success: boolean) => void): void;
    }

    export abstract class VDefaultAdsManager implements VAdsManager {

        constructor() {
        }

        canShowInterstitial(): boolean {
            return false;
        }

        showInterstitial(oncomplete: (success: boolean) => void): void {
            oncomplete(false);
        }

        canShowRewardedVideo(): boolean {
            return false;
        }

        showRewardedVideo(oncomplete: (success: boolean) => void): void {
            oncomplete(false);
        }
    }

    export class VDummyAdsManager extends VDefaultAdsManager {

        constructor() {
            super();
        }

        canShowInterstitial(): boolean {
            return true;
        }

        showInterstitial(oncomplete: (success: boolean) => void): void {
            logger.debug("Showing interstitial..");
            setTimeout(function () {
                logger.debug("ads display complete");
                oncomplete(true);
            }, 250);
        }

        canShowRewardedVideo(): boolean {
            return true;
        }

        showRewardedVideo(oncomplete: (success: boolean) => void): void {
            logger.debug("Showing rewarded video..");
            setTimeout(function () {
                logger.debug("ads display complete");
                oncomplete(true);
            }, 250);
        }
    }
}