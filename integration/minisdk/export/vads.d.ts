/// <reference path="../../../plume3d/export/latest/plume3d.core.d.ts" />
/// <reference path="../../../plume3d/export/latest/plume3d.gui.d.ts" />
declare namespace vads {
    let logger: plume.Logger;
    type Config = {
        gameId: string;
        gameVersion: string;
    };
    abstract class VAdsGame extends plume.Game {
        config: Config;
        adsHandler: AdsHandler;
        constructor(width: number, height: number, options?: plume.GameOption);
        abstract newAdsHandler(): AdsHandler;
        protected initialize(config: Config): void;
        protected loadAssets(assets: Array<plume.AssetDef>): void;
        protected goToGame(): void;
        protected onAssetsLoaded(): void;
        protected onResize(): void;
        private initNetwork;
        switchScene(scene: plume.GuiScene): void;
        readonly gui: plume.CanvasGui;
    }
}
declare namespace vads {
    interface AdConfig {
        inline: boolean;
    }
    type EventType = "gameover" | "update" | "ready";
    interface GameplayParameters {
    }
    abstract class AdsHandler {
        private static INST;
        _config: AdConfig;
        _handlerByType: plume.HashMap<plume.Handler<any>>;
        _gameplay: GameplayParameters;
        assets: {
            [name: string]: string;
        };
        constructor();
        static get(): AdsHandler;
        protected abstract initGameplayParameters(): GameplayParameters;
        abstract gameplay: GameplayParameters;
        isPlayable(): boolean;
        setupLoader(loader: plume.Loader3d): void;
        fire(event: EventType, data: any): void;
        private ensureHandler;
        private init;
        abstract start(): void;
        on(event: EventType, cb: () => void): void;
    }
}
declare module vads {
    abstract class VSplashScene extends plume.GuiScene {
        protected loader: plume.Loader3d;
        constructor();
        private ensureModelLoadedAndSwitch;
        private loadDracoWasm;
        protected areModelsLoaded(loader: plume.Loader3d): boolean;
        protected abstract loadModels(loader: plume.Loader3d): void;
        protected abstract goToMenu(): void;
    }
}
