"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var vsauce;
(function (vsauce) {
    var VDefaultAdsManager = /** @class */ (function () {
        function VDefaultAdsManager() {
        }
        VDefaultAdsManager.prototype.canShowInterstitial = function () {
            return false;
        };
        VDefaultAdsManager.prototype.showInterstitial = function (oncomplete) {
            oncomplete(false);
        };
        VDefaultAdsManager.prototype.canShowRewardedVideo = function () {
            return false;
        };
        VDefaultAdsManager.prototype.showRewardedVideo = function (oncomplete) {
            oncomplete(false);
        };
        return VDefaultAdsManager;
    }());
    vsauce.VDefaultAdsManager = VDefaultAdsManager;
    var VDummyAdsManager = /** @class */ (function (_super) {
        __extends(VDummyAdsManager, _super);
        function VDummyAdsManager() {
            return _super.call(this) || this;
        }
        VDummyAdsManager.prototype.canShowInterstitial = function () {
            return true;
        };
        VDummyAdsManager.prototype.showInterstitial = function (oncomplete) {
            vsauce.logger.debug("Showing interstitial..");
            setTimeout(function () {
                vsauce.logger.debug("ads display complete");
                oncomplete(true);
            }, 250);
        };
        VDummyAdsManager.prototype.canShowRewardedVideo = function () {
            return true;
        };
        VDummyAdsManager.prototype.showRewardedVideo = function (oncomplete) {
            vsauce.logger.debug("Showing rewarded video..");
            setTimeout(function () {
                vsauce.logger.debug("ads display complete");
                oncomplete(true);
            }, 250);
        };
        return VDummyAdsManager;
    }(VDefaultAdsManager));
    vsauce.VDummyAdsManager = VDummyAdsManager;
})(vsauce || (vsauce = {}));
var vsauce;
(function (vsauce) {
    var VDefaultBoostrap = /** @class */ (function () {
        function VDefaultBoostrap() {
        }
        return VDefaultBoostrap;
    }());
    vsauce.VDefaultBoostrap = VDefaultBoostrap;
    var VDummyBoostrap = /** @class */ (function (_super) {
        __extends(VDummyBoostrap, _super);
        function VDummyBoostrap() {
            return _super.call(this) || this;
        }
        VDummyBoostrap.prototype.onReady = function (assets, cb) {
            var self = this;
            var lc = vsauce.getGame().loadingConfig;
            if (assets.length <= 0) {
                // no assets
                vsauce.logger.debug("No assets to load. Preloading complete.");
                cb();
            }
            else {
                var preloadingScene = new plume.H5PreLoadingScene(assets, function () {
                    cb();
                });
                preloadingScene.optDelayBeforeCompleteFire = lc.optDelayBeforeCompleteFire;
                preloadingScene.optEstimateLoadingTime = lc.optEstimateLoadingTime;
                preloadingScene.optProgressAlgo = lc.optProgressAlgo;
                preloadingScene.progressListener.add(function (progress) {
                    progress = plume.Mathf.clamp01(progress);
                    progress *= 100;
                    self.setLoadingProgress(progress);
                });
                vsauce.getGui().switchScene(preloadingScene);
            }
        };
        VDummyBoostrap.prototype.setLoadingProgress = function (percentage) {
            vsauce.logger.debug("Loading " + percentage + "%");
        };
        return VDummyBoostrap;
    }(VDefaultBoostrap));
    vsauce.VDummyBoostrap = VDummyBoostrap;
})(vsauce || (vsauce = {}));
var vsauce;
(function (vsauce) {
    var VDefaultDataManager = /** @class */ (function () {
        function VDefaultDataManager() {
        }
        return VDefaultDataManager;
    }());
    vsauce.VDefaultDataManager = VDefaultDataManager;
    var VDummyDataManager = /** @class */ (function (_super) {
        __extends(VDummyDataManager, _super);
        function VDummyDataManager() {
            var _this = _super.call(this) || this;
            var game = vsauce.getGame();
            _this._localStorage = new plume.LocalStorage(game.config.gameId);
            return _this;
        }
        VDummyDataManager.prototype.getDataAsync = function (keys, fn) {
            var res = {};
            for (var i = 0; i < keys.length; i++) {
                var k = keys[i];
                var v = this._localStorage.get(k);
                res[k] = v;
            }
            setTimeout(function () {
                fn(res);
            }, 100);
        };
        VDummyDataManager.prototype.setDataAsync = function (data) {
            for (var k in data) {
                var v = data[k];
                this._localStorage.put(k, v);
            }
        };
        VDummyDataManager.prototype.logEvent = function (eventName, valueToSum, parameters) {
            vsauce.logger.debug("event:" + eventName + ", v=" + valueToSum + " ,params=" + (parameters == null ? "null" : JSON.stringify(parameters)));
        };
        VDummyDataManager.prototype.getPlayer = function () {
            var pid = this._localStorage.get("playerId", null);
            if (pid == null) {
                var now = Date.now();
                pid = "player" + now;
                this._localStorage.put("playerId", pid);
                this._localStorage.put("playerName", "guest" + now);
            }
            var name = this._localStorage.get("playerName", pid);
            return {
                id: pid,
                name: name,
            };
        };
        return VDummyDataManager;
    }(VDefaultDataManager));
    vsauce.VDummyDataManager = VDummyDataManager;
})(vsauce || (vsauce = {}));
/// <reference path="../../../../../plume3d/export/latest/plume3d.core.d.ts" />
/// <reference path="../../../../../plume3d/export/latest/plume3d.gui.d.ts" />
//
// Mock
// Will need to plug back FB API if needed
//
var vsauce;
(function (vsauce) {
    vsauce.logger = plume.logger;
    var VGame = /** @class */ (function (_super) {
        __extends(VGame, _super);
        function VGame() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        // socialManager: VSocialManager;
        // To be called by game
        VGame.prototype.initialize = function (config) {
            this.config = config;
            // util.enableDomConsole();
            vsauce.logger.debug("Starting " + config.gameId + " " + config.gameVersion);
            vsauce.logger.debug("✰ html5 version by https://gamebuilt.com/ ✰");
            var gui = new plume.CanvasGui({ canvasId: "2d" });
            var engine = new plume.Engine({ canvasId: "3d" });
            this.initializeEngine(engine);
            this.initializeGui(gui);
            // let factory: VFactory;
            // if (config.publisher == "dummy") {
            //     factory = new vsauce["VDummyFactory"]();
            // } else if (config.publisher == "fbmessenger") {
            //     factory = new vsauce["VFBMessengerFactory"]();
            // }
            // this.bootstrap = factory.newVBootstrap();
            // this.dataManager = factory.newVDataManager();
            // this.scoreManager = factory.newVScoreManager();
            // this.adsManager = factory.newVAdsManager();
            // this.socialManager = factory.newVSocialManager();
            this.bootstrap = new vsauce.VDummyBoostrap();
            this.dataManager = new vsauce.VDummyDataManager();
            this.adsManager = new vsauce.VDummyAdsManager();
            this.scaleManager.resizeHandler.add(this.onResize, this);
            this.onResize();
        };
        // To be called by game
        VGame.prototype.loadAssets = function (assets, loadingConfig) {
            var self = this;
            this.loadingConfig = loadingConfig;
            if (this.loadingConfig.optDelayBeforeCompleteFire == null)
                this.loadingConfig.optDelayBeforeCompleteFire = 50;
            if (this.loadingConfig.optEstimateLoadingTime == null)
                this.loadingConfig.optEstimateLoadingTime = 5000;
            if (this.loadingConfig.optProgressAlgo == null)
                this.loadingConfig.optProgressAlgo = 1 /* Assets */;
            this.bootstrap.onReady(assets, function () {
                self.onAssetsLoaded();
                self.goToGame();
            });
        };
        // To be overrided by game (show menu). Called by lib when all assets/initialization steps are finished
        VGame.prototype.goToGame = function () {
        };
        // Optional
        VGame.prototype.onAssetsLoaded = function () {
        };
        VGame.prototype.onResize = function () {
        };
        // Helper
        VGame.prototype.switchScene = function (scene) {
            this.gui.switchScene(scene);
        };
        Object.defineProperty(VGame.prototype, "gui", {
            get: function () {
                return this._gui;
            },
            enumerable: true,
            configurable: true
        });
        return VGame;
    }(plume.Game));
    vsauce.VGame = VGame;
})(vsauce || (vsauce = {}));
var vsauce;
(function (vsauce) {
    var VSplashScene = /** @class */ (function (_super) {
        __extends(VSplashScene, _super);
        function VSplashScene() {
            var _this = _super.call(this) || this;
            var game = _this.game;
            var spashduration = 0; // get from config
            _this.loader = game.engine.loader;
            _this.loadModels(_this.loader);
            var self = _this;
            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, spashduration);
            return _this;
        }
        VSplashScene.prototype.ensureModelLoadedAndSwitch = function () {
            if (this.areModelsLoaded(this.loader)) {
                this.goToMenu();
                return;
            }
            var self = this;
            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, 100);
        };
        // Can be overload
        VSplashScene.prototype.areModelsLoaded = function (loader) {
            return loader.isLoaded();
        };
        return VSplashScene;
    }(plume.GuiScene));
    vsauce.VSplashScene = VSplashScene;
})(vsauce || (vsauce = {}));
var vsauce;
(function (vsauce) {
    function getGame() {
        return plume.Game.get();
    }
    vsauce.getGame = getGame;
    function getGui() {
        return plume.Game.get().getGui();
    }
    vsauce.getGui = getGui;
})(vsauce || (vsauce = {}));
//# sourceMappingURL=vsauce.js.map