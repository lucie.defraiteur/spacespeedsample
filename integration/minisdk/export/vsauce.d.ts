/// <reference path="../../../plume3d/export/latest/plume3d.core.d.ts" />
/// <reference path="../../../plume3d/export/latest/plume3d.gui.d.ts" />
declare namespace vsauce {
    interface VAdsManager {
        canShowInterstitial(): boolean;
        showInterstitial(oncomplete: (success: boolean) => void): void;
        canShowRewardedVideo(): boolean;
        showRewardedVideo(oncomplete: (success: boolean) => void): void;
    }
    abstract class VDefaultAdsManager implements VAdsManager {
        constructor();
        canShowInterstitial(): boolean;
        showInterstitial(oncomplete: (success: boolean) => void): void;
        canShowRewardedVideo(): boolean;
        showRewardedVideo(oncomplete: (success: boolean) => void): void;
    }
    class VDummyAdsManager extends VDefaultAdsManager {
        constructor();
        canShowInterstitial(): boolean;
        showInterstitial(oncomplete: (success: boolean) => void): void;
        canShowRewardedVideo(): boolean;
        showRewardedVideo(oncomplete: (success: boolean) => void): void;
    }
}
declare namespace vsauce {
    interface VBootstrap {
        onReady(assets: Array<plume.AssetDef>, cb: () => void): void;
    }
    abstract class VDefaultBoostrap implements VBootstrap {
        constructor();
        abstract onReady(assets: Array<plume.AssetDef>, cb: () => void): void;
    }
    class VDummyBoostrap extends VDefaultBoostrap {
        constructor();
        onReady(assets: Array<plume.AssetDef>, cb: () => void): void;
        private setLoadingProgress;
    }
}
declare namespace vsauce {
    type DataObject = {
        [key: string]: any;
    };
    type VPlayer = {
        id: string;
        name?: string;
        photo?: string;
    };
    interface VDataManager {
        getDataAsync(keys: string[], fn: (data: DataObject) => void): void;
        setDataAsync(data: DataObject): void;
        logEvent(eventName: string, valueToSum?: number, parameters?: Object): void;
        getPlayer(): VPlayer;
    }
    abstract class VDefaultDataManager implements VDataManager {
        constructor();
        abstract getDataAsync(keys: string[], fn: (data: DataObject) => void): void;
        abstract setDataAsync(data: DataObject): void;
        abstract logEvent(eventName: string, valueToSum?: number, parameters?: Object): void;
        abstract getPlayer(): VPlayer;
    }
    class VDummyDataManager extends VDefaultDataManager {
        private _localStorage;
        constructor();
        getDataAsync(keys: string[], fn: (data: DataObject) => void): void;
        setDataAsync(data: DataObject): void;
        logEvent(eventName: string, valueToSum?: number, parameters?: Object): void;
        getPlayer(): VPlayer;
    }
}
declare namespace vsauce {
    let logger: plume.Logger;
    type Config = {
        publisher: string;
        gameId: string;
        gameVersion: string;
        fbm?: {
            interstitialPlacementId: string;
            rewardedVideoPlacementId: string;
        };
    };
    type LoadingConfig = {
        optDelayBeforeCompleteFire?: number;
        optProgressAlgo?: number;
        optEstimateLoadingTime?: number;
    };
    class VGame extends plume.Game {
        config: Config;
        loadingConfig: LoadingConfig;
        bootstrap: VBootstrap;
        dataManager: VDataManager;
        adsManager: VAdsManager;
        protected initialize(config: Config): void;
        protected loadAssets(assets: Array<plume.AssetDef>, loadingConfig: LoadingConfig): void;
        protected goToGame(): void;
        protected onAssetsLoaded(): void;
        protected onResize(): void;
        switchScene(scene: plume.GuiScene): void;
        readonly gui: plume.CanvasGui;
    }
}
declare module vsauce {
    abstract class VSplashScene extends plume.GuiScene {
        protected loader: plume.Loader3d;
        constructor();
        private ensureModelLoadedAndSwitch;
        protected areModelsLoaded(loader: plume.Loader3d): boolean;
        protected abstract loadModels(loader: plume.Loader3d): void;
        protected abstract goToMenu(): void;
    }
}
declare namespace vsauce {
    function getGame(): VGame;
    function getGui(): plume.CanvasGui;
}
