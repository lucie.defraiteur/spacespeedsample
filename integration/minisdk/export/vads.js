"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/// <reference path="../../../../../plume3d/export/latest/plume3d.core.d.ts" />
/// <reference path="../../../../../plume3d/export/latest/plume3d.gui.d.ts" />
//
// Helper class for voodoo playable ads
//
var vads;
(function (vads) {
    vads.logger = plume.logger;
    var VAdsGame = /** @class */ (function (_super) {
        __extends(VAdsGame, _super);
        function VAdsGame(width, height, options) {
            var _this = _super.call(this, width, height, options) || this;
            _this.initNetwork();
            _this.adsHandler = _this.newAdsHandler();
            return _this;
        }
        // To be called by game
        VAdsGame.prototype.initialize = function (config) {
            this.config = config;
            vads.logger.debug("Starting " + config.gameId + " " + config.gameVersion);
            var gui = new plume.CanvasGui({ canvasId: "2d" });
            var engine = new plume.Engine({ canvasId: "3d" });
            this.initializeEngine(engine);
            this.initializeGui(gui);
            // voodoo request
            this.inputManager.keyboard.propagateAllKeys = true;
            // not a good idea..
            // this.inputManager.mouse.propagateAllEvents = true;
            this.scaleManager.resizeHandler.add(this.onResize, this);
            this.onResize();
        };
        // To be called by game
        VAdsGame.prototype.loadAssets = function (assets) {
            var self = this;
            var preloadingScene = new plume.H5PreLoadingScene(assets, function () {
                self.onAssetsLoaded();
                self.goToGame();
            });
            preloadingScene.progressListener.add(function (progress) {
                progress = plume.Mathf.clamp01(progress);
                progress *= 100;
                vads.logger.debug("Loading " + progress + "%");
            });
            this.getGui().switchScene(preloadingScene);
        };
        // To be overrided by game (show menu). Called by lib when all assets/initialization steps are finished
        VAdsGame.prototype.goToGame = function () {
        };
        // Optional
        VAdsGame.prototype.onAssetsLoaded = function () {
        };
        VAdsGame.prototype.onResize = function () {
        };
        VAdsGame.prototype.initNetwork = function () {
            // applovin failed to load image created with document.createElementNS
            var createElementNSOrig = document.createElementNS;
            window["createElementNSOrig"] = createElementNSOrig;
            var d = document;
            d.createElementNS = function (ns, tag) {
                return document.createElement(tag);
            };
        };
        // Helper
        VAdsGame.prototype.switchScene = function (scene) {
            this.gui.switchScene(scene);
        };
        Object.defineProperty(VAdsGame.prototype, "gui", {
            get: function () {
                return this._gui;
            },
            enumerable: true,
            configurable: true
        });
        return VAdsGame;
    }(plume.Game));
    vads.VAdsGame = VAdsGame;
})(vads || (vads = {}));
var vads;
(function (vads) {
    var AdsHandler = /** @class */ (function () {
        function AdsHandler() {
            this._handlerByType = new plume.HashMap();
            this.assets = {};
            if (AdsHandler.INST != null) {
                throw new Error("Already created");
            }
            AdsHandler.INST = this;
            this._gameplay = this.initGameplayParameters();
            // Retrieve private config from html
            this._config = window["__gb_adsconfig"];
            this.init();
            // Expose adshandler for voodoo
            window["__gb_adshandler"] = this;
        }
        AdsHandler.get = function () {
            return this.INST;
        };
        AdsHandler.prototype.isPlayable = function () {
            return this._config != null;
        };
        AdsHandler.prototype.setupLoader = function (loader) {
            if (this.isPlayable() && this._config.inline) {
                var inlineLoadingManager = new plume.InlineLoadingManager();
                inlineLoadingManager.initialize(loader);
            }
        };
        AdsHandler.prototype.fire = function (event, data) {
            var h = this._handlerByType.get(event);
            if (h == null) {
                // no handler registered
                return;
            }
            h.fire(data);
        };
        AdsHandler.prototype.ensureHandler = function (event) {
            var h = this._handlerByType.get(event);
            if (h != null)
                return h;
            var handler = new plume.Handler();
            this._handlerByType.put(event, handler);
            return handler;
        };
        AdsHandler.prototype.init = function () {
            if (this._config == null) {
                vads.logger.debug("No ads config found");
                return;
            }
            if (this._config.inline) {
                plume.LoaderProxy.set(new plume.InlineLoader());
            }
        };
        AdsHandler.prototype.on = function (event, cb) {
            var h = this.ensureHandler(event);
            h.add(cb);
        };
        return AdsHandler;
    }());
    vads.AdsHandler = AdsHandler;
})(vads || (vads = {}));
var vads;
(function (vads) {
    var VSplashScene = /** @class */ (function (_super) {
        __extends(VSplashScene, _super);
        function VSplashScene() {
            var _this = _super.call(this) || this;
            var game = _this.game;
            var spashduration = 0; // get from config
            _this.loader = game.engine.loader;
            var self = _this;
            setTimeout(function () {
                self.loadDracoWasm();
                self.loadModels(self.loader);
            }, 0);
            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, spashduration);
            return _this;
        }
        VSplashScene.prototype.ensureModelLoadedAndSwitch = function () {
            if (this.areModelsLoaded(this.loader)) {
                this.goToMenu();
                return;
            }
            var self = this;
            setTimeout(function () {
                self.ensureModelLoadedAndSwitch();
            }, 100);
        };
        VSplashScene.prototype.loadDracoWasm = function () {
            var inlineLoadingManager = plume.InlineLoadingManager.get();
            if (inlineLoadingManager != null && THREE["DRACOLoader"] != null) {
                // WASM version not working on FB playable
                // let dracoWasmUrl: string = null;
                // let dracoWasmContent: string = null;
                // let assets = inlineLoadingManager.assetByName;
                // assets.forEach(function (k, v) {
                //     if (k.indexOf("draco_decoder.wasm") > 0) {
                //         dracoWasmUrl = k;
                //         dracoWasmContent = v.data;
                //     }
                // }, this);
                // if (dracoWasmUrl != null) {
                //     // DRACOLoader is calling DRACOLoader.getDecoderModule() before decoding draco file.
                //     // We need to initialize it to avoid network request
                //     let decoded = window.atob(dracoWasmContent);
                //     let view = new Uint8Array(decoded.length);
                //     for (var i = 0; i < decoded.length; i++) {
                //         view[i] = decoded.charCodeAt(i);
                //     }
                //     let wasmBinary = view.buffer;
                //     let config = THREE["DRACOLoader"].decoderConfig;
                //     config.wasmBinaryFile = dracoWasmUrl;
                //     config.wasmBinary = wasmBinary;
                // }
            }
        };
        // Can be overload
        VSplashScene.prototype.areModelsLoaded = function (loader) {
            return loader.isLoaded();
        };
        return VSplashScene;
    }(plume.GuiScene));
    vads.VSplashScene = VSplashScene;
})(vads || (vads = {}));
//# sourceMappingURL=vads.js.map