/* global module */

module.exports = function (grunt) {
    'use strict';

    // Create 2 tasks for each target
    var allSubproject = ['vads', 'vsauce'];
    var allCompileTask = [];
    var allWatchTask = [];
    var tscConfig = {};
    for (var k in allSubproject) {
        var name = allSubproject[k];
        tscConfig["compile-" + name] = {
            options: {
                project: "src/" + name
            }
        }
        tscConfig["watch-" + name] = {
            options: {
                project: "src/" + name,
                watch: true
            }
        }
        allCompileTask.push("tsc:compile-" + name);
        allWatchTask.push("tsc:watch-" + name);
    }



    grunt.initConfig({
        tsc: tscConfig,
        clean: {
            pre: ['build', 'bin', 'obj'],
            post: ['.tscache']
        },
        concurrent: {
            all: {
                tasks: allWatchTask,
                options: {
                    logConcurrentOutput: true
                }
            }
        },
        copy: {
            all: {
                expand: true,
                cwd: 'build/',
                src: ['**/*.js', '!**/*.min.js', '**/*.d.ts'],
                dest: 'export/',
            },
          },
    });


    grunt.task.registerTask('dev', allCompileTask.concat(['concurrent:all'])); // compile all project and watch
    grunt.task.registerTask('watch', ['concurrent:all']); // watch only
    grunt.task.registerTask('release', ['default', 'copy:all']); // final build saved in 'export' (export is commited)

    // if grunt --watch, default is dev
    var watch = grunt.option('watch');
    if (watch != null) {
        grunt.task.registerTask('default', ['dev']);
    } else {
        grunt.task.registerTask('default', ['clean:pre'].concat(allCompileTask).concat(['clean:post']));
    }

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadTasks('../../build/grunt');
};
