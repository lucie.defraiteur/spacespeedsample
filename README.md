## Welcome to space speed sample

How to use it:

- first, open a terminal at root, and type http-server -c-1 -p 8088, if you have an error, you may need to install http-server via npm.
- then, enter "samples/spaceSpeed/" folder, and in the terminal, type npm install, then type grunt --watch
- finally, go to http://localhost:8088/samples/spacespeed/index-dev.html on your web browser, to see the result.
