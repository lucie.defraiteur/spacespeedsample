module.exports = function (grunt) {

    var path = require('path');
    var fs = require('fs');

    function getTscVersion(tscPath) {
        const pkg = JSON.parse(fs.readFileSync(path.resolve(tscPath, '..', 'package.json')).toString());
        return '' + pkg.version;
    }

    function ensureDirectoryExistence(filePath) {
        var dirname = path.dirname(filePath);
        if (fs.existsSync(dirname)) {
            return true;
        }
        fs.mkdirSync(dirname);
    }

    grunt.registerMultiTask('tsc', 'Compile typescript', function (prop) {

        //grunt.log.writeln('Running tsc:' + this.target);

        let options = this.data.options || {};
        let tscPath = path.join('node_modules', 'typescript', 'bin');
        let tscBin = path.join(tscPath, 'tsc');

        grunt.log.writeln("tsc task options: " + JSON.stringify(options));

        let args = [];

        if (options.files != null && options.files.length > 0) {
            args = args.concat(options.files);
        }

        if (options.project != null) {
            args.push('--project', options.project);
        } else {
            if (options.files == null) {
                args.push('--project', 'tsconfig.json');
            }
        }

        if (options.watch != null && options.watch == true) {
            args.push('--watch');
        }

        //args.push('--version');
        
        //let watch = grunt.config.get('watch');
        // grunt.log.writeln('WATCH 22?? #' + watch);
        // if (watch) {
        //     args.push('--watch');
        // }

        let randid = Math.floor(Math.random() * (100000 - 1)) + 1;
        let tempfilename = 'build/tscommand' + randid + '.tmp';
        ensureDirectoryExistence(tempfilename);
        fs.writeFileSync(tempfilename, args.join(' '));

        grunt.log.writeln("tsc task args: " + args.join(' '));

        var done = this.async();
        var child = grunt.util.spawn({
            cmd: process.execPath,
            args: [tscBin, '@' + tempfilename],
            opts: {
                cwd: "."
            }
        }, function (err, result, code) {
            if (err) {
                grunt.log.error(">> Failed to compile: " + err);
                fs.unlinkSync(tempfilename);
                done(false);
            } else {
                grunt.log.writeln(">> Compilation terminated");
                fs.unlinkSync(tempfilename);
                done(true);
            }
        });
        child.stdout.pipe(process.stdout);
        child.stderr.pipe(process.stderr);
    });

    // Ex: setConfig:target:staging
    grunt.registerTask('setConfig', 'Set a config property.', function (name, val) {
        if (val === "true") val = true;
        else if (val === "false") val = false;

        grunt.config.set(name, val);
    });

};
